package com.riverview.harrys.adapter;

import java.util.ArrayList;

import android.app.Activity;
import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.riverview.harrys.R;
import com.riverview.harrys.helpers.Utils;
import com.riverview.harrys.objects.BeanFeedbackAttachment;

public class FeedbackAttachemntAdapter extends BaseAdapter {
	private ArrayList<BeanFeedbackAttachment> mItems = new ArrayList<BeanFeedbackAttachment>();
	private Activity context;

	public FeedbackAttachemntAdapter(Activity context,
			ArrayList<BeanFeedbackAttachment> list) {
		this.context = context;
		this.mItems = list;
	}

	@Override
	public int getCount() {
		try {
			return mItems.size();
		} catch (Exception e) {
			return 0;
		}
	}

	@Override
	public BeanFeedbackAttachment getItem(int pos) {
		return mItems.get(pos);
	}

	@Override
	public long getItemId(int position) {
		return mItems.get(position).getId();
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ViewHolder holder;

		if (convertView == null) {
			holder = new ViewHolder();
			LayoutInflater inflater = (LayoutInflater) context
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

			convertView = inflater.inflate(
					R.layout.cell_feedback_attachemnt_item, parent, false);
			holder.LLRoot = (LinearLayout) convertView
					.findViewById(R.id.LLRoot);
			holder.tvPromoName = (TextView) convertView
					.findViewById(R.id.tvPromoName);
			holder.tvStatus = (TextView) convertView
					.findViewById(R.id.tvStatus);
			convertView.setTag(holder);

		} else {
			holder = (ViewHolder) convertView.getTag();
		}

		Utils.setTextViewFontSizeBasedOnScreenDensity(context,
				holder.tvPromoName, 23f);
		Utils.setTextViewFontSizeBasedOnScreenDensity(context, holder.tvStatus,
				30f, Typeface.BOLD);

		Utils.setMarginByPercent(context, holder.LLRoot, 0.01f, true);

		BeanFeedbackAttachment obj = getItem(position);

		if (obj.getAttachment_type().startsWith("image")) {
			holder.tvPromoName.setText("Uploaded Image");
			holder.tvStatus.setText("OPEN");
		} else if (obj.getAttachment_type().startsWith("video")) {
			holder.tvPromoName.setText("Uploaded Video");
			holder.tvStatus.setText("PLAY");
		} else if (obj.getAttachment_type().startsWith("audio")) {
			holder.tvPromoName.setText("Uploaded Audio");
			holder.tvStatus.setText("PLAY");
		}

		holder.tvStatus.setBackgroundColor(context.getResources().getColor(
				R.color.feedback_open));

		return convertView;
	}

	public class ViewHolder {

		TextView tvPromoName;
		TextView tvStatus;
		LinearLayout LLRoot;
	}
}
