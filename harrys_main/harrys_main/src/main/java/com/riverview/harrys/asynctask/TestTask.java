package com.riverview.harrys.asynctask;

import static com.riverview.harrys.constant.AppConst.HTTP_METHOD_POST;

import java.util.ArrayList;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.DefaultResponseErrorHandler;
import org.springframework.web.client.RestTemplate;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.riverview.harrys.R;
import com.riverview.harrys.objects.ServiceResponse;
import com.riverview.harrys.util.AppUtil;
import com.riverview.harrys.util.WebServiceUtil;

import fr.castorflex.android.circularprogressbar.CircularProgressDrawable;

public class TestTask extends AsyncTask<Void, Void, ServiceResponse<Object>> {

	private static final String TAG = TestTask.class.getSimpleName();

	public AsyncResponse<ServiceResponse<Object>> asyncResponse;

	private ProgressDialog mProgressDialog;

	private Context mContext;
	private String userName = null;
	private String password = null;

	String[] commandString = null;
	ArrayList<NameValuePair> httpParams = null;

	public TestTask(Context context, String userName, String password) {
		this.mContext = context;
		this.userName = userName;
		this.password = password;
	}

	@Override
	protected void onPreExecute() {
		Log.d(TAG, "onPreExecute");
		super.onPreExecute();

		mProgressDialog = new ProgressDialog(mContext);
		mProgressDialog
				.setIndeterminateDrawable(new CircularProgressDrawable.Builder(
						mContext)
						.colors(mContext.getResources().getIntArray(
								R.array.gplus_colors)).sweepSpeed(1f)
						.strokeWidth(16f)
						.style(CircularProgressDrawable.Style.ROUNDED).build());

		mProgressDialog.setCancelable(false);
		mProgressDialog.setMessage("Authenticating.....");
		mProgressDialog.show();

		try {
			ArrayList<NameValuePair> arrayList = new ArrayList<NameValuePair>();

			// arrayList.add(new BasicNameValuePair(ATTR_TAG_PASSWORD,
			// password));
			// arrayList.add(new BasicNameValuePair(ATTR_TAG_USER_NAME,
			// userName));

			arrayList.add(new BasicNameValuePair("udid", "357865058444873"));
			arrayList.add(new BasicNameValuePair("app_version", "1"));
			arrayList.add(new BasicNameValuePair("name", "Tariq"));
			arrayList.add(new BasicNameValuePair("os_version", "4.2.2"));
			arrayList.add(new BasicNameValuePair("terminal_id", "1"));
			arrayList.add(new BasicNameValuePair("user_id", "1"));

			httpParams = AppUtil.generateAuthCommandString(arrayList,
					"api/terminaldevices/register", HTTP_METHOD_POST);

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	@Override
	protected void onPostExecute(ServiceResponse<Object> response) {
		Log.d(TAG, "onPostExecute");

		if (mProgressDialog != null && mProgressDialog.isShowing()) {
			mProgressDialog.dismiss();
		}
		asyncResponse.processFinish(response);
	}

	@Override
	protected ServiceResponse<Object> doInBackground(Void... voids) {
		Log.d(TAG, "doInBackground");

		ServiceResponse<Object> serviceResponse = null;

		RestTemplate restTemplate = WebServiceUtil.getRestTemplateInstance();

		restTemplate.setErrorHandler(new DefaultResponseErrorHandler() {
			protected boolean hasError(HttpStatus statusCode) {
				Log.d(TAG, "Error ----> " + statusCode);
				return false;
			}
		});

		// String url = String.format(SERVICE_END_POINT + "%s", URL_SIGN_IN);
		String url = "http://dev.riverviewms.com/api/terminaldevices/register";

		HttpHeaders requestHeaders = new HttpHeaders();
		requestHeaders.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
		requestHeaders.set("Accept", "application/json");

		MultiValueMap<String, String> map = new LinkedMultiValueMap<String, String>();
		for (NameValuePair a : httpParams) {
			if (a.getName() == "udid") {
				map.add("UDID", a.getValue());
			} else {
				map.add(a.getName(), a.getValue());
			}

		}

		HttpEntity<MultiValueMap<String, String>> requestEntity = new HttpEntity<MultiValueMap<String, String>>(
				map, requestHeaders);

		Log.d(TAG, "Url " + url);
		Log.d(TAG, "Request Entity " + requestEntity.toString());

		try {
			serviceResponse = restTemplate.exchange(url, HttpMethod.POST,
					requestEntity,
					new ParameterizedTypeReference<ServiceResponse<Object>>() {
					}).getBody();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return serviceResponse;
	}

	@Override
	protected void onCancelled() {

		if (mProgressDialog != null && mProgressDialog.isShowing()) {
			mProgressDialog.dismiss();
			asyncResponse.processFinish(null);
		}
	}
}
