package com.riverview.harrys.util;

/**
 * Utility class for byte/hex/integer related operations.
 * 
 * @author Wiraj
 * 
 */
public final class ByteUtils {

	/**
	 * Convert hexadecimal byte array to string.
	 * 
	 * @param bytes
	 *            hexadecimal byte array.
	 * @return string value.
	 */
	public static String hexToString(byte[] bytes) {
		StringBuffer sb = new StringBuffer();

		for (int i = 0; i < bytes.length; i++) {
			String tmp = Integer.toHexString(bytes[i] & 0xFF);
			while (tmp.length() < 2)
				tmp = "0" + tmp;
			if (i != bytes.length - 1)
				sb.append(tmp);
			else
				sb.append(tmp);
		}
		return sb.toString();
	}

}
