package com.riverview.harrys.fragments;

import java.util.ArrayList;
import org.json.JSONException;
import org.json.JSONObject;
import android.content.Intent;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.AdapterView.OnItemClickListener;

import com.iapps.libs.helpers.BaseHelper;
import com.iapps.libs.helpers.BaseHelper.CancelListener;
import com.iapps.libs.helpers.BaseHelper.ConfirmListener;
import com.riverview.harrys.MainActivity;
import com.riverview.harrys.R;
import com.riverview.harrys.RCGenericFragment;
import com.riverview.harrys.adapter.FeedbackAttachemntAdapter;
import com.riverview.harrys.asynctask.AsyncResponse;
import com.riverview.harrys.asynctask.FeedbackAttachmentAsyncTask;
import com.riverview.harrys.helpers.Utils;
import com.riverview.harrys.objects.BeanFeedback;
import com.riverview.harrys.objects.BeanFeedbackAttachment;
import com.riverview.harrys.objects.BeanFeedbackAttachmentWrapper;
import com.riverview.harrys.objects.ServiceResponse;
import com.riverview.harrys.util.AnaylaticsAppender;
import com.riverview.harrys.util.AppUtil;

public class FeedbackDetailsFragment extends RCGenericFragment implements
		AsyncResponse<ServiceResponse<BeanFeedbackAttachmentWrapper>> {

	private static final String TAG = FeedbackDetailsFragment.class
			.getSimpleName();

	private ListView lvStores;
	private TextView tv1;
	private TextView tv2;
	private TextView tv3;
	private TextView tv4;
	private TextView tv5;
	private TextView tvFeedbackTitle;
	private TextView tvFeedbackType;
	private TextView tvFeedbackStatus;
	private TextView tvFeedbackPost;
	private TextView tvFeedbackCreatedOn;

	private View v;

	private BeanFeedback mBeanFeedbackItem;

	private FeedbackAttachmentAsyncTask mFeedbackAttachemntAsyncTask;

	private AnaylaticsAppender mAppender = null;

	private ArrayList<BeanFeedbackAttachment> mFeedbackAttachments = null;
	private FeedbackAttachemntAdapter mAdapter;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		mBeanFeedbackItem = (BeanFeedback) getArguments().getParcelable(
				"feedback");
	}

	public void onPrepareOptionsMenu(Menu menu) {
		//getSherlockActivity().getSupportMenuInflater().inflate(R.menu.menu_feedback_details, menu);
	}

	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.rv_menu_feedback_messages:
			// feedback messages

			FeedbackPostListFragment frag = new FeedbackPostListFragment();
			Bundle bundle = new Bundle();
			bundle.putParcelable("feedback", mBeanFeedbackItem);
			frag.setArguments(bundle);
			getHome().setFragment(frag);

			break;

		default:
			return false;
		}
		return true;
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		v = inflater.inflate(R.layout.feedback_detail_fragment_layout,
				container, false);
		lvStores = (ListView)v.findViewById(R.id.listViewStore);
		tv1 = (TextView)v.findViewById(R.id.tv1);
		tv2 = (TextView)v.findViewById(R.id.tv2);
		tv3 = (TextView)v.findViewById(R.id.tv3);
		tv4 = (TextView)v.findViewById(R.id.tv4);
		tv5 = (TextView)v.findViewById(R.id.tv5);
		tvFeedbackTitle = (TextView)v.findViewById(R.id.tvFeedbackTitle);
		tvFeedbackType = (TextView)v.findViewById(R.id.tvFeedbackType);
		tvFeedbackStatus = (TextView)v.findViewById(R.id.tvFeedbackStatus);
		tvFeedbackPost = (TextView)v.findViewById(R.id.tvFeedbackPost);
		tvFeedbackCreatedOn = (TextView)v.findViewById(R.id.tvFeedbackCreatedOn);
		setHasOptionsMenu(false);
		((MainActivity) getActivity()).setBackActionBar(mBeanFeedbackItem
				.getTitle(),View.INVISIBLE,"");

		return v;
	}

	@Override
	public void onResume() {
		super.onResume();

		try {
			mAppender = AnaylaticsAppender.getInstance(getActivity());

			JSONObject attributes = new JSONObject();
			try {
				attributes.put("title", mBeanFeedbackItem.getTitle());
			} catch (JSONException e) {
				e.printStackTrace();
			}

			mAppender.updateAnaylitsData(AppUtil
					.createAnalyticsJsonEventAction("feedback",
							mBeanFeedbackItem.getTitle(), "view",
							attributes.toString()));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);

		Utils.setTextViewFontSizeBasedOnScreenDensity(getActivity(), tv1, 23f,
				Typeface.BOLD);
		Utils.setTextViewFontSizeBasedOnScreenDensity(getActivity(), tv2, 23f,
				Typeface.BOLD);
		Utils.setTextViewFontSizeBasedOnScreenDensity(getActivity(), tv3, 23f,
				Typeface.BOLD);
		Utils.setTextViewFontSizeBasedOnScreenDensity(getActivity(), tv4, 23f,
				Typeface.BOLD);
		Utils.setTextViewFontSizeBasedOnScreenDensity(getActivity(), tv5, 23f,
				Typeface.BOLD);

		Utils.setTextViewFontSizeBasedOnScreenDensity(getActivity(),
				tvFeedbackTitle, 23f);
		Utils.setTextViewFontSizeBasedOnScreenDensity(getActivity(),
				tvFeedbackType, 23f);
		Utils.setTextViewFontSizeBasedOnScreenDensity(getActivity(),
				tvFeedbackStatus, 23f);
		Utils.setTextViewFontSizeBasedOnScreenDensity(getActivity(),
				tvFeedbackPost, 23f);
		Utils.setTextViewFontSizeBasedOnScreenDensity(getActivity(),
				tvFeedbackCreatedOn, 23f);

		lvStores.setOnItemClickListener(ListenerClickItem);

		// set relevent data
		if (mBeanFeedbackItem != null) {
			tvFeedbackTitle.setText(mBeanFeedbackItem.getTitle());
			tvFeedbackType.setText(mBeanFeedbackItem.getFeedback_type());
			tvFeedbackStatus.setText(mBeanFeedbackItem.getFeedback_status());
			tvFeedbackPost.setText(mBeanFeedbackItem.getPost());
			tvFeedbackCreatedOn.setText(mBeanFeedbackItem.getCreated_at());

			// call service to get attachments
			startFeedbackSubCategoryAsyncTask();
		}
	}

	public OnItemClickListener ListenerClickItem = new OnItemClickListener() {

		@Override
		public void onItemClick(AdapterView<?> adapter, View view, int pos,
				long arg3) {

			BeanFeedbackAttachment attachment = mFeedbackAttachments.get(pos);

			// check the type and start the activities

			if (attachment.getAttachment_type().startsWith("image")) {

				Intent i = new Intent(Intent.ACTION_VIEW);
				i.setData(Uri.parse(attachment.getAttachment()));

				if (i.resolveActivity(getHome().getPackageManager()) != null) {
					startActivity(i);
				} else {
					Toast.makeText(getHome(),
							"No application avaliable to complete this action",
							Toast.LENGTH_SHORT).show();
				}
			} else if (attachment.getAttachment_type().startsWith("video")) {

				Intent i = new Intent(Intent.ACTION_VIEW);
				i.setData(Uri.parse(attachment.getAttachment()));

				if (i.resolveActivity(getHome().getPackageManager()) != null) {
					startActivity(i);
				} else {
					Toast.makeText(getHome(),
							"No application avaliable to complete this action",
							Toast.LENGTH_SHORT).show();
				}

			} else if (attachment.getAttachment_type().startsWith("audio")) {

				Intent i = new Intent(Intent.ACTION_VIEW);
				i.setData(Uri.parse(attachment.getAttachment()));

				if (i.resolveActivity(getHome().getPackageManager()) != null) {
					startActivity(i);
				} else {
					Toast.makeText(getHome(),
							"No application avaliable to complete this action",
							Toast.LENGTH_SHORT).show();
				}

			}

		}
	};

	private void startFeedbackSubCategoryAsyncTask() {
		if (AppUtil.checkNetworkConnection(getActivity())) {

			mFeedbackAttachemntAsyncTask = new FeedbackAttachmentAsyncTask(
					getActivity(), String.valueOf(mBeanFeedbackItem.getId()));
			mFeedbackAttachemntAsyncTask.asyncResponse = FeedbackDetailsFragment.this;
			mFeedbackAttachemntAsyncTask.executeOnExecutor(
					AsyncTask.THREAD_POOL_EXECUTOR, (Void) null);

		} else {

			BaseHelper.confirm(getActivity(), "Network Unavaliable",
					"Please turn on network to connect with server",
					new ConfirmListener() {

						@Override
						public void onYes() {
							AppUtil.showSystemSettingsDialog(getActivity());
						}
					}, new CancelListener() {

						@Override
						public void onNo() {
							getHome().backToHomeScreen();

						}
					});
		}
	}

	@Override
	public void processFinish(
			ServiceResponse<BeanFeedbackAttachmentWrapper> details) {
		if (details == null) {
			Log.d(TAG, "Null details");
			try {
				BaseHelper.showAlert(getActivity(), "Harry's",
						"Operation fail.");
			} catch (Exception e) {
				e.printStackTrace();
			}
			return;
		}

		if (details.getStatus() == 200 && !details.error) {
			Log.d(TAG, "Create feedback success");

			mFeedbackAttachments = details.getResponse().getAttachments();

			// setup the list adapter and show the list
			mAdapter = new FeedbackAttachemntAdapter(getActivity(),
					mFeedbackAttachments);
			lvStores.setAdapter(mAdapter);
			mAdapter.notifyDataSetChanged();

		} else {
			BaseHelper.showAlert(getActivity(), "Error", (details
					.getError_message() != null) ? details.getError_message()
					: "Unable to create feedback. Please try again.");
		}

	}

}
