package com.riverview.harrys;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

import android.Manifest;
import android.app.ActionBar;
import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentSender;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.opengl.Visibility;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.multidex.MultiDex;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;

import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toolbar;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStates;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.iapps.libs.helpers.BaseHelper;
import com.iapps.libs.helpers.BaseHelper.ConfirmListener;
import com.riverview.harrys.asynctask.AdsAsyncTask;
import com.riverview.harrys.asynctask.DevicesAsyncTask;
import com.riverview.harrys.fragments.HomeFragment;
import com.riverview.harrys.fragments.SlideMenuFragment;
import com.riverview.harrys.fragments.SplashFragment;
import com.riverview.harrys.helpers.Constants;
import com.riverview.harrys.helpers.FontUtil;
import com.riverview.harrys.helpers.SharedPref;
import com.riverview.harrys.helpers.UserInfoManager;
import com.riverview.harrys.helpers.Utils;
import com.riverview.harrys.helpers.menuListener;
import com.riverview.harrys.objects.BeanDevice;
import com.riverview.harrys.util.AppUtil;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

import static com.riverview.harrys.fragments.HomeFragment.hasPermissions;


public class MainActivity extends FragmentActivity implements ActionBar.OnNavigationListener,GoogleApiClient.OnConnectionFailedListener, GoogleApiClient.ConnectionCallbacks{

	private static final String TAG = MainActivity.class.getSimpleName();

	private FrameLayout flRoot;
	private FrameLayout FLSlideFragment;
	private KillReceiver exitApplicationReceiver;
	private Menu menu = null;
	private menuListener slidelistener;
	public static DrawerLayout mDrawerLayout;
	private Toolbar mToolbar;
	public static ImageView result_main_image;
	private ActionBarDrawerToggle mDrawerToggle;

    public static boolean loc_permission = false;
    Location location;
    GoogleApiClient googleApiClient;
	@Override
	public boolean onNavigationItemSelected(int itemPosition, long itemId) {
		return false;
	}



    /**
	 * This list item click listener implements very simple view switching by
	 * changing the primary content text. The drawer is closed when a selection
	 * is made.
	 */
	private class DrawerItemClickListener implements
			ListView.OnItemClickListener {

		@Override
		public void onItemClick(AdapterView<?> parent, View view, int position,
								long id) {

			mDrawerLayout.closeDrawer(FLSlideFragment);
		}
	}
/*

	private class DemoDrawerListener implements DrawerLayout.DrawerListener {
		@Override
		public void onDrawerOpened(View drawerView) {
			mDrawerToggle.onDrawerOpened(drawerView);
			mActionBar.onDrawerOpened();
		}

		@Override
		public void onDrawerClosed(View drawerView) {
			mDrawerToggle.onDrawerClosed(drawerView);
			mActionBar.onDrawerClosed();
		}

		@Override
		public void onDrawerSlide(View drawerView, float slideOffset) {
			mDrawerToggle.onDrawerSlide(drawerView, slideOffset);
		}

		@Override
		public void onDrawerStateChanged(int newState) {
			mDrawerToggle.onDrawerStateChanged(newState);
		}
	}
*/

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.main_layout);
		flRoot = (FrameLayout)findViewById (R.id.layoutFragment);
		FLSlideFragment = (FrameLayout)findViewById (R.id.layoutSlideFragment);

		mDrawerLayout = (DrawerLayout)findViewById(R.id.drawer_layout);

		mToolbar = (Toolbar) findViewById(R.id.action_bar);
        getLocation();
        String[] PERMISSIONS = {

                android.Manifest.permission.ACCESS_COARSE_LOCATION,
                Manifest.permission.ACCESS_FINE_LOCATION,
				Manifest.permission.READ_EXTERNAL_STORAGE,
				Manifest.permission.WRITE_EXTERNAL_STORAGE,
				Manifest.permission.CAMERA
        };
        int PERMISSION_ALL=1;
        if (!hasPermissions(MainActivity.this, PERMISSIONS)) {
            ActivityCompat.requestPermissions(MainActivity.this, PERMISSIONS, PERMISSION_ALL);
        }

        setFragment(new HomeFragment());

		invalidateMenuItem();

		//callAdsService();

		// checkUserWhenAppStart();

		//mDrawerLayout.setDrawerListener(new DemoDrawerListener());
		//mDrawerLayout.setDrawerShadow(R.drawable.ic_dialog_close_light, GravityCompat.START);

		//mActionBar = createActionBarHelper();
		//mActionBar.init();

		// ActionBarDrawerToggle provides convenient helpers for tying together
		// the
		// prescribed interactions between a top-level sliding drawer and the
		// action bar.
		//mDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout,R.string.drawer_open,R.string.drawer_close);
		/*
		 * mDrawerToggle = new SherlockActionBarDrawerToggle(this,
		 * mDrawerLayout, R.drawable.sidemenu_icon, R.string.drawer_open,
		 * R.string.drawer_close);
		 */
//		mDrawerToggle.syncState();

		// call device register service
		try {
			Log.d(TAG, "calling device reg API");
			callDeviceRegApi();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void setBackActionBar(String Title,int visible,String from) {
		getActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
		getActionBar().setCustomView(R.layout.custom_title_action_bar);
		TextView actionBarTitleView = (TextView) getActionBar().getCustomView()
				.findViewById(R.id.myTitle);
		actionBarTitleView.setSelected(true);

		Utils.setTextViewFontSizeBasedOnScreenDensity(this, actionBarTitleView,
				18f, Typeface.BOLD);
		new FontUtil(this).setFontRegularType(actionBarTitleView);
		actionBarTitleView.setText(Title);
		View view =getActionBar().getCustomView();

		ImageView backimg= (ImageView)view.findViewById(R.id.backimg);
		result_main_image = (ImageView)view.findViewById(R.id.result_main_image);
		result_main_image.setVisibility(visible);
		if(visible== View.INVISIBLE)
		{

		}else if(visible==View.VISIBLE)
		{
			if(from.equalsIgnoreCase("mail"))
			{
				result_main_image.setImageResource(R.drawable.ic_action_send_mail);

			}else if(from.equalsIgnoreCase("map"))
			{
				result_main_image.setImageResource(R.drawable.ic_action_locations_icon);
			}
		}
		backimg.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				getSupportFragmentManager().popBackStack();
			}
		});

	}
	public void setHomeActionBar() {
		getActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
		getActionBar().setCustomView(R.layout.custom_action_bar_main);

		getActionBar().setDisplayShowHomeEnabled(false);
		//getActionBar().setLogo(R.drawable.harry_logo);
		//getActionBar().setIcon(new ColorDrawable(getResources().getColor(R.color.primarycolor)));
		getActionBar().setDisplayShowTitleEnabled(false);
		View view =getActionBar().getCustomView();

		ImageView imageButton= (ImageView)view.findViewById(R.id.menuimg);
		imageButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				if(mDrawerLayout.isDrawerOpen(Gravity.LEFT))
					closeDrawer();
				else
					mDrawerLayout.openDrawer(Gravity.LEFT);
			}
		});
		// Setup Navigation Drawer Layout

		mDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout, R.drawable.ic_drawer, R.string.drawer_open, R.string.drawer_close) {

			@Override
			public void onDrawerOpened(View drawerView) {
				super.onDrawerOpened(drawerView);
			}

			@Override
			public void onDrawerClosed(View drawerView) {
				super.onDrawerClosed(drawerView);
			}
		};
		mDrawerLayout.setDrawerListener(mDrawerToggle);

		mDrawerLayout.post(new Runnable() {
			@Override
			public void run() {
				mDrawerToggle.syncState();
			}
		});
		//mDrawerToggle.setDrawerIndicatorEnabled(true);
		/*actionBar = getActionBar();

		actionBar.setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
		actionBar.setCustomView(R.layout.custom_action_bar_main);

		getWindow().setFeatureInt(Window.FEATURE_CUSTOM_TITLE, R.layout.custom_action_bar_main);

		int titleId = getResources().getIdentifier("action_bar_title", "id",
				"android");

		// If you're using sherlock, in older versions of android you are not
		// supposed to get a reference to android.R.id.action_bar_title, So
		// here's little hack for that.
		if (titleId == 0) {
			titleId = com.actionbarsherlock.R.id.abs__action_bar_title;
		}

		actionBar.setDisplayShowTitleEnabled(false);
		actionBar.setDisplayHomeAsUpEnabled(true);
		actionBar.setDisplayShowHomeEnabled(true);
		actionBar.setDisplayUseLogoEnabled(false);
*/
		//actionBar.setIcon(new ColorDrawable(getResources().getColor(android.R.color.transparent)));

		//mActionBar = createActionBarHelper();
		//mActionBar.init();
		//mDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout, R.string.drawer_open,R.string.drawer_close);
		//mDrawerToggle.syncState();

	}
	private void callDeviceRegApi() {

		if (!UserInfoManager.getInstance(this).isSignedIn(this)) {

			BeanDevice beanDevice = AppUtil.calculateDeviceRegData(this,
					SharedPref.getString(this, SharedPref.COMPANY_ID_KEY, "1"),
					SharedPref.getString(this, SharedPref.APPID_KEY, "1"),
					SharedPref.getInteger(this, SharedPref.USERID_KEY, 0));

			DevicesAsyncTask mDeviceTask = new DevicesAsyncTask(this,
					beanDevice, true);
			mDeviceTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR,
					(Void) null);
		}
	}

	private boolean callAdsService() {

		if (AppUtil.checkNetworkConnection(this)) {
			AdsAsyncTask adsAsyncTask = new AdsAsyncTask(this);
			// adsAsyncTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR,(Void)
			// null);
			adsAsyncTask.execute();

			return true;

		}
		return false;
	}

	public void refreshMenu() {

		// set the Behind View
		SlideMenuFragment slideFrag = new SlideMenuFragment();
		slidelistener = slideFrag;
		// setBehindContentView(R.layout.menu_frame);
		getSupportFragmentManager().beginTransaction()
				.replace(R.id.layoutSlideFragment, slideFrag).commit();

	}

	public void checkUserWhenAppStart() {
		// if (SharedPref
		// .getBoolean(this, SharedPref.FIRST_TIMEAPPSTART_KEY, true)) {
		// setFragment(new HomeFragment());
		// } else {
		// setFragment(new HomeFragment());
		// }

		// setFragment(new SplashFragment());
		setFragment(new HomeFragment());

	}

	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		super.onConfigurationChanged(newConfig);
		//mDrawerToggle.onConfigurationChanged(newConfig);
	}

	@Override
	protected void attachBaseContext(Context newBase) {
		// CalligraphyConfig.initDefault(Constants.FONT_DEFAULT);
		// CalligraphyConfig.initDefault(Constants.GLOBAL_FONT_APP);
		super.attachBaseContext(new CalligraphyContextWrapper(newBase));
	}

	public void setFragment(Fragment frag) {

		getSupportFragmentManager().beginTransaction()
				.replace(R.id.layoutFragment, frag).addToBackStack(null)
				.commit();
		getActionBar().setDisplayHomeAsUpEnabled(false);
		try {
			refreshMenu();
			slidelistener.refreshSlidingMenu();
		} catch (Exception e) {
		}

	}

	public void setFragmentClearStack(Fragment frag) {

		getSupportFragmentManager().popBackStack(null,
				FragmentManager.POP_BACK_STACK_INCLUSIVE);
		getSupportFragmentManager().beginTransaction()
				.replace(R.id.layoutFragment, frag).addToBackStack(null)
				.commit();
		getActionBar().setDisplayHomeAsUpEnabled(false);
		// getSlidingMenu().showContent();
		try {
			refreshMenu();
			slidelistener.refreshSlidingMenu();
		} catch (Exception e) {
		}
	}

	public void setTitle(int res) {
		getActionBar().setTitle(res);
	}

	public void resetFragmentSctack() {
		FragmentManager fm = getSupportFragmentManager();
		for (int i = 0; i < fm.getBackStackEntryCount(); ++i) {
			fm.popBackStack();
		}
		try {
			refreshMenu();
			slidelistener.refreshSlidingMenu();
		} catch (Exception e) {
		}
	}

	public void backToHomeScreen() {
		Log.d(TAG, "backToHomeScreen()");
		// resetFragmentSctack();
		// setFragment(new HomeFragment());
		setFragmentClearStack(new HomeFragment());
	}

	@Override
	public void onBackPressed() {
		Log.d(TAG, "onBackPressed() "
				+ getSupportFragmentManager().getBackStackEntryCount());
		if (getSupportFragmentManager().getBackStackEntryCount() == 1) {

			BaseHelper.confirm(MainActivity.this, R.string.rc_exit_message,
					new ConfirmListener() {

						@Override
						public void onYes() {
							SharedPreferences preferences = SharedPref
									.getPref(getApplicationContext());
							if (!preferences.getBoolean(
									SharedPref.REMEMBER_ME_KEY, false)) {
								preferences.edit().clear().commit();
							}
							resetFragmentSctack();
							closeApp();
							finish();
							checkUserWhenAppStart();
						}
					});

		} else {
			super.onBackPressed();
		}
	}

	public static void hideKeyboard(Context ctx) {
		try {

			InputMethodManager inputManager = (InputMethodManager) ctx
					.getSystemService(Context.INPUT_METHOD_SERVICE);

			// check if no view has focus:
			View v = ((Activity) ctx).getCurrentFocus();
			if (v == null)
				return;

			inputManager.hideSoftInputFromWindow(v.getWindowToken(), 0);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void closeApp() {
		Intent intent = new Intent(Constants.APP_PACKAGE);
		sendBroadcast(intent);
	}

	private final class KillReceiver extends BroadcastReceiver {
		@Override
		public void onReceive(Context context, Intent intent) {
			finish();
			android.os.Process.killProcess(android.os.Process.myPid());
		}
	}

	@Override
	public boolean onPrepareOptionsMenu(Menu menu) {

		// this.menu = menu;
		//
		// menu.clear();
		//
		// menu.add(getResources().getString(R.string.setting))
		// .setIcon(R.drawable.ic_action_action_settings)
		// .setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM);
		//
		// try {
		// if(getCurrentFragment() instanceof ListUsersFragment ||
		// getCurrentFragment() instanceof SettingFragment){
		// menu.getItem(0).setVisible(true);
		// }else{
		// menu.getItem(0).setVisible(false);
		// }
		//
		// } catch (Exception e) {
		// e.printStackTrace();
		// }

		return super.onPrepareOptionsMenu(menu);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {

		return true;
	}

	public void refreshActivity() {
		onCreate(null);
	}

	public void invalidateMenuItem() {
		if (Build.VERSION.SDK_INT >= 11) {
			supportInvalidateOptionsMenu();
		}
	}

	public Menu getMenu() {
		return menu;
	}

	public void setMenu(Menu menu) {
		this.menu = menu;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {

		if (getCurrentFragment() instanceof HomeFragment) {
			if (mDrawerToggle.onOptionsItemSelected(item)) {
				return true;
			}
		} else {
			switch (item.getItemId()) {

				case android.R.id.home:
					onBackPressed();
					return true;
			}
		}

		return false;
	}

	public void Slide_In_Frag(Fragment frag) {

		FragmentTransaction ft = getSupportFragmentManager().beginTransaction();

		ft.setCustomAnimations(R.anim.slide_down, R.anim.popup_hide,
				R.anim.slide_up, R.anim.popup_hide);
		ft.replace(R.id.layoutFragment, frag).addToBackStack(null);
		ft.commit();

		getActionBar().setDisplayHomeAsUpEnabled(false);

	}

	public Fragment getCurrentFragment() {

		return getSupportFragmentManager()
				.findFragmentById(R.id.layoutFragment);
	}

	public void closeDrawer() {
		mDrawerLayout.closeDrawers();
	}

    @Override

    public void onRequestPermissionsResult(int permsRequestCode, String[] permissions, int[] grantResults) {

        switch (permsRequestCode) {

            case 1:

                boolean first = grantResults[0] == PackageManager.PERMISSION_GRANTED;

                boolean second = grantResults[1] == PackageManager.PERMISSION_GRANTED;
                Log.e("test", first + " " + second);
                if (first) {
                    loc_permission = true;
                }
                if (loc_permission) {

                    if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                        // TODO: Consider calling
                        //    ActivityCompat#requestPermissions
                        // here to request the missing permissions, and then overriding
                        //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                        //                                          int[] grantResults)
                        // locationManager = (LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE); to handle the case where the user grants the permission. See the documentation
                        // for ActivityCompat#requestPermissions for more details.
                        return;
                    }
                    location = LocationServices.FusedLocationApi.getLastLocation(
                            googleApiClient);
                    if (location != null) {
                        Utils.latitude = location.getLatitude();
                        Utils.longitude = location.getLongitude();

                    }
                }
                break;

        }

    }
    @Override
    public void onConnected(@Nullable Bundle bundle) {
        if (ActivityCompat.checkSelfPermission(MainActivity.this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(MainActivity.this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            //    ActivityCompat#requestPermissions
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        if(loc_permission)
            location = LocationServices.FusedLocationApi.getLastLocation(
                    googleApiClient);
        if (location != null) {
            Utils.latitude = location.getLatitude();
            Utils.longitude = location.getLongitude();

        }


    }
    public void getLocation() {
        if (googleApiClient == null) {
            googleApiClient = new GoogleApiClient.Builder(MainActivity.this)
                    .addApi(LocationServices.API)
                    .addConnectionCallbacks(this)
                    .addOnConnectionFailedListener(this).build();
            googleApiClient.connect();

            LocationRequest locationRequest = LocationRequest.create();
            locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
            locationRequest.setInterval(30 * 1000);
            locationRequest.setFastestInterval(5 * 1000);
            LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                    .addLocationRequest(locationRequest);

            //**************************
            builder.setAlwaysShow(true); //this is the key ingredient
            //**************************

            PendingResult<LocationSettingsResult> result =
                    LocationServices.SettingsApi.checkLocationSettings(googleApiClient, builder.build());
            result.setResultCallback(new ResultCallback<LocationSettingsResult>() {
                @Override
                public void onResult(LocationSettingsResult result) {
                    final Status status = result.getStatus();

                    final LocationSettingsStates state = result.getLocationSettingsStates();
                    switch (status.getStatusCode()) {
                        case LocationSettingsStatusCodes.SUCCESS:
                            // All location settings are satisfied. The client can initialize location
                            // requests here.

                            break;
                        case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                            // Location settings are not satisfied. But could be fixed by showing the user
                            // a dialog.
                            try {
                                // Show the dialog by calling startResolutionForResult(),
                                // and check the result in onActivityResult().
                                status.startResolutionForResult(
                                        MainActivity.this, 1000);
                            } catch (IntentSender.SendIntentException e) {
                                // Ignore the error.
                            }
                            break;
                        case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                            // Location settings are not satisfied. However, we have no way to fix the
                            // settings so we won't show the dialog.
                            break;
                    }
                }
            });
        }
    }
    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

}
