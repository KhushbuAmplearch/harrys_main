package com.riverview.harrys.helpers;

import java.util.ArrayList;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

import android.app.IntentService;
import android.content.Intent;
import android.util.Log;

import com.iapps.libs.helpers.BaseHelper;
import com.riverview.harrys.asynctask.AsyncResponse;
import com.riverview.harrys.asynctask.PreferencesAsyncTask;
import com.riverview.harrys.asynctask.PreferencesUpdateAsyncTask;
import com.riverview.harrys.constant.AppConst;
import com.riverview.harrys.objects.BeanPreference;
import com.riverview.harrys.objects.ServiceResponse;
import com.riverview.harrys.util.AppUtil;

public class PreferenceUpdateService extends IntentService implements
		AsyncResponse<ServiceResponse<ArrayList<BeanPreference>>> {

	private static final String TAG = PreferenceUpdateService.class
			.getSimpleName();

	private final String SWITCH_DES = "preferences[%s]";
	private String referralCode;
	private String mobileUserId = "0";

	public PreferenceUpdateService() {
		super(TAG);

	}

	@Override
	protected void onHandleIntent(Intent intent) {
		if (AppUtil.checkNetworkConnection(getApplicationContext())) {

			referralCode = intent.getStringExtra(AppConst.TAG_REF_ID);
			mobileUserId = intent.getStringExtra(AppConst.ATTR_TAG_MOBILEUSER_ID);
			
			PreferencesAsyncTask mPreferencesTask = new PreferencesAsyncTask(
					getApplicationContext(), mobileUserId);
			mPreferencesTask.asyncResponse = PreferenceUpdateService.this;
			mPreferencesTask.execute((Void) null);
		}

	}

	@Override
	public void processFinish(ServiceResponse<ArrayList<BeanPreference>> details) {

		if (details != null && details.getStatus() == 200 && !details.error) {
			Log.i(TAG, "Preferences download Success - service");

			// change preference status to active and upload
			if (details.getResponse() != null && details.response.size() > 0) {
				ArrayList<NameValuePair> arrayList = new ArrayList<NameValuePair>();
				ArrayList<BeanPreference> bf = details.getResponse();

				for (BeanPreference bean : bf) {
					arrayList.add(new BasicNameValuePair(String.format(
							SWITCH_DES, bean.getId()), "1"));
				}

				// Add referral code
				if (!BaseHelper.isEmpty(referralCode)) {
					arrayList.add(new BasicNameValuePair(String.format(
							SWITCH_DES, 99), referralCode));
				}

				// upload the preferences
				if (AppUtil.checkNetworkConnection(getApplicationContext())) {

					PreferencesUpdateAsyncTask mPreferencesTask = new PreferencesUpdateAsyncTask(
							getApplicationContext(), arrayList, false);
					// mPreferencesTask.asyncResponse =
					// PreferencesFragment.this;
					mPreferencesTask.execute((Void) null);
				}

			}

		}

	}

}
