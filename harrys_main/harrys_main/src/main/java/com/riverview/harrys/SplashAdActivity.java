package com.riverview.harrys;

import com.squareup.picasso.Picasso;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageView;

public class SplashAdActivity extends Activity {

	private static int AUTO_HIDE_DELAY_MILLIS = 3000;

	ImageView adImage;

	String imagPath;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_splash_ad);

		adImage = (ImageView) findViewById(R.id.imgAd);

		String adTime = getIntent().getStringExtra("time");

		if (!adTime.isEmpty() || !adTime.equals("null")) {
			AUTO_HIDE_DELAY_MILLIS = Integer.parseInt(adTime);
		}
		imagPath = getIntent().getStringExtra("imgPath");

		try {
			loadImageWithPlaceholder(getApplication(), imagPath, adImage, R.drawable.placeholder);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		new Handler().postDelayed(new Runnable() {
			
			@Override
			public void run() {
				SplashAdActivity.this.finish();
				
			}
		}, AUTO_HIDE_DELAY_MILLIS);
		
	}

	public static void loadImageWithPlaceholder(Context context, String url,
			ImageView img, int placeholder) {
		Picasso.with(context).load(url).placeholder(null).into(img);
	}
}
