package com.riverview.harrys.util;

import static com.riverview.harrys.constant.AppConst.ATTR_TAG_AUTH_KEY;
import static com.riverview.harrys.constant.AppConst.ATTR_TAG_AUTH_SIGNATURE;
import static com.riverview.harrys.constant.AppConst.ATTR_TAG_AUTH_VERSION;
import static com.riverview.harrys.constant.AppConst.ATTR_TAG_UNIX_TIMESTAMP;
import static com.riverview.harrys.constant.AppConst.AUTH_VERSION;
import static com.riverview.harrys.constant.AppConst.SERVICE_API_KEY;

import java.io.InputStream;
import java.io.OutputStream;
import java.net.URLDecoder;
import java.text.DateFormat;
import java.text.Format;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.TimeZone;

import org.apache.http.NameValuePair;
import org.apache.http.client.utils.URLEncodedUtils;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONObject;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager.NameNotFoundException;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.IBinder;
import android.provider.Settings;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.KeyEvent;
import android.view.inputmethod.InputMethodManager;

import com.riverview.harrys.constant.AppConst;
import com.riverview.harrys.crypto.AppAuthMangerImpl;
import com.riverview.harrys.crypto.AuthException;
import com.riverview.harrys.helpers.Analytics;
import com.riverview.harrys.helpers.SharedPref;
import com.riverview.harrys.helpers.Utils;
import com.riverview.harrys.objects.BeanDevice;

public class AppUtil {

	private static final String TAG = AppUtil.class.getSimpleName();

	private static String SYM_EQ = "=";
	private static String SYM_AMP = "&";
	private static String SYM_NEW_LINE = "\n";

	public static BeanDevice calculateDeviceRegData(Context mContext,
			String companyId, String appId, int mobileuserId) {

		BeanDevice device = new BeanDevice(companyId, appId);
		device.setStatus("ACTIVE");
		device.setDevice("ANDROID");
		device.setUDID(getDeviceId(mContext));
		device.setMobileuser_id(String.valueOf(mobileuserId));
		device.setApp_version(String.valueOf(AppUtil.getAppVersion(mContext)));
		device.setOs_version(String.valueOf(android.os.Build.VERSION.SDK_INT));

		return device;

	}

	public static String getDeviceId(Context mContext) {
		String android_id = Settings.Secure.getString(mContext.getContentResolver(),
				Settings.Secure.ANDROID_ID);

		return android_id;
	}

	public static long getUnixTimeStamp() {
		return System.currentTimeMillis() / 1000L;
	}

	public static ArrayList<NameValuePair> generateAuthCommandString(
			ArrayList<NameValuePair> values, String url, String action) {
		try {

			ArrayList<NameValuePair> httpParams;
			StringBuffer stringBuffer;

			if (values == null) {
				httpParams = new ArrayList<NameValuePair>();
			} else {
				httpParams = values;
			}

			long timeStamp = getUnixTimeStamp();
			String timeStamp1 = String.valueOf(timeStamp);

			httpParams.add(new BasicNameValuePair(ATTR_TAG_UNIX_TIMESTAMP,
					timeStamp1));
			httpParams.add(new BasicNameValuePair(ATTR_TAG_AUTH_VERSION,
					AUTH_VERSION));
			httpParams.add(new BasicNameValuePair(ATTR_TAG_AUTH_KEY,
					SERVICE_API_KEY));

			Comparator<NameValuePair> comp = new Comparator<NameValuePair>() {

				@Override
				public int compare(NameValuePair p1, NameValuePair p2) {
					return p1.getName().toLowerCase()
							.compareTo(p2.getName().toLowerCase());
				}
			};
			Collections.sort(httpParams, comp);

			String requestParameters = URLEncodedUtils.format(httpParams,
					"UTF-8");

			Log.d(TAG, "Req Param ----------- " + requestParameters);

			requestParameters = URLDecoder.decode(requestParameters, "UTF-8");
			stringBuffer = new StringBuffer(action).append(SYM_NEW_LINE)
					.append(url).append(SYM_NEW_LINE).append(requestParameters);

			String generatedHash = new AppAuthMangerImpl()
					.getHMACHash(stringBuffer.toString());

			httpParams.add(new BasicNameValuePair(ATTR_TAG_AUTH_SIGNATURE,
					generatedHash));

			return httpParams;
		} catch (AuthException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	public static String appendUrlString(String attrName, String value,
			String existingValue) {

		StringBuilder builder = new StringBuilder(existingValue);

		return builder.append(SYM_AMP).append(attrName).append(SYM_EQ)
				.append(value).toString();
	}

	/**
	 * Check the network connection of the device.
	 * 
	 * @param context
	 *            current context value
	 * @return true if network connection available false otherwise
	 */
	public static boolean checkNetworkConnection(Context context) {
		ConnectivityManager connectivityManager = null;
		try {
			connectivityManager = (ConnectivityManager) context
					.getSystemService(Context.CONNECTIVITY_SERVICE);
		} catch (Exception e) {
			e.printStackTrace();
		}
		if (connectivityManager == null) {
			return false;
		}

		NetworkInfo activeNetworks = connectivityManager.getActiveNetworkInfo();
		if (activeNetworks != null && activeNetworks.isConnected()) {
			return activeNetworks.isConnectedOrConnecting();
		}
		return false;
	}

	public static void showSystemSettingsDialog(Context context) {

		if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
			context.startActivity(new Intent(Settings.ACTION_WIRELESS_SETTINGS));

		} else {
			context.startActivity(new Intent(Settings.ACTION_WIRELESS_SETTINGS));
		}
	}

	public static boolean isUsernameValid(String username) {

		return !(username == null | username == "")
				&& !(username.length() > 20);

	}

	public static boolean isPasswordValid(String password) {

		return !(password == null | password == "")
				&& !(password.length() < 4 | password.length() > 15);

	}

	public static void hideSoftKeyInput(Context context, IBinder iBinder) {

		final InputMethodManager mInputMethodManager = (InputMethodManager) context
				.getSystemService(Context.INPUT_METHOD_SERVICE);
		mInputMethodManager.hideSoftInputFromWindow(iBinder, 0);
	}

	public static void CopyStream(InputStream is, OutputStream os) {
		final int buffer_size = 1024;
		try {
			byte[] bytes = new byte[buffer_size];
			for (;;) {
				int count = is.read(bytes, 0, buffer_size);
				if (count == -1)
					break;
				os.write(bytes, 0, count);
			}
		} catch (Exception ex) {
		}
	}

	/**
	 * get android application version
	 * 
	 * @param context
	 *            current context
	 * @return application version id
	 */
	public static int getAppVersion(Context context) {
		try {
			PackageInfo packageInfo = context.getPackageManager()
					.getPackageInfo(context.getPackageName(), 0);
			return packageInfo.versionCode;
		} catch (NameNotFoundException e) {
			// should never happen
			// throw new RuntimeException("Could not get package name: " + e);
			e.printStackTrace();
		}
		return 0;
	}

	public static String getTimeOfDay() {
		GregorianCalendar todaysDate = new GregorianCalendar();
		int hour_of_day;

		hour_of_day = todaysDate.get(Calendar.HOUR_OF_DAY);

		if (hour_of_day < 12) {
			return "Good Morning";
		} else if (hour_of_day > 11 && hour_of_day < 17) {
			return "Good Afternoon";
		} else if (hour_of_day >= 17 && hour_of_day < 24) {
			return "Good Evening";
		}
		return "Hello";
	}

	public static String convertFirstLetterUpper(String s) {

		try {
			String convertedText = Character.toUpperCase(s.charAt(0))
					+ s.substring(1);
			return convertedText;
		} catch (Exception e) {
			return s;
		}
	}

	public static String getNationalityJSON(Context context) {

		try {
			InputStream stream = context.getAssets().open("nationalities.json");
			int size = stream.available();
			byte[] buffer = new byte[size];
			stream.read(buffer);

			stream.close();

			return new String(buffer, "UTF-8");

		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	public static String getDeviceName() {
		String manufacturer = Build.MANUFACTURER;
		String model = Build.MODEL;
		if (model.startsWith(manufacturer)) {
			return capitalize(model);
		} else {
			return capitalize(manufacturer) + " " + model;
		}
	}

	private static String capitalize(String s) {
		if (s == null || s.length() == 0) {
			return "";
		}
		char first = s.charAt(0);
		if (Character.isUpperCase(first)) {
			return s;
		} else {
			return Character.toUpperCase(first) + s.substring(1);
		}
	}

	public static String convertDate(Context mContext, String strDate,
			boolean sysFormat) {

		try {
			if (strDate.equals("0000-00-00")) {
				return strDate;
			}

			Date date;
			DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
			date = (Date) formatter.parse(strDate);

			Format dateFormat = android.text.format.DateFormat
					.getDateFormat(mContext);
			String pattern = ((SimpleDateFormat) dateFormat)
					.toLocalizedPattern();

			DateFormat formatter1;
			if (sysFormat) {
				formatter1 = new SimpleDateFormat(pattern);
			} else {
				formatter1 = new SimpleDateFormat("d MMM yyyy");
			}
			return formatter1.format(date);

		} catch (ParseException e) {
			Log.e(TAG, "Exception :" + e);
		} catch (Exception e) {
			e.printStackTrace();
		}

		return strDate;
	}

	public static String convertDateCms(Context mContext, String strDate) {

		try {
			if (strDate.equals("0000-00-00")) {
				return strDate;
			}

			Date date;
			DateFormat formatter = new SimpleDateFormat("d MMM yyyy");
			// DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
			date = (Date) formatter.parse(strDate);

			DateFormat formatter1;
			formatter1 = new SimpleDateFormat("yyyy-MM-dd");

			return formatter1.format(date);

		} catch (ParseException e) {
			Log.e(TAG, "Exception :" + e);
		} catch (Exception e) {
			e.printStackTrace();
		}

		return strDate;
	}

	public static String convertDate(Context mContext, String strDate) {

		try {
			if (strDate.equals("0000-00-00")) {
				return strDate;
			}

			Date date;
			DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
			date = (Date) formatter.parse(strDate);

			DateFormat formatter1;
			formatter1 = new SimpleDateFormat("d-MMM-yyyy");

			return formatter1.format(date);

		} catch (ParseException e) {
			Log.e(TAG, "Exception :" + e);
		} catch (Exception e) {
			e.printStackTrace();
		}

		return null;
	}

	public static Date convertDate(String strDate) {
		Date date = null;
		try {

			DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			date = (Date) formatter.parse(strDate);
			return date;

		} catch (ParseException e) {
			Log.e(TAG, "Exception :" + e);
		} catch (Exception e) {
			e.printStackTrace();
		}

		return null;
	}

	public static JSONObject createAnalyticsJsonEvent(String type,
			String value, String attributes) {

		JSONObject jObjectData = new JSONObject();
		try {
			jObjectData.put("event_type", type);
			jObjectData.put("timestamp", getISO8601String());
			jObjectData.put("event_value", value);
			try {
				jObjectData.put("attributes", new JSONObject(attributes));
			} catch (Exception e) {
				e.printStackTrace();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		return jObjectData;

	}

	public static JSONObject createAnalyticsJsonEventAction(String type,
			String value, String action, String attributes) {

		JSONObject jObjectData = new JSONObject();
		try {
			jObjectData.put("event_type", type);
			jObjectData.put("event_action", action);
			jObjectData.put("timestamp", getISO8601String());
			jObjectData.put("event_value", value);
			try {
				if (attributes != null) {
					jObjectData.put("attributes", new JSONObject(attributes));
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		return jObjectData;

	}

	public static JSONObject getAnalyticsWrapper(Context context, JSONArray data) {
		JSONObject jObjectData = null;
		try {
			jObjectData = new JSONObject();

			jObjectData = createDeviceInfo(context);
			// jObjectData.put("app_id", AppConst.SERVICE_API_KEY);

			jObjectData.put("events", data);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return jObjectData;
	}

	public static AlertDialog showAlert(Context context, String title,
			String message, DialogInterface.OnClickListener listener) {
		if (listener == null) {
			listener = new DialogInterface.OnClickListener() {

				@Override
				public void onClick(DialogInterface dialog, int which) {

				}
			};
		}

		AlertDialog d = new AlertDialog.Builder(context).setMessage(message)
				.setTitle(title).setCancelable(false)
				.setNeutralButton(android.R.string.ok, listener).show();
		return d;
	}

	public static interface ConfirmListener {

		public void onYes();
	}

	public static void confirmForceUpdate(Context context, String title,
			String message, final ConfirmListener l) {
		AlertDialog.Builder b = buildAlert(context, title, message);

		b.setCancelable(false);
		b.setOnKeyListener(new DialogInterface.OnKeyListener() {
			@Override
			public boolean onKey(DialogInterface dialog, int keyCode,
					KeyEvent event) {
				// Prevent dialog close on back press button
				return keyCode == KeyEvent.KEYCODE_BACK;
			}
		});

		b.setPositiveButton("Update", new DialogInterface.OnClickListener() {

			@Override
			public void onClick(DialogInterface dialog, int which) {
				l.onYes();
			}
		});

		b.show();
	}

	public static AlertDialog showAlertCustom(Activity context, String title,
			String message, DialogInterface.OnClickListener listener) {
		if (listener == null) {
			listener = new DialogInterface.OnClickListener() {

				@Override
				public void onClick(DialogInterface dialog, int which) {

				}
			};
		}

		AlertDialog d = new AlertDialog.Builder(context).setMessage(message)
				.setTitle(title).setCancelable(false)
				.setNeutralButton(android.R.string.ok, listener).show();
		return d;
	}

	private static AlertDialog.Builder buildAlert(Context context,
			String title, String message) {
		AlertDialog.Builder dlg = new AlertDialog.Builder(context)
				.setMessage(message).setTitle(title).setCancelable(true);
		return dlg;
	}

	private static JSONObject createDeviceInfo(Context context) {

		JSONObject jObjectData = new JSONObject();
		try {
			jObjectData.put("app", Analytics.getApplicationName(context));
			jObjectData.put("app_id", AppConst.SERVICE_API_KEY);
			jObjectData.put("app_version", Double.parseDouble((context
					.getPackageManager().getPackageInfo(
					context.getPackageName(), 0)).versionName));
			jObjectData.put("app_package_name", context.getPackageName());
			jObjectData.put("platform", "Android");
			jObjectData.put("model", Analytics.getModelName());
			jObjectData.put("make", Analytics.getManufacturerName());
			jObjectData.put("platform_version",
					android.os.Build.VERSION.SDK_INT);
			jObjectData.put("rms_version", "1.0.0");
			jObjectData.put("locale", (context.getResources()
					.getConfiguration().locale).toString());
			jObjectData.put("mac_address", Analytics.getMacAdress(context));
			jObjectData.put("UDID", AppUtil.getDeviceId(context));
			jObjectData.put("session", getSessionMetrics(context));

			// try {
			// SharedPreferences sf = SharedPref.getPref(context);
			// jObjectData.put("user_id", sf.getInt(SharedPref.USERID_KEY, 0));
			// } catch (Exception e) {
			// e.printStackTrace();
			// }

		} catch (Exception e) {
			e.printStackTrace();
		}
		return jObjectData;

	}

	public static JSONObject getSessionMetrics(Context context) {

		JSONObject jObjectData = new JSONObject();
		try {

			jObjectData.put("timestamp", getISO8601String());
			jObjectData.put("session_id", AppUtil.getDeviceId(context));

			String lat = String.valueOf(Utils.latitude);
			String lon = String.valueOf(Utils.longitude);

			// jObjectData.put("ip", "1.1.1.1");
			jObjectData.put("location", lat.concat(",").concat(lon));
		} catch (Exception e) {
		}

		return jObjectData;
	}

	private static String getISO8601String() {
		TimeZone tz = TimeZone.getTimeZone("UTC");
		DateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ");
		df.setTimeZone(tz);
		String nowAsISO = df.format(new Date());
		return nowAsISO;
	}

}
