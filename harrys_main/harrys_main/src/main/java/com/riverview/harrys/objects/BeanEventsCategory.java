package com.riverview.harrys.objects;

import java.util.ArrayList;

public class BeanEventsCategory {

	private int id;
	private String company_id, name, description, created_at, updated_at, deleted_at;

	// private BeanImage image;
	private ArrayList<BeanEvents> events;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getCompany_id() {
		return company_id;
	}

	public void setCompany_id(String company_id) {
		this.company_id = company_id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getCreated_at() {
		return created_at;
	}

	public void setCreated_at(String created_at) {
		this.created_at = created_at;
	}

	public String getUpdated_at() {
		return updated_at;
	}

	public void setUpdated_at(String updated_at) {
		this.updated_at = updated_at;
	}

	public String getDeleted_at() {
		return deleted_at;
	}

	public void setDeleted_at(String deleted_at) {
		this.deleted_at = deleted_at;
	}

	public ArrayList<BeanEvents> getEvents() {
		return events;
	}

	public void setEvents(ArrayList<BeanEvents> events) {
		this.events = events;
	}

}
