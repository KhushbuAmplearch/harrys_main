package com.riverview.harrys.helpers;

import java.util.HashMap;

import android.content.Context;
import android.graphics.Typeface;
import android.widget.TextView;

public class FontUtil {

	// public static final String DEFAULT_TYPEFACE = "arial.ttf";
	// public static final String ARIAL_BOLD = "arial_bold.ttf";

	public static final String DEFAULT_TYPEFACE = "Georgia.ttf";
	public static final String ARIAL_BOLD = "Georgia.ttf";

	private Context context;
	private HashMap<String, Typeface> fonts;

	public FontUtil(Context context) {
		this.context = context;
		fonts = new HashMap<String, Typeface>();

	}

	public void setFontRegularType(TextView txtView) {
		setFontType(txtView, DEFAULT_TYPEFACE);
	}

	public void setFontBookType(TextView txtView) {
		setFontType(txtView, ARIAL_BOLD);
	}

	public void setFontBoldType(TextView txtView) {
		setFontType(txtView, ARIAL_BOLD);
	}

	public void setFontType(TextView txtView, String fontType) {
		Typeface typeFace = fonts.get(fontType);
		if (typeFace == null) {
			try {
				typeFace = Typeface.createFromAsset(context.getAssets(),
						fontType);
				if (typeFace != null) {
					txtView.setTypeface(typeFace);
					fonts.put(fontType, typeFace);
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
}
