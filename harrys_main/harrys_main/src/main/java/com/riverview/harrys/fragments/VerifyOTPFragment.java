package com.riverview.harrys.fragments;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;

import com.iapps.libs.helpers.BaseHelper;
import com.iapps.libs.helpers.BaseHelper.ConfirmListener;
import com.riverview.harrys.MainActivity;
import com.riverview.harrys.R;
import com.riverview.harrys.RCGenericFragment;
import com.riverview.harrys.asynctask.AsyncResponse;
import com.riverview.harrys.asynctask.ResendOTPAsyncTask;
import com.riverview.harrys.asynctask.VerifyOTPAsyncTask;
import com.riverview.harrys.constant.AppConst;
import com.riverview.harrys.helpers.SharedPref;
import com.riverview.harrys.helpers.Utils;
import com.riverview.harrys.objects.BeanMobileUser;
import com.riverview.harrys.objects.ServiceResponse;
import com.riverview.harrys.util.AnaylaticsAppender;
import com.riverview.harrys.util.AppUtil;

public class VerifyOTPFragment extends RCGenericFragment implements
		AsyncResponse<ServiceResponse<BeanMobileUser>> {

	private static final String TAG = VerifyOTPFragment.class.getSimpleName();

	private LinearLayout LLSignIn;
	private EditText editTextOtp;
	private Button buttonSignIn;
	private Button buttonResendOtp;

	View v;

	private boolean isFromSignUp;

	final MyCounter timer = new MyCounter(120000, 1000);

	private AnaylaticsAppender mAppender = null;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		getActivity().getActionBar().setDisplayShowHomeEnabled(false);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
							 Bundle savedInstanceState) {
		v = inflater.inflate(R.layout.verify_otp_fragment_layout, container,
				false);
		LLSignIn = (LinearLayout)v.findViewById(R.id.LLSignIn);
		editTextOtp = (EditText)v.findViewById(R.id.editTextOtp);
		buttonSignIn = (Button)v.findViewById(R.id.buttonSignIn);
		buttonResendOtp = (Button)v.findViewById(R.id.buttonResendOtp);

		setHasOptionsMenu(false);
		((MainActivity) getActivity()).setBackActionBar(getHome()
				.getResources().getString(R.string.rc_verify_otp),View.INVISIBLE,"");

		try {
			if (getArguments() != null) {
				isFromSignUp = getArguments().getBoolean("from_signup", false);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		hideDefaultKeyboard();

		return v;
	}

	private void hideDefaultKeyboard() {
		try {
			getActivity().getWindow().setSoftInputMode(
					WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

			getActivity();
			InputMethodManager imm = (InputMethodManager) getActivity()
					.getSystemService(Context.INPUT_METHOD_SERVICE);
			imm.hideSoftInputFromWindow(editTextOtp.getWindowToken(), 0);

		} catch (Exception e) {
		}

	}

	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);

		LLSignIn.setOnClickListener(clickListeners);

		Utils.setHeightByPercent(getActivity(), editTextOtp, 0.08f);

		Utils.setHeightByPercent(getActivity(), buttonSignIn, 0.09f);
		Utils.setHeightByPercent(getActivity(), buttonResendOtp, 0.09f);

		buttonSignIn.setText(getString(R.string.rc_submit));
		buttonSignIn.setOnClickListener(clickListeners);

		buttonResendOtp.setText(getString(R.string.rc_resend_otp));
		buttonResendOtp.setOnClickListener(clickListeners);

		Utils.setEditTextFontSizeBasedOnScreenDensity(getActivity(),
				editTextOtp, Utils.FONT_SMALL_DENSITY_SIZE);
		Utils.setButtonFontSizeBasedOnScreenDensity(getActivity(),
				buttonSignIn, Utils.FONT_SMALL_DENSITY_SIZE, Typeface.BOLD);

		timer.start();
	}

	View.OnClickListener clickListeners = new View.OnClickListener() {
		public void onClick(View v) {

			switch (v.getId()) {

				case R.id.buttonSignIn:
					if (BaseHelper.isEmpty(editTextOtp)) {
						BaseHelper
								.showAlert(getActivity(), "Please enter the OTP.");
					} else {
						if (AppUtil.checkNetworkConnection(getActivity())) {
							submitOTP();

							// Stop timer
							try {
								timer.cancel();
							} catch (Exception e) {
								// TODO: handle exception
							}

						} else {
							BaseHelper
									.confirm(
											getActivity(),
											"Network Unavaliable",
											"Please turn on network to connect with server",
											new ConfirmListener() {

												@Override
												public void onYes() {
													AppUtil.showSystemSettingsDialog(getActivity());
												}
											});
						}
						hideDefaultKeyboard();
					}

					break;

				case R.id.buttonResendOtp:
					resendOTP();
					try {
						timer.start();
					} catch (Exception e) {
						// TODO: handle exception
					}

					break;

				default:
					break;
			}
		}
	};

	private void resendOTP() {
		if (AppUtil.checkNetworkConnection(getActivity())) {
			ResendOTPAsyncTask resendOTPAsyncTask = new ResendOTPAsyncTask(
					getActivity(), SharedPref.getInteger(getActivity(),
					SharedPref.USERID_KEY, 0), true);
			resendOTPAsyncTask.execute((Void) null);
		} else {
			BaseHelper.confirm(getActivity(), "Network Unavaliable",
					"Please turn on network to connect with server",
					new ConfirmListener() {

						@Override
						public void onYes() {
							AppUtil.showSystemSettingsDialog(getActivity());
						}
					});
		}
	}

	private void submitOTP() {

		VerifyOTPAsyncTask mAuthTask = new VerifyOTPAsyncTask(getHome(),
				SharedPref.getInteger(getActivity(), SharedPref.USERID_KEY, 0),
				editTextOtp.getText().toString().trim());
		mAuthTask.asyncResponse = VerifyOTPFragment.this;
		mAuthTask.execute((Void) null);

	}

	@Override
	public void onDestroy() {
		super.onDestroy();
		//hideDefaultKeyboard();
		Utils.hideKeyboard(getActivity());
	}

	@Override
	public void onDetach() {
		super.onDetach();
		//hideDefaultKeyboard();
		Utils.hideKeyboard(getActivity());
	}

	@Override
	public void processFinish(ServiceResponse<BeanMobileUser> details) {

		if (details == null) {
			try {
				BaseHelper.showAlert(getHome(), "Error when verifying OTP.");
			} catch (Exception e) {
			}
			return;
		}

		if (details != null
				&& ((details.getStatus() == 200 | details.getStatus() == 0))
				&& !details.error) {
			try {
				// save user details

				Log.d(TAG, "UserName " + details.getResponse().getUsername());

				SharedPref.saveBoolean(getActivity(), SharedPref.VERIFY_KEY,
						true);

				SharedPref.saveString(getActivity(), SharedPref.USERNAME_KEY,
						details.getResponse().getUsername());
				SharedPref.saveString(getActivity(), SharedPref.USERFNAME_KEY,
						details.getResponse().getFirstname());
				SharedPref.saveString(getActivity(), SharedPref.USERLNAME_KEY,
						details.getResponse().getSurname());
				SharedPref.saveString(getActivity(), SharedPref.MOBILE_NO_KEY,
						details.getResponse().getMobilenumber());
				SharedPref.saveString(getActivity(), SharedPref.EMAIL_KEY,
						details.getResponse().getEmail());
				SharedPref.saveInteger(getActivity(), SharedPref.USERID_KEY,
						details.getResponse().getId());
				SharedPref.saveString(getActivity(), SharedPref.APPID_KEY,
						details.getResponse().getApplication_id());
				SharedPref.saveString(getActivity(), SharedPref.COMPANY_ID_KEY,
						details.getResponse().getCompany_id());

				if (isFromSignUp) {

					HomeFragment frag = new HomeFragment();
					Bundle bundle = new Bundle();

					bundle.putBoolean("show_signup_dialog", true);

					frag.setArguments(bundle);
					getHome().setFragment(frag);
				} else {
					getHome().setFragmentClearStack(new HomeFragment());
				}

				mAppender.updateAnaylitsData(AppUtil
						.createAnalyticsJsonEventAction("verified",
								String.valueOf(details.getResponse().getId()),
								"update", null));

			} catch (Exception e) {
				e.printStackTrace();
			}

		} else {
			try {
				BaseHelper
						.showAlert(
								getHome(),
								"Harry's ",
								(BaseHelper.isEmpty(details.getError_message()) ? "Unable to verify OTP."
										: details.getError_message()));
			} catch (Exception e) {
			}

		}
	}

	public class MyCounter extends CountDownTimer {

		public MyCounter(long millisInFuture, long countDownInterval) {
			super(millisInFuture, countDownInterval);
		}

		@Override
		public void onFinish() {
			try {
				buttonResendOtp.setText(getString(R.string.rc_resend_otp));
				buttonResendOtp.setEnabled(true);
			} catch (Exception e) {
				// TODO: handle exception
			}

		}

		@Override
		public void onTick(long millisUntilFinished) {

			try {
				buttonResendOtp.setText(String.format(
						getString(R.string.rc_resend_timer),
						(millisUntilFinished / 1000)));
				buttonResendOtp.setEnabled(false);
			} catch (Exception e) {
				e.printStackTrace();
			}

		}
	}

	@Override
	public void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		LocalBroadcastManager.getInstance(getActivity()).registerReceiver(
				mMessageReceiver, new IntentFilter(AppConst.HARRYS_OTP_ACTION));

		try {
			mAppender = AnaylaticsAppender.getInstance(getActivity());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public void onPause() {
		// Unregister since the activity is not visible
		LocalBroadcastManager.getInstance(getActivity()).unregisterReceiver(
				mMessageReceiver);
		super.onPause();
	}

	// handler for received Intents for the "my-event" event
	private BroadcastReceiver mMessageReceiver = new BroadcastReceiver() {
		@Override
		public void onReceive(Context context, Intent intent) {
			// Extract data included in the Intent
			String message = intent.getStringExtra("content");
			String code = extractDigits(message);

			editTextOtp.setText(code);
		}
	};

	public static String extractDigits(final String in) {
		final Pattern p = Pattern.compile("(\\d{6})");
		final Matcher m = p.matcher(in);
		if (m.find()) {
			return m.group(0);
		}
		return "";
	}

}
