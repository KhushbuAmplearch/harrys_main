package com.riverview.harrys.asynctask;

import static com.riverview.harrys.constant.AppConst.ATTR_TAG_CONFIRMATION_CODE;
import static com.riverview.harrys.constant.AppConst.ATTR_TAG_ID;
import static com.riverview.harrys.constant.AppConst.ATTR_TAG_UDID;
import static com.riverview.harrys.constant.AppConst.HTTP_METHOD_POST;
import static com.riverview.harrys.constant.AppConst.SERVICE_END_POINT;
import static com.riverview.harrys.constant.AppConst.URL_VERIFY_OTP;

import java.io.IOException;
import java.util.ArrayList;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.type.TypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.client.ClientHttpResponse;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.DefaultResponseErrorHandler;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.riverview.harrys.R;
import com.riverview.harrys.exception.RestServiceException;
import com.riverview.harrys.objects.BeanMobileUser;
import com.riverview.harrys.objects.BeanUserData;
import com.riverview.harrys.objects.ErrorServiceResponse;
import com.riverview.harrys.objects.ServiceResponse;
import com.riverview.harrys.util.AppUtil;
import com.riverview.harrys.util.WebServiceUtil;

import fr.castorflex.android.circularprogressbar.CircularProgressDrawable;

public class VerifyOTPAsyncTask extends
		AsyncTask<Void, Void, ServiceResponse<BeanMobileUser>> {

	private static final String TAG = VerifyOTPAsyncTask.class.getSimpleName();

	public AsyncResponse<ServiceResponse<BeanMobileUser>> asyncResponse;

	private Context mContext;

	String[] commandString = null;
	ArrayList<NameValuePair> httpParams = null;
	private ProgressDialog mProgressDialog;

	private int userId = -1;
	private String apiUrl = null;
	private String confCode = null;

	public VerifyOTPAsyncTask(Context context, int id, String confirmationCode) {
		this.mContext = context;
		this.userId = id;
		this.confCode = confirmationCode;
	}

	@Override
	protected void onPreExecute() {
		Log.d(TAG, "onPreExecute");
		super.onPreExecute();

		mProgressDialog = new ProgressDialog(mContext);
		mProgressDialog
				.setIndeterminateDrawable(new CircularProgressDrawable.Builder(
						mContext)
						.colors(mContext.getResources().getIntArray(
								R.array.harrys_colors)).sweepSpeed(1f)
						.strokeWidth(12f)
						.style(CircularProgressDrawable.Style.ROUNDED).build());

		mProgressDialog.setCancelable(false);
		mProgressDialog.setMessage("Verifying OTP..");
		mProgressDialog.show();

		try {

			ArrayList<NameValuePair> arrayList = new ArrayList<NameValuePair>();

			arrayList.add(new BasicNameValuePair(ATTR_TAG_CONFIRMATION_CODE,
					confCode));
			arrayList.add(new BasicNameValuePair(ATTR_TAG_ID, String
					.valueOf(userId)));

			apiUrl = String.format(URL_VERIFY_OTP, String.valueOf(userId));
			httpParams = AppUtil.generateAuthCommandString(arrayList, apiUrl,
					HTTP_METHOD_POST);

		} catch (Exception e) {

			e.printStackTrace();
		}

	}

	@Override
	protected void onPostExecute(ServiceResponse<BeanMobileUser> response) {
		Log.d(TAG, "onPostExecute");

		if (mProgressDialog != null && mProgressDialog.isShowing()) {
			mProgressDialog.dismiss();
		}
		if (asyncResponse != null) {
			asyncResponse.processFinish(response);
		}
	}

	@Override
	protected ServiceResponse<BeanMobileUser> doInBackground(Void... voids) {
		Log.d(TAG, "doInBackground");

		ServiceResponse<BeanMobileUser> serviceResponse = null;
		BeanUserData response = null;

		RestTemplate restTemplate = WebServiceUtil.getRestTemplateInstance();

		restTemplate.setErrorHandler(new DefaultResponseErrorHandler() {
			@Override
			public void handleError(ClientHttpResponse response)
					throws IOException {

				ObjectMapper mapper = WebServiceUtil.getErrorObjMapper();
				ErrorServiceResponse errorRes;
				errorRes = mapper.readValue(response.getBody(),
						new TypeReference<ErrorServiceResponse>() {
						});

				throw new RestServiceException(errorRes, errorRes
						.getError_message());
			}

			protected boolean hasError(HttpStatus statusCode) {
				Log.d(TAG, "Status ----> " + statusCode);
				if (statusCode.value() == 200) {
					return false;
				}
				return true;
			}
		});

		String url = String.format(SERVICE_END_POINT + "%s", apiUrl);

		// String urlParams = URLEncodedUtils.format(httpParams, "UTF-8");
		//
		// String finalUrl = String.format(url + "?" + "%s", urlParams);

		Log.d(TAG, "Final URL ------> " + url);

		HttpHeaders requestHeaders = new HttpHeaders();
		requestHeaders.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
		requestHeaders.set("Accept", "application/json");
		requestHeaders.add(ATTR_TAG_UDID, AppUtil.getDeviceId(mContext));

		MultiValueMap<String, String> map = new LinkedMultiValueMap<String, String>();
		for (NameValuePair a : httpParams) {
			map.add(a.getName(), a.getValue());
		}

		HttpEntity<MultiValueMap<String, String>> requestEntity = new HttpEntity<MultiValueMap<String, String>>(
				map, requestHeaders);

		try {
			response = restTemplate.exchange(url, HttpMethod.POST,
					requestEntity, BeanUserData.class).getBody();

			serviceResponse = new ServiceResponse<BeanMobileUser>();
			serviceResponse.setResponse(response.getMobileuser());
			serviceResponse.setError(false);
			serviceResponse.setStatus(200);

			// serviceResponse = restTemplate
			// .exchange(
			// url,
			// HttpMethod.POST,
			// requestEntity,
			// new ParameterizedTypeReference<ServiceResponse<BeanMobileUser>>()
			// {
			// }).getBody();
			Log.d(TAG,
					"serviceResponse.getStatus() ---->> "
							+ serviceResponse.getStatus());

		} catch (RestClientException e) {
			serviceResponse = new ServiceResponse<BeanMobileUser>();
			serviceResponse.setError(true);
			serviceResponse.setError_message(e.getCause().getMessage());

		} catch (Exception e) {
			Log.e(TAG, "Exception " + e.getMessage());
			e.printStackTrace();
		}
		return serviceResponse;
	}

	@Override
	protected void onCancelled() {

		if (mProgressDialog != null && mProgressDialog.isShowing()) {
			mProgressDialog.dismiss();
		}
		if (asyncResponse != null) {
			asyncResponse.processFinish(null);
		}
	}
}
