package com.riverview.harrys.objects;

import android.os.Parcel;
import android.os.Parcelable;

public class BeanCardItem implements Parcelable, Comparable<BeanCardItem> {

	public static final String OBJ_NAME = BeanCardItem.class.getSimpleName();

	private int id;
	private String loyaltyUserName, cardnumber, balance, rebates,
			rebate_validity, card_name;
	private int resId;
	private String card_face_url;
	private int club_card;

	public BeanCardItem(int id) {
		super();
		this.id = id;
	}

	public BeanCardItem(Parcel parcel) {
		super();
		this.id = parcel.readInt();
		this.loyaltyUserName = parcel.readString();
		this.cardnumber = parcel.readString();
		this.balance = parcel.readString();
		this.resId = parcel.readInt();
		this.rebates = parcel.readString();
		this.rebate_validity = parcel.readString();
		this.card_face_url = parcel.readString();
		this.card_name = parcel.readString();
		this.club_card = parcel.readInt();
	}

	public static final Parcelable.Creator<BeanCardItem> CREATOR = new Parcelable.Creator<BeanCardItem>() {

		@Override
		public BeanCardItem createFromParcel(Parcel source) {
			return new BeanCardItem(source);
		}

		@Override
		public BeanCardItem[] newArray(int size) {
			return new BeanCardItem[size];
		}
	};

	@Override
	public int describeContents() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public void writeToParcel(Parcel out, int flag) {
		out.writeInt(id);
		out.writeString(loyaltyUserName);
		out.writeString(cardnumber);
		out.writeString(balance);
		out.writeInt(resId);
		out.writeString(rebates);
		out.writeString(rebate_validity);
		out.writeString(card_face_url);
		out.writeString(card_name);
		out.writeInt(club_card);
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getLoyaltyUserName() {
		return loyaltyUserName;
	}

	public void setLoyaltyUserName(String loyaltyUserName) {
		this.loyaltyUserName = loyaltyUserName;
	}

	public String getCardnumber() {
		return cardnumber;
	}

	public void setCardnumber(String cardnumber) {
		this.cardnumber = cardnumber;
	}

	public String getBalance() {
		return balance;
	}

	public void setBalance(String balance) {
		this.balance = balance;
	}

	public int getResId() {
		return resId;
	}

	public void setResId(int resId) {
		this.resId = resId;
	}

	public String getRebates() {
		return rebates;
	}

	public void setRebates(String rebates) {
		this.rebates = rebates;
	}

	public String getRebate_validity() {
		return rebate_validity;
	}

	public void setRebate_validity(String rebate_validity) {
		this.rebate_validity = rebate_validity;
	}

	public String getCard_face_url() {
		return card_face_url;
	}

	public void setCard_face_url(String card_face_url) {
		this.card_face_url = card_face_url;
	}

	public String getCard_name() {
		return card_name;
	}

	public void setCard_name(String card_name) {
		this.card_name = card_name;
	}

	public int getClub_card() {
		return club_card;
	}

	public void setClub_card(int club_card) {
		this.club_card = club_card;
	}

	@Override
	public int compareTo(BeanCardItem another) {
		int clubcard = ((BeanCardItem) another).club_card;

		return clubcard - this.club_card;
	}

}
