package com.riverview.harrys.fragments;

import static com.riverview.harrys.constant.AppConst.PULL_TO_REFRESH_TIMEOUT;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewStub;
import android.widget.ListView;
import android.widget.TextView;


import com.iapps.libs.helpers.BaseHelper;
import com.iapps.libs.helpers.BaseHelper.CancelListener;
import com.iapps.libs.helpers.BaseHelper.ConfirmListener;
import com.iapps.libs.views.LoadingCompound;
import com.orangegangsters.github.swipyrefreshlayout.library.SwipyRefreshLayout;
import com.orangegangsters.github.swipyrefreshlayout.library.SwipyRefreshLayout.OnRefreshListener;
import com.orangegangsters.github.swipyrefreshlayout.library.SwipyRefreshLayoutDirection;
import com.riverview.harrys.MainActivity;
import com.riverview.harrys.R;
import com.riverview.harrys.RCGenericFragment;
import com.riverview.harrys.adapter.TransactionsAdapter;
import com.riverview.harrys.asynctask.AsyncResponse;
import com.riverview.harrys.asynctask.SalesTransactionAsyncTask;
import com.riverview.harrys.helpers.SharedPref;
import com.riverview.harrys.helpers.Utils;
import com.riverview.harrys.objects.BeanSalesTransaction;
import com.riverview.harrys.objects.Paginator;
import com.riverview.harrys.objects.ServiceResponse;
import com.riverview.harrys.util.AnaylaticsAppender;
import com.riverview.harrys.util.AppUtil;

public class TransactionsFragment extends RCGenericFragment implements
		OnRefreshListener,
		AsyncResponse<ServiceResponse<ArrayList<BeanSalesTransaction>>> {

	private static final String TAG = TransactionsFragment.class
			.getSimpleName();

	public static final int TAG_LIST_STORE = 0;

	private ListView lvStores;
	private LoadingCompound ld;
	private ViewStub emptyView;
	private TextView tv1;
	private TextView tv2;
	private TextView tv4;
	private TextView tv5;
	private SwipyRefreshLayout mSwipyRefreshLayout;

	private View v;
	private TransactionsAdapter mAdapter;
	private ArrayList<BeanSalesTransaction> transList;
	private ArrayList<BeanSalesTransaction> transFList = new ArrayList<BeanSalesTransaction>();

	private int loyaltyUserId;

	private int page = 1;
	private int lastPage = 1;
	private int count = 20;
	private int from = 0;

	private AnaylaticsAppender mAppender = null;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		// SharedPref.deleteString(getActivity(), SharedPref.LISTDATA_KEY);

	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
							 Bundle savedInstanceState) {
		v = inflater.inflate(R.layout.transactions_fragment_layout, container,
				false);
		lvStores = (ListView)v.findViewById(R.id.listViewStore);
		ld = (LoadingCompound)v.findViewById(R.id.ld);
		emptyView = (ViewStub)v.findViewById(R.id.emptyView);
		tv1 = (TextView)v.findViewById(R.id.tv1);
		tv2 = (TextView)v.findViewById(R.id.tv2);
		tv4 = (TextView)v.findViewById(R.id.tv4);
		tv5 = (TextView)v.findViewById(R.id.tv5);
		mSwipyRefreshLayout = (SwipyRefreshLayout)v.findViewById(R.id.swipyrefreshlayout);

		setHasOptionsMenu(false);
		loyaltyUserId = (Integer) getArguments().getInt("loyalty_user_id", -1);

		((MainActivity) getActivity()).setBackActionBar(getActivity()
				.getResources().getString(R.string.rc_transactions),View.INVISIBLE,"");

		return v;
	}

	public void onPrepareOptionsMenu(Menu menu) {
		// getSherlockActivity().getSupportMenuInflater().inflate(
		// R.menu.menu_store, menu);
	}

	@Override
	public void onPause() {

		super.onPause();
		// outletSearchField.setText("");
	}

	@Override
	public void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		try {
			mAppender = AnaylaticsAppender.getInstance(getActivity());
			mAppender.updateAnaylitsData(AppUtil
					.createAnalyticsJsonEventAction("transactions_list",
							String.valueOf(loyaltyUserId),
							"null", null));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);

		mSwipyRefreshLayout.setOnRefreshListener(this);

		lvStores.setEmptyView(emptyView);

		Utils.setTextViewFontSizeBasedOnScreenDensity(getActivity(), tv1, 23f,
				Typeface.BOLD);
		Utils.setTextViewFontSizeBasedOnScreenDensity(getActivity(), tv2, 23f,
				Typeface.BOLD);
		Utils.setTextViewFontSizeBasedOnScreenDensity(getActivity(), tv4, 23f,
				Typeface.BOLD);
		Utils.setTextViewFontSizeBasedOnScreenDensity(getActivity(), tv5, 23f,
				Typeface.BOLD);

		callAPIService(page, count);
	}

	View.OnClickListener clickListeners = new View.OnClickListener() {
		public void onClick(View v) {

		}
	};

	private boolean callAPIService(int page, int itemCount) {
		if (AppUtil.checkNetworkConnection(getActivity())) {

			int mobileUserId = SharedPref.getInteger(getHome(),
					SharedPref.USERID_KEY, 0);

			SalesTransactionAsyncTask mAuthTask = new SalesTransactionAsyncTask(
					getActivity(), mobileUserId, loyaltyUserId, page, itemCount);
			mAuthTask.asyncResponse = TransactionsFragment.this;
			mAuthTask.execute((Void) null);

			ld.showLoading();
			return true;

		} else {
			BaseHelper.confirm(getActivity(), "Network Unavaliable",
					"Please turn on network to connect with server",
					new ConfirmListener() {

						@Override
						public void onYes() {
							AppUtil.showSystemSettingsDialog(getActivity());
						}
					}, new CancelListener() {

						@Override
						public void onNo() {
							getHome().backToHomeScreen();

						}
					});
		}
		return false;
	}

	@Override
	public void processFinish(
			ServiceResponse<ArrayList<BeanSalesTransaction>> details) {
		Log.d(TAG, "Process Finished");

		ld.hide();
		mSwipyRefreshLayout.setRefreshing(false);

		if (details != null && details.getStatus() == 200 && !details.error) {
			transList = new ArrayList<BeanSalesTransaction>();

			try {
				transList = details.getResponse();

				transFList.addAll(transList);

				Collections.sort(transFList,
						new Comparator<BeanSalesTransaction>() {

							@Override
							public int compare(BeanSalesTransaction lhs,
											   BeanSalesTransaction rhs) {
								if (lhs.getCreated_at() == null
										| rhs.getCreated_at() == null) {
									return 0;
								}
								return (AppUtil
										.convertDate(lhs.getCreated_at())
										.before(AppUtil.convertDate(rhs
												.getCreated_at())) ? 1 : -1);
							}
						});

				if (transList != null && transList.size() > 0) {

					mAdapter = new TransactionsAdapter(getActivity(),
							transFList);
					lvStores.setAdapter(mAdapter);
					mAdapter.notifyDataSetChanged();

					if (details.getPaginator() != null) {
						Paginator paginator = details.getPaginator();

						page = paginator.getCurrent_page() + 1;
						lastPage = paginator.getLast_page();
						from = paginator.getFrom();

					} else {
						page++;
					}

					lvStores.setSelection(from);
				}
			} catch (Exception e) {
				e.printStackTrace();
				// BaseHelper.showAlert(getActivity(),
				// "No outlet details avaliable");
				return;
			}

		} else {

			Log.e(TAG, "Download transactions fail.");
		}

	}

	@Override
	public void onRefresh(SwipyRefreshLayoutDirection direction) {
		if (page <= lastPage && callAPIService(page, count)) {
			new Handler().postDelayed(new Runnable() {
				@Override
				public void run() {
					// Hide the refresh after n sec
					try {
						getActivity().runOnUiThread(new Runnable() {
							@Override
							public void run() {
								mSwipyRefreshLayout.setRefreshing(false);
							}
						});
					} catch (Exception e) {
					}
				}
			}, PULL_TO_REFRESH_TIMEOUT);
		} else {
			mSwipyRefreshLayout.setRefreshing(false);
		}
	}
}
