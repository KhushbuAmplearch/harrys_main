package com.riverview.harrys.objects;

import java.util.ArrayList;

public class BeanOutlet {

	// company_id, store_code

	private String id, merchant_id, application_id, company_id, district_id,
			title, address, postcode, telephone, type, opening_hours;
	private String use_merchant_image, longitude, latitude, created_at,
			updated_at, deleted_at, mall_id, manager_id, lang, email;
	private String qr_status, use_company_timezone, timezone, store_code,
			imageurl, bletagcount, nfctagcount;

	// private Boolean use_parent_image;
	private Object pivot;

	private BeanImage image;
	private ArrayList<BeanPhoto> photos;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getMerchant_id() {
		return merchant_id;
	}

	public void setMerchant_id(String merchant_id) {
		this.merchant_id = merchant_id;
	}

	public String getApplication_id() {
		return application_id;
	}

	public void setApplication_id(String application_id) {
		this.application_id = application_id;
	}

	public String getCompany_id() {
		return company_id;
	}

	public void setCompany_id(String company_id) {
		this.company_id = company_id;
	}

	public String getDistrict_id() {
		return district_id;
	}

	public void setDistrict_id(String district_id) {
		this.district_id = district_id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getPostcode() {
		return postcode;
	}

	public void setPostcode(String postcode) {
		this.postcode = postcode;
	}

	public String getTelephone() {
		return telephone;
	}

	public void setTelephone(String telephone) {
		this.telephone = telephone;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getOpening_hours() {
		return opening_hours;
	}

	public void setOpening_hours(String opening_hours) {
		this.opening_hours = opening_hours;
	}

	public String getUse_merchant_image() {
		return use_merchant_image;
	}

	public void setUse_merchant_image(String use_merchant_image) {
		this.use_merchant_image = use_merchant_image;
	}

	public String getLongitude() {
		return longitude;
	}

	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}

	public String getLatitude() {
		return latitude;
	}

	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}

	public String getCreated_at() {
		return created_at;
	}

	public void setCreated_at(String created_at) {
		this.created_at = created_at;
	}

	public String getUpdated_at() {
		return updated_at;
	}

	public void setUpdated_at(String updated_at) {
		this.updated_at = updated_at;
	}

	public String getDeleted_at() {
		return deleted_at;
	}

	public void setDeleted_at(String deleted_at) {
		this.deleted_at = deleted_at;
	}

	public String getMall_id() {
		return mall_id;
	}

	public void setMall_id(String mall_id) {
		this.mall_id = mall_id;
	}

	public String getManager_id() {
		return manager_id;
	}

	public void setManager_id(String manager_id) {
		this.manager_id = manager_id;
	}

	public String getLang() {
		return lang;
	}

	public void setLang(String lang) {
		this.lang = lang;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getQr_status() {
		return qr_status;
	}

	public void setQr_status(String qr_status) {
		this.qr_status = qr_status;
	}

	public String getUse_company_timezone() {
		return use_company_timezone;
	}

	public void setUse_company_timezone(String use_company_timezone) {
		this.use_company_timezone = use_company_timezone;
	}

	public String getTimezone() {
		return timezone;
	}

	public void setTimezone(String timezone) {
		this.timezone = timezone;
	}

	public String getStore_code() {
		return store_code;
	}

	public void setStore_code(String store_code) {
		this.store_code = store_code;
	}

	public String getImageurl() {
		return imageurl;
	}

	public void setImageurl(String imageurl) {
		this.imageurl = imageurl;
	}

	public String getBletagcount() {
		return bletagcount;
	}

	public void setBletagcount(String bletagcount) {
		this.bletagcount = bletagcount;
	}

	public String getNfctagcount() {
		return nfctagcount;
	}

	public void setNfctagcount(String nfctagcount) {
		this.nfctagcount = nfctagcount;
	}

	// public String getPivot() {
	// return pivot;
	// }
	// public void setPivot(String pivot) {
	// this.pivot = pivot;
	// }
	public Object getPivot() {
		return pivot;
	}

	public void setPivot(Object pivot) {
		this.pivot = pivot;
	}

	public BeanImage getImage() {
		return image;
	}

	public void setImage(BeanImage image) {
		this.image = image;
	}

	public ArrayList<BeanPhoto> getPhotos() {
		return photos;
	}

	public void setPhotos(ArrayList<BeanPhoto> photos) {
		this.photos = photos;
	}

}
