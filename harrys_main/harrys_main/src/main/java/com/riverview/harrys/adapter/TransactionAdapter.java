package com.riverview.harrys.adapter;

import java.util.ArrayList;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.riverview.harrys.R;
import com.riverview.harrys.helpers.Utils;
import com.riverview.harrys.objects.BeanMessages;
import com.riverview.harrys.rcinterface.TransactionListener;

public class TransactionAdapter extends BaseAdapter {
	private ArrayList<BeanMessages> mItems = new ArrayList<BeanMessages>();
	private Activity context;
	private TransactionListener listener;

	public TransactionAdapter(Activity context, ArrayList<BeanMessages> list,
			TransactionListener listener) {
		this.context = context;
		this.mItems = list;
		this.listener = listener;
	}

	@Override
	public int getCount() {
		try {
			return mItems.size();
		} catch (Exception e) {
			return 0;
		}
	}

	@Override
	public BeanMessages getItem(int pos) {
		return mItems.get(pos);
	}

	@Override
	public long getItemId(int position) {
		return mItems.get(position).getId();
	}

	public void delete(int pos) {
		mItems.remove(pos);
		notifyDataSetChanged();
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		ViewHolder holder;

		if (convertView == null) {
			holder = new ViewHolder();
			LayoutInflater inflater = (LayoutInflater) context
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

			convertView = inflater.inflate(R.layout.cell_transaction_item,
					parent, false);
			holder.LLRoot = (LinearLayout) convertView
					.findViewById(R.id.LLRoot);
			// holder.LLFront = (LinearLayout)
			// convertView.findViewById(R.id.front);
			holder.title = (TextView) convertView
					.findViewById(R.id.textViewTitle);
			holder.date = (TextView) convertView
					.findViewById(R.id.textViewDate);
			holder.content = (TextView) convertView
					.findViewById(R.id.textViewTransType);
			holder.more = (Button) convertView.findViewById(R.id.btnMore);
			holder.delete = (Button) convertView.findViewById(R.id.btnDelete);
			convertView.setTag(holder);

		} else {
			holder = (ViewHolder) convertView.getTag();
		}

		// set cell height by screen height ratio
		// Utils.setWidthHeightByPercent(context, holder.imgStore, 0.30f,
		// 0.15f);

		// set textview size by screen width ratio
		// Utils.procTextsizeBasedScreen(context, holder.title, 0.85f);
		// Utils.procTextsizeBasedScreen(context, holder.date, 0.85f);
		// Utils.procTextsizeBasedScreen(context, holder.content, 0.85f);

		Utils.setTextViewFontSizeBasedOnScreenDensity(context, holder.title,
				24f);
		Utils.setTextViewFontSizeBasedOnScreenDensity(context, holder.date, 24f);
		Utils.setTextViewFontSizeBasedOnScreenDensity(context, holder.content,
				24f);

		Utils.setTextViewFontSizeBasedOnScreenDensity(context, holder.more, 21f);
		Utils.setTextViewFontSizeBasedOnScreenDensity(context, holder.delete,
				21f);

		Utils.setMarginByPercent(context, holder.LLRoot, 0.02f);
		// Utils.setMarginByPercent(context, holder.distanceStore, 0.02f);

		BeanMessages obj = getItem(position);
		holder.title.setText(obj.getTitle());
		holder.date.setText(obj.getDate());
		holder.content.setText(obj.getContent());

		holder.LLRoot.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View view) {
				listener.clickONList(position);
			}
		});

		holder.more.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View view) {
				listener.openMoreDialog(position);
			}
		});

		holder.delete.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View view) {
				listener.deleteItem(position);
			}
		});

		return convertView;
	}

	public class ViewHolder {
		Button more;
		Button delete;
		TextView title;
		TextView date;
		TextView content;
		LinearLayout LLRoot;
		// LinearLayout LLFront;
	}
}
