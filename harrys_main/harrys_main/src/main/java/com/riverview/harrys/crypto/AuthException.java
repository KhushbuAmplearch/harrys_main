package com.riverview.harrys.crypto;

public class AuthException extends Exception {

	public static int BAD_PARAMETERS = 1;
	public static int GENERAL_ERROR = 2;

	private int errorCode;
	private String reason;

	public AuthException(String detailMessage, int errorCode, String reason) {
		super(detailMessage);
		this.errorCode = errorCode;
		this.reason = reason;
	}

	public AuthException(String detailMessage, Throwable throwable,
			int errorCode, String reason) {
		super(detailMessage, throwable);
		this.errorCode = errorCode;
		this.reason = reason;
	}

	public int getErrorCode() {
		return errorCode;
	}

	public void setErrorCode(int errorCode) {
		this.errorCode = errorCode;
	}

	public String getReason() {
		return reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}
}
