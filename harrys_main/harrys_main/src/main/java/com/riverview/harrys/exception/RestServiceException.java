package com.riverview.harrys.exception;

import java.io.IOException;

import com.riverview.harrys.objects.ErrorServiceResponse;

public class RestServiceException extends IOException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6963099103524567684L;

	private ErrorServiceResponse response;

	public RestServiceException(String msg) {
		super(msg);
	}

	public RestServiceException(ErrorServiceResponse response, String msg) {
		super(msg);
		this.response = response;
	}

	public ErrorServiceResponse getResponse() {
		return response;
	}

	public void setResponse(ErrorServiceResponse response) {
		this.response = response;
	}
}
