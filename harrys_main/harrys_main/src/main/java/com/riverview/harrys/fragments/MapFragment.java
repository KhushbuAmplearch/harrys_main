package com.riverview.harrys.fragments;

import roboguice.inject.InjectView;

import android.Manifest;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.DialogFragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMap.OnMyLocationButtonClickListener;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.iapps.libs.helpers.BaseHelper;
import com.riverview.harrys.MainActivity;
import com.riverview.harrys.R;
import com.riverview.harrys.RCGenericFragment;

public class MapFragment extends RCGenericFragment implements LocationListener,OnMapReadyCallback {

	private static final String TAG = MapFragment.class.getSimpleName();

	private LinearLayout LLMapDirection;

	static LatLng STORE_LOCATION;

	private GoogleMap map;
	private MapView storeMap;

	private View v;
	private String title;

	private final static int CONNECTION_FAILURE_RESOLUTION_REQUEST = 9000;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
							 Bundle savedInstanceState) {
		Log.d(TAG, "onCreateView");
		v = inflater.inflate(R.layout.map_fragment_layout, container, false);
		LLMapDirection = (LinearLayout) v.findViewById(R.id.LLMapDirection);

		setHasOptionsMenu(false);

		title = getArguments().getString("title");
		((MainActivity) getActivity()).setBackActionBar(title,View.INVISIBLE,"");

		storeMap = (MapView) v.findViewById(R.id.mapView);
		//map = storeMap.getMap();

		storeMap.getMapAsync(this);

		storeMap.onCreate(savedInstanceState);
		try {
			MapsInitializer.initialize(getActivity());

			STORE_LOCATION = getArguments().getParcelable("storeLocation");
			if (STORE_LOCATION == null) {
				Toast.makeText(getHome(),
						getString(R.string.rc_store_location_unavaliable),
						Toast.LENGTH_SHORT).show();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		return v;
	}

	private void setMapData() {
		try {
			// map = storeMap.getMap();
			//storeMap.getMapAsync(this);
			MarkerOptions marker = new MarkerOptions().position(STORE_LOCATION)
					.title(title);
			marker.icon(BitmapDescriptorFactory
					.defaultMarker(BitmapDescriptorFactory.HUE_GREEN));

			final CameraPosition cameraPosition = new CameraPosition.Builder()
					.target(STORE_LOCATION).zoom(15) // Sets the zoom
					.bearing(0) // Sets the orientation of the camera to east
					.tilt(30) // Sets the tilt of the camera to 30 degrees
					.build();

			map.addMarker(marker);
			map.moveCamera(CameraUpdateFactory
					.newCameraPosition(cameraPosition));

			// animate camera to my location
			map.setOnMyLocationButtonClickListener(new OnMyLocationButtonClickListener() {
				@Override
				public boolean onMyLocationButtonClick() {

					try {
						Location location = map.getMyLocation();
						CameraPosition position = new CameraPosition.Builder()
								.target(new LatLng(location.getLatitude(),
										location.getLongitude())).zoom(12)
								.build();

						map.animateCamera(CameraUpdateFactory
								.newCameraPosition(position));
					} catch (Exception e) {
						e.printStackTrace();
					}
					return true;
				}
			});

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);

		LLMapDirection.setOnClickListener(clickListeners);

	}

	private boolean servicesConnected() {

		int resultCode = GooglePlayServicesUtil
				.isGooglePlayServicesAvailable(getActivity());

		if (ConnectionResult.SUCCESS == resultCode) {


			if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
				// TODO: Consider calling
				//    ActivityCompat#requestPermissions
				// here to request the missing permissions, and then overriding
				//   public void onRequestPermissionsResult(int requestCode, String[] permissions,
				//                                          int[] grantResults)
				// to handle the case where the user grants the permission. See the documentation
				// for ActivityCompat#requestPermissions for more details.
				return true;
			}
			map.setMyLocationEnabled(true);

			LocationManager locationManager = (LocationManager) getHome()
					.getSystemService(Context.LOCATION_SERVICE);
			locationManager.getProviders(true);

			// Creating a criteria object to retrieve provider
			Criteria criteria = new Criteria();

			// Getting the name of the best provider
			String provider = locationManager.getBestProvider(criteria, true);

			if (provider == null) {
				BaseHelper.showAlert(getHome(),
						"Unable to get service provider");
				return false;
			}
			// Getting Current Location
			if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
				// TODO: Consider calling
				//    ActivityCompat#requestPermissions
				// here to request the missing permissions, and then overriding
				//   public void onRequestPermissionsResult(int requestCode, String[] permissions,
				//                                          int[] grantResults)
				// to handle the case where the user grants the permission. See the documentation
				// for ActivityCompat#requestPermissions for more details.
				return true;
			}
			Location location = locationManager.getLastKnownLocation(provider);

			if (location != null) {
				Log.d(TAG, "location have");
				onLocationChanged(location);
			}
			locationManager.requestLocationUpdates(provider, 0, 0, this);

			return true;
		} else {
			// Get the error dialog from Google Play services
			Dialog errorDialog = GooglePlayServicesUtil.getErrorDialog(
					resultCode, getActivity(),
					CONNECTION_FAILURE_RESOLUTION_REQUEST);

			// If Google Play services can provide an error dialog
			if (errorDialog != null) {
				// Create a new DialogFragment for the error dialog
				ErrorDialogFragment errorFragment = new ErrorDialogFragment();
				// Set the dialog in the DialogFragment
				errorFragment.setDialog(errorDialog);
				// Show the error dialog in the DialogFragment
				errorFragment.show(getChildFragmentManager(),
						"Location Updates");
			}
		}
		return false;
	}

	@Override
	public void onLocationChanged(Location location) {
		// Getting latitude of the current location
		double latitude = location.getLatitude();

		// Getting longitude of the current location
		double longitude = location.getLongitude();

		// Creating a LatLng object for the current location
		LatLng latLng = new LatLng(latitude, longitude);

		// Showing the current location in Google Map
		// map.moveCamera(CameraUpdateFactory.newLatLng(latLng));

		// Zoom in the Google Map
		// map.animateCamera(CameraUpdateFactory.zoomTo(15));
		map.addMarker(new MarkerOptions().position(latLng).title(
				"You are here!"));

	}

	@Override
	public void onProviderDisabled(String provider) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onProviderEnabled(String provider) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onStatusChanged(String provider, int status, Bundle extras) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {

		switch (requestCode) {

			case CONNECTION_FAILURE_RESOLUTION_REQUEST:
				/*
				 * If the result code is Activity.RESULT_OK, try to connect again
				 */
				switch (resultCode) {
					case Activity.RESULT_OK:
						setMapData();

						break;
				}

		}
	}

	@Override
	public void onMapReady(GoogleMap googleMap) {
		map=googleMap;
		try {
			// map = storeMap.getMap();
			//storeMap.getMapAsync(this);

			if (servicesConnected()) {
				setMapData();
			}

			MarkerOptions marker = new MarkerOptions().position(STORE_LOCATION)
					.title(title);
			marker.icon(BitmapDescriptorFactory
					.defaultMarker(BitmapDescriptorFactory.HUE_GREEN));

			final CameraPosition cameraPosition = new CameraPosition.Builder()
					.target(STORE_LOCATION).zoom(15) // Sets the zoom
					.bearing(0) // Sets the orientation of the camera to east
					.tilt(30) // Sets the tilt of the camera to 30 degrees
					.build();

			map.addMarker(marker);
			map.moveCamera(CameraUpdateFactory
					.newCameraPosition(cameraPosition));

			// animate camera to my location
			map.setOnMyLocationButtonClickListener(new OnMyLocationButtonClickListener() {
				@Override
				public boolean onMyLocationButtonClick() {

					try {
						Location location = map.getMyLocation();
						CameraPosition position = new CameraPosition.Builder()
								.target(new LatLng(location.getLatitude(),
										location.getLongitude())).zoom(12)
								.build();

						map.animateCamera(CameraUpdateFactory
								.newCameraPosition(position));
					} catch (Exception e) {
						e.printStackTrace();
					}
					return true;
				}
			});

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static class ErrorDialogFragment extends DialogFragment {
		// Global field to contain the error dialog
		private Dialog mDialog;

		// Default constructor. Sets the dialog field to null
		public ErrorDialogFragment() {
			super();
			mDialog = null;
		}

		// Set the dialog to display
		public void setDialog(Dialog dialog) {
			mDialog = dialog;
		}

		// Return a Dialog to the DialogFragment.
		@Override
		public Dialog onCreateDialog(Bundle savedInstanceState) {
			return mDialog;
		}
	}

	View.OnClickListener clickListeners = new View.OnClickListener() {
		public void onClick(View v) {

			if (v.getId() == R.id.LLMapDirection) {

				try {
					Location location = map.getMyLocation();
					if (location != null && STORE_LOCATION != null) {

						// String url = Geo4.this.getDirectionsUrl(new
						// LatLng(location.getLatitude(),
						// location.getLongitude()), RIVERVIEW);
						String url = makeURL(location.getLatitude(),
								location.getLongitude(),
								STORE_LOCATION.latitude,
								STORE_LOCATION.longitude);

						Intent intent = new Intent(
								android.content.Intent.ACTION_VIEW,
								Uri.parse(url));
						startActivity(intent);
					} else {
						Toast.makeText(getHome(),
								"Your store location not avaliable",
								Toast.LENGTH_SHORT).show();
					}
				} catch (Exception e) {

				}

			}
		}
	};

	private String makeURL(double sourcelat, double sourcelog, double destlat,
						   double destlog) {
		StringBuilder urlString = new StringBuilder();
		urlString.append("http://maps.google.com/maps");
		urlString.append("?saddr=");// from
		urlString.append(Double.toString(sourcelat));
		urlString.append(",");
		urlString.append(Double.toString(sourcelog));
		urlString.append("&daddr=");// to
		urlString.append(Double.toString(destlat));
		urlString.append(",");
		urlString.append(Double.toString(destlog));

		return urlString.toString();
	}

	@Override
	public void onResume() {
		super.onResume();
		storeMap.onResume();
	}

	@Override
	public void onPause() {
		super.onPause();
		storeMap.onPause();
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
		storeMap.onDestroy();
	}

	@Override
	public void onLowMemory() {
		super.onLowMemory();
		storeMap.onLowMemory();
	}

}
