package com.riverview.harrys.objects;

import java.io.Serializable;

import android.os.Parcel;
import android.os.Parcelable;

public class BeanProductItem implements Parcelable, Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4123206972074413167L;

	public static final String OBJ_NAME = "BeanProductItem";

	private int id;
	private String name, description, base_price, detail_description, category,
			category_discription, brand, brand_discription, url, orgUrl,
			subcategory_id;

	public BeanProductItem(int id, String name) {
		super();
		this.id = id;
		this.name = name;

	}

	public BeanProductItem(Parcel parcel) {
		super();
		this.id = parcel.readInt();
		this.name = parcel.readString();
		this.description = parcel.readString();
		this.base_price = parcel.readString();
		this.detail_description = parcel.readString();
		this.category = parcel.readString();
		this.category_discription = parcel.readString();
		this.brand = parcel.readString();
		this.brand_discription = parcel.readString();
		this.url = parcel.readString();
		this.orgUrl = parcel.readString();
		this.subcategory_id = parcel.readString();
	}

	public static final Parcelable.Creator<BeanProductItem> CREATOR = new Parcelable.Creator<BeanProductItem>() {

		@Override
		public BeanProductItem createFromParcel(Parcel source) {
			return new BeanProductItem(source);
		}

		@Override
		public BeanProductItem[] newArray(int size) {
			return new BeanProductItem[size];
		}
	};

	@Override
	public int describeContents() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public void writeToParcel(Parcel out, int flag) {
		out.writeInt(id);
		out.writeString(name);
		out.writeString(description);
		out.writeString(base_price);
		out.writeString(detail_description);
		out.writeString(category);
		out.writeString(category_discription);
		out.writeString(brand);
		out.writeString(brand_discription);
		out.writeString(url);
		out.writeString(orgUrl);
		out.writeString(subcategory_id);

	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getBase_price() {
		return base_price;
	}

	public void setBase_price(String base_price) {
		this.base_price = base_price;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getDetail_description() {
		return detail_description;
	}

	public void setDetail_description(String detail_description) {
		this.detail_description = detail_description;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public String getCategory_discription() {
		return category_discription;
	}

	public void setCategory_discription(String category_discription) {
		this.category_discription = category_discription;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getBrand() {
		return brand;
	}

	public void setBrand(String brand) {
		this.brand = brand;
	}

	public String getBrand_discription() {
		return brand_discription;
	}

	public void setBrand_discription(String brand_discription) {
		this.brand_discription = brand_discription;
	}

	public String getOrgUrl() {
		return orgUrl;
	}

	public void setOrgUrl(String orgUrl) {
		this.orgUrl = orgUrl;
	}

	public String getSubcategory_id() {
		return subcategory_id;
	}

	public void setSubcategory_id(String subcategory_id) {
		this.subcategory_id = subcategory_id;
	}
}
