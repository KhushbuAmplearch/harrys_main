package com.iapps.libs.objects;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;

import com.iapps.libs.helpers.BaseKeys;

public class Response {
	private int statusCode = 400;
	private JSONObject content;

	public static final Response getDummyResponse() {
		return new Response(200, "");
	}

	public Response(int statusCode, String content) {
		this.statusCode = statusCode;
		try {
			this.content = new JSONObject(new JSONTokener(content));
		} catch (JSONException e) {

			e.printStackTrace();

			try {
				JSONArray jAr = new JSONArray(new JSONTokener(content));
				this.content = new JSONObject();
				this.content.put(BaseKeys.RESULTS, jAr);
			} catch (JSONException ex) {
				this.content = new JSONObject();
				ex.printStackTrace();
			}

		}

		// if(!this.content.isNull(BaseKeys.STATUS_CODE)){
		// this.statusCode = this.content.optInt(BaseKeys.STATUS_CODE,
		// statusCode);
		// }

	}

	public int getStatusCode() {
		return statusCode;
	}

	public JSONObject getContent() {
		return content;
	}

}
