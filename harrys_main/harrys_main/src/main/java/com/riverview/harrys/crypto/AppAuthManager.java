package com.riverview.harrys.crypto;

public interface AppAuthManager {

	public String getHMACHash(String data) throws AuthException;
}
