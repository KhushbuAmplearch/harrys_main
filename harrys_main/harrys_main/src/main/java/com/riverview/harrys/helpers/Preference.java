package com.riverview.harrys.helpers;

import android.content.Context;

public class Preference {
	protected final String TESTING_TOKEN = "98eaa511378c109d4d5bdc67873a0aa5";
	Context context;

	public Preference(Context context) {
		this.context = context;
	}

	public String getAuthToken() {
		return TESTING_TOKEN;
	}
}
