ECLIPSE ANDROID PROJECT IMPORT SUMMARY
======================================

Manifest Merging:
-----------------
Your project uses libraries that provide manifests, and your Eclipse
project did not explicitly turn on manifest merging. In Android Gradle
projects, manifests are always merged (meaning that contents from your
libraries' manifests will be merged into the app manifest. If you had
manually copied contents from library manifests into your app manifest
you may need to remove these for the app to build correctly.

Ignored Files:
--------------
The following files were *not* copied into the new Gradle project; you
should evaluate whether these are still needed in your project and if
so manually move them:

From harrys_autoresizetetxview_lib:
* .gitignore
* README.md
* ic_launcher-web.png
* proguard-project.txt
From harrys_common_lib:
* .gitignore
* proguard-project.txt
From harrys_datetimepicker_lib:
* .gitignore
* build.gradle
* gradle.properties
* proguard-project.txt
From harrys_imageslider_lib:
* .gitignore
* proguard-project.txt
From harrys_main:
* .gitignore
* ic_launcher-web.png
* proguard-project.txt
From harrys_nineold_lib:
* .gitignore
* AndroidManifest.xml
* pom.xml
From harrys_pulltorefresh_abs_lib:
* .gitignore
* build.gradle
* gradle.properties
From harrys_pulltorefresh_lib:
* .gitignore
* build.gradle
* gradle.properties
From harrys_roundedimageview_lib:
* .gitignore
* build.gradle
* pom.xml
* proguard-project.txt
From harrys_sherlocknavigationdrawer_lib:
* .gitignore
* build.gradle
* gradle\
* gradle\wrapper\
* gradle\wrapper\gradle-wrapper.jar
* gradle\wrapper\gradle-wrapper.properties
* gradlew
* gradlew.bat
* proguard-project.txt
From harrys_slide_datetimepicker:
* .gitignore
From harrys_slidingmenu_lib:
* .gitignore
* LICENSE.txt
* build.gradle
* library.iml
* pom.xml
From harrys_smoothprogressbar_circular_lib:
* .gitignore
From harrys_styled_lib:
* .gitignore
* ic_launcher-web.png
* proguard-project.txt
From harrys_swipeto_dismiss_undolist_lib:
* .gitignore
* README.md
* ant.properties
* build.properties
* build.xml
* default.properties
* proguard-project.txt
From harrys_swipy_refresh_lib:
* .gitignore
From harrys_switch_backport_lib:
* .gitignore
* build.gradle
* build.xml
* library.iml
* pom.xml
* proguard-project.txt
* proguard-rules.txt
From harrys_viewbadger_lib:
* .gitignore
* LICENSE
* README.markdown
* lib\
* lib\android-viewbadger.jar
* proguard.cfg
From harrys_viewpager_lib:
* .gitignore
* pom.xml

Replaced Jars with Dependencies:
--------------------------------
The importer recognized the following .jar files as third party
libraries and replaced them with Gradle dependencies instead. This has
the advantage that more explicit version information is known, and the
libraries can be updated automatically. However, it is possible that
the .jar file in your project was of an older version than the
dependency we picked, which could render the project not compileable.
You can disable the jar replacement in the import wizard and try again:

android-support-v4.jar => com.android.support:support-v4:20.0.0
joda-time-2.2.jar => joda-time:joda-time:2.2

Potentially Missing Dependency:
-------------------------------
When we replaced the following .jar files with a Gradle dependency, we
inferred the dependency version number from the filename. This
specific version may not actually be available from the repository.
If you get a build error stating that the dependency is missing, edit
the version number to for example "+" to pick up the latest version
instead. (This may require you to update your code if the library APIs
have changed.)

joda-time-2.2.jar => version 2.2 in joda-time:joda-time:2.2

Replaced Libraries with Dependencies:
-------------------------------------
The importer recognized the following library projects as third party
libraries and replaced them with Gradle dependencies instead. This has
the advantage that more explicit version information is known, and the
libraries can be updated automatically. However, it is possible that
the source files in your project were of an older version than the
dependency we picked, which could render the project not compileable.
You can disable the library replacement in the import wizard and try
again:

harrys_actionbarsherlock_lib =>
    com.actionbarsherlock:actionbarsherlock:4.4.0@aar
    com.android.support:support-v4:20.0.0
harrys_googleplay_services_lib => [com.google.android.gms:play-services:+]

Moved Files:
------------
Android Gradle projects use a different directory structure than ADT
Eclipse projects. Here's how the projects were restructured:

In harrys_autoresizetetxview_lib:
* AndroidManifest.xml => harrys_autoresizetetxview_lib\src\main\AndroidManifest.xml
* res\ => harrys_autoresizetetxview_lib\src\main\res\
* src\ => harrys_autoresizetetxview_lib\src\main\java\
In harrys_nineold_lib:
* src\ => harrys_nineold_lib\src\main\java\
In harrys_common_lib:
* AndroidManifest.xml => harrys_common_lib\src\main\AndroidManifest.xml
* libs\android-support-multidex.jar => harrys_common_lib\libs\android-support-multidex.jar
* libs\barcodelib.jar => harrys_common_lib\libs\barcodelib.jar
* libs\calligraphy-1.2.0.jar => harrys_common_lib\libs\calligraphy-1.2.0.jar
* libs\commons-io-2.4.jar => harrys_common_lib\libs\commons-io-2.4.jar
* libs\commons-validator-1.4.0.jar => harrys_common_lib\libs\commons-validator-1.4.0.jar
* libs\core.jar => harrys_common_lib\libs\core.jar
* libs\gcm-src.jar => harrys_common_lib\libs\gcm-src.jar
* libs\guice-3.0-no_aop.jar => harrys_common_lib\libs\guice-3.0-no_aop.jar
* libs\javax.inject-1.jar => harrys_common_lib\libs\javax.inject-1.jar
* libs\picasso-2.3.3.jar => harrys_common_lib\libs\picasso-2.3.3.jar
* libs\roboguice-2.0.jar => harrys_common_lib\libs\roboguice-2.0.jar
* libs\roboguice-sherlock-1.4.jar => harrys_common_lib\libs\roboguice-sherlock-1.4.jar
* lint.xml => harrys_common_lib\lint.xml
* res\ => harrys_common_lib\src\main\res\
* src\ => harrys_common_lib\src\main\java\
In harrys_datetimepicker_lib:
* AndroidManifest.xml => harrys_datetimepicker_lib\src\main\AndroidManifest.xml
* lint.xml => harrys_datetimepicker_lib\lint.xml
* res\ => harrys_datetimepicker_lib\src\main\res\
* src\ => harrys_datetimepicker_lib\src\main\java\
In harrys_imageslider_lib:
* AndroidManifest.xml => harrys_imageslider_lib\src\main\AndroidManifest.xml
* res\ => harrys_imageslider_lib\src\main\res\
* src\ => harrys_imageslider_lib\src\main\java\
In harrys_pulltorefresh_lib:
* AndroidManifest.xml => harrys_pulltorefresh_lib\src\main\AndroidManifest.xml
* res\ => harrys_pulltorefresh_lib\src\main\res\
* src\ => harrys_pulltorefresh_lib\src\main\java\
In harrys_pulltorefresh_abs_lib:
* AndroidManifest.xml => harrys_pulltorefresh_abs_lib\src\main\AndroidManifest.xml
* res\ => harrys_pulltorefresh_abs_lib\src\main\res\
* src\ => harrys_pulltorefresh_abs_lib\src\main\java\
In harrys_roundedimageview_lib:
* AndroidManifest.xml => harrys_roundedimageview_lib\src\main\AndroidManifest.xml
* res\ => harrys_roundedimageview_lib\src\main\res\
* src\ => harrys_roundedimageview_lib\src\main\java\
In harrys_sherlocknavigationdrawer_lib:
* AndroidManifest.xml => harrys_sherlocknavigationdrawer_lib\src\main\AndroidManifest.xml
* res\ => harrys_sherlocknavigationdrawer_lib\src\main\res\
* src\ => harrys_sherlocknavigationdrawer_lib\src\main\java\
In harrys_slide_datetimepicker:
* AndroidManifest.xml => harrys_slide_datetimepicker\src\main\AndroidManifest.xml
* lint.xml => harrys_slide_datetimepicker\lint.xml
* res\ => harrys_slide_datetimepicker\src\main\res\
* src\ => harrys_slide_datetimepicker\src\main\java\
In harrys_smoothprogressbar_circular_lib:
* AndroidManifest.xml => harrys_smoothprogressbar_circular_lib\src\main\AndroidManifest.xml
* res\ => harrys_smoothprogressbar_circular_lib\src\main\res\
* src\ => harrys_smoothprogressbar_circular_lib\src\main\java\
In harrys_styled_lib:
* AndroidManifest.xml => harrys_styled_lib\src\main\AndroidManifest.xml
* res\ => harrys_styled_lib\src\main\res\
* src\ => harrys_styled_lib\src\main\java\
In harrys_swipeto_dismiss_undolist_lib:
* AndroidManifest.xml => harrys_swipeto_dismiss_undolist_lib\src\main\AndroidManifest.xml
* res\ => harrys_swipeto_dismiss_undolist_lib\src\main\res\
* src\ => harrys_swipeto_dismiss_undolist_lib\src\main\java\
In harrys_slidingmenu_lib:
* AndroidManifest.xml => harrys_slidingmenu_lib\src\main\AndroidManifest.xml
* res\ => harrys_slidingmenu_lib\src\main\res\
* src\ => harrys_slidingmenu_lib\src\main\java\
In harrys_swipy_refresh_lib:
* AndroidManifest.xml => harrys_swipy_refresh_lib\src\main\AndroidManifest.xml
* res\ => harrys_swipy_refresh_lib\src\main\res\
* src\ => harrys_swipy_refresh_lib\src\main\java\
In harrys_switch_backport_lib:
* AndroidManifest.xml => harrys_switch_backport_lib\src\main\AndroidManifest.xml
* res\ => harrys_switch_backport_lib\src\main\res\
* src\ => harrys_switch_backport_lib\src\main\java\
In harrys_viewbadger_lib:
* AndroidManifest.xml => harrys_viewbadger_lib\src\main\AndroidManifest.xml
* res\ => harrys_viewbadger_lib\src\main\res\
* src\ => harrys_viewbadger_lib\src\main\java\
In harrys_viewpager_lib:
* AndroidManifest.xml => harrys_viewpager_lib\src\main\AndroidManifest.xml
* res\ => harrys_viewpager_lib\src\main\res\
* src\ => harrys_viewpager_lib\src\main\java\
In main:
* AndroidManifest.xml => main\src\main\AndroidManifest.xml
* res\ => main\src\main\res\
* src\ => main\src\main\java\
In main:
* AndroidManifest.xml => main\src\main\AndroidManifest.xml
* res\ => main\src\main\res\
* src\ => main\src\main\java\
In harrys_main:
* AndroidManifest.xml => harrys_main\src\main\AndroidManifest.xml
* assets\ => harrys_main\src\main\assets\
* libs\commons-codec-1.10.jar => harrys_main\libs\commons-codec-1.10.jar
* libs\commons-collections4-4.0.jar => harrys_main\libs\commons-collections4-4.0.jar
* libs\commons-lang3-3.4.jar => harrys_main\libs\commons-lang3-3.4.jar
* libs\jackson-core-asl-1.9.13.jar => harrys_main\libs\jackson-core-asl-1.9.13.jar
* libs\jackson-mapper-asl-1.9.12.jar => harrys_main\libs\jackson-mapper-asl-1.9.12.jar
* libs\spring-android-core-2.0.0.M1.jar => harrys_main\libs\spring-android-core-2.0.0.M1.jar
* libs\spring-android-rest-template-2.0.0.M1.jar => harrys_main\libs\spring-android-rest-template-2.0.0.M1.jar
* lint.xml => harrys_main\lint.xml
* res\ => harrys_main\src\main\res\
* src\ => harrys_main\src\main\java\

Next Steps:
-----------
You can now build the project. The Gradle project needs network
connectivity to download dependencies.

Bugs:
-----
If for some reason your project does not build, and you determine that
it is due to a bug or limitation of the Eclipse to Gradle importer,
please file a bug at http://b.android.com with category
Component-Tools.

(This import summary is for your information only, and can be deleted
after import once you are satisfied with the results.)
