package com.riverview.harrys.adapter;

import java.util.ArrayList;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.riverview.harrys.R;
import com.riverview.harrys.helpers.FontUtil;
import com.riverview.harrys.helpers.Utils;
import com.riverview.harrys.objects.BeanSlideMenu;

public class UserGuideAdapter extends BaseAdapter {

	private ArrayList<BeanSlideMenu> mMenu = new ArrayList<BeanSlideMenu>();
	private Activity context;

	public UserGuideAdapter(Activity context, ArrayList<BeanSlideMenu> list) {
		this.context = context;
		this.mMenu = list;
	}

	@Override
	public int getCount() {
		try {
			return mMenu.size();
		} catch (Exception e) {
			return 0;
		}
	}

	@Override
	public BeanSlideMenu getItem(int pos) {
		return mMenu.get(pos);
	}

	@Override
	public long getItemId(int position) {
		return mMenu.get(position).getId();
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ViewHolder holder;

		if (convertView == null) {
			holder = new ViewHolder();
			LayoutInflater inflater = (LayoutInflater) context
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

			convertView = inflater.inflate(R.layout.cell_userguide, parent,
					false);
			holder.LLRoot = (LinearLayout) convertView
					.findViewById(R.id.LLRoot);
			holder.titleMenu = (TextView) convertView
					.findViewById(R.id.textViewMenuTitle);
			// /holder.imgMenuIcon = (ImageView)
			// convertView.findViewById(R.id.imgMenuIcon);
			convertView.setTag(holder);

		} else {
			holder = (ViewHolder) convertView.getTag();
		}

		// set cell height by screen height ratio
		Utils.setHeightByPercent(context, holder.LLRoot, 0.1f);

		// set textview size by screen width ratio
		// Utils.procTextsizeBasedScreen(context, holder.titleMenu, 0.8f);
		// Utils.setTextViewFontSizeBasedOnScreenDensity(context,
		// holder.titleMenu, Utils.FONT_SMALL_DENSITY_SIZE);
		Utils.setTextViewFontSizeBasedOnScreenDensity(context,
				holder.titleMenu, 23f);

		BeanSlideMenu obj = getItem(position);
		holder.titleMenu.setText(obj.getName());
		new FontUtil(context).setFontRegularType(holder.titleMenu);
		if (obj.isEnable())
			holder.titleMenu.setTextColor(Color.BLACK);
		else
			holder.titleMenu.setTextColor(Color.GRAY);
		// holder.imgMenuIcon.setImageResource(obj.getDrawableId());
		holder.LLRoot.setTag(position);
		// holder.LLRoot.setOnClickListener(ListenerClick);

		return convertView;
	}

	public class ViewHolder {

		LinearLayout LLRoot;
		TextView titleMenu;
		// ImageView imgMenuIcon;
	}
}
