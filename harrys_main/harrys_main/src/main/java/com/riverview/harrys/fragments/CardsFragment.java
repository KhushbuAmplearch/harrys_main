package com.riverview.harrys.fragments;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;


import android.app.Dialog;
import android.content.DialogInterface;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AccelerateInterpolator;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.iapps.libs.helpers.BaseHelper;
import com.iapps.libs.helpers.BaseHelper.CancelListener;
import com.iapps.libs.helpers.BaseHelper.ConfirmListener;
import com.iapps.libs.helpers.BaseUIHelper;
import com.iapps.libs.views.LoadingCompound;
import com.nineoldandroids.animation.ValueAnimator;
import com.riverview.harrys.MainActivity;
import com.riverview.harrys.R;
import com.riverview.harrys.RCGenericFragment;
import com.riverview.harrys.adapter.TabAdapter;
import com.riverview.harrys.asynctask.AsyncResponse;
import com.riverview.harrys.asynctask.LoyaltyUserAsyncTask;
import com.riverview.harrys.helpers.HeightEvaluator;
import com.riverview.harrys.helpers.SharedPref;
import com.riverview.harrys.helpers.Utils;
import com.riverview.harrys.objects.BeanCardItem;
import com.riverview.harrys.objects.BeanLoyaltyUser;
import com.riverview.harrys.objects.BeanMobileUser;
import com.riverview.harrys.objects.ServiceResponse;
import com.riverview.harrys.util.AppUtil;
import com.viewpagerindicator.CirclePageIndicator;

public class CardsFragment extends RCGenericFragment implements
		OnPageChangeListener, Parcelable,
		AsyncResponse<ServiceResponse<BeanMobileUser>> {

	private static final String TAG = CardsFragment.class.getSimpleName();

	private LinearLayout LLOptionsView;
	private LinearLayout LLMainContainer;
	private LoadingCompound ld;
	private ViewPager vp;
	private CirclePageIndicator indicator;
	private TextView textMenuTransactions;
	private TextView textMenuAddCard;
	private TextView textMenuCancel;
	private LinearLayout LLMenuTransactions;
	private LinearLayout LLMenuCancel;
	private LinearLayout LLMenuAddCard;
	private LinearLayout LLCardOptionSpace;
	private Button buttonOptions;
	private FrameLayout slideDownView;

	private static float height = 0f;

	private View v;
	private ArrayList<BeanCardItem> cardData;
	private ArrayList<Fragment> alFrag;
	private TabAdapter adapter;

	private static boolean toogleStatus = false;
	private static final int VIEW_ANIMATION_INTERVAL = 300;

	public static int position;

	private String customerName = null;

	private int mobileUserIdForTransactions = -1;

	private LoyaltyUserAsyncTask mLoyaltyUserTask = null;

	public static void initial(int pos) {
		position = pos;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setHasOptionsMenu(false);

		if (savedInstanceState != null) {
			int selectedTabIndex = savedInstanceState
					.getInt("selectedTabIndex");
			getHome().getActionBar().setSelectedNavigationItem(
					selectedTabIndex);
		}
	}

	@Override
	public void onSaveInstanceState(Bundle outState) {

		super.onSaveInstanceState(outState);
		outState.putInt("selectedTabIndex", getHome().getActionBar()
				.getSelectedNavigationIndex());
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
							 Bundle savedInstanceState) {
		v = inflater.inflate(R.layout.cards_fragment_layout, container, false);

		LLOptionsView = (LinearLayout)v.findViewById(R.id.LLOptionsView);
		LLMainContainer = (LinearLayout)v.findViewById(R.id.LLMainContainer);
		ld = (LoadingCompound)v.findViewById(R.id.ld);
		vp = (ViewPager)v.findViewById(R.id.vpActive);
		indicator = (CirclePageIndicator)v.findViewById(R.id.indicatorActive);
		textMenuTransactions = (TextView)v.findViewById(R.id.textMenuTransactions);
		textMenuAddCard = (TextView)v.findViewById(R.id.textMenuAddCard);
		textMenuCancel = (TextView)v.findViewById(R.id.textMenuCancel);
		LLMenuTransactions = (LinearLayout)v.findViewById(R.id.LLMenuTransactions);
		LLMenuCancel = (LinearLayout)v.findViewById(R.id.LLMenuCancel);
		LLMenuAddCard = (LinearLayout)v.findViewById(R.id.LLMenuAddCard);
		LLCardOptionSpace = (LinearLayout)v.findViewById(R.id.cardOptionSpace);
		buttonOptions = (Button)v.findViewById(R.id.buttonOptions);
		slideDownView = (FrameLayout)v.findViewById(R.id.card_options_slideup_layout);

		getActivity().getActionBar().show();
		((MainActivity) getActivity()).setBackActionBar(getActivity()
				.getResources().getString(R.string.rc_harrys_cards),View.INVISIBLE,"");

		toogleStatus = false;

		cardData = null;
		alFrag = null;

		return v;
	}


	public void onPrepareOptionsMenu(Menu menu) {
		// getActivity().getSupportMenuInflater().inflate(R.menu.menu_orders,
		// menu);
	}

	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);

		Utils.setTextViewFontSizeBasedOnScreenDensity(getActivity(),
				textMenuTransactions, 18f);
		Utils.setTextViewFontSizeBasedOnScreenDensity(getActivity(),
				textMenuAddCard, 18f);
		Utils.setTextViewFontSizeBasedOnScreenDensity(getActivity(),
				textMenuCancel, 14f, Typeface.BOLD);
		Utils.setButtonFontSizeBasedOnScreenDensity(getActivity(),
				buttonOptions, Utils.FONT_SMALL_DENSITY_SIZE, Typeface.BOLD);

		StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.append(AppUtil.convertFirstLetterUpper(SharedPref
				.getString(getHome(), SharedPref.USERFNAME_KEY, "")));
		stringBuilder.append(" ");
		stringBuilder.append(AppUtil.convertFirstLetterUpper(SharedPref
				.getString(getHome(), SharedPref.USERLNAME_KEY, "")));
		customerName = stringBuilder.toString();

		buttonOptions.setOnClickListener(clickListeners);

		LLMenuCancel.setOnClickListener(clickListeners);
		LLMenuAddCard.setOnClickListener(clickListeners);
		LLMenuTransactions.setOnClickListener(clickListeners);
		LLCardOptionSpace.setOnClickListener(clickListeners);

		ld.showLoading();

		callAPIService();
	}

	@Override
	public void onPause() {
		super.onPause();

		if (mLoyaltyUserTask != null) {
			mLoyaltyUserTask.cancel(true);
		}
	}

	private void callAPIService() {

		if (AppUtil.checkNetworkConnection(getActivity())) {

			int mobileUserId = SharedPref.getInteger(getHome(),
					SharedPref.USERID_KEY, 0);

			mLoyaltyUserTask = new LoyaltyUserAsyncTask(getActivity(),
					mobileUserId);
			mLoyaltyUserTask.asyncResponse = CardsFragment.this;
			mLoyaltyUserTask.execute((Void) null);

			ld.showLoading();
			// mPullToRefresh.setRefreshing(true);

		} else {

			BaseHelper.confirm(getActivity(), "Network Unavaliable",
					"Please turn on network to connect with server",
					new ConfirmListener() {

						@Override
						public void onYes() {
							AppUtil.showSystemSettingsDialog(getActivity());
						}
					}, new CancelListener() {

						@Override
						public void onNo() {
							getHome().backToHomeScreen();

						}
					});
		}

	}

	View.OnTouchListener touchListener = new View.OnTouchListener() {

		@Override
		public boolean onTouch(View v, MotionEvent event) {
			switch (event.getAction()) {
				case MotionEvent.ACTION_DOWN:
					// v.performClick();
					closeToggle();
					break;

				default:
					break;
			}
			return false;
		}
	};

	View.OnClickListener clickListeners = new View.OnClickListener() {
		public void onClick(View v) {

			switch (v.getId()) {

				case R.id.buttonOptions:
					Log.d(TAG, "Option button clicked");
					toggle(slideDownView);
					break;

				case R.id.LLMenuAddCard:
					Log.e(TAG, "Menu Add card");

					alFrag = null;
					if(cardData.size()>2) {
						showAlertDialog();
					}else
					{
						AddClubCardFragment frag = new AddClubCardFragment();
						getHome().setFragment(frag);

					}
					break;

				case R.id.LLMenuTransactions:
					TransactionsFragment transFrag = new TransactionsFragment();

					try {

						int currentItem = vp.getCurrentItem();

						Bundle itemBundle = adapter.getItem(currentItem)
								.getArguments();
						if (itemBundle != null) {
							BeanCardItem cardItem = itemBundle
									.getParcelable("card_data");

							Bundle bundle = new Bundle();
							bundle.putInt("loyalty_user_id", cardItem.getId());

							transFrag.setArguments(bundle);
							getHome().setFragment(transFrag);
						} else {
							Toast.makeText(getHome(),
									"Loyalty user data not found",
									Toast.LENGTH_SHORT).show();
						}

					} catch (Exception e) {
						Toast.makeText(getHome(), "Loyalty user data not found",
								Toast.LENGTH_SHORT).show();
						e.printStackTrace();
					}
					cardData = null;
					alFrag = null;

					break;

				case R.id.LLMenuCancel:
					closeToggle();
					break;

				case R.id.cardOptionSpace:
					closeToggle();
					break;

				default:
					break;
			}
		}
	};

	public void showAlertDialog() {
		final Dialog dialog = new Dialog(getActivity());
		// Include dialog.xml file
		dialog.setContentView(R.layout.custom_alert_dialog_for_card);
		// Set dialog title
		dialog.setTitle("Alert!");
		final Button btn_ok = (Button) dialog.findViewById(R.id.btn_ok);
		btn_ok.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				dialog.dismiss();
			}
		});
		dialog.show();




/*


		final AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(getActivity());
		LayoutInflater inflater = getActivity().getLayoutInflater();
		final View dialogView = inflater.inflate(R.layout.custom_alert_dialog_for_card, null);
		dialogBuilder.setView(dialogView);


		final Button btn_ok = (Button) dialogView.findViewById(R.id.btn_ok);
		btn_ok.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				dialogBuilder.create().dismiss();
			}
		});
		AlertDialog b = dialogBuilder.create();
		b.show();
*/
	}

	@Override
	public int describeContents() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public void writeToParcel(Parcel arg0, int arg1) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onPageScrollStateChanged(int arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onPageScrolled(int arg0, float arg1, int arg2) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onPageSelected(int arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void processFinish(ServiceResponse<BeanMobileUser> details) {
		Log.d(TAG, "Process Finished");

		ld.hide();

		if (details != null && details.getStatus() == 200 && !details.error) {
			Log.i(TAG, "Cards download Success ");

			cardData = null;
			cardData = new ArrayList<BeanCardItem>();

			BeanMobileUser data = details.getResponse();

			ArrayList<BeanLoyaltyUser> list = (ArrayList<BeanLoyaltyUser>) data
					.getLoyaltyuser();
			for (Iterator<BeanLoyaltyUser> iterator = list.iterator(); iterator
					.hasNext();) {

				BeanLoyaltyUser loyaltyUser = (BeanLoyaltyUser) iterator.next();
				BeanCardItem item = new BeanCardItem(loyaltyUser.getId());

				try {
					item.setClub_card((loyaltyUser.getClub_card()) ? 1 : 0);
				} catch (Exception e) {
				}

				try {
					item.setCard_face_url((BaseHelper.isEmpty(loyaltyUser
							.getTier().getCard_face()) ? null : loyaltyUser
							.getTier().getCard_face()));
				} catch (Exception e) {
				}

				try {
					item.setCard_name(loyaltyUser.getTier().getCard_name());
				} catch (Exception e) {
				}

				try {
					item.setCardnumber(loyaltyUser.getMember_number());
				} catch (Exception e) {
				}

				// Get mobile user id of the loyalty program id 1
				int loyaltyProgrameId = Integer.valueOf(loyaltyUser
						.getLoyalty_program_id());
				if (loyaltyProgrameId == 1) {
					mobileUserIdForTransactions = loyaltyUser.getId();
				}

				Double walletBalance = Double.parseDouble(loyaltyUser.getMobile_wallet_balance());
				Double rebates = Double.parseDouble(loyaltyUser.getRebates());

				// Define decimal format
				DecimalFormat decimalFormat = new DecimalFormat("0.00");

				String formattedWalletBalance =  decimalFormat.format(walletBalance);
				String formattedRebatesBalance =  decimalFormat.format(rebates);

				item.setLoyaltyUserName(customerName);
				item.setBalance(formattedWalletBalance);
				item.setRebates(formattedRebatesBalance);

				String formattedDate = AppUtil.convertDate(getActivity(),
						loyaltyUser.getExpiry_date());
				item.setRebate_validity((formattedDate != null) ? formattedDate
						: AppUtil.convertDate(getActivity(),
						loyaltyUser.getExpiry_date(), true));

				cardData.add(item);
			}
			try {
				Collections.sort(cardData);
			} catch (Exception e) {
			}
			Log.e("carddate",""+cardData.size());

			setupTab(cardData);
			ld.hide();

		} else {
			ld.hide();
			try {
				BaseHelper.showAlert(getHome(), "Error ",
						details.getError_message());
			} catch (Exception e) {
			}

		}

	}

	public void setupTab(ArrayList<BeanCardItem> cardItems) {

		if (alFrag == null || alFrag.isEmpty()) {
			alFrag = new ArrayList<Fragment>();
			// alFrag.add(a);
			// alFrag.add(c);
		}

		if (cardItems != null && cardItems.size() > 0) {
			try {
				for (Iterator<BeanCardItem> iterator = cardItems.iterator(); iterator
						.hasNext();) {
					BeanCardItem beanCardItem = (BeanCardItem) iterator.next();
					Bundle mBundle = new Bundle();
					mBundle.putParcelable("card_data", beanCardItem);

					Fragment cardDetailFragment = Fragment.instantiate(
							getActivity(), CardDetailsFragment.class.getName(),
							mBundle);
					alFrag.add(cardDetailFragment);
				}
				vp.destroyDrawingCache();
				adapter = new TabAdapter(getChildFragmentManager(), alFrag);

				vp.setAdapter(adapter);
				vp.getAdapter().notifyDataSetChanged();

				indicator.setViewPager(vp);
				indicator.setOnPageChangeListener(this);

			} catch (Exception e) {
				e.printStackTrace();
			}
		} else {
			// Go to home
			BaseHelper.confirm(getActivity(), "Membership Cards",
					"You have no membership cards registered with our service",
					new ConfirmListener() {

						@Override
						public void onYes() {
							getHome().backToHomeScreen();
						}
					}, null);
		}
	}

	private void toggle(final FrameLayout v) {

		if (!toogleStatus) {
			LLMainContainer.setEnabled(false);

			height = BaseUIHelper.getScreenHeight(getHome());
			height = (float) (height * 1);

			v.setVisibility(View.VISIBLE);
			ValueAnimator va = ValueAnimator.ofFloat(0f, height).setDuration(
					VIEW_ANIMATION_INTERVAL);
			va.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
				public void onAnimationUpdate(ValueAnimator animation) {
					Integer value = (Integer) Math.round((float) animation
							.getAnimatedValue());
					v.getLayoutParams().height = value.intValue();
					v.invalidate();
					v.requestLayout();

				}
			});
			va.setInterpolator(new AccelerateInterpolator(8));
			va.start();

			toogleStatus = true;
		} else {

			closeToggle();

		}
	}

	private void closeToggle() {

		LLMainContainer.setEnabled(true);
		toogleStatus = false;

		int startHeight = slideDownView.getHeight();
		ValueAnimator animation = ValueAnimator.ofObject(
				new HeightEvaluator(slideDownView), startHeight, (int) 0)
				.setDuration(VIEW_ANIMATION_INTERVAL);
		animation.setInterpolator(new AccelerateInterpolator(8));
		animation.start();
	}
}
