package com.riverview.harrys.fragments;

import static com.riverview.harrys.constant.AppConst.PULL_TO_REFRESH_TIMEOUT;

import java.util.ArrayList;
import java.util.Iterator;

import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewStub;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import com.iapps.libs.helpers.BaseHelper;
import com.iapps.libs.helpers.BaseHelper.CancelListener;
import com.iapps.libs.helpers.BaseHelper.ConfirmListener;
import com.iapps.libs.views.LoadingCompound;
import com.orangegangsters.github.swipyrefreshlayout.library.SwipyRefreshLayout;
import com.orangegangsters.github.swipyrefreshlayout.library.SwipyRefreshLayout.OnRefreshListener;
import com.orangegangsters.github.swipyrefreshlayout.library.SwipyRefreshLayoutDirection;
import com.riverview.harrys.MainActivity;
import com.riverview.harrys.R;
import com.riverview.harrys.RCGenericFragment;
import com.riverview.harrys.adapter.NewsAdapter;
import com.riverview.harrys.asynctask.AsyncResponse;
import com.riverview.harrys.asynctask.NewsBuilderAsyncTask;
import com.riverview.harrys.helpers.ObjectSerializer;
import com.riverview.harrys.helpers.SharedPref;
import com.riverview.harrys.objects.BeanNews;
import com.riverview.harrys.objects.BeanPromotionItem;
import com.riverview.harrys.objects.ServiceResponse;
import com.riverview.harrys.util.AnaylaticsAppender;
import com.riverview.harrys.util.AppUtil;

public class NewsFragment extends RCGenericFragment implements
		OnRefreshListener, AsyncResponse<ServiceResponse<ArrayList<BeanNews>>> {

	private static final String TAG = NewsFragment.class.getSimpleName();

	public static final int TAG_LIST_STORE = 0;

	private ListView lvStores;
	private LoadingCompound ld;
	private ViewStub emptyView;
	private SwipyRefreshLayout mSwipyRefreshLayout;

	private View v;
	private NewsAdapter mAdapter;
	private ArrayList<BeanPromotionItem> storeList;
	private NewsBuilderAsyncTask mNewsTask = null;
	private AnaylaticsAppender mAppender = null;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		SharedPref.deleteString(getActivity(), SharedPref.LISTDATA_KEY);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
							 Bundle savedInstanceState) {
		v = inflater.inflate(R.layout.news_fragment_layout, container, false);
		lvStores = (ListView)v.findViewById(R.id.listViewStore);
		ld =(LoadingCompound)v.findViewById(R.id.ld);
		emptyView = (ViewStub)v.findViewById(R.id.emptyView);
		mSwipyRefreshLayout = (SwipyRefreshLayout)v.findViewById(R.id.swipyrefreshlayout);

		setHasOptionsMenu(false);
		((MainActivity) getActivity()).setBackActionBar(getActivity()
				.getResources().getString(R.string.rc_menu_home_nine),View.INVISIBLE,"");

		return v;
	}

	public void onPrepareOptionsMenu(Menu menu) {
		// getSherlockActivity().getSupportMenuInflater().inflate(
		// R.menu.menu_news, menu);
	}

	@Override
	public void onResume() {
		super.onResume();

		try {
			callAPI();
			mAppender = AnaylaticsAppender.getInstance(getActivity());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);

		mSwipyRefreshLayout.setOnRefreshListener(this);
		lvStores.setOnItemClickListener(ListenerClickItem);

		lvStores.setEmptyView(emptyView);
	}

	@SuppressWarnings("unchecked")
	public void callAPI() {

		// get saved json data
		String serilizedJsonString = SharedPref.getString(getActivity(),
				SharedPref.LISTDATA_KEY, null);
		if (serilizedJsonString != null) {
			try {
				storeList = (ArrayList<BeanPromotionItem>) ObjectSerializer
						.deserialize(serilizedJsonString);

				mAdapter = new NewsAdapter(getActivity(), storeList);
				lvStores.setAdapter(mAdapter);
				mAdapter.notifyDataSetChanged();

				ld.hide();

			} catch (Exception e) {
				callAPIService();
				Log.e(NewsFragment.TAG,
						"Error deserilization " + e.getMessage());
			}
		} else {
			callAPIService();
		}
	}

	@Override
	public void onPause() {
		super.onPause();

		if (mNewsTask != null) {
			mNewsTask.cancel(true);
		}
	}

	private boolean callAPIService() {

		if (AppUtil.checkNetworkConnection(getActivity())) {

			mNewsTask = new NewsBuilderAsyncTask(getActivity());
			mNewsTask.asyncResponse = NewsFragment.this;
			mNewsTask.execute((Void) null);

			// LLEmptyContainer.setVisibility(View.GONE);

			ld.bringToFront();
			ld.showLoading();
			// mPullToRefresh.setRefreshing(true);

			return true;

		} else {

			BaseHelper.confirm(getActivity(), "Network Unavaliable",
					"Please turn on network to connect with server",
					new ConfirmListener() {

						@Override
						public void onYes() {
							AppUtil.showSystemSettingsDialog(getActivity());
						}
					}, new CancelListener() {

						@Override
						public void onNo() {
							getHome().backToHomeScreen();

						}
					});
		}
		return false;

	}

	public OnItemClickListener ListenerClickItem = new OnItemClickListener() {

		@Override
		public void onItemClick(AdapterView<?> adapter, View view, int pos,
								long arg3) {

			// Set the data as json in Shared Prefs
			try {
				SharedPref.saveString(getActivity(), SharedPref.LISTDATA_KEY,
						ObjectSerializer.serialize(storeList));
			} catch (Exception e) {
				Log.e(TAG, "Error serilization " + e.getMessage());
			}

			NewsDetailsFragment frag = new NewsDetailsFragment();
			Bundle bundle = new Bundle();
			bundle.putParcelable("news", storeList.get(pos));
			frag.setArguments(bundle);
			getHome().setFragment(frag);

			mAppender.updateAnaylitsData(AppUtil
					.createAnalyticsJsonEventAction("news",
							String.valueOf(storeList.get(pos).getId()),
							"click", null));
		}
	};

	View.OnClickListener clickListeners = new View.OnClickListener() {
		public void onClick(View v) {

		}
	};

	@Override
	public void processFinish(ServiceResponse<ArrayList<BeanNews>> details) {
		Log.d(TAG, "Process Finished");

		ld.hide();
		mSwipyRefreshLayout.setRefreshing(false);

		// mPullToRefresh.setRefreshComplete();

		// if (details == null) {
		// BaseHelper.showAlert(getActivity(), "News feed Error");
		// return;
		// }

		if (details != null && details.getStatus() == 200 && !details.error) {
			Log.i(TAG, "News feed download Success ");

			storeList = new ArrayList<BeanPromotionItem>();

			ArrayList<BeanNews> list = (ArrayList<BeanNews>) details
					.getResponse();

			for (Iterator<BeanNews> iterator = list.iterator(); iterator
					.hasNext();) {
				BeanNews beanPromotions = (BeanNews) iterator.next();

				if (beanPromotions.getStatus().equalsIgnoreCase("PUBLISH")) {
					BeanPromotionItem temp = new BeanPromotionItem(
							beanPromotions.getId(), beanPromotions.getTitle());
					temp.setDescription(beanPromotions.getDescription());
					temp.setStartdate(beanPromotions.getStart_date());
					temp.setEnddate(beanPromotions.getEnd_date());

					try {
						temp.setImageurl((BaseHelper.isEmpty(beanPromotions
								.getImage()) ? null : beanPromotions.getImage()));
					} catch (Exception e) {
						Log.e(TAG, "Promotion images null");
					}

					storeList.add(temp);
				}
			}

			if (storeList != null && storeList.size() > 0) {
				mAdapter = new NewsAdapter(getActivity(), storeList);
				lvStores.setAdapter(mAdapter);
				mAdapter.notifyDataSetChanged();

			} else {
				// No latest buzzes to display
				BaseHelper.showAlert(getActivity(), "Harry's",
						"No latest buzzes to display");
			}

		} else {
			try {

				BaseHelper.showAlert(getActivity(), "Harry's",
						"No latest buzzes to display");
			} catch (Exception e) {
			}
		}
	}

	@Override
	public void onRefresh(SwipyRefreshLayoutDirection direction) {
		if (callAPIService()) {
			new Handler().postDelayed(new Runnable() {
				@Override
				public void run() {
					// Hide the refresh after n sec
					try {
						getActivity().runOnUiThread(new Runnable() {
							@Override
							public void run() {
								mSwipyRefreshLayout.setRefreshing(false);
							}
						});
					} catch (Exception e) {
					}
				}
			}, PULL_TO_REFRESH_TIMEOUT);
		} else {
			mSwipyRefreshLayout.setRefreshing(false);
		}
	}
}
