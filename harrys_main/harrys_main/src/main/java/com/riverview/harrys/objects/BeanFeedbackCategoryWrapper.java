package com.riverview.harrys.objects;

import java.util.ArrayList;

public class BeanFeedbackCategoryWrapper {

    public static final String OBJ_NAME = "BeanFeedbackCategoryWrapper";

    private ArrayList<BeanFeedbackCatagory> categories;

    public ArrayList<BeanFeedbackCatagory> getCategories() {
        return categories;
    }

    public void setCategories(ArrayList<BeanFeedbackCatagory> categories) {
        this.categories = categories;
    }

    @Override
    public String toString() {
        return "BeanFeedbackCategoryWrapper{" +
                "categories=" + categories +
                '}';
    }
}
