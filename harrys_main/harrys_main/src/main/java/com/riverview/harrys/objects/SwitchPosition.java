package com.riverview.harrys.objects;

public class SwitchPosition {

	private int pos;
	private String des;
	private int status;

	public String getDes() {
		return des;
	}

	public void setDes(String des) {
		this.des = des;
	}

	public int getPos() {
		return pos;
	}

	public void setPos(int pos) {
		this.pos = pos;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public SwitchPosition(int pos, String des, int status) {
		super();
		this.pos = pos;
		this.des = des;
		this.status = status;
	}

}
