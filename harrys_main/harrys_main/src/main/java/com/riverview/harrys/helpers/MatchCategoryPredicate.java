package com.riverview.harrys.helpers;

import java.io.Serializable;

import org.apache.commons.collections4.Predicate;

import com.riverview.harrys.objects.BeanProductItem;

public class MatchCategoryPredicate implements Predicate<Object>, Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2925098655403219815L;

	private final int subCategoryId;

	public MatchCategoryPredicate(int subCatagoryId) {
		super();
		this.subCategoryId = subCatagoryId;
	}

	@Override
	public boolean evaluate(Object object) {
		if (object instanceof BeanProductItem) {
			int value = Integer.parseInt(((BeanProductItem) object)
					.getSubcategory_id());
			return subCategoryId == value ? true : false;

		} else {
			return false;
		}
	}
}
