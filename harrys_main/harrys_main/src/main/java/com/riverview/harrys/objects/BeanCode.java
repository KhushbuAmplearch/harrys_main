package com.riverview.harrys.objects;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by wiraj on 5/2/15.
 */
public class BeanCode implements Parcelable {

    private String id, campaign_id, entity_id, code, created_at, updated_at;

    public BeanCode() {
    }

    public BeanCode(Parcel parcel) {
        super();

        this.id = parcel.readString();
        this.campaign_id = parcel.readString();
        this.entity_id = parcel.readString();
        this.code = parcel.readString();
        this.created_at = parcel.readString();
        this.updated_at = parcel.readString();
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(id);
        parcel.writeString(campaign_id);
        parcel.writeString(entity_id);
        parcel.writeString(code);
        parcel.writeString(created_at);
        parcel.writeString(updated_at);
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCampaign_id() {
        return campaign_id;
    }

    public void setCampaign_id(String campaign_id) {
        this.campaign_id = campaign_id;
    }

    public String getEntity_id() {
        return entity_id;
    }

    public void setEntity_id(String entity_id) {
        this.entity_id = entity_id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(String updated_at) {
        this.updated_at = updated_at;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<BeanCode> CREATOR = new Creator<BeanCode>() {

        @Override
        public BeanCode createFromParcel(Parcel source) {
            return new BeanCode(source);
        }

        @Override
        public BeanCode[] newArray(int size) {
            return new BeanCode[size];
        }
    };
}
