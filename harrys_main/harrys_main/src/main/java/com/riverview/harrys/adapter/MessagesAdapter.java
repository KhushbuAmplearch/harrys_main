package com.riverview.harrys.adapter;

import java.util.ArrayList;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.riverview.harrys.R;
import com.riverview.harrys.helpers.FontUtil;
import com.riverview.harrys.helpers.Utils;
import com.riverview.harrys.objects.BeanMessages;

public class MessagesAdapter extends BaseAdapter {
	private ArrayList<BeanMessages> mItems = new ArrayList<BeanMessages>();
	private Activity context;

	public MessagesAdapter(Activity context, ArrayList<BeanMessages> list) {
		this.context = context;
		this.mItems = list;
	}

	@Override
	public int getCount() {
		try {
			return mItems.size();
		} catch (Exception e) {
			return 0;
		}
	}

	@Override
	public BeanMessages getItem(int pos) {
		return mItems.get(pos);
	}

	@Override
	public long getItemId(int position) {
		return mItems.get(position).getId();
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ViewHolder holder;

		if (convertView == null) {
			holder = new ViewHolder();
			LayoutInflater inflater = (LayoutInflater) context
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

			convertView = inflater.inflate(R.layout.cell_messages_item, parent,
					false);
			holder.LLRoot = (LinearLayout) convertView
					.findViewById(R.id.LLRoot);
			holder.title = (TextView) convertView
					.findViewById(R.id.textViewTitle);
			holder.date = (TextView) convertView
					.findViewById(R.id.textViewDate);
			holder.content = (TextView) convertView
					.findViewById(R.id.textViewContent);
			convertView.setTag(holder);

		} else {
			holder = (ViewHolder) convertView.getTag();
		}

		// set cell height by screen height ratio
		// Utils.setWidthHeightByPercent(context, holder.imgStore, 0.30f,
		// 0.15f);

		// set textview size by screen width ratio
		// Utils.procTextsizeBasedScreen(context, holder.title, 0.85f);
		// Utils.procTextsizeBasedScreen(context, holder.date, 0.85f);
		// Utils.procTextsizeBasedScreen(context, holder.content, 0.85f);

		new FontUtil(context).setFontBoldType(holder.title);
		new FontUtil(context).setFontBoldType(holder.date);

		Utils.setTextViewFontSizeBasedOnScreenDensity(context, holder.title,
				22f);
		Utils.setTextViewFontSizeBasedOnScreenDensity(context, holder.date, 25f);
		Utils.setTextViewFontSizeBasedOnScreenDensity(context, holder.content,
				25f);

		Utils.setMarginByPercent(context, holder.LLRoot, 0.01f);
		// Utils.setMarginByPercent(context, holder.distanceStore, 0.02f);

		BeanMessages obj = getItem(position);
		holder.title.setText(obj.getTitle());
		holder.date.setText(obj.getDate());
		holder.content.setText(obj.getContent());

		return convertView;
	}

	public class ViewHolder {
		TextView title;
		TextView date;
		TextView content;
		LinearLayout LLRoot;
	}
}
