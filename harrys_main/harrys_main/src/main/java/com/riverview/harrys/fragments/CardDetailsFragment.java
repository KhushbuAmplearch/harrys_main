package com.riverview.harrys.fragments;

import java.util.ArrayList;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.iapps.libs.helpers.BaseHelper;
import com.riverview.harrys.CardImageActivity;
import com.riverview.harrys.R;
import com.riverview.harrys.RCGenericFragment;
import com.riverview.harrys.helpers.Utils;
import com.riverview.harrys.objects.BeanCardItem;
import com.riverview.harrys.objects.BeanOption;
import com.squareup.picasso.Picasso;

public class CardDetailsFragment extends RCGenericFragment {

	private static final String TAG = CardDetailsFragment.class.getSimpleName();

	private TextView tv1;
	private TextView tv2;
	private TextView tv3;
	private TextView tv4;
	private TextView tv5;
	private TextView tv6;
	private ImageView imageCard;
	private TextView textCardId;
	private TextView textCardHolderName;
	private TextView tvCardHolderName;
	private TextView tvNameHeader;
	private LinearLayout pointDetailView;

	private View v;
	public static ArrayList<BeanOption> items;

	private BeanCardItem cardItem;


	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
							 Bundle savedInstanceState) {
		v = inflater.inflate(R.layout.card_details_fragment_layout, container,
				false);

		tv1 = (TextView)v.findViewById(R.id.tv1);
		tv2 = (TextView)v.findViewById(R.id.tv2);
		tv3 = (TextView)v.findViewById(R.id.tv3);
		tv4 = (TextView)v.findViewById(R.id.tv4);
		tv5 = (TextView)v.findViewById(R.id.tv5);
		tv6 = (TextView)v.findViewById(R.id.tv6);

		imageCard = (ImageView)v.findViewById(R.id.imgCardImage);
		textCardId = (TextView)v.findViewById(R.id.textCardId);
		textCardHolderName = (TextView)v.findViewById(R.id.textCardHolderName);
		tvCardHolderName = (TextView)v.findViewById(R.id.tvCardHolderName);
		tvNameHeader = (TextView)v.findViewById(R.id.tvNameHeader);
		pointDetailView = (LinearLayout)v.findViewById(R.id.pointDetailView);

		cardItem = (BeanCardItem) getArguments().getParcelable("card_data");
		setHasOptionsMenu(false);

		return v;
	}

	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);

		Utils.setTextViewFontSizeBasedOnScreenDensity(getActivity(), tv1, 23f);
		Utils.setTextViewFontSizeBasedOnScreenDensity(getActivity(), tv2, 23f);
		Utils.setTextViewFontSizeBasedOnScreenDensity(getActivity(), tv3, 23f);
		Utils.setTextViewFontSizeBasedOnScreenDensity(getActivity(), tv4, 23f);
		Utils.setTextViewFontSizeBasedOnScreenDensity(getActivity(), tv5, 23f);
		Utils.setTextViewFontSizeBasedOnScreenDensity(getActivity(), tv6, 23f);

		Utils.setTextViewFontSizeBasedOnScreenDensity(getActivity(),
				textCardId, 20f, Typeface.BOLD);
		Utils.setTextViewFontSizeBasedOnScreenDensity(getActivity(),
				textCardHolderName, 20f);
		Utils.setTextViewFontSizeBasedOnScreenDensity(getActivity(),
				tvNameHeader, 18f);
		Utils.setTextViewFontSizeBasedOnScreenDensity(getActivity(),
				tvCardHolderName, 16f, Typeface.BOLD);

		if (!BaseHelper.isEmpty(cardItem.getCard_name())) {
			tvNameHeader.setText(cardItem.getCard_name());
		}
		tvCardHolderName.setText(cardItem.getLoyaltyUserName());

		if (cardItem.getClub_card() == 1) {
			pointDetailView.setVisibility(View.INVISIBLE);
		} else {

			tv2.setText(BaseHelper.isEmpty(cardItem.getBalance()) ? "$ 00.00"
					: "$ ".concat(cardItem.getBalance()));
			tv4.setText(BaseHelper.isEmpty(cardItem.getRebates()) ? "$ 00.00"
					: "$ ".concat(cardItem.getRebates()));
			tv6.setText(BaseHelper.isEmpty(cardItem.getRebate_validity()) ? "Unavailable"
					: cardItem.getRebate_validity());
			tv6.setText(BaseHelper.isEmpty(cardItem.getRebate_validity()) ? "Unavailable"
					: cardItem.getRebate_validity());
		}
		Picasso.with(getActivity()).load(cardItem.getCard_face_url())
				.placeholder(R.drawable.placeholder).into(imageCard);

		textCardHolderName.setText(cardItem.getLoyaltyUserName());
		textCardId.setText(cardItem.getCardnumber());

		imageCard.setOnClickListener(clickListeners);

	}

	View.OnClickListener clickListeners = new View.OnClickListener() {
		public void onClick(View v) {
			switch (v.getId()) {
				case R.id.imgCardImage:
					// Toast.makeText(getHome(), "Card selected " +
					// cardItem.getCardnumber(), Toast.LENGTH_SHORT).show();

					// CardImageFragment frag = new CardImageFragment();
					// Bundle bundle = new Bundle();
					//
					// bundle.putString("card_id", cardItem.getCardnumber());
					// bundle.putString("card_name", cardItem.getLoyaltyUserName());
					// bundle.putString("card_face", cardItem.getCard_face_url());
					//
					// frag.setArguments(bundle);
					// getHome().setFragment(frag);
					Intent intent = new Intent(getHome(), CardImageActivity.class);
					intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
					intent.putExtra("card_id", cardItem.getCardnumber());
					intent.putExtra("card_name", cardItem.getCard_name());
					intent.putExtra("card_face", cardItem.getCard_face_url());
					intent.putExtra("club_card",cardItem.getClub_card());

					getActivity().startActivity(intent);

					break;

				default:
					break;
			}

		}
	};

}
