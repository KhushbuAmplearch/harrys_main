/* AUTO-GENERATED FILE.  DO NOT MODIFY.
 *
 * This class was automatically generated by the
 * gradle plugin from the resource data it found. It
 * should not be modified by hand.
 */
package de.timroes.swipetodismiss;

public final class R {
    public static final class anim {
        public static int popup_hide = 0x7f01000d;
        public static int popup_show = 0x7f01000e;
    }
    public static final class color {
        public static int btn_focused = 0x7f0600b3;
        public static int btn_normal = 0x7f0600b4;
        public static int btn_pressed = 0x7f0600b5;
        public static int popup_bg_color = 0x7f0600ef;
        public static int popup_text_color = 0x7f0600f0;
        public static int separator_color = 0x7f060100;
    }
    public static final class drawable {
        public static int ic_action_undo = 0x7f09009f;
        public static int popup_bg = 0x7f090177;
        public static int undo_btn_bg = 0x7f0901a6;
        public static int undo_btn_bg_focused = 0x7f0901a7;
        public static int undo_btn_bg_pressed = 0x7f0901a8;
    }
    public static final class id {
        public static int text = 0x7f0c00f3;
        public static int undo = 0x7f0c0108;
    }
    public static final class layout {
        public static int undo_popup = 0x7f0f004b;
    }
    public static final class string {
        public static int undo = 0x7f15009e;
        public static int undoall = 0x7f15009f;
    }
    public static final class style {
        public static int fade_animation = 0x7f160189;
    }
}
