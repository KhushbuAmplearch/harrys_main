package com.riverview.harrys.dialogfragment;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.riverview.harrys.MainActivity;
import com.riverview.harrys.R;
import com.riverview.harrys.RCGenericDialogFragment;
import com.riverview.harrys.helpers.Utils;


public class ForceUpdateDialog extends RCGenericDialogFragment {

	private Button btnYes;
	private TextView tv1;
	private TextView tv2;

	private View v;
	
	private String dialogMessage;
	
	public ForceUpdateDialog(String message) {
		this.dialogMessage = message;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		v = inflater.inflate(R.layout.force_update_fragment_layout, container,
				false);
		setHasOptionsMenu(false);
		btnYes = (Button)v.findViewById (R.id.btnYes);
		tv1 = (TextView) v.findViewById (R.id.tv1);
		tv2 = (TextView)v.findViewById (R.id.tv2);


		return v;
	}

	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);

		Utils.setTextViewFontSizeBasedOnScreenDensity(getActivity(), tv1, 20f,
				Typeface.BOLD);
		Utils.setTextViewFontSizeBasedOnScreenDensity(getActivity(), tv2, 23f);

		Utils.setButtonFontSizeBasedOnScreenDensity(getActivity(), btnYes,
				Utils.FONT_SMALL_DENSITY_SIZE);

		btnYes.setOnClickListener(clickListeners);
		tv2.setText(dialogMessage);

	}

	View.OnClickListener clickListeners = new View.OnClickListener() {
		public void onClick(View v) {

			switch (v.getId()) {

			case R.id.btnYes:

				String appPackageName = "com.riverview.harrys";
				try {
					getActivity().startActivity(
							new Intent(Intent.ACTION_VIEW, Uri
									.parse("market://details?id="
											+ appPackageName)));
				} catch (android.content.ActivityNotFoundException anfe) {
					startActivity(new Intent(
							Intent.ACTION_VIEW,
							Uri.parse("https://play.google.com/store/apps/details?id="
									+ appPackageName)));
				}
				
//				try {
//					getDialog().dismiss();
//				} catch (Exception e) {
//					e.printStackTrace();
//				}
				
				
				break;

			default:
				break;
			}
		}

	};	

	@Override
	public void onDismiss(DialogInterface dialog) {
		super.onDismiss(dialog);
		Log.d("TAG", "onDismiss");
		try {
			((MainActivity) getActivity()).backToHomeScreen();
		} catch (Exception e) {
		}
	}

}
