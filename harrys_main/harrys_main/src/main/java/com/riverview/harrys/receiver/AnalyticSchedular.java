package com.riverview.harrys.receiver;

import java.util.Calendar;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.riverview.harrys.constant.AppConst;

/**
 * Created by wiraj on 4/29/15.
 */
public class AnalyticSchedular extends BroadcastReceiver {

	private static final String TAG = AnalyticSchedular.class.getSimpleName();

	private static final long REPEAT_TIME = 1000 * AppConst.ANALYTICS_SERVICE_SCHEDULAR_INTERVAL_SECONDS;
	private static final int REQUEST_CODE = 20;

	@Override
	public void onReceive(Context context, Intent intent) {
		if (intent.getAction().equals(AppConst.ACTION_PROCESS_ANALYTICS)) {
			Log.i(TAG, "Analytics service schedular");

			AlarmManager alarmManager = (AlarmManager) context
					.getSystemService(Context.ALARM_SERVICE);
			Intent i = new Intent(context, AnalyticsUploadInvoker.class);
			PendingIntent pendingIntent = PendingIntent.getBroadcast(context,
					REQUEST_CODE, i, PendingIntent.FLAG_UPDATE_CURRENT);

			Calendar calendar = Calendar.getInstance();
			calendar.add(Calendar.SECOND,
					AppConst.ANALYTICS_SERVICE_SCHEDULAR_INTERVAL_SECONDS);

			// Do process after each REPEAT_TIME seconds
			alarmManager.setInexactRepeating(AlarmManager.RTC_WAKEUP,
					calendar.getTimeInMillis(), REPEAT_TIME, pendingIntent);
		}
	}
}
