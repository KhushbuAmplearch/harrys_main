package com.riverview.harrys.adapter;

import java.util.ArrayList;

import android.os.Parcelable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

public class TabAdapter extends FragmentStatePagerAdapter {

	ArrayList<Fragment> alFrag;

	public TabAdapter(FragmentManager fm, ArrayList<Fragment> alFrag) {
		super(fm);
		this.alFrag = alFrag;
	}

	@Override
	public Fragment getItem(int arg0) {
		return alFrag.get(arg0);
	}

	@Override
	public int getCount() {
		return alFrag.size();
	}

	@Override
	public int getItemPosition(Object object) {
		return POSITION_NONE;
	}

	@Override
	public Parcelable saveState() {
		// TODO Auto-generated method stub
		return null;
	}

}