package com.riverview.harrys.helpers;

import java.net.URLEncoder;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.TimeZone;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.content.Context;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.util.Log;

import com.iapps.libs.helpers.HTTPAsyncTask;
import com.iapps.libs.objects.Response;
import com.riverview.harrys.constant.AppConst;
import com.riverview.harrys.location.LocationConstants;
import com.riverview.harrys.objects.BeanLogin;
import com.riverview.harrys.util.AppUtil;

public class Analytics extends Thread {

	protected final String URL = "http://worker.riverviewms.com";
	public final static String appID = AppConst.SERVICE_API_KEY;

	private static final String TAG = Waiter.class.getName();
	private long lastUsed;
	private long period = 30000;// every 30secs
	private boolean stop;
	private Activity context;
	private static JSONArray jsonArrayEvents = new JSONArray();
	public static Analytics analytic;
	private static BeanLogin userAccount;
	private boolean toggleRunning = false;

	public static Analytics getInstance(Activity ctx) {
		if (analytic == null) {
			analytic = new Analytics(ctx);
		}

		return analytic;

	}

	public void checkAndStartOrNot() {
		try {
			if (!analytic.isAlive()) {
				analytic = new Analytics(context);
				analytic.start();
			}
		} catch (Exception e) {
		}
	}

	public Analytics(Activity ctx) {
		context = ctx;
		stop = false;

	}

	long idle;

	public void run() {
		idle = 0;
		this.touch();
		do {
			idle = System.currentTimeMillis() - lastUsed;

			try {
				context.runOnUiThread(new Runnable() {

					public void run() {

						try {

						} catch (Exception e) {

						}
					}
				});

				Thread.sleep(1000); // check every 1 seconds
			} catch (InterruptedException e) {
				Log.d(TAG, "Waiter interrupted!");
			}
			if (idle > period) {
				// stop=true;
				idle = 0;

				// do something here - e.g. call popup or so

				// ensure the thread only run once
				if (toggleRunning == true)
					return;

				try {
					context.runOnUiThread(new Runnable() {

						@SuppressWarnings("deprecation")
						public void run() {
							toggleRunning = true;
							try {
								PushEventsAsync lpa = new PushEventsAsync();

								JSONObject jObjectData = new JSONObject();
								try {
									createDeviceInfo(context, jObjectData);
									jObjectData.put("app_id", appID);
									jObjectData.put("UDID",
											AppUtil.getDeviceId(context));
									jObjectData.put("session",
											getSessionMetrics(context));
									cleanDuplicationAndDummyData();
									jObjectData.put("events", jsonArrayEvents);
								} catch (Exception e) {
								}

								lpa.setUrl(String.format(
										"%s/?app_key=%s&data=%s", URL, appID,
										URLEncoder.encode(jObjectData
												.toString())));
								lpa.setMethod("GET");

								if (jsonArrayEvents.length() > 0) {
									lpa.execute();

									// reset
									jsonArrayEvents = new JSONArray();
								}

							} catch (Exception e) {
								toggleRunning = false;
							}
						}
					});

					touch();
				} catch (Exception e) {
					toggleRunning = false;
				}

			}
		} while (!stop);
		Log.d(TAG, "Finishing Waiter thread");
	}

	public synchronized void touch() {
		lastUsed = System.currentTimeMillis();
	}

	public synchronized void forceInterrupt() {
		this.interrupt();
	}

	// soft stopping of thread
	public synchronized void stopIT() {
		stop = true;
	}

	public synchronized void setPeriod(long period) {
		this.period = period;
	}

	public static String getApplicationName(Context context) {
		int stringId = context.getApplicationInfo().labelRes;
		return context.getString(stringId);
	}

	public static String getMacAdress(Context context) {
		String address;
		try {
			WifiManager manager = (WifiManager) context
					.getSystemService(Context.WIFI_SERVICE);
			WifiInfo info = manager.getConnectionInfo();
			address = info.getMacAddress();
		} catch (Exception e) {
			address = "";
		}
		return address;
	}

	public static String getManufacturerName() {
		String manufacturer = Build.MANUFACTURER;
		String model = Build.MODEL;
		if (model.startsWith(manufacturer)) {
			return capitalize(model);
		} else {
			return capitalize(manufacturer);
		}
	}

	public static String getModelName() {
		String manufacturer = Build.MANUFACTURER;
		String model = Build.MODEL;
		if (model.startsWith(manufacturer)) {
			return capitalize(model);
		} else {
			return capitalize(manufacturer) + " " + model;
		}
	}

	private static String capitalize(String s) {
		if (s == null || s.length() == 0) {
			return "";
		}
		char first = s.charAt(0);
		if (Character.isUpperCase(first)) {
			return s;
		} else {
			return Character.toUpperCase(first) + s.substring(1);
		}
	}

	public static void createDeviceInfo(Activity context, JSONObject jObjectData) {

		try {
			jObjectData.put("app", getApplicationName(context));
			jObjectData.put(
					"app_version",
					(context.getPackageManager().getPackageInfo(
							context.getPackageName(), 0)).versionName);
			jObjectData.put("app_package_name", context.getPackageName());
			jObjectData.put("platform", "Android");
			jObjectData.put("model", getModelName());
			jObjectData.put("make", getManufacturerName());
			jObjectData.put("platform_version",
					android.os.Build.VERSION.SDK_INT);
			jObjectData.put("rms_version", "1.0.0");
			jObjectData.put("locale", (context.getResources()
					.getConfiguration().locale).toString());
			jObjectData.put("mac_address", getMacAdress(context));

			try {
				// userAccount = Converter.toBeanOSSignIn(new JSONObject(
				// SharedPref.getString(context,
				// SharedPref.SAVED_LOGIN_DATA, null)));
				jObjectData.put("user_id", SharedPref.getInteger(context,
						SharedPref.USERID_KEY, 0));
			} catch (Exception e) {
			}

		} catch (Exception e) {
		}

	}

	public static JSONObject createEvent(Context context, String value,
			String attributes, String type) {

		JSONObject jObjectData = new JSONObject();
		try {
			jObjectData.put("event_type", type);
			jObjectData.put("timestamp", getISO8601String());
			jObjectData.put("timestampdummy", getISO8601StringDummy());
			jObjectData.put("event_value", value);
			try {
				jObjectData.put("attributes", new JSONObject(attributes));
			} catch (Exception e) {
			}
			jObjectData.put("location", LocationConstants.latitude + ","
					+ LocationConstants.longitude);
		} catch (Exception e) {
		}

		return jObjectData;
	}

	public static JSONObject createEventAction(Context context, String value,
			String action, String type) {

		JSONObject jObjectData = new JSONObject();
		try {
			jObjectData.put("event_type", type);
			jObjectData.put("timestamp", getISO8601String());
			jObjectData.put("timestampdummy", getISO8601StringDummy());
			jObjectData.put("event_action", action);
			jObjectData.put("event_value", value);
			jObjectData.put("location", LocationConstants.latitude + ","
					+ LocationConstants.longitude);
		} catch (Exception e) {
		}

		return jObjectData;
	}

	public void addArrayEvent(Context context, String value, String attributes,
			String type) {

		jsonArrayEvents.put(createEvent(context, value, attributes, type));

	}

	public void addArrayEventAction(Context context, String value,
			String action, String type) {

		jsonArrayEvents.put(createEventAction(context, value, action, type));

	}

	public static JSONObject getSessionMetrics(Context context) {

		JSONObject jObjectData = new JSONObject();
		try {
			jObjectData.put("session_id", AppUtil.getDeviceId(context));
			jObjectData.put("timestamp", getISO8601String());
		} catch (Exception e) {
		}

		return jObjectData;
	}

	public static String getISO8601String() {
		TimeZone tz = TimeZone.getTimeZone("UTC");
		DateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ");
		df.setTimeZone(tz);
		String nowAsISO = df.format(new Date());
		return nowAsISO;
	}

	public static String getISO8601StringDummy() {
		TimeZone tz = TimeZone.getTimeZone("UTC");
		DateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mmZ");
		df.setTimeZone(tz);
		String nowAsISO = df.format(new Date());
		return nowAsISO;
	}

	public class PushEventsAsync extends HTTPAsyncTask {
		@Override
		protected void onPreExecute() {

		}

		@Override
		protected void onPostExecute(Response response) {
			toggleRunning = false;
			try {
				JSONObject json = Utils.handleResponseCustom(response, null,
						true, context);

			} catch (Exception e) {

			}

		}
	}

	public static void cleanDuplicationAndDummyData() {

		JSONArray temparray = jsonArrayEvents;

		ArrayList<String> tempTimestamp = new ArrayList<String>();
		for (int c = 0; c < jsonArrayEvents.length(); c++) {
			try {
				tempTimestamp.add(jsonArrayEvents.getJSONObject(c).getString(
						"timestamp"));
			} catch (JSONException e) {
			}
		}

		// remove timestamp temporary
		try {
			for (int i = 0; i < jsonArrayEvents.length(); i++) {

				try {
					JSONObject x = jsonArrayEvents.getJSONObject(i);
					x.remove("timestamp");
				} catch (JSONException e) {
				}
			}
		} catch (Exception e) {
		}

		try {
			int total = jsonArrayEvents.length();
			for (int x = 0; x < total; x++) {
				int check = 0;
				for (int i = 0; i < jsonArrayEvents.length(); i++) {

					try {
						if (jsonArrayEvents
								.get(x)
								.toString()
								.compareToIgnoreCase(
										jsonArrayEvents.get(i).toString()) == 0) {
							check = check + 1;
						}
					} catch (Exception e) {
					}

				}

				if (check > 1) { // duplication found

					int check2nd = 0;
					outerloop: for (int j = 0; j < jsonArrayEvents.length(); j++) {

						try {
							if (temparray
									.get(x)
									.toString()
									.compareToIgnoreCase(
											jsonArrayEvents.get(j).toString()) == 0) {
								if (check2nd >= 1) {

									jsonArrayEvents = removeJSONArray(
											jsonArrayEvents, j);
									break outerloop;
								} else
									check2nd = check2nd + 1;
							}
						} catch (Exception e) {
						}

					}
				}

			}
		} catch (Exception e) {
		}

		// remove timestampdummy and put the correct timestamp
		try {
			for (int i = 0; i < jsonArrayEvents.length(); i++) {

				try {
					JSONObject x = jsonArrayEvents.getJSONObject(i);
					x.remove("timestampdummy");
				} catch (Exception e) {
				}

				try {
					jsonArrayEvents.getJSONObject(i).put("timestamp",
							tempTimestamp.get(i));
				} catch (Exception e) {
				}
			}
		} catch (Exception e) {
		}

	}

	public static JSONArray removeJSONArray(JSONArray jsonArray, int index) {
		try {
			JSONArray list = new JSONArray();
			int len = jsonArray.length();
			if (jsonArray != null) {
				for (int i = 0; i < len; i++) {
					// Excluding the item at position
					if (i != index) {
						list.put(jsonArray.get(i));
					}
				}
			}
			jsonArray = list;
		} catch (Exception e) {
		}
		return jsonArray;
	}

}
