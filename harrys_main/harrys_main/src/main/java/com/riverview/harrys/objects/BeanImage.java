package com.riverview.harrys.objects;

import java.io.Serializable;

public class BeanImage implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4054098632424379185L;
	private String id, user_id, imageable_id, imageable_type, filename,
			created_at, updated_at, original;
	private String medium, small, thumb, filetype, filesize;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getUser_id() {
		return user_id;
	}

	public void setUser_id(String user_id) {
		this.user_id = user_id;
	}

	public String getImageable_id() {
		return imageable_id;
	}

	public void setImageable_id(String imageable_id) {
		this.imageable_id = imageable_id;
	}

	public String getImageable_type() {
		return imageable_type;
	}

	public void setImageable_type(String imageable_type) {
		this.imageable_type = imageable_type;
	}

	public String getFilename() {
		return filename;
	}

	public void setFilename(String filename) {
		this.filename = filename;
	}

	public String getCreated_at() {
		return created_at;
	}

	public void setCreated_at(String created_at) {
		this.created_at = created_at;
	}

	public String getUpdated_at() {
		return updated_at;
	}

	public void setUpdated_at(String updated_at) {
		this.updated_at = updated_at;
	}

	public String getOriginal() {
		return original;
	}

	public void setOriginal(String original) {
		this.original = original;
	}

	public String getMedium() {
		return medium;
	}

	public void setMedium(String medium) {
		this.medium = medium;
	}

	public String getSmall() {
		return small;
	}

	public void setSmall(String small) {
		this.small = small;
	}

	public String getThumb() {
		return thumb;
	}

	public void setThumb(String thumb) {
		this.thumb = thumb;
	}

	public String getFiletype() {
		return filetype;
	}

	public void setFiletype(String filetype) {
		this.filetype = filetype;
	}

	public String getFilesize() {
		return filesize;
	}

	public void setFilesize(String filesize) {
		this.filesize = filesize;
	}

}
