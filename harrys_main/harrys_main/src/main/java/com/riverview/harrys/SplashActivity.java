package com.riverview.harrys;

import static com.riverview.harrys.constant.AppConst.SPLASH_TIMEOUT;
import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;

import com.riverview.harrys.helpers.Waiter;
import com.riverview.harrys.util.SystemUiHider;

public class SplashActivity extends Activity {

	private static final String TAG = SplashActivity.class.getSimpleName();


	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setContentView(R.layout.splash_layout);
		new Handler().postDelayed(new Runnable() {

			@Override
			public void run() {

			//	mSystemUiHider.hide();
				startActivity(new Intent(SplashActivity.this,
						MainActivity.class));


			}
		}, 1000);

	}



}
