package com.riverview.harrys.objects;

import java.util.ArrayList;

public class BeanFeedbackAttachmentWrapper {

    public static final String OBJ_NAME = "BeanFeedbackAttachmentWrapper";

    private ArrayList<BeanFeedbackAttachment> attachments;

	public ArrayList<BeanFeedbackAttachment> getAttachments() {
		return attachments;
	}

	public void setAttachments(ArrayList<BeanFeedbackAttachment> attachments) {
		this.attachments = attachments;
	}

    
}
