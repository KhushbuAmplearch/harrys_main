package com.riverview.harrys.objects;

import android.os.Parcel;
import android.os.Parcelable;

import java.io.Serializable;

public class BeanFeedbackType implements Parcelable, Serializable {

	public static final String OBJ_NAME = "BeanFeedback";
	private static final long serialVersionUID = 5588254270496437773L;

	private String type_key, type;

	public BeanFeedbackType(Parcel parcel) {
		super();
		this.type_key = parcel.readString();
		this.type = parcel.readString();
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {

		dest.writeString(type_key);
		dest.writeString(type);

	}

	public static final Creator<BeanFeedbackType> CREATOR = new Creator<BeanFeedbackType>() {

		@Override
		public BeanFeedbackType createFromParcel(Parcel source) {
			return new BeanFeedbackType(source);
		}

		@Override
		public BeanFeedbackType[] newArray(int size) {
			return new BeanFeedbackType[size];
		}
	};

	@Override
	public int describeContents() {
		// TODO Auto-generated method stub
		return 0;
	}

	public BeanFeedbackType() {
		super();
	}

	public String getType_key() {
		return type_key;
	}

	public void setType_key(String type_key) {
		this.type_key = type_key;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

}
