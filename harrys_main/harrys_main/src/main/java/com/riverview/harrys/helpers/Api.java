package com.riverview.harrys.helpers;

import android.content.Context;

public class Api {

	// Development
	protected final String BASE_URL = "http://54.254.213.6/api/index.php/";

	// Development 2
	// private final String BASE_URL = "http://54.255.135.41/api/index.php/";

	// Staging internal IP
	// private final String BASE_URL = "http://10.208.72.5:443/api/index.php/";

	public final String DEMO_URL = "http://www.google.com";

	protected final String POST_FACILITY_SCAN = "facility/gym/scan";
	protected final String POST_FACILITY_MANUAL = "facility/gym/manual";
	protected final String GET_FACILITY_PASS = "facility/gym/pass";
	protected final String POST_FACILITY_ADD = "facility/gym/pass/add";
	protected final String POST_FACILITY_DOB = "facility/gym/dob/edit";
	protected final String GET_FACILITY_VENUES = "facility/gym/";
	protected final String GET_FACILITY_SWIM_VENUES = "facility/swim";
	protected final String POST_FACILITY_LOGIN = "facility/gym/go/login";
	protected final String GET_LIST_USERS = "facility/gym/go/list/";
	protected final String GET_USERS_DETAIL = "facility/gym/go/user";
	protected final String POST_CHECKOUT_USER = "facility/gym/go/checkout";
	protected final String POST_CHECKIN_USER = "facility/gym/go/checkin";
	
	protected final String GET_ADS = "/ads";

	protected final String ACCOUNT_LOGIN = "account/account_login";

	// http://54.254.213.6/api/index.php/facility/gym/manual
	// Connection Keep-Alive
	// Content-Type application/x-www-form-urlencoded
	// {pass_type_id=3, pass_for=218783, venue_id=701, ewallet_passcode=999938}
	Context context;

	public Api(Context context) {
		this.context = context;
	}

	// Base URL
	public String getBaseUrl() {
		return BASE_URL;
	}

	// Post methods
	public String postFacilityScan() {
		return getBaseUrl() + POST_FACILITY_SCAN;
	}

	public String postFacilityManual() {
		return getBaseUrl() + POST_FACILITY_MANUAL;
	}

	public String gettFacilityPass() {
		return getBaseUrl() + GET_FACILITY_PASS;
	}

	public String postFacilityAdd() {
		return getBaseUrl() + POST_FACILITY_ADD;
	}

	public String postFacilityDob() {
		return getBaseUrl() + POST_FACILITY_DOB;
	}

	public String getFacilityVenues() {
		return getBaseUrl() + GET_FACILITY_VENUES;
	}

	public String getFacilitySwimVenues() {
		return getBaseUrl() + GET_FACILITY_SWIM_VENUES;
	}

	public String postFacilityLogin() {
		return getBaseUrl() + POST_FACILITY_LOGIN;
	}

	public String postFacilityAccountLogin() {
		return getBaseUrl() + ACCOUNT_LOGIN;
	}

	public String getListUsers() {
		return getBaseUrl() + GET_LIST_USERS;
	}

	public String getUsersDetail() {
		return getBaseUrl() + GET_USERS_DETAIL;
	}

	public String postCheckoutUser() {
		return getBaseUrl() + POST_CHECKOUT_USER;
	}

	public String postCheckinUser() {
		return getBaseUrl() + POST_CHECKIN_USER;
	}
	
	public String getAds() {
		return getBaseUrl() + GET_ADS;
	}

}
