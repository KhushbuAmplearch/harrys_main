package com.riverview.harrys.helpers;

import java.nio.charset.Charset;
import java.security.GeneralSecurityException;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.location.Location;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnCompletionListener;
import android.os.IBinder;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout.LayoutParams;
import android.widget.TextView;

import com.iapps.libs.helpers.BaseConstants;
import com.iapps.libs.helpers.BaseHelper;
import com.iapps.libs.helpers.BaseKeys;
import com.iapps.libs.objects.Response;
import com.iapps.libs.views.LoadingCompound;
import com.riverview.harrys.receiver.UserStateCheckerReceiver;

public class Utils {

	public final static double FONT_SMALLER_DENSITY_SIZE = 23f;
	public final static double FONT_SMALL_DENSITY_SIZE = 19f;
	public final static double FONT_MEDIUM_DENSITY_SIZE = 14f;
	public final static double FONT_LARGE_DENSITY_SIZE = 10f;
	public static double latitude=0.0;
	public static double longitude=0.0;

	public static void closeKeyboard(Context c, IBinder windowToken) {
		InputMethodManager mgr = (InputMethodManager) c
				.getSystemService(Context.INPUT_METHOD_SERVICE);
		mgr.hideSoftInputFromWindow(windowToken, 0);
	}

	public static void suspendUser(Context context) {

		Intent intentSuspend = new Intent(context,UserStateCheckerReceiver.class);
		intentSuspend.setAction(UserStateCheckerReceiver.ACTION_START_USER_STATUS_CHECKER);
		context.sendBroadcast(intentSuspend);
	}

	public static void hideKeyboard(Context ctx) {
		try {

			InputMethodManager inputManager = (InputMethodManager) ctx
					.getSystemService(Context.INPUT_METHOD_SERVICE);

			// check if no view has focus:
			View v = ((Activity) ctx).getCurrentFocus();
			if (v == null)
				return;

			inputManager.hideSoftInputFromWindow(v.getWindowToken(), 0);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public static JSONObject handleResponseCustom(Response response,
			LoadingCompound loading, boolean shouldDisplayDialog,
			Activity context) {

		if (response != null) {

			JSONObject json = response.getContent();

			if (response.getStatusCode() == BaseConstants.STATUS_SUCCESS) {
				return json;
			} else if (response.getStatusCode() == BaseConstants.STATUS_NOT_FOUND
					|| response.getStatusCode() == BaseConstants.STATUS_BAD_REQUEST) {

				int status_code = 0;
				try {
					status_code = json.getInt(Keys.STATUS_CODE);
				} catch (JSONException e) {
				}

				// if(status_code == Constants.STATUSCODE_9103){
				// BaseHelper.showAlert(context, null,
				// context.getResources().getString(R.string.err_msg_status_code_9103));
				// }else if(status_code == Constants.STATUSCODE_9134){
				// BaseHelper.showAlert(context, null,
				// context.getResources().getString(R.string.err_msg_status_code_9134));
				// }else if(status_code == Constants.STATUSCODE_9125){
				// BaseHelper.showAlert(context, null,
				// context.getResources().getString(R.string.err_msg_status_code_9125));
				// }else if(status_code == Constants.STATUSCODE_9205){
				// BaseHelper.showAlert(context, null,
				// context.getResources().getString(R.string.err_msg_status_code_9205));
				// }else if(status_code == Constants.STATUSCODE_9214){
				// try {
				// BaseHelper.showAlert(context, json.getString(Keys.MESSAGE));
				// } catch (Exception e) {
				// BaseHelper.showAlert(context, null,
				// context.getResources().getString(R.string.err_msg_status_code_9205));
				// }
				//
				// }else
				// return json;

			} else if (response.getStatusCode() == Constants.STATUSCODE_403) {

				// UserInfoManager.getInstance(context).logout();

				// Intent intent = context.getIntent();
				// intent.putExtra(Keys.INVALID_OAUTH, true);
				// intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
				// ((MainActivity) context).finish();
				// context.startActivity(intent);

			} else {
				try {
					String message = json.getString(BaseKeys.MESSAGE);
					if (loading != null) {
						loading.showError(null, message);
					} else if (shouldDisplayDialog && context != null) {
						BaseHelper.showAlert(context, null, message);

					}
				} catch (Exception e) {
					e.printStackTrace();
					if (loading != null) {
						loading.showUnknownResponse();
					} else if (shouldDisplayDialog && context != null) {
						BaseHelper.showUnknownResponseError(context);
					}
				}

			}
		} else {
			if (loading != null) {
				loading.showInternetError();
			} else if (shouldDisplayDialog && context != null) {
				BaseHelper.showInternetError(context);
			}
		}

		return null;
	}

	public static double distance(double lat1, double lon1, double lat2,
			double lon2) {
		Location selected_location = new Location("locationA");
		selected_location.setLatitude(lat1);
		selected_location.setLongitude(lon1);
		Location near_locations = new Location("locationB");
		near_locations.setLatitude(lat2);
		near_locations.setLongitude(lon2);

		double distance = selected_location.distanceTo(near_locations);
		return Double.parseDouble(String.format("%.1f", distance / 1000));// in
																			// km
	}

	public static void playSound(Activity act, int resRawAudio) {

		try {
			MediaPlayer mp = MediaPlayer.create(act, resRawAudio);
			mp.setOnCompletionListener(new OnCompletionListener() {

				@Override
				public void onCompletion(MediaPlayer mp) {
					try {
						mp.release();
					} catch (Exception e) {
						e.printStackTrace();
					}
				}

			});
			mp.start();
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	public static int getDip(Context ctx, int pixel) {
		return (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP,
				pixel, ctx.getResources().getDisplayMetrics());
	}

	@SuppressWarnings({ "static-access", "deprecation" })
	public static void setHeightByPercent(Context ctx, View Root,
			float percentage) {
		WindowManager windowManager = (WindowManager) ctx
				.getSystemService(ctx.WINDOW_SERVICE);
		int device_height = windowManager.getDefaultDisplay().getHeight();

		float res_scale = (float) device_height * percentage;
		Root.setMinimumHeight(Math.round(res_scale));

		Root.setLayoutParams(new LayoutParams(LayoutParams.MATCH_PARENT, Math
				.round(res_scale)));

		Root.requestLayout();
	}

	@SuppressWarnings({ "static-access", "deprecation" })
	public static void setHeightByPercentWeight(Context ctx, View Root,
			float percentage) {
		WindowManager windowManager = (WindowManager) ctx
				.getSystemService(ctx.WINDOW_SERVICE);
		int device_height = windowManager.getDefaultDisplay().getHeight();

		float res_scale = (float) device_height * percentage;
		Root.setMinimumHeight(Math.round(res_scale));

		Root.setLayoutParams(new LayoutParams(dps2pixels(0, ctx), Math
				.round(res_scale)));

		Root.requestLayout();
	}

	@SuppressWarnings({ "static-access", "deprecation" })
	public static void setWidthHeightByPercent(Context ctx, View Root,
			float percentageWidth, float percentageHeight) {
		WindowManager windowManager = (WindowManager) ctx
				.getSystemService(ctx.WINDOW_SERVICE);
		int device_width = windowManager.getDefaultDisplay().getWidth();
		int device_height = windowManager.getDefaultDisplay().getHeight();

		float res_scale_width = (float) device_width * percentageWidth;
		float res_scale_height = (float) device_height * percentageHeight;
		Root.setMinimumHeight(Math.round(res_scale_height));
		Root.setLayoutParams(new LayoutParams(Math.round(res_scale_width), Math
				.round(res_scale_height)));
		Root.requestLayout();
	}

	@SuppressWarnings({ "static-access", "deprecation" })
	public static int setMarginByPercent(Context ctx, View Root,
			float percentage) {
		WindowManager windowManager = (WindowManager) ctx
				.getSystemService(ctx.WINDOW_SERVICE);
		int device_height = windowManager.getDefaultDisplay().getHeight();

		float res_scale = (float) device_height * percentage;

		LayoutParams temp = (LayoutParams) Root.getLayoutParams();

		temp.setMargins((int) res_scale, (int) res_scale, (int) res_scale,
				(int) res_scale);

		return (int) res_scale;
	}

	@SuppressWarnings({ "static-access", "deprecation" })
	public static int setMarginByPercent(Context ctx, View Root,
			float percentage, boolean isLeftMarginZero) {
		WindowManager windowManager = (WindowManager) ctx
				.getSystemService(ctx.WINDOW_SERVICE);
		int device_height = windowManager.getDefaultDisplay().getHeight();

		float res_scale = (float) device_height * percentage;

		LayoutParams temp = (LayoutParams) Root.getLayoutParams();

		if (!isLeftMarginZero) {
			temp.setMargins((int) res_scale, (int) res_scale, (int) res_scale,
					(int) res_scale);
		} else {
			temp.setMargins((int) 0, (int) res_scale, (int) res_scale,
					(int) res_scale);
		}

		return (int) res_scale;
	}

	private static float scale = 0;

	public static int dps2pixels(int dps, Context context) {
		if (0 == scale) {
			scale = context.getResources().getDisplayMetrics().density;
		}
		return (int) (dps * scale + 0.5f);
	}

	public static ViewGroup getParent(View view) {
		return (ViewGroup) view.getParent();
	}

	public static void removeView(View view) {
		ViewGroup parent = getParent(view);
		if (parent != null) {
			parent.removeView(view);
		}
	}

	public static void replaceView(View currentView, View newView) {
		ViewGroup parent = getParent(currentView);
		if (parent == null) {
			return;
		}
		final int index = parent.indexOfChild(currentView);
		removeView(currentView);
		removeView(newView);
		parent.addView(newView, index);
	}

	public static void setButtonFontSizeBasedOnScreenDensity(Activity activity,
			Button btn, double size) {

		DisplayMetrics dMetrics = new DisplayMetrics();
		activity.getWindowManager().getDefaultDisplay().getMetrics(dMetrics);
		final float WIDE = activity.getResources().getDisplayMetrics().widthPixels;
		int valueWide = (int) (WIDE / size / (dMetrics.scaledDensity));
		btn.setTextSize(valueWide);
	}

	public static void setButtonFontSizeBasedOnScreenDensity(Activity activity,
			Button btn, double size, int style) {

		DisplayMetrics dMetrics = new DisplayMetrics();
		activity.getWindowManager().getDefaultDisplay().getMetrics(dMetrics);
		final float WIDE = activity.getResources().getDisplayMetrics().widthPixels;
		int valueWide = (int) (WIDE / size / (dMetrics.scaledDensity));
		btn.setTextSize(valueWide);
		btn.setTypeface(btn.getTypeface(), style);
	}

	public static void setTextViewFontSizeBasedOnScreenDensity(Activity activity, TextView tv, double size) {

		DisplayMetrics dMetrics = new DisplayMetrics();
		activity.getWindowManager().getDefaultDisplay().getMetrics(dMetrics);
		final float WIDE = activity.getResources().getDisplayMetrics().widthPixels;
		int valueWide = (int) (WIDE / size / (dMetrics.scaledDensity));
		tv.setTextSize(valueWide);
	}

	public static void setTextViewFontSizeBasedOnScreenDensity(
			Activity activity, TextView tv, double size, int style) {

		DisplayMetrics dMetrics = new DisplayMetrics();
		activity.getWindowManager().getDefaultDisplay().getMetrics(dMetrics);
		final float WIDE = activity.getResources().getDisplayMetrics().widthPixels;
		int valueWide = (int) (WIDE / size / (dMetrics.scaledDensity));
		tv.setTextSize(valueWide);
		tv.setTypeface(tv.getTypeface(), style);
	}

	public static void setTextViewFontSizeBasedOnScreenDensity(
			Activity activity, TextView tv, double size, boolean makeBold) {

		DisplayMetrics dMetrics = new DisplayMetrics();
		activity.getWindowManager().getDefaultDisplay().getMetrics(dMetrics);
		final float WIDE = activity.getResources().getDisplayMetrics().widthPixels;
		int valueWide = (int) (WIDE / size / (dMetrics.scaledDensity));
		if (makeBold) {
			tv.setTypeface(Typeface.DEFAULT_BOLD);
		}
		tv.setTextSize(valueWide);
	}

	public static void setEditTextFontSizeBasedOnScreenDensity(
			Activity activity, EditText et, double size) {

		DisplayMetrics dMetrics = new DisplayMetrics();
		activity.getWindowManager().getDefaultDisplay().getMetrics(dMetrics);
		final float WIDE = activity.getResources().getDisplayMetrics().widthPixels;
		int valueWide = (int) (WIDE / size / (dMetrics.scaledDensity));
		et.setTextSize(valueWide);
	}

	public static void setTextViewFontSizeBasedOnScreenDensity(
			Activity activity, TextView tv) {

		DisplayMetrics dMetrics = new DisplayMetrics();
		activity.getWindowManager().getDefaultDisplay().getMetrics(dMetrics);
		final float WIDE = activity.getResources().getDisplayMetrics().widthPixels;
		int valueWide = (int) (WIDE / 19.0f / (dMetrics.scaledDensity));
		tv.setTextSize(valueWide);
	}

	public static void setEditTextFontSizeBasedOnScreenDensity(
			Activity activity, EditText et) {

		DisplayMetrics dMetrics = new DisplayMetrics();
		activity.getWindowManager().getDefaultDisplay().getMetrics(dMetrics);
		final float WIDE = activity.getResources().getDisplayMetrics().widthPixels;
		int valueWide = (int) (WIDE / 19.0f / (dMetrics.scaledDensity));
		et.setTextSize(valueWide);
	}
	
	public static long getUnixTimestamp() {
		return System.currentTimeMillis() / 1000L;
	}
	
	public static String generateHmacSHA256Signature(String data, String key)
			throws GeneralSecurityException {
		// byte[] hmacData = null;
		//
		// try {
		// SecretKeySpec secretKey = new SecretKeySpec(key.getBytes("UTF-8"),
		// "HmacSHA256");
		// Mac mac = Mac.getInstance("HmacSHA256");
		// mac.init(secretKey);
		// hmacData = mac.doFinal(data.getBytes("UTF-8"));//new
		// Base64Encoder().encode(hmacData)
		// return Base64.encodeToString(hmacData, Base64.DEFAULT);
		// } catch (UnsupportedEncodingException e) {
		// throw new GeneralSecurityException(e);
		// }

		String result;
		try {
			final Charset asciiCs = Charset.forName("US-ASCII");
			final Mac sha256_HMAC = Mac.getInstance("HmacSHA256");
			final SecretKeySpec secret_key = new javax.crypto.spec.SecretKeySpec(
					asciiCs.encode(key).array(), "HmacSHA256");
			try {
				sha256_HMAC.init(secret_key);
			} catch (Exception e) {
			}
			final byte[] mac_data = sha256_HMAC.doFinal(asciiCs.encode(data)
					.array());
			result = "";
			for (final byte element : mac_data) {
				result += Integer.toString((element & 0xff) + 0x100, 16)
						.substring(1);
			}
			// System.out.println("Result:[" + result + "]");
		} catch (Exception e) {
			result = null;
		}

		return result;

	}
}
