package com.riverview.harrys.adapter;

import java.util.ArrayList;

import android.app.Activity;
import android.content.Context;
import android.graphics.Typeface;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.iapps.libs.helpers.BaseUIHelper;
import com.riverview.harrys.R;
import com.riverview.harrys.helpers.Utils;
import com.riverview.harrys.objects.BeanProductItem;

public class ProductsAdapter extends BaseAdapter implements Filterable {

	private static final String TAG = ProductsAdapter.class.getSimpleName();

	private ArrayList<BeanProductItem> mItems = new ArrayList<BeanProductItem>();
	private ArrayList<BeanProductItem> mFilteredItems = new ArrayList<BeanProductItem>();
	private Activity context;

	public ProductsAdapter(Activity context, ArrayList<BeanProductItem> list) {
		this.context = context;

		this.mFilteredItems = list;
		this.mItems = list;
	}

	@Override
	public int getCount() {
		try {
			return mFilteredItems.size();
		} catch (Exception e) {
			return 0;
		}
	}

	@Override
	public BeanProductItem getItem(int pos) {
		return mFilteredItems.get(pos);
	}

	@Override
	public long getItemId(int position) {
		return mFilteredItems.get(position).getId();
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ViewHolder holder;

		if (convertView == null) {
			holder = new ViewHolder();
			LayoutInflater inflater = (LayoutInflater) context
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

			convertView = inflater.inflate(R.layout.cell_products_item, parent,
					false);
			holder.LLRoot = (LinearLayout) convertView
					.findViewById(R.id.LLRoot);
			holder.infoStore = (TextView) convertView
					.findViewById(R.id.textViewStoreNameAddress);
			holder.imgStore = (ImageView) convertView
					.findViewById(R.id.imgStoreItem);
			/*
			 * holder.distanceStore = (TextView) convertView
			 * .findViewById(R.id.textViewStoreDistance);
			 */
			holder.title = (TextView) convertView
					.findViewById(R.id.textViewProductTitle);
			convertView.setTag(holder);

		} else {
			holder = (ViewHolder) convertView.getTag();
		}

		// set cell height by screen height ratio 30 15
		Utils.setWidthHeightByPercent(context, holder.imgStore, 0.35f, 0.15f);

		Utils.setTextViewFontSizeBasedOnScreenDensity(context,
				holder.infoStore, 30f);
		// Utils.setTextViewFontSizeBasedOnScreenDensity(context,
		// holder.distanceStore, 30f);
		Utils.setTextViewFontSizeBasedOnScreenDensity(context, holder.title,
				23f, Typeface.BOLD);

		Utils.setMarginByPercent(context, holder.LLRoot, 0.01f, true);

		BeanProductItem obj = getItem(position);
		holder.infoStore.setText(obj.getDescription());
		BaseUIHelper.loadImageWithPlaceholder(context, obj.getOrgUrl(),
				holder.imgStore, R.drawable.placeholder);
		// holder.distanceStore.setText(obj.getBase_price());
		holder.title.setText(obj.getName());

		return convertView;
	}

	public class ViewHolder {

		TextView infoStore;
		ImageView imgStore;
		// TextView distanceStore;
		LinearLayout LLRoot;
		TextView title;
	}

	@Override
	public Filter getFilter() {
		Filter filter = new Filter() {

			@SuppressWarnings("unchecked")
			@Override
			protected void publishResults(CharSequence constraint,
					FilterResults results) {
				Log.d(TAG, "publist result");
				mFilteredItems = (ArrayList<BeanProductItem>) results.values;
				notifyDataSetChanged();
			}

			@Override
			protected FilterResults performFiltering(CharSequence constraint) {
				Log.d(TAG, "performFiltering");
				String filterPhase = constraint.toString().toLowerCase();
				FilterResults results = new FilterResults();

				final ArrayList<BeanProductItem> list = mItems;
				int count = list.size();

				final ArrayList<BeanProductItem> filteredList = new ArrayList<>(
						count);

				String filterableString;
				for (int i = 0; i < count; i++) {
					filterableString = list.get(i).getName()
							.concat(list.get(i).getDescription());

					Log.i(TAG, "Filetered string  " + filterableString);

					if (filterableString.toLowerCase().contains(filterPhase)) {
						filteredList.add(list.get(i));
					}
				}

				results.values = filteredList;
				results.count = filteredList.size();

				return results;
			}
		};
		return filter;
	}
}
