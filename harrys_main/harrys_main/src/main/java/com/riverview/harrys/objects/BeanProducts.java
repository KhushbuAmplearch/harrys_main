package com.riverview.harrys.objects;

public class BeanProducts {

	private String id, company_id, brand_id, category_id, subcategory_id, code,
			title, description, base_price, merchant_id, delivery,
			self_collection, created_at, updated_at, deleted_at;

	private BeanImage image;
	private BeanBrand product_brand;
	private BeanCategory product_category;
	private BeanSubCategory product_subcategory;
	// Need to add sub category
	private BeanMerchant merchant;
	private BeanPrice produce_pricing;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getCompany_id() {
		return company_id;
	}

	public void setCompany_id(String company_id) {
		this.company_id = company_id;
	}

	public String getBrand_id() {
		return brand_id;
	}

	public void setBrand_id(String brand_id) {
		this.brand_id = brand_id;
	}

	public String getCategory_id() {
		return category_id;
	}

	public void setCategory_id(String category_id) {
		this.category_id = category_id;
	}

	public String getSubcategory_id() {
		return subcategory_id;
	}

	public void setSubcategory_id(String subcategory_id) {
		this.subcategory_id = subcategory_id;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getBase_price() {
		return base_price;
	}

	public void setBase_price(String base_price) {
		this.base_price = base_price;
	}

	public String getMerchant_id() {
		return merchant_id;
	}

	public void setMerchant_id(String merchant_id) {
		this.merchant_id = merchant_id;
	}

	public String getDelivery() {
		return delivery;
	}

	public void setDelivery(String delivery) {
		this.delivery = delivery;
	}

	public String getSelf_collection() {
		return self_collection;
	}

	public void setSelf_collection(String self_collection) {
		this.self_collection = self_collection;
	}

	public String getCreated_at() {
		return created_at;
	}

	public void setCreated_at(String created_at) {
		this.created_at = created_at;
	}

	public String getUpdated_at() {
		return updated_at;
	}

	public void setUpdated_at(String updated_at) {
		this.updated_at = updated_at;
	}

	public String getDeleted_at() {
		return deleted_at;
	}

	public void setDeleted_at(String deleted_at) {
		this.deleted_at = deleted_at;
	}

	public BeanImage getImage() {
		return image;
	}

	public void setImage(BeanImage image) {
		this.image = image;
	}

	public BeanBrand getProduct_brand() {
		return product_brand;
	}

	public void setProduct_brand(BeanBrand product_brand) {
		this.product_brand = product_brand;
	}

	public BeanCategory getProduct_category() {
		return product_category;
	}

	public void setProduct_category(BeanCategory product_category) {
		this.product_category = product_category;
	}

	public BeanSubCategory getProduct_subcategory() {
		return product_subcategory;
	}

	public void setProduct_subcategory(BeanSubCategory product_subcategory) {
		this.product_subcategory = product_subcategory;
	}

	public BeanMerchant getMerchant() {
		return merchant;
	}

	public void setMerchant(BeanMerchant merchant) {
		this.merchant = merchant;
	}

	public BeanPrice getProduce_pricing() {
		return produce_pricing;
	}

	public void setProduce_pricing(BeanPrice produce_pricing) {
		this.produce_pricing = produce_pricing;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

}
