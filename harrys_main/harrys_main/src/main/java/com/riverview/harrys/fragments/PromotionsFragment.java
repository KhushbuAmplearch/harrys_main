package com.riverview.harrys.fragments;

import static com.riverview.harrys.constant.AppConst.PULL_TO_REFRESH_TIMEOUT;

import java.util.ArrayList;
import java.util.Iterator;

import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewStub;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import com.iapps.libs.helpers.BaseHelper;
import com.iapps.libs.helpers.BaseHelper.CancelListener;
import com.iapps.libs.helpers.BaseHelper.ConfirmListener;
import com.iapps.libs.views.LoadingCompound;
import com.orangegangsters.github.swipyrefreshlayout.library.SwipyRefreshLayout;
import com.orangegangsters.github.swipyrefreshlayout.library.SwipyRefreshLayout.OnRefreshListener;
import com.orangegangsters.github.swipyrefreshlayout.library.SwipyRefreshLayoutDirection;
import com.riverview.harrys.MainActivity;
import com.riverview.harrys.R;
import com.riverview.harrys.RCGenericFragment;
import com.riverview.harrys.adapter.PromotionsAdapter;
import com.riverview.harrys.asynctask.AsyncResponse;
import com.riverview.harrys.asynctask.OffersAsyncTask;
import com.riverview.harrys.asynctask.PromotionsAsyncTask;
import com.riverview.harrys.helpers.ObjectSerializer;
import com.riverview.harrys.helpers.SharedPref;
import com.riverview.harrys.objects.BeanOffers;
import com.riverview.harrys.objects.BeanPromotion;
import com.riverview.harrys.objects.BeanPromotionItem;
import com.riverview.harrys.objects.Paginator;
import com.riverview.harrys.objects.ServiceResponse;
import com.riverview.harrys.util.AnaylaticsAppender;
import com.riverview.harrys.util.AppUtil;

public class PromotionsFragment extends RCGenericFragment implements
		OnRefreshListener,
		AsyncResponse<ServiceResponse<ArrayList<BeanOffers>>> {

	private static final String TAG = PromotionsFragment.class.getSimpleName();

	public static final int TAG_LIST_STORE = 0;

	private ListView lvStores;
	private LoadingCompound ld;
	private ViewStub emptyView;
	private SwipyRefreshLayout mSwipyRefreshLayout;

	private View v;
	private PromotionsAdapter mAdapter;
	private ArrayList<BeanPromotionItem> storeList = new ArrayList<BeanPromotionItem>();

	private PromotionsAsyncTask mPromotionsAsyncTask = null;

	private AnaylaticsAppender mAppender = null;

	private int page = 1;
	private int count = 20;
	private boolean downloaded = false;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		SharedPref.deleteString(getActivity(), SharedPref.LISTDATA_KEY);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		v = inflater.inflate(R.layout.promotions_fragment_layout, container,
				false);
		lvStores = (ListView)v.findViewById(R.id.listViewStore);
		ld = (LoadingCompound)v.findViewById(R.id.ld);
		emptyView = (ViewStub) v.findViewById(R.id.emptyView);
		mSwipyRefreshLayout = (SwipyRefreshLayout)v.findViewById(R.id.swipyrefreshlayout);
		setHasOptionsMenu(false);
		((MainActivity) getActivity()).setBackActionBar(getHome()
				.getResources().getString(R.string.rc_promotions),View.INVISIBLE,"");

		return v;
	}

	public void onPrepareOptionsMenu(Menu menu) {
	}

	@Override
	public void onResume() {
		super.onResume();

		page = 1;
		downloaded = false;
		storeList.clear();
		callAPIService(page);
		try {
			mAppender = AnaylaticsAppender.getInstance(getActivity());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);

		mSwipyRefreshLayout.setOnRefreshListener(this);
		lvStores.setOnItemClickListener(ListenerClickItem);

		lvStores.setEmptyView(emptyView);
	}

	public OnItemClickListener ListenerClickItem = new OnItemClickListener() {

		@Override
		public void onItemClick(AdapterView<?> adapter, View view, int pos,
				long arg3) {

			// Set the data as json in Shared Prefs
			try {
				SharedPref.saveString(getActivity(), SharedPref.LISTDATA_KEY,
						ObjectSerializer.serialize(storeList));
			} catch (Exception e) {
				Log.e(TAG, "Error serilization " + e.getMessage());
			}

			PromotionsDetailsFragment frag = new PromotionsDetailsFragment();
			Bundle bundle = new Bundle();
			bundle.putParcelable("promotions", storeList.get(pos));
			frag.setArguments(bundle);
			getHome().setFragment(frag);

			mAppender.updateAnaylitsData(AppUtil
					.createAnalyticsJsonEventAction("promotion",
							String.valueOf(storeList.get(pos).getId()),
							"click", null));
		}
	};

	@Override
	public void onPause() {
		super.onPause();

		if (mPromotionsAsyncTask != null) {
			mPromotionsAsyncTask.cancel(true);
		}
	}

	private boolean callAPIService(int page) {

		if (AppUtil.checkNetworkConnection(getActivity())) {

			OffersAsyncTask mOfferAsyncTask = new OffersAsyncTask(
					getActivity(), page, count);
			mOfferAsyncTask.asyncResponse = PromotionsFragment.this;
			mOfferAsyncTask.execute((Void) null);

			ld.showLoading();
			return true;

		} else {

			BaseHelper.confirm(getActivity(), "Network Unavaliable",
					"Please turn on network to connect with server",
					new ConfirmListener() {

						@Override
						public void onYes() {
							AppUtil.showSystemSettingsDialog(getActivity());
						}
					}, new CancelListener() {

						@Override
						public void onNo() {
							getHome().backToHomeScreen();

						}
					});
		}
		return false;
	}

	View.OnClickListener clickListeners = new View.OnClickListener() {
		public void onClick(View v) {

		}
	};

	@Override
	public void processFinish(ServiceResponse<ArrayList<BeanOffers>> details) {
		Log.e(TAG, "Process Finished"+details.getError_message());
		try {

			ld.hide();
			mSwipyRefreshLayout.setRefreshing(false);

			if (details != null && details.getStatus() == 200 && !details.error) {

				ArrayList<BeanOffers> list = details.getResponse();
				StringBuilder newList = new StringBuilder();

				for (Iterator<BeanOffers> iterator = list.iterator(); iterator
						.hasNext();) {
					BeanOffers beanOffers = (BeanOffers) iterator
							.next();

					BeanPromotionItem temp = new BeanPromotionItem(
							beanOffers.getId(), beanOffers.getTitle());
					temp.setDescription(beanOffers.getDescription());
					temp.setStartdate(beanOffers.getStart_date());
					temp.setEnddate(beanOffers.getEnd_date());
					temp.setTerms(beanOffers.getTerms_and_conditions());

					try {
						temp.setImageurl((BaseHelper.isEmpty(beanOffers
								.getImage()) ? null
								: beanOffers.getImage()));
					} catch (Exception e) {
						Log.e(TAG, "Offer images null");
					}
					
//					try {
//						temp.setCoupon_title(beanPromotions.getType());
//					} catch (Exception e) {
//						e.printStackTrace();
//					}

					newList.append(String.valueOf(beanOffers.getId())
							.concat("|"));

					storeList.add(temp);

				}

				if (storeList.size() > 0) {
					SharedPref.saveString(getActivity(),
							SharedPref.COUPON_LIST_KEY, newList.toString());
				}

				// Paginator related
				Paginator paginator = details.getPaginator();
				if (paginator != null) {
					int currentPagge = paginator.getCurrent_page();
					int lastPage = paginator.getLast_page();
					
					Log.d(TAG, "CP " + currentPagge);
					Log.d(TAG, "LP " + lastPage);

					if (currentPagge == lastPage) {
						// Done
						downloaded = true;
					} else {
						page = paginator.getNext_page();
					}
				} else {
					Log.d(TAG, "NULL Paginator");
					downloaded = true;
				}

				mAdapter = new PromotionsAdapter(getActivity(), storeList);
				lvStores.setAdapter(mAdapter);
				mAdapter.notifyDataSetChanged();

			} else {
				// BaseHelper.showAlert(getActivity(), "Harry's",
				// details.getError_message());
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public void onRefresh(SwipyRefreshLayoutDirection direction) {
		if (!downloaded && callAPIService(page)) {
			new Handler().postDelayed(new Runnable() {
				@Override
				public void run() {
					// Hide the refresh after n sec
					try {
						getActivity().runOnUiThread(new Runnable() {
							@Override
							public void run() {
								mSwipyRefreshLayout.setRefreshing(false);
							}
						});
					} catch (Exception e) {
					}
				}
			}, PULL_TO_REFRESH_TIMEOUT);
		} else {
			mSwipyRefreshLayout.setRefreshing(false);
		}
	}
}
