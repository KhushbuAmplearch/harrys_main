package com.riverview.harrys.fragments;

import org.json.JSONException;
import org.json.JSONObject;

import android.graphics.Typeface;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.iapps.libs.helpers.BaseUIHelper;
import com.riverview.harrys.MainActivity;
import com.riverview.harrys.R;
import com.riverview.harrys.RCGenericFragment;
import com.riverview.harrys.asynctask.AsyncResponse;
import com.riverview.harrys.asynctask.UpcByCatagoryAsyncTask;
import com.riverview.harrys.helpers.Utils;
import com.riverview.harrys.objects.BeanProductItem;
import com.riverview.harrys.objects.BeanUpcProduct;
import com.riverview.harrys.objects.ServiceResponse;
import com.riverview.harrys.util.AnaylaticsAppender;
import com.riverview.harrys.util.AppUtil;

public class UpcDetailsFragment extends RCGenericFragment implements
		AsyncResponse<ServiceResponse<BeanUpcProduct>> {

	private static final String TAG = UpcDetailsFragment.class.getSimpleName();

	private ImageView imgStoreDetail;
	private TextView textViewContent;
	private TextView textViewTitle;
	private TextView textViewDetailContent;
	private LinearLayout LLSpaceContainer;
	private LinearLayout LLSpaceContainer1;

	private View v;

	private BeanProductItem productItem;
	private UpcByCatagoryAsyncTask mUpcProductTask = null;

	private AnaylaticsAppender mAppender = null;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		// setHasOptionsMenu(true);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
							 Bundle savedInstanceState) {
		v = inflater.inflate(R.layout.upc_detail_fragment_layout, container,
				false);

		imgStoreDetail = (ImageView)v.findViewById(R.id.imgStoreDetail);
		textViewContent = (TextView)v.findViewById (R.id.textViewContent);
		textViewTitle = (TextView)v.findViewById (R.id.textViewTitle);
		textViewDetailContent = (TextView)v.findViewById (R.id.textViewDetailContent);
		LLSpaceContainer = (LinearLayout)v.findViewById(R.id.LLSpaceContainer);
		LLSpaceContainer1 = (LinearLayout)v.findViewById(R.id.LLSpaceContainer1);

		productItem = (BeanProductItem) getArguments().getParcelable("details");
		setHasOptionsMenu(false);
		((MainActivity) getActivity()).setBackActionBar(productItem.getName(),View.INVISIBLE,"");

		return v;
	}

	public void onPrepareOptionsMenu(Menu menu) {
		// getSherlockActivity().getSupportMenuInflater().inflate(
		// R.menu.menu_product_detail, menu);
	}

	@Override
	public void onResume() {
		// TODO Auto-generated method stub
		super.onResume();

		try {
			mAppender = AnaylaticsAppender.getInstance(getActivity());

			JSONObject attributes = new JSONObject();
			try {
				attributes.put("title", productItem.getName());
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			mAppender.updateAnaylitsData(AppUtil
					.createAnalyticsJsonEventAction("product",
							String.valueOf(productItem.getId()), "view",
							attributes.toString()));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);

		Utils.setMarginByPercent(getActivity(), LLSpaceContainer, .02f);
		Utils.setMarginByPercent(getActivity(), LLSpaceContainer1, .02f);

		Utils.setTextViewFontSizeBasedOnScreenDensity(getActivity(),
				textViewContent, Utils.FONT_SMALLER_DENSITY_SIZE);
		// Utils.setTextViewFontSizeBasedOnScreenDensity(getActivity(),
		// textViewProductContent, Utils.FONT_SMALLER_DENSITY_SIZE);
		Utils.setTextViewFontSizeBasedOnScreenDensity(getActivity(),
				textViewDetailContent, Utils.FONT_SMALLER_DENSITY_SIZE);
		Utils.setTextViewFontSizeBasedOnScreenDensity(getActivity(),
				textViewTitle, Utils.FONT_SMALL_DENSITY_SIZE, Typeface.BOLD);

		callApi();
	}

	private void callApi() {
		// if (AppUtil.checkNetworkConnection(getActivity())) {
		//
		// mUpcProductTask = new UpcProductsAsyncTask(getActivity(),
		// productItem.getId());
		// mUpcProductTask.asyncResponse = UpcDetailsFragment.this;
		// mUpcProductTask.execute((Void) null);
		//
		// // ld.showLoading();
		// // mPullToRefresh.setRefreshing(true);
		//
		// } else {
		//
		// BaseHelper.confirm(getActivity(), "Network Unavaliable",
		// "Please turn on network to connect with server",
		// new ConfirmListener() {
		//
		// @Override
		// public void onYes() {
		// AppUtil.showSystemSettingsDialog(getActivity());
		// }
		//
		// }, new CancelListener() {
		//
		// @Override
		// public void onNo() {
		// getHome().backToHomeScreen();
		//
		// }
		// });
		// }
		BaseUIHelper.loadImageWithPlaceholder(getActivity(), ((productItem
						.getOrgUrl() != null) ? productItem.getOrgUrl() : null),
				imgStoreDetail, R.drawable.placeholder);

		// ArrayList<BeanProducts> products = upcProduct.getProducts();
		// if (products != null && products.size() > 0) {
		// StringBuilder sb = new StringBuilder();
		// for (Iterator<BeanProducts> iterator = products.iterator();
		// iterator
		// .hasNext();) {
		// BeanProducts beanProducts = (BeanProducts) iterator.next();
		// sb.append(beanProducts.getTitle());
		// sb.append("\n");
		// }
		// textViewProductContent.setText(sb.toString());
		// } else {
		// textViewProductContent.setText("No Products avaliable");
		// }

		textViewTitle.setText(productItem.getName());
		textViewContent.setText(productItem.getDescription());
		textViewDetailContent.setText(productItem.getDetail_description());

	}

	@Override
	public void onPause() {
		super.onPause();

		if (mUpcProductTask != null) {
			mUpcProductTask.cancel(true);
		}
	}

	@Override
	public void processFinish(ServiceResponse<BeanUpcProduct> details) {

		// ld.hide();
		// mPullToRefresh.setRefreshComplete();
		// details = null;

		if (details != null && details.getStatus() == 200 && !details.error) {
			Log.i(TAG, "Upc product download Success ");

			BeanUpcProduct upcProduct = details.getResponse();

			BaseUIHelper.loadImageWithPlaceholder(getActivity(), ((upcProduct
					.getImage() != null) ? upcProduct.getImage().getOriginal()
					: null), imgStoreDetail, R.drawable.placeholder);

			// ArrayList<BeanProducts> products = upcProduct.getProducts();
			// if (products != null && products.size() > 0) {
			// StringBuilder sb = new StringBuilder();
			// for (Iterator<BeanProducts> iterator = products.iterator();
			// iterator
			// .hasNext();) {
			// BeanProducts beanProducts = (BeanProducts) iterator.next();
			// sb.append(beanProducts.getTitle());
			// sb.append("\n");
			// }
			// textViewProductContent.setText(sb.toString());
			// } else {
			// textViewProductContent.setText("No Products avaliable");
			// }

			textViewTitle.setText(upcProduct.getTitle());
			textViewContent.setText(upcProduct.getDescription());
			textViewDetailContent.setText(upcProduct
					.getAdditional_description());

		} else {
			// emptyText.setText("Unable to get UPC details");
		}

	}
}
