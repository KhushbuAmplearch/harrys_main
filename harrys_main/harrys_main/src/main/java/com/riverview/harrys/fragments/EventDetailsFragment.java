package com.riverview.harrys.fragments;

import org.apache.commons.lang3.StringEscapeUtils;
import org.json.JSONException;
import org.json.JSONObject;

import android.graphics.Typeface;
import android.os.Bundle;
import android.text.util.Linkify;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.iapps.libs.helpers.BaseHelper;
import com.iapps.libs.helpers.BaseUIHelper;
import com.iapps.libs.helpers.BaseHelper.ConfirmListener;
import com.iapps.libs.views.LoadingCompound;
import com.riverview.harrys.MainActivity;
import com.riverview.harrys.R;
import com.riverview.harrys.RCGenericFragment;
import com.riverview.harrys.asynctask.AsyncResponse;
import com.riverview.harrys.asynctask.EventContentAsyncTask;
import com.riverview.harrys.asynctask.OfferContentAsyncTask;
import com.riverview.harrys.helpers.Utils;
import com.riverview.harrys.objects.BeanEventItem;
import com.riverview.harrys.objects.ServiceResponse;
import com.riverview.harrys.util.AnaylaticsAppender;
import com.riverview.harrys.util.AppUtil;

public class EventDetailsFragment extends RCGenericFragment implements
		AsyncResponse<ServiceResponse<String>> {

	private static final String TAG = EventDetailsFragment.class
			.getSimpleName();

	private LinearLayout LLBGContainer;
	private WebView eventWebView;
	private LoadingCompound ld;

	private View v;

	private BeanEventItem eventItem;

	private AnaylaticsAppender mAppender = null;

	private EventContentAsyncTask mEventContentAsyncTask;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		eventItem = (BeanEventItem) getArguments().getParcelable("news");
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
							 Bundle savedInstanceState) {
		v = inflater.inflate(R.layout.events_detail_fragment_layout, container,
				false);

		LLBGContainer = (LinearLayout)v.findViewById(R.id.LLBGContainer);
		eventWebView = (WebView)v.findViewById(R.id.eventWebView);
		ld=(LoadingCompound)v.findViewById(R.id.ld);

		setHasOptionsMenu(false);
		((MainActivity) getActivity()).setBackActionBar(eventItem.getName(),View.INVISIBLE,"");

		return v;
	}

	@Override
	public void onResume() {
		// TODO Auto-generated method stub
		super.onResume();

		callEventContentAsyncTask(eventItem.getId());

		try {
			mAppender = AnaylaticsAppender.getInstance(getActivity());

			JSONObject attributes = new JSONObject();
			try {
				attributes.put("title", eventItem.getName());
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			mAppender
					.updateAnaylitsData(AppUtil.createAnalyticsJsonEventAction(
							"event", eventItem.getName(), "view",
							attributes.toString()));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);


	}

	private void callEventContentAsyncTask(int offerId) {
		if (AppUtil.checkNetworkConnection(getActivity())) {

			mEventContentAsyncTask = new EventContentAsyncTask(getActivity(),
					String.valueOf(offerId));
			mEventContentAsyncTask.asyncResponse = EventDetailsFragment.this;
			mEventContentAsyncTask.execute((Void) null);

		} else {
			BaseHelper.confirm(getActivity(), "Network Unavaliable",
					"Please turn on network to connect with server",
					new ConfirmListener() {

						@Override
						public void onYes() {
							AppUtil.showSystemSettingsDialog(getActivity());
						}
					});
		}

	}

	@Override
	public void processFinish(ServiceResponse<String> details) {

		ld.hide();

		if (details != null && details.getStatus() == 200 && !details.error) {
			Log.d(TAG, "Offer content downloaded");

			String webContent = StringEscapeUtils.unescapeHtml4(details
					.getResponse());

			eventWebView.getSettings().setUseWideViewPort(false);
			eventWebView.getSettings().setLoadWithOverviewMode(true);
			eventWebView.getSettings().setJavaScriptEnabled(true);

			eventWebView.loadDataWithBaseURL(null, webContent, "text/html",
					"UTF-8", null);

		} else {
			try {

				BaseHelper.showAlert(getActivity(), "Harry's",
						details.getError_message());
			} catch (Exception e) {
				// TODO: handle exception
			}
		}
	}

}
