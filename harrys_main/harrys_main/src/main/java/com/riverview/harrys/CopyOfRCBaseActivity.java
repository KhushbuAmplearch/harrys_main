/*
package com.riverview.harrys;

import java.lang.reflect.Field;
import java.util.ArrayList;

import uk.co.chrisjenx.calligraphy.CalligraphyConfig;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

import android.app.ActionBar;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewConfiguration;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import com.iapps.libs.helpers.BaseUIHelper;
import com.jeremyfeinstein.slidingmenu.lib.SlidingMenu;
import com.jeremyfeinstein.slidingmenu.lib.app.SlidingFragmentActivity;
import com.riverview.harrys.helpers.Constants;
import com.riverview.harrys.helpers.FontUtil;
import com.riverview.harrys.helpers.Utils;

public class CopyOfRCBaseActivity extends SlidingFragmentActivity implements
		ActionBar.OnNavigationListener {


	private static final String STATE_SELECTED_NAVIGATION_ITEM = "selected_navigation_item";

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		CalligraphyConfig.initDefault(Constants.GLOBAL_FONT_APP);

		// customize the SlidingMenu
		SlidingMenu sm = getSlidingMenu();
		sm.setShadowWidthRes(R.dimen.shadow_width);
		sm.setShadowDrawable(R.drawable.shadow);
		// sm.setBehindWidth(BaseConverter.convertToDp(this,
		// BaseUIHelper.getScreenWidth(this) / 4));
		sm.setBehindOffsetRes(R.dimen.slidingmenu_offset);
		sm.setFadeDegree(0.35f);
		sm.setTouchmodeMarginThreshold(BaseUIHelper.getScreenWidth(this) / 3);
		sm.setTouchModeAbove(SlidingMenu.TOUCHMODE_MARGIN);
		setSlidingActionBarEnabled(false);
		try {
			ViewConfiguration config = ViewConfiguration.get(this);
			Field menuKeyField = ViewConfiguration.class
					.getDeclaredField("sHasPermanentMenuKey");
			if (menuKeyField != null) {
				menuKeyField.setAccessible(true);
				menuKeyField.setBoolean(config, false);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	*/
/*public void setHomeActionBar() {
		// getSupportActionBar().setDisplayShowCustomEnabled(false);
		getSupportActionBar().setDisplayUseLogoEnabled(true);
		// getSupportActionBar().setDisplayShowTitleEnabled(true);
		getSupportActionBar().setDisplayHomeAsUpEnabled(false);
		getSupportActionBar().setDisplayShowHomeEnabled(true);
		getSupportActionBar().setTitle(
				getResources().getString(R.string.app_name));
		getSupportActionBar().setHomeButtonEnabled(true);
		getSupportActionBar().setIcon(R.drawable.ic_drawer);
		getSupportActionBar().setNavigationMode(
				ActionBar.NAVIGATION_MODE_STANDARD);
		// getSupportActionBar().setBackgroundDrawable(getResources().getDrawable(
		// R.drawable.bg_action ));
		int titleId = getResources().getIdentifier("action_bar_title", "id",
				"android");

		// If you're using sherlock, in older versions of android you are not
		// supposed to get a reference to android.R.id.action_bar_title, So
		// here's little hack for that.
		if (titleId == 0) {
			titleId = com.actionbarsherlock.R.id.abs__action_bar_title;
		}

		TextView actionBarTitleView = (TextView) findViewById(titleId);
		new FontUtil(this).setFontRegularType(actionBarTitleView);

	}*//*


	public void setBackActionBar(String Title) {
		*/
/*getSupportActionBar().setDisplayUseLogoEnabled(false);
		getSupportActionBar().setDisplayShowTitleEnabled(true);
		getSupportActionBar().setTitle(Title);
		getSupportActionBar().setDisplayHomeAsUpEnabled(true);
		getSupportActionBar().setDisplayShowHomeEnabled(false);
		getSupportActionBar().setNavigationMode(ActionBar.NAVIGATION_MODE_STANDARD);
*//*

		// getSupportActionBar().setBackgroundDrawable(new
		// ColorDrawable(Color.parseColor("#F0F6FD")));
		// getSupportActionBar().setBackgroundDrawable(getResources().getDrawable(
		// R.drawable.bg_action ));
		//int titleId = getResources().getIdentifier("action_bar_title", "id","android");
		int titleId=0;
		// If you're using sherlock, in older versions of android you are not
		// supposed to get a reference to android.R.id.action_bar_title, So
		// here's little hack for that.
		if (titleId == 0) {
			titleId = com.actionbarsherlock.R.id.abs__action_bar_title;
		}

		TextView actionBarTitleView = (TextView) findViewById(titleId);
		new FontUtil(this).setFontRegularType(actionBarTitleView);
	}

*/
/*	public void setDropDownActionBar(String Title, ArrayList<String> stringList) {
		getSupportActionBar().setDisplayShowCustomEnabled(false);
		// getSupportActionBar().setDisplayUseLogoEnabled(true);
		getSupportActionBar().setDisplayShowTitleEnabled(true);
		getSupportActionBar().setDisplayHomeAsUpEnabled(true);
		// getSupportActionBar().setDisplayShowHomeEnabled(true);
		getSupportActionBar().setTitle(Title);
		getSupportActionBar().setHomeButtonEnabled(true);
		// getSupportActionBar().setIcon(R.drawable.ic_launcher);

		getSupportActionBar().setNavigationMode(ActionBar.NAVIGATION_MODE_LIST);

		SpinnerAdapter spinAdapter = new SpinnerAdapter(this,
				android.R.layout.simple_spinner_item, stringList);

		spinAdapter
				.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

		// Set up the dropdown list navigation in the action bar.
		getSupportActionBar().setListNavigationCallbacks(spinAdapter, this);

	}*//*


*/
/*
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater = this.getSupportMenuInflater();
		inflater.inflate(R.menu.menu_product, menu);
		return super.onCreateOptionsMenu(menu);
	}

	@Override
	public void onRestoreInstanceState(Bundle savedInstanceState) {
		// Restore the previously serialized current dropdown position.
		if (savedInstanceState.containsKey(STATE_SELECTED_NAVIGATION_ITEM)) {
			getActionBar().setSelectedNavigationItem(
					savedInstanceState.getInt(STATE_SELECTED_NAVIGATION_ITEM));
		}
	}
*//*


	@Override
	public void onSaveInstanceState(Bundle outState) {
		// Serialize the current dropdown position.
		//outState.putInt(STATE_SELECTED_NAVIGATION_ITEM, getActionBar().getSelectedNavigationIndex());
	}

	*/
/*@Override
	protected void attachBaseContext(Context newBase) {
		super.attachBaseContext(new CalligraphyContextWrapper(newBase));
	}*//*


	@Override
	public boolean onNavigationItemSelected(int itemPosition, long itemId) {

		return true;
	}

	private class SpinnerAdapter extends ArrayAdapter<String> {
		Context context;
		ArrayList<String> items = new ArrayList<String>();

		public SpinnerAdapter(final Context context,
							  final int textViewResourceId, ArrayList<String> objects) {
			super(context, textViewResourceId, objects);
			this.items = objects;
			this.context = context;
		}

		@Override
		public View getDropDownView(int position, View convertView,
									ViewGroup parent) {

			if (convertView == null) {
				LayoutInflater inflater = LayoutInflater.from(context);
				convertView = inflater.inflate(
						android.R.layout.simple_spinner_item, parent, false);
			}

			TextView tv = (TextView) convertView
					.findViewById(android.R.id.text1);
			tv.setText(items.get(position));
			try {
				Utils.setTextViewFontSizeBasedOnScreenDensity(
						CopyOfRCBaseActivity.this, tv);
			} catch (Exception e) {
			}
			return convertView;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			if (convertView == null) {
				LayoutInflater inflater = LayoutInflater.from(context);
				convertView = inflater.inflate(
						android.R.layout.simple_spinner_item, parent, false);
			}

			// android.R.id.text1 is default text view in resource of the
			// android.
			// android.R.layout.simple_spinner_item is default layout in
			// resources of android.

			TextView tv = (TextView) convertView
					.findViewById(android.R.id.text1);
			tv.setText(items.get(position));
			try {
				Utils.setTextViewFontSizeBasedOnScreenDensity(
						CopyOfRCBaseActivity.this, tv);
			} catch (Exception e) {
			}
			return convertView;
		}
	}

}
*/
