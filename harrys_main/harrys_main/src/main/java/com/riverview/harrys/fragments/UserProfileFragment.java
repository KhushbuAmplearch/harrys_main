package com.riverview.harrys.fragments;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import org.apache.commons.lang3.text.WordUtils;
import org.joda.time.DateTime;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;


import com.iapps.libs.helpers.BaseHelper;
import com.iapps.libs.helpers.BaseHelper.CancelListener;
import com.iapps.libs.helpers.BaseHelper.ConfirmListener;
import com.riverview.harrys.MainActivity;
import com.riverview.harrys.R;
import com.riverview.harrys.RCGenericFragment;
import com.riverview.harrys.asynctask.AsyncResponse;
import com.riverview.harrys.asynctask.UserDataAsyncTask;
import com.riverview.harrys.asynctask.UserUpdateAsyncTask;
import com.riverview.harrys.helpers.Constants;
import com.riverview.harrys.helpers.SharedPref;
import com.riverview.harrys.helpers.Utils;
import com.riverview.harrys.objects.BeanMobileUser;
import com.riverview.harrys.objects.BeanSignUp;
import com.riverview.harrys.objects.ServiceResponse;
import com.riverview.harrys.util.AnaylaticsAppender;
import com.riverview.harrys.util.AppUtil;

public class UserProfileFragment extends RCGenericFragment implements
		AsyncResponse<ServiceResponse<BeanMobileUser>> {

	private static final String TAG = UserProfileFragment.class.getSimpleName();

	private LinearLayout LLProfileSubmit;
	private LinearLayout LLPasswordChange;
	private EditText editTextUserId;
	private EditText editTextFirstName;
	private EditText editTextLastName;
	private EditText editTextMobile;
	private EditText editTextEmail;
	private EditText editTextDob;
	private EditText editTextPostelCode;
	private EditText editTextGender;
	private EditText editTextNationality;
	private EditText editTextIdentification;
	private EditText editTextAddressLine1;
	private EditText editTextAddressLine2;
	private TextView tv1;
	private TextView tv2;
	private TextView tv3;
	private TextView tv4;
	private TextView tv5;
	private TextView tv6;
	private TextView tv7;
	private TextView tv8;
	private TextView tv9;
	private TextView tv10;
	private TextView tv11;
	private TextView tv12;
	private TextView tv13;
	private TextView tv14;
	private Button buttonSubmit;
	Calendar myCalendar;
	private Dialog dialog;
	private DatePickerDialog datePickerDialog;

	View v;

	private boolean userAddressFlag = false;
	private final String DATEPICKER_TAG = "datepicker";

	// private final String[] natArray =null;

	private UserDataAsyncTask mUserProfileAsyncTask = null;

	private AnaylaticsAppender mAppender = null;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
							 Bundle savedInstanceState) {
		v = inflater
				.inflate(R.layout.profile_fragment_layout, container, false);

		LLProfileSubmit = (LinearLayout)v.findViewById (R.id.LLProfileSubmit);
		LLPasswordChange = (LinearLayout)v.findViewById (R.id.LLPasswordChange);
		editTextUserId = (EditText)v.findViewById (R.id.etUserID);
		editTextFirstName = (EditText)v.findViewById (R.id.etFirstName);
		editTextLastName = (EditText)v.findViewById (R.id.etLastName);
		editTextMobile = (EditText)v.findViewById (R.id.etMobileNumber);
		editTextEmail = (EditText)v.findViewById (R.id.etEmail);
		editTextDob = (EditText)v.findViewById (R.id.etDob);
		editTextPostelCode = (EditText)v.findViewById (R.id.etPostelCode);
		editTextGender = (EditText)v.findViewById (R.id.etGender);
		editTextNationality = (EditText)v.findViewById (R.id.etNationality);
		editTextIdentification = (EditText)v.findViewById (R.id.etIdentificationNo);
		editTextAddressLine1 = (EditText)v.findViewById (R.id.etAddressLine1);
		editTextAddressLine2 = (EditText)v.findViewById (R.id.etAddressLine2);
		tv1 = (TextView)v.findViewById(R.id.tv1);
		tv2 = (TextView)v.findViewById(R.id.tv2);
		tv3 = (TextView)v.findViewById(R.id.tv3);
		tv4 = (TextView)v.findViewById(R.id.tv4);
		tv5 = (TextView)v.findViewById(R.id.tv5);
		tv6 = (TextView)v.findViewById(R.id.tv6);
		tv7 = (TextView)v.findViewById(R.id.tv7);
		tv8 = (TextView)v.findViewById(R.id.tv8);
		tv9 = (TextView)v.findViewById(R.id.tv9);
		tv10 = (TextView)v.findViewById(R.id.tv10);
		tv11 = (TextView)v.findViewById(R.id.tv11);
		tv12 = (TextView)v.findViewById(R.id.tv12);
		tv13 = (TextView)v.findViewById(R.id.tv13);
		tv14 = (TextView)v.findViewById(R.id.tv14);
		buttonSubmit = (Button)v.findViewById(R.id.buttonSignUp);
		Date date = new Date();
		myCalendar = new GregorianCalendar();
		myCalendar.setTime(date);
		myCalendar.add(Calendar.YEAR, -18);
		setHasOptionsMenu(false);
		((MainActivity) getActivity()).setBackActionBar(getActivity()
				.getResources().getString(R.string.rc_menu_profile),View.INVISIBLE,"");

		return v;
	}

	public void onPrepareOptionsMenu(Menu menu) {
		// getSherlockActivity().getSupportMenuInflater().inflate(
		// R.menu.menu_profile, menu);
	}

	@Override
	public void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		try {
			mAppender = AnaylaticsAppender.getInstance(getActivity());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public void onDestroy() {

		super.onDestroy();
		//hideDefaultKeyboard();
		Utils.hideKeyboard(getActivity());
	}

	@Override
	public void onDetach() {
		super.onDetach();
		//hideDefaultKeyboard();
		Utils.hideKeyboard(getActivity());
	}

	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);

		Utils.setHeightByPercent(getActivity(), LLProfileSubmit, 0.08f);

		Utils.setTextViewFontSizeBasedOnScreenDensity(getActivity(),
				editTextUserId, Utils.FONT_SMALL_DENSITY_SIZE);
		Utils.setTextViewFontSizeBasedOnScreenDensity(getActivity(),
				editTextFirstName, Utils.FONT_SMALL_DENSITY_SIZE);
		Utils.setTextViewFontSizeBasedOnScreenDensity(getActivity(),
				editTextLastName, Utils.FONT_SMALL_DENSITY_SIZE);
		Utils.setTextViewFontSizeBasedOnScreenDensity(getActivity(),
				editTextMobile, Utils.FONT_SMALL_DENSITY_SIZE);
		Utils.setTextViewFontSizeBasedOnScreenDensity(getActivity(),
				editTextEmail, Utils.FONT_SMALL_DENSITY_SIZE);
		Utils.setTextViewFontSizeBasedOnScreenDensity(getActivity(),
				editTextPostelCode, Utils.FONT_SMALL_DENSITY_SIZE);
		Utils.setTextViewFontSizeBasedOnScreenDensity(getActivity(),
				editTextDob, Utils.FONT_SMALL_DENSITY_SIZE);
		Utils.setTextViewFontSizeBasedOnScreenDensity(getActivity(),
				editTextGender, Utils.FONT_SMALL_DENSITY_SIZE);
		Utils.setTextViewFontSizeBasedOnScreenDensity(getActivity(),
				editTextNationality, Utils.FONT_SMALL_DENSITY_SIZE);
		Utils.setTextViewFontSizeBasedOnScreenDensity(getActivity(),
				editTextIdentification, Utils.FONT_SMALL_DENSITY_SIZE);
		Utils.setTextViewFontSizeBasedOnScreenDensity(getActivity(),
				editTextAddressLine1, Utils.FONT_SMALL_DENSITY_SIZE);
		Utils.setTextViewFontSizeBasedOnScreenDensity(getActivity(),
				editTextAddressLine2, Utils.FONT_SMALL_DENSITY_SIZE);

		Utils.setTextViewFontSizeBasedOnScreenDensity(getActivity(), tv1, 23f);
		Utils.setTextViewFontSizeBasedOnScreenDensity(getActivity(), tv2, 23f);
		Utils.setTextViewFontSizeBasedOnScreenDensity(getActivity(), tv3, 23f);
		Utils.setTextViewFontSizeBasedOnScreenDensity(getActivity(), tv4, 23f);
		Utils.setTextViewFontSizeBasedOnScreenDensity(getActivity(), tv5, 23f);
		Utils.setTextViewFontSizeBasedOnScreenDensity(getActivity(), tv6, 23f);
		Utils.setTextViewFontSizeBasedOnScreenDensity(getActivity(), tv7, 23f);
		Utils.setTextViewFontSizeBasedOnScreenDensity(getActivity(), tv8, 23f);
		Utils.setTextViewFontSizeBasedOnScreenDensity(getActivity(), tv9, 23f);
		Utils.setTextViewFontSizeBasedOnScreenDensity(getActivity(), tv10, 23f);
		Utils.setTextViewFontSizeBasedOnScreenDensity(getActivity(), tv13, 23f);
		Utils.setTextViewFontSizeBasedOnScreenDensity(getActivity(), tv14, 23f);

		Utils.setTextViewFontSizeBasedOnScreenDensity(getActivity(), tv11, 20f,
				Typeface.BOLD);
		Utils.setTextViewFontSizeBasedOnScreenDensity(getActivity(), tv12, 20f,
				Typeface.BOLD);

		Utils.setButtonFontSizeBasedOnScreenDensity(getActivity(),
				buttonSubmit, Utils.FONT_SMALL_DENSITY_SIZE, Typeface.BOLD);
		// Utils.setTextViewFontSizeBasedOnScreenDensity(getActivity(),
		// tvSignUp,
		// 18f);

		// tvSignUp.setText("Submit");
		buttonSubmit.setText("Submit");
		buttonSubmit.setEnabled(false);
		buttonSubmit.setOnClickListener(clickListeners);
		LLPasswordChange.setOnClickListener(clickListeners);

		editTextGender.setOnTouchListener(new OnTouchListener() {

			@Override
			public boolean onTouch(View v, MotionEvent event) {
				showSpinnerDialog();
				return false;
			}
		});

		editTextNationality.setOnTouchListener(new OnTouchListener() {

			@Override
			public boolean onTouch(View v, MotionEvent event) {
				showNationalitySpinnerDialog();
				return false;
			}
		});

		// call API to retrieve mobile user details
		setupCalendar();
		callAPI();
	}

	public void setupCalendar() {

		editTextDob.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {

				new android.app.DatePickerDialog(getActivity(), date, myCalendar
						.get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
						myCalendar.get(Calendar.DAY_OF_MONTH)).show();

			}
		});
	}
	android.app.DatePickerDialog.OnDateSetListener date = new android.app.DatePickerDialog.OnDateSetListener() {

		@Override
		public void onDateSet(DatePicker view, int year, int monthOfYear,
							  int dayOfMonth) {
			// TODO Auto-generated method stub
			myCalendar.set(Calendar.YEAR, year);
			myCalendar.set(Calendar.MONTH, monthOfYear);
			myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);

			DateTime dt = DateTime.now().withYear(myCalendar.get(Calendar.YEAR))
					.withMonthOfYear(myCalendar.get(Calendar.MONTH) + 1)
					.withDayOfMonth(myCalendar.get(Calendar.DAY_OF_MONTH));
			editTextDob.setText(dt.toString(Constants.DATE_JSON));
		}

	};


	@Override
	public void onPause() {
		super.onPause();

		if (mUserProfileAsyncTask != null) {
			mUserProfileAsyncTask.cancel(true);
		}
	}

	private void callAPI() {
		if (AppUtil.checkNetworkConnection(getActivity())) {

			UserDataAsyncTask mUserProfileAsyncTask = new UserDataAsyncTask(
					getActivity(), SharedPref.getInteger(getHome(),
					SharedPref.USERID_KEY, 0));
			mUserProfileAsyncTask.asyncResponse = UserProfileFragment.this;
			mUserProfileAsyncTask.execute((Void) null);

		} else {

			BaseHelper.confirm(getActivity(), "Network Unavaliable",
					"Please turn on network to connect with server",
					new ConfirmListener() {

						@Override
						public void onYes() {
							AppUtil.showSystemSettingsDialog(getActivity());
						}
					}, new CancelListener() {

						@Override
						public void onNo() {
							getHome().backToHomeScreen();

						}
					});
		}
	}

	OnTouchListener ontouchedittextlisteners = new OnTouchListener() {

		@Override
		public boolean onTouch(View v, MotionEvent event) {
			if (event.getAction() == MotionEvent.ACTION_UP) {
				hideDefaultKeyboard();
				// enableCustKeyboard();
				return true;
			}
			return false;
		}
	};

	private void hideDefaultKeyboard() {
		getActivity().getWindow().setSoftInputMode(
				WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

	}

	View.OnClickListener clickListeners = new View.OnClickListener() {
		public void onClick(View v) {

			switch (v.getId()) {
				case R.id.LLSubmit:
					getHome().backToHomeScreen();
					break;

				case R.id.buttonSignUp:
					updateProfile();

					break;

				case R.id.LLPasswordChange:
					getHome().setFragment(new ChangePasswordFragment());

					break;

				default:
					break;
			}

		}
	};

	private void updateProfile() {

		if (AppUtil.checkNetworkConnection(getActivity())) {

			BeanSignUp beanSignUp = new BeanSignUp();

			beanSignUp.setUsername(editTextUserId.getText().toString().trim());
			beanSignUp.setEmail(editTextEmail.getText().toString().trim());
			beanSignUp.setFirstname(editTextFirstName.getText().toString()
					.trim());
			beanSignUp.setSurname(editTextLastName.getText().toString().trim());
			beanSignUp.setMobilenumber(editTextMobile.getText().toString()
					.trim());
			beanSignUp.setPostalcode(editTextPostelCode.getText().toString()
					.trim());

			// Optional fields

			beanSignUp.setGender(editTextGender.getText().toString().toUpperCase());
			beanSignUp.setDob(editTextDob.getText().toString());
			beanSignUp.setNationality(editTextNationality.getText().toString()
					.trim());
			if(!editTextIdentification.getText().toString().trim().equalsIgnoreCase("")) {
				beanSignUp.setIdentification(editTextIdentification.getText().toString().trim());
			}
			// Address Line 1 & 2
			beanSignUp.setAddressLine1(editTextAddressLine1.getText()
					.toString().trim());
			beanSignUp.setAddressLine2(editTextAddressLine2.getText()
					.toString().trim());

			mobileUserUpdate(beanSignUp);

			mAppender.updateAnaylitsData(AppUtil
					.createAnalyticsJsonEventAction("profile",
							null,
							"update", null));

		} else {
			BaseHelper.confirm(getActivity(), "Network Unavaliable",
					"Please turn on network to connect with server",
					new ConfirmListener() {

						@Override
						public void onYes() {
							AppUtil.showSystemSettingsDialog(getActivity());
						}
					});
		}

		hideDefaultKeyboard();
		// }

	}

	private void mobileUserUpdate(BeanSignUp beanSignUp) {
		UserUpdateAsyncTask mAuthTask = new UserUpdateAsyncTask(getHome(),
				beanSignUp, SharedPref.getInteger(getHome(),
				SharedPref.USERID_KEY, 0));
		mAuthTask.execute((Void) null);
	}

	public void showSpinnerDialog() {
		AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
		String[] gender = { "Male", "Female" };

		builder.setTitle("Choose a gender");
		builder.setItems(gender, new DialogInterface.OnClickListener() {

			@Override
			public void onClick(DialogInterface dialog, int pos) {

				if (pos == 0)
					editTextGender.setText("Male");
				else
					editTextGender.setText("Female");
			}
		});

		builder.setNegativeButton("Cancel",
				new DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {

					}
				});

		try {
			if (!dialog.isShowing()) {
				dialog = builder.create();
				dialog.show();
			}
		} catch (Exception e) {
			try {
				dialog = builder.create();
				dialog.show();
			} catch (Exception e1) {
			}
		}
	}

	public void showNationalitySpinnerDialog() {
		AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

		try {
			JSONObject natinalityObject = new JSONObject(
					AppUtil.getNationalityJSON(getHome()));
			JSONArray array = natinalityObject.getJSONArray("nat");
			final String[] natArray = new String[array.length()];

			for (int i = 0; i < array.length(); i++) {
				natArray[i] = array.getString(i);
			}

			builder.setTitle("Choose your nationality");
			builder.setItems(natArray, new DialogInterface.OnClickListener() {

				@Override
				public void onClick(DialogInterface dialog, int pos) {
					editTextNationality.setText(natArray[pos]);
				}
			});

			builder.setNegativeButton("Cancel",
					new DialogInterface.OnClickListener() {

						@Override
						public void onClick(DialogInterface dialog, int which) {

						}
					});

			try {
				if (!dialog.isShowing()) {
					dialog = builder.create();
					dialog.show();
				}
			} catch (Exception e) {
				try {
					dialog = builder.create();
					dialog.show();
				} catch (Exception e1) {
				}
			}

		} catch (JSONException e2) {
		} catch (Exception e) {
		}
	}

	@Override
	public void processFinish(ServiceResponse<BeanMobileUser> details) {
		Log.d(TAG, "Process Finished");

		if (details == null) {
			BaseHelper.showAlert(getActivity(), "Fetch mobile user data Error");
			return;
		}

		if (details != null && details.getStatus() == 200 && !details.error) {
			Log.i(TAG, "User data download Success ");

			BeanMobileUser user = details.getResponse();

			String GenderString = user.getGender().toLowerCase();
			if(GenderString.equals("male") || GenderString.equals("female")){
				GenderString = Character.toString(GenderString.charAt(0)).toUpperCase()+ GenderString.substring(1);
				editTextGender.setText(GenderString);
			}

			editTextUserId.setText(user.getUsername());
			editTextFirstName.setText(user.getFirstname());
			editTextLastName.setText(user.getSurname());
			editTextMobile.setText(user.getMobilenumber());
			editTextEmail.setText(user.getEmail());
			editTextPostelCode.setText(user.getPost_code());
			editTextIdentification.setText(user.getIcno());

			Log.d(TAG, "Dob " + user.getDob());
			if (BaseHelper.isEmpty(user.getDob())
					| user.getDob().equalsIgnoreCase("0000-00-00")
					| user.getDob().equalsIgnoreCase("0001-11-30")) {
				Log.d(TAG, "Dob null");
				// DOB empty. Make the field editable
				editTextDob.setFocusable(true);
				editTextDob.setEnabled(true);
			} else {
				Log.d(TAG, "Dob not null");
				editTextDob.setText(AppUtil.convertDate(getHome(),
						user.getDob(), false));
			}

			if (BaseHelper.isEmpty(user.getNationality())) {
				editTextNationality.setText("Singaporean");
			} else {
				editTextNationality.setText(user.getNationality());
			}

			editTextAddressLine1.setText(user.getAddress_line_1());
			if (!BaseHelper.isEmpty(user.getAddress_line_1())) {
				userAddressFlag = true;
			}
			editTextAddressLine2.setText(user.getAddress_line_2());
			// enable button
			buttonSubmit.setEnabled(true);

			// detect mobile number update and if yes verify it from OTP.
			String verifyMobile = details.getResponse()
					.getPendingMobileUpdate();
			Log.d(TAG, "Verify " + verifyMobile);

			if (!BaseHelper.isEmpty(verifyMobile)
					&& !Boolean.valueOf(verifyMobile)) {
				try {
					BaseHelper.confirm(getHome(), getString(R.string.app_name),
							getString(R.string.rc_verify_request),
							confirmListener1, new CancelListener() {

								@Override
								public void onNo() {
									// getHome().backToHomeScreen();

								}
							});
				} catch (Exception e) {
					e.printStackTrace();
				}
			}

		} else {
			BaseHelper
					.showAlert(getActivity(), "Mobile user data fetch Error ",
							details.getError_message());
		}

	}

	ConfirmListener confirmListener1 = new ConfirmListener() {

		@Override
		public void onYes() {
			((MainActivity) getActivity())
					.setFragment(new VerifyMobileOTPFragment());
		}
	};

//	@Override
//	public void onDateSet(DatePickerDialog datePickerDialog, int year,
//			int month, int day) {
//		DateTime dt = DateTime.now().withYear(year).withMonthOfYear(month + 1)
//				.withDayOfMonth(day);
//		editTextDob.setText(dt.toString(Constants.DATE_JSON));
//
//	}

}
