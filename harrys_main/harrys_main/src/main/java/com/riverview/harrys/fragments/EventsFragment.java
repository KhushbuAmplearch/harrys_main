package com.riverview.harrys.fragments;

import static com.riverview.harrys.constant.AppConst.PULL_TO_REFRESH_TIMEOUT;

import java.util.ArrayList;
import java.util.Iterator;

import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewStub;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;


import com.iapps.libs.helpers.BaseHelper;
import com.iapps.libs.helpers.BaseHelper.CancelListener;
import com.iapps.libs.helpers.BaseHelper.ConfirmListener;
import com.iapps.libs.views.LoadingCompound;
import com.orangegangsters.github.swipyrefreshlayout.library.SwipyRefreshLayout;
import com.orangegangsters.github.swipyrefreshlayout.library.SwipyRefreshLayout.OnRefreshListener;
import com.orangegangsters.github.swipyrefreshlayout.library.SwipyRefreshLayoutDirection;
import com.riverview.harrys.MainActivity;
import com.riverview.harrys.R;
import com.riverview.harrys.RCGenericFragment;
import com.riverview.harrys.adapter.EventsAdapter;
import com.riverview.harrys.asynctask.AsyncResponse;
import com.riverview.harrys.asynctask.EventsAsyncTask;
import com.riverview.harrys.helpers.ObjectSerializer;
import com.riverview.harrys.helpers.SharedPref;
import com.riverview.harrys.objects.BeanEventItem;
import com.riverview.harrys.objects.BeanEvents;
import com.riverview.harrys.objects.ServiceResponse;
import com.riverview.harrys.util.AnaylaticsAppender;
import com.riverview.harrys.util.AppUtil;

public class EventsFragment extends RCGenericFragment implements
		OnRefreshListener,
		AsyncResponse<ServiceResponse<ArrayList<BeanEvents>>> {

	private static final String TAG = EventsFragment.class.getSimpleName();

	public static final int TAG_LIST_STORE = 0;

	private ListView lvStores;
	private LoadingCompound ld;
	private ViewStub emptyView;
	private SwipyRefreshLayout mSwipyRefreshLayout;

	private View v;
	private EventsAdapter mAdapter;
	private ArrayList<BeanEventItem> storeList;


	private EventsAsyncTask mEventsTask = null;

	private AnaylaticsAppender mAppender = null;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		SharedPref.deleteString(getActivity(), SharedPref.LISTDATA_KEY);
	}

	@Override
	public void onResume() {
		super.onResume();

		try {
			callAPI();
			mAppender = AnaylaticsAppender.getInstance(getActivity());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
							 Bundle savedInstanceState) {
		v = inflater.inflate(R.layout.news_fragment_layout, container, false);

		lvStores = (ListView)v.findViewById(R.id.listViewStore);
		ld = (LoadingCompound)v.findViewById(R.id.ld);
		emptyView= (ViewStub)v.findViewById(R.id.emptyView);
		mSwipyRefreshLayout = (SwipyRefreshLayout)v.findViewById(R.id.swipyrefreshlayout);

		setHasOptionsMenu(false);
		((MainActivity) getActivity()).setBackActionBar(getActivity()

				.getResources().getString(R.string.rc_menu_events),View.INVISIBLE,"");

		return v;
	}


	public void onPrepareOptionsMenu(Menu menu) {
		// getSherlockActivity().getSupportMenuInflater().inflate(
		// R.menu.menu_news, menu);
	}

	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);

		mSwipyRefreshLayout.setOnRefreshListener(this);
		lvStores.setOnItemClickListener(ListenerClickItem);

		lvStores.setEmptyView(emptyView);
	}

	@SuppressWarnings("unchecked")
	public void callAPI() {

		// get saved json data
		String serilizedJsonString = SharedPref.getString(getActivity(),
				SharedPref.LISTDATA_KEY, null);
		if (serilizedJsonString != null) {
			try {
				storeList = (ArrayList<BeanEventItem>) ObjectSerializer
						.deserialize(serilizedJsonString);

				mAdapter = new EventsAdapter(getActivity(), storeList);
				lvStores.setAdapter(mAdapter);
				mAdapter.notifyDataSetChanged();

				ld.hide();
				// mPullToRefresh.setRefreshComplete();

			} catch (Exception e) {
				callAPIService();
				Log.e(EventsFragment.TAG,
						"Error deserilization " + e.getMessage());
			}
		} else {
			callAPIService();
		}
	}

	private boolean callAPIService() {

		if (AppUtil.checkNetworkConnection(getActivity())) {

			mEventsTask = new EventsAsyncTask(getActivity());
			mEventsTask.asyncResponse = EventsFragment.this;
			mEventsTask.execute((Void) null);

			// LLEmptyContainer.setVisibility(View.GONE);

			ld.bringToFront();
			ld.showLoading();

			return true;
		} else {

			BaseHelper.confirm(getActivity(), "Network Unavaliable",
					"Please turn on network to connect with server",
					new ConfirmListener() {

						@Override
						public void onYes() {
							AppUtil.showSystemSettingsDialog(getActivity());
						}
					}, new CancelListener() {

						@Override
						public void onNo() {
							getHome().backToHomeScreen();

						}
					});
		}
		return false;
	}

	public OnItemClickListener ListenerClickItem = new OnItemClickListener() {

		@Override
		public void onItemClick(AdapterView<?> adapter, View view, int pos,
								long arg3) {

			// Set the data as json in Shared Prefs
			try {
				SharedPref.saveString(getActivity(), SharedPref.LISTDATA_KEY,
						ObjectSerializer.serialize(storeList));
			} catch (Exception e) {
				Log.e(TAG, "Error serilization " + e.getMessage());
			}

			EventDetailsFragment frag = new EventDetailsFragment();
			Bundle bundle = new Bundle();
			bundle.putParcelable("news", storeList.get(pos));
			frag.setArguments(bundle);
			getHome().setFragment(frag);

			mAppender.updateAnaylitsData(AppUtil
					.createAnalyticsJsonEventAction("event",
							String.valueOf(storeList.get(pos).getId()),
							"click", null));

		}
	};

	View.OnClickListener clickListeners = new View.OnClickListener() {
		public void onClick(View v) {

		}
	};

	@Override
	public void onPause() {
		super.onPause();

		if (mEventsTask != null) {
			mEventsTask.cancel(true);
		}
	}

	@Override
	public void processFinish(ServiceResponse<ArrayList<BeanEvents>> details) {
		Log.d(TAG, "Process Finished");

		ld.hide();
		mSwipyRefreshLayout.setRefreshing(false);

		if (details != null && details.getStatus() == 200 && !details.error) {
			Log.i(TAG, "News download Success ");

			storeList = new ArrayList<BeanEventItem>();

			ArrayList<BeanEvents> list = (ArrayList<BeanEvents>) details
					.getResponse();
			// if(list.size() == 0) {
			// LLEmptyContainer.setVisibility(View.VISIBLE);
			// }

			for (Iterator<BeanEvents> iterator = list.iterator(); iterator
					.hasNext();) {
				BeanEvents beanEvents = (BeanEvents) iterator.next();

				BeanEventItem temp = new BeanEventItem(beanEvents.getId(),
						beanEvents.getTitle());

				temp.setDescription(beanEvents.getDescription());
				temp.setAdditionalDes(beanEvents.getMore_details());

				try {
					temp.setStartdate(AppUtil.convertDate(getHome(),
							beanEvents.getStart_date(), false));
					temp.setEnddate(AppUtil.convertDate(getHome(),
							beanEvents.getEnd_date(), false));
				} catch (Exception e) {
				}
				try {
					// temp.setImageurl(beanEvents.getImage().getOriginal());
					temp.setImageurl((BaseHelper.isEmpty(beanEvents.getImage()) ? null
							: beanEvents.getImage()));
				} catch (Exception e) {
					Log.e(TAG, "Promotion images null");
				}

				storeList.add(temp);

			}

			mAdapter = new EventsAdapter(getActivity(), storeList);
			lvStores.setAdapter(mAdapter);
			mAdapter.notifyDataSetChanged();

		} else {
			try {

				BaseHelper.showAlert(getActivity(), "Harry's",
						details.getError_message());
			} catch (Exception e) {
				// TODO: handle exception
			}
		}
	}

	@Override
	public void onRefresh(SwipyRefreshLayoutDirection direction) {
		if (callAPIService()) {
			new Handler().postDelayed(new Runnable() {
				@Override
				public void run() {
					// Hide the refresh after n sec
					try {
						getActivity().runOnUiThread(new Runnable() {
							@Override
							public void run() {
								mSwipyRefreshLayout.setRefreshing(false);
							}
						});
					} catch (Exception e) {
					}
				}
			}, PULL_TO_REFRESH_TIMEOUT);
		} else {
			mSwipyRefreshLayout.setRefreshing(false);
		}
	}

}
