package com.riverview.harrys.objects;

import java.io.Serializable;

import android.os.Parcel;
import android.os.Parcelable;

public class BeanEventItem implements Parcelable, Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7218415002174186972L;

	public static final String OBJ_NAME = "BeanEventItem";

	private int id;
	private String name, description, startdate, enddate, imageurl, terms,
			additionalDes, categoryName;

	public BeanEventItem(int id, String name) {
		super();
		this.id = id;
		this.name = name;

	}

	public BeanEventItem(Parcel parcel) {
		super();
		this.id = parcel.readInt();
		this.name = parcel.readString();
		this.description = parcel.readString();
		this.startdate = parcel.readString();
		this.enddate = parcel.readString();
		this.imageurl = parcel.readString();
		this.terms = parcel.readString();
		this.additionalDes = parcel.readString();
		this.categoryName = parcel.readString();
	}

	public static final Parcelable.Creator<BeanEventItem> CREATOR = new Parcelable.Creator<BeanEventItem>() {

		@Override
		public BeanEventItem createFromParcel(Parcel source) {
			return new BeanEventItem(source);
		}

		@Override
		public BeanEventItem[] newArray(int size) {
			return new BeanEventItem[size];
		}
	};

	@Override
	public int describeContents() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public void writeToParcel(Parcel out, int flag) {
		out.writeInt(id);
		out.writeString(name);
		out.writeString(description);
		out.writeString(startdate);
		out.writeString(enddate);
		out.writeString(imageurl);
		out.writeString(terms);
		out.writeString(additionalDes);
		out.writeString(categoryName);
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getStartdate() {
		return startdate;
	}

	public void setStartdate(String startdate) {
		this.startdate = startdate;
	}

	public String getEnddate() {
		return enddate;
	}

	public void setEnddate(String enddate) {
		this.enddate = enddate;
	}

	public String getImageurl() {
		return imageurl;
	}

	public void setImageurl(String imageurl) {
		this.imageurl = imageurl;
	}

	public String getTerms() {
		return terms;
	}

	public void setTerms(String terms) {
		this.terms = terms;
	}

	public String getAdditionalDes() {
		return additionalDes;
	}

	public void setAdditionalDes(String additionalDes) {
		this.additionalDes = additionalDes;
	}

	public String getCategoryName() {
		return categoryName;
	}

	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}

}
