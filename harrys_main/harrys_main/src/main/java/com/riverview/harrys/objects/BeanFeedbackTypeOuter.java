package com.riverview.harrys.objects;

import java.util.ArrayList;

public class BeanFeedbackTypeOuter {

	public static final String OBJ_NAME = "BeanFeedback";

	private ArrayList<BeanFeedbackType> feedback_types;

	public ArrayList<BeanFeedbackType> getFeedback_types() {
		return feedback_types;
	}

	public void setFeedback_types(ArrayList<BeanFeedbackType> feedback_types) {
		this.feedback_types = feedback_types;
	}

}
