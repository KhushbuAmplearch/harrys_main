package com.riverview.harrys.adapter;

import java.util.ArrayList;

import android.app.Activity;
import android.content.Context;
import android.graphics.Typeface;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.iapps.libs.helpers.BaseUIHelper;
import com.riverview.harrys.R;
import com.riverview.harrys.helpers.Utils;
import com.riverview.harrys.objects.BeanStoreItem;

public class StoresAdapter extends BaseAdapter implements Filterable {

	private static final String TAG = StoresAdapter.class.getSimpleName();

	private ArrayList<BeanStoreItem> mItems = new ArrayList<BeanStoreItem>();
	private ArrayList<BeanStoreItem> mFilteredItems = new ArrayList<BeanStoreItem>();
	private Activity context;

	public StoresAdapter(Activity context, ArrayList<BeanStoreItem> list) {
		this.context = context;

		this.mFilteredItems = list;
		this.mItems = list;
	}

	@Override
	public int getCount() {
		try {
			return mFilteredItems.size();
		} catch (Exception e) {
			return 0;
		}
	}

	@Override
	public BeanStoreItem getItem(int pos) {
		return mFilteredItems.get(pos);
	}

	@Override
	public long getItemId(int position) {
		return mFilteredItems.get(position).getId();
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ViewHolder holder;

		if (convertView == null) {
			holder = new ViewHolder();
			LayoutInflater inflater = (LayoutInflater) context
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

			convertView = inflater.inflate(R.layout.cell_stores_item, parent,
					false);
			holder.LLRoot = (LinearLayout) convertView
					.findViewById(R.id.LLRoot);
			holder.infoStore = (TextView) convertView
					.findViewById(R.id.textViewStoreNameAddress);
			holder.imgStore = (ImageView) convertView
					.findViewById(R.id.imgStoreItem);
			holder.distanceStore = (TextView) convertView
					.findViewById(R.id.textViewDistance);
			holder.title = (TextView) convertView
					.findViewById(R.id.textViewStoreTitle);
			convertView.setTag(holder);

		} else {
			holder = (ViewHolder) convertView.getTag();
		}

		// set cell height by screen height ratio
		Utils.setWidthHeightByPercent(context, holder.imgStore, 0.35f, 0.15f);

		// set textview size by screen width ratio
		// Utils.procTextsizeBasedScreen(context, holder.infoStore, 0.65f);
		// Utils.procTextsizeBasedScreen(context, holder.distanceStore, 0.65f);
		Utils.setTextViewFontSizeBasedOnScreenDensity(context,
				holder.infoStore, 30f);
		Utils.setTextViewFontSizeBasedOnScreenDensity(context,
				holder.distanceStore, 30f, Typeface.BOLD);
		// Utils.setTextViewFontSizeBasedOnScreenDensity(context,
		// holder.distanceStore, 30f);
		Utils.setTextViewFontSizeBasedOnScreenDensity(context, holder.title,
				23f, Typeface.BOLD);

		Utils.setMarginByPercent(context, holder.LLRoot, 0.01f, true);

		BeanStoreItem obj = getItem(position);
		holder.infoStore.setText(obj.getAddress());
		BaseUIHelper.loadImageWithPlaceholder(context, obj.getUrl(),
				holder.imgStore, R.drawable.placeholder);

		// Picasso.with(context).load(obj.getUrl()).placeholder(R.drawable.placeholder).into(holder.imgStore);

		holder.title.setText(obj.getInfo());
		if (obj.getDistance() != 0.0) {
			holder.distanceStore.setText(String.valueOf(obj.getDistance())
					.concat(" km"));
		}

		return convertView;
	}

	public class ViewHolder {

		TextView infoStore;
		ImageView imgStore;
		TextView distanceStore;
		LinearLayout LLRoot;
		TextView title;
	}

	@Override
	public Filter getFilter() {
		Filter filter = new Filter() {

			@SuppressWarnings("unchecked")
			@Override
			protected void publishResults(CharSequence constraint,
					FilterResults results) {
				Log.d(TAG, "publist result");
				mFilteredItems = (ArrayList<BeanStoreItem>) results.values;
				notifyDataSetChanged();
			}

			@Override
			protected FilterResults performFiltering(CharSequence constraint) {
				Log.d(TAG, "performFiltering");
				String filterPhase = constraint.toString().toLowerCase();
				FilterResults results = new FilterResults();

				final ArrayList<BeanStoreItem> list = mItems;
				int count = list.size();

				final ArrayList<BeanStoreItem> filteredList = new ArrayList<>(
						count);

				String filterableString;
				for (int i = 0; i < count; i++) {
					filterableString = list.get(i).getInfo();

					Log.i(TAG, "Filetered string  " + filterableString);

					if (filterableString.toLowerCase().contains(filterPhase)) {
						filteredList.add(list.get(i));
					}
				}

				results.values = filteredList;
				results.count = filteredList.size();

				return results;
			}
		};
		return filter;
	}
}
