package com.riverview.harrys.helpers;

import android.content.Context;
import android.util.AttributeSet;
import android.view.ViewGroup;
import android.widget.ListView;

public class ExpandedListView extends ListView {

	// private boolean isExpanded = true;
	//
	// public ExpandedListView(Context context, AttributeSet attrs) {
	// super(context, attrs);
	// }
	//
	// @Override
	// public void onMeasure(
	// final int widthMeasureSpec, final int heightMeasureSpec) {
	// if (this.isExpanded) {
	// final int expandSpec = MeasureSpec.makeMeasureSpec(
	// Integer.MAX_VALUE >> 2, MeasureSpec.AT_MOST);
	// super.onMeasure(widthMeasureSpec, expandSpec);
	// final ViewGroup.LayoutParams params = this.getLayoutParams();
	// params.height = this.getMeasuredHeight();
	// } else {
	// super.onMeasure(widthMeasureSpec, heightMeasureSpec);
	// }
	//
	// super.onMeasure(widthMeasureSpec, heightMeasureSpec);
	// }

	// private android.view.ViewGroup.LayoutParams params;
	// private int old_count = 0;
	//
	// public ExpandedListView(Context context, AttributeSet attrs) {
	// super(context, attrs);
	// }
	// @Override
	// protected void onDraw(Canvas canvas) {
	// if (getCount() != old_count) {
	// old_count = getCount();
	// params = getLayoutParams();
	// params.height = getCount() * (old_count > 0 ? getChildAt(0).getHeight() +
	// 1 : 0);
	// setLayoutParams(params);
	// }
	// super.onDraw(canvas);
	// }
	// @Override
	// public boolean dispatchTouchEvent(MotionEvent ev){
	// if(ev.getAction()== MotionEvent.ACTION_MOVE)
	// return true;
	// return super.dispatchTouchEvent(ev);
	// }

	private boolean isExpanded = true;

	public ExpandedListView(Context context, AttributeSet attrs) {
		super(context, attrs);
	}

	@Override
	public void onMeasure(final int widthMeasureSpec,
			final int heightMeasureSpec) {
		// HACK! TAKE THAT ANDROID!
		if (isExpanded) {
			// Calculate entire height by providing a very large height hint.
			// View.MEASURED_SIZE_MASK represents the largest height possible.
			int expandSpec = MeasureSpec.makeMeasureSpec(MEASURED_SIZE_MASK,
					MeasureSpec.AT_MOST);
			super.onMeasure(widthMeasureSpec, expandSpec);

			ViewGroup.LayoutParams params = getLayoutParams();
			params.height = getMeasuredHeight();
		} else {
			super.onMeasure(widthMeasureSpec, heightMeasureSpec);
		}
	}

}