package com.iapps.libs.generics;

import android.support.v4.app.FragmentActivity;

import com.github.rtyley.android.sherlock.roboguice.activity.RoboSherlockFragmentActivity;

public class GenericFragmentActivity extends FragmentActivity {
	protected GenericFragmentActivity getActivityInstance() {
		return this;
	}
}
