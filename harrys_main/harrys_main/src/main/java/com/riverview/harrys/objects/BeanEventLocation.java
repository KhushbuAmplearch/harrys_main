package com.riverview.harrys.objects;

import java.util.ArrayList;

public class BeanEventLocation {

	private int id, event_id, merchant_id;

	private String check_in_via, created_at, updated_at, rooms;
	private ArrayList<BeanEventLocationRoom> event_location_rooms;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getEvent_id() {
		return event_id;
	}

	public void setEvent_id(int event_id) {
		this.event_id = event_id;
	}

	public int getMerchant_id() {
		return merchant_id;
	}

	public void setMerchant_id(int merchant_id) {
		this.merchant_id = merchant_id;
	}

	public String getCheck_in_via() {
		return check_in_via;
	}

	public void setCheck_in_via(String check_in_via) {
		this.check_in_via = check_in_via;
	}

	public String getCreated_at() {
		return created_at;
	}

	public void setCreated_at(String created_at) {
		this.created_at = created_at;
	}

	public String getUpdated_at() {
		return updated_at;
	}

	public void setUpdated_at(String updated_at) {
		this.updated_at = updated_at;
	}

	public String getRooms() {
		return rooms;
	}

	public void setRooms(String rooms) {
		this.rooms = rooms;
	}

	public ArrayList<BeanEventLocationRoom> getEvent_location_rooms() {
		return event_location_rooms;
	}

	public void setEvent_location_rooms(
			ArrayList<BeanEventLocationRoom> event_location_rooms) {
		this.event_location_rooms = event_location_rooms;
	}
}
