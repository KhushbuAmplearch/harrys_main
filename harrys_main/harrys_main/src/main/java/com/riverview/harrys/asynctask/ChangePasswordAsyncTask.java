package com.riverview.harrys.asynctask;

import static com.riverview.harrys.constant.AppConst.ATTR_TAG_CONT_PASSWORD;
import static com.riverview.harrys.constant.AppConst.ATTR_TAG_DEVICE_ID;
import static com.riverview.harrys.constant.AppConst.ATTR_TAG_OLD_PASSWORD;
import static com.riverview.harrys.constant.AppConst.ATTR_TAG_PASSWORD;
import static com.riverview.harrys.constant.AppConst.HTTP_METHOD_POST;
import static com.riverview.harrys.constant.AppConst.SERVICE_END_POINT;
import static com.riverview.harrys.constant.AppConst.URL_PASSWORD_CHANGE;

import java.io.IOException;
import java.util.ArrayList;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.type.TypeReference;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.client.ClientHttpResponse;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.DefaultResponseErrorHandler;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.iapps.libs.helpers.BaseHelper;
import com.riverview.harrys.R;
import com.riverview.harrys.exception.RestServiceException;
import com.riverview.harrys.objects.BeanMobileUser;
import com.riverview.harrys.objects.BeanUserData;
import com.riverview.harrys.objects.ErrorServiceResponse;
import com.riverview.harrys.objects.ServiceResponse;
import com.riverview.harrys.util.AppUtil;
import com.riverview.harrys.util.WebServiceUtil;

import fr.castorflex.android.circularprogressbar.CircularProgressDrawable;

public class ChangePasswordAsyncTask extends
		AsyncTask<Void, Void, ServiceResponse<BeanMobileUser>> {

	private static final String TAG = ChangePasswordAsyncTask.class
			.getSimpleName();

	// public AsyncResponse<ServiceResponse<BeanLogin>> asyncResponse;

	private ProgressDialog mProgressDialog;

	private Context mContext;
	private String oldPassword = null;
	private String newPassword = null;
	private String confPassword = null;
	private int userId;

	String[] commandString = null;
	ArrayList<NameValuePair> httpParams = null;

	private String urlApi = null;

	public ChangePasswordAsyncTask(Context context, int userId,
			String oldPassword, String newPassword, String confPassword) {
		this.mContext = context;
		this.oldPassword = oldPassword;
		this.newPassword = newPassword;
		this.confPassword = confPassword;
		this.userId = userId;
	}

	@Override
	protected void onPreExecute() {
		Log.d(TAG, "onPreExecute");
		super.onPreExecute();

		mProgressDialog = new ProgressDialog(mContext);
		mProgressDialog
				.setIndeterminateDrawable(new CircularProgressDrawable.Builder(
						mContext)
						.colors(mContext.getResources().getIntArray(
								R.array.harrys_colors)).sweepSpeed(1f)
						.strokeWidth(12f)
						.style(CircularProgressDrawable.Style.ROUNDED).build());

		mProgressDialog.setCancelable(false);
		mProgressDialog.setMessage("Updating password.....");
		mProgressDialog.show();

		try {
			ArrayList<NameValuePair> arrayList = new ArrayList<NameValuePair>();

			arrayList
					.add(new BasicNameValuePair(ATTR_TAG_PASSWORD, newPassword));
			arrayList.add(new BasicNameValuePair(ATTR_TAG_OLD_PASSWORD,
					oldPassword));
			arrayList.add(new BasicNameValuePair(ATTR_TAG_CONT_PASSWORD,
					confPassword));

			urlApi = String.format(URL_PASSWORD_CHANGE, userId);

			httpParams = AppUtil.generateAuthCommandString(arrayList, urlApi,
					HTTP_METHOD_POST);

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	@Override
	protected void onPostExecute(ServiceResponse<BeanMobileUser> response) {
		Log.d(TAG, "onPostExecute");

		if (mProgressDialog != null && mProgressDialog.isShowing()) {
			mProgressDialog.dismiss();
		}

		if (response == null) {
			BaseHelper
					.showAlert(mContext, "Harry's ", "Operation unsuccessful");
			return;
		}

		if (response.getStatus() == 200 && !response.error) {
			//
			// BeanMobileUser details = response.getResponse();
			//
			// SharedPref.saveString(mContext, SharedPref.USERFNAME_KEY,
			// details.getFirstname());
			// SharedPref.saveString(mContext, SharedPref.USERLNAME_KEY,
			// details.getSurname());
			// SharedPref.saveInteger(mContext, SharedPref.USERID_KEY,
			// details.getId());
			// SharedPref.saveString(mContext, SharedPref.APPID_KEY,
			// details.getApplication_id());
			// SharedPref.saveString(mContext, SharedPref.COMPANY_ID_KEY,
			// details.getCompany_id());

			BaseHelper.showAlert(mContext, "Harry's ",
					"Your password was successfully saved");

		} else {
			BaseHelper.showAlert(
					mContext,
					"Harry's ",
							((response.getError_message() != null) ? response
									.getError_message() : "Operation unsuccessful"));
		}
	}

	@Override
	protected ServiceResponse<BeanMobileUser> doInBackground(Void... voids) {
		Log.d(TAG, "doInBackground");

		ServiceResponse<BeanMobileUser> serviceResponse = null;
		BeanUserData response = null;
		
		RestTemplate restTemplate = WebServiceUtil.getRestTemplateInstance();

		restTemplate.setErrorHandler(new DefaultResponseErrorHandler() {

			@Override
			public void handleError(ClientHttpResponse response)
					throws IOException {
				ObjectMapper mapper = WebServiceUtil.getErrorObjMapper();
				ErrorServiceResponse errorRes;
				errorRes = mapper.readValue(response.getBody(),
						new TypeReference<ErrorServiceResponse>() {
						});

				throw new RestServiceException(errorRes, errorRes
						.getError_message());

			}

			protected boolean hasError(HttpStatus statusCode) {
				Log.d(TAG, "Error ----> " + statusCode);
				if (statusCode.value() == 200) {
					return false;
				}
				return true;
			}
		});


		/*RestTemplate restTemplate = WebServiceUtil.getRestTemplateInstance();

		restTemplate.setErrorHandler(new DefaultResponseErrorHandler() {
			@Override
			public void handleError(ClientHttpResponse response)
					throws IOException {
				ObjectMapper mapper = WebServiceUtil.getErrorObjMapper();
				ErrorServiceResponse errorRes;
				errorRes = mapper.readValue(response.getBody(),
						new TypeReference<ErrorServiceResponse>() {
						});

				throw new RestServiceException(errorRes, errorRes
						.getMessage());
			}

			protected boolean hasError(HttpStatus statusCode) {
				Log.d(TAG, "Status ----> " + statusCode);
				if (statusCode.value() == 200) {
					return false;
				}
				return true;
			}
		});*/

		String url = String.format(SERVICE_END_POINT + "%s", urlApi);

		HttpHeaders requestHeaders = new HttpHeaders();
		requestHeaders.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
		requestHeaders.set("Accept", "application/json");
		requestHeaders.add(ATTR_TAG_DEVICE_ID, AppUtil.getDeviceId(mContext));

		Log.d(TAG, "Device Id ----> " + AppUtil.getDeviceId(mContext));

		MultiValueMap<String, String> map = new LinkedMultiValueMap<String, String>();
		for (NameValuePair a : httpParams) {
			map.add(a.getName(), a.getValue());
		}

		HttpEntity<MultiValueMap<String, String>> requestEntity = new HttpEntity<MultiValueMap<String, String>>(
				map, requestHeaders);

		Log.d(TAG, "Url " + url);
		Log.d(TAG, "Request Entity " + requestEntity.toString());
		
		try {
            serviceResponse = restTemplate
                    .exchange(
                    		url,
                            HttpMethod.POST,
                            requestEntity,
                            new ParameterizedTypeReference<ServiceResponse<BeanMobileUser>>() {
                            }).getBody();

            Log.i(TAG, "MEssage -> " + serviceResponse.getError_message());

        } catch (RestClientException e) {

            Log.e(TAG, "Message 11 -> " + e.getLocalizedMessage());

            serviceResponse = new ServiceResponse<BeanMobileUser>();
            serviceResponse.setError(true);
            serviceResponse.setError_message(e.getCause().getMessage());

        } catch (Exception e) {
            Log.e(TAG, "Exception " + e.getLocalizedMessage());
            e.printStackTrace();
        }

		/*try {
			// serviceResponse = restTemplate
			// .exchange(
			// url,
			// HttpMethod.POST,
			// requestEntity,
			// new ParameterizedTypeReference<ServiceResponse<BeanMobileUser>>()
			// {
			// }).getBody();
			//
			response = restTemplate.exchange(url, HttpMethod.POST,
					requestEntity, BeanUserData.class).getBody();

			serviceResponse = new ServiceResponse<BeanMobileUser>();
			serviceResponse.setResponse(response.getMobileuser());
			serviceResponse.setError(false);
			serviceResponse.setStatus(200);

			Log.d(TAG, "Sssss ---->> " + serviceResponse.getStatus());
		} catch (RestClientException e) {
			serviceResponse = new ServiceResponse<BeanMobileUser>();
			serviceResponse.setError(true);
			serviceResponse.setError_message(e.getCause().getMessage());

		} catch (Exception e) {
			serviceResponse = new ServiceResponse<BeanMobileUser>();
			serviceResponse.setError(true);
			serviceResponse.setError_message(e.getCause().getMessage());
		}*/
		return serviceResponse;
	}

	@Override
	protected void onCancelled() {

		if (mProgressDialog != null && mProgressDialog.isShowing()) {
			mProgressDialog.dismiss();
			// asyncResponse.processFinish(null);
		}
	}
}
