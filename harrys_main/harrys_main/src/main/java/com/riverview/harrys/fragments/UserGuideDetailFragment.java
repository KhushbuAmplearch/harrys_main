package com.riverview.harrys.fragments;

import android.graphics.Color;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;

import com.riverview.harrys.MainActivity;
import com.riverview.harrys.R;
import com.riverview.harrys.RCGenericFragment;

public class UserGuideDetailFragment extends RCGenericFragment {

	private WebView wv;

	View v;
	private String url;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		v = inflater.inflate(R.layout.userguide_detail_fragment_layout,
				container, false);
		wv = (WebView)v.findViewById(R.id.wv);

		setHasOptionsMenu(false);
		String title = getArguments().getString("title");
		url = getArguments().getString("url");
		((MainActivity) getActivity()).setBackActionBar(title,View.INVISIBLE,"");

		return v;
	}

	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);

		wv.setBackgroundColor(Color.TRANSPARENT);
		wv.setLayerType(WebView.LAYER_TYPE_SOFTWARE, null);
		wv.loadUrl("http://harryscard.com.sg/faq-page#n403");

	}

	View.OnClickListener clickListeners = new View.OnClickListener() {
		public void onClick(View v) {

		}
	};

}
