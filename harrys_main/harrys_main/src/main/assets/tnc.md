######The Riverview MS Mobile App 

######Terms & Conditions

1. The Riverview MS® Mobile App ("The Riverview MS App") is a prepaid mobile wallet issued by The Riverview MS (Singapore) Pte. Ltd. (Riverview) and managed by the technology of Riverview MS Pte Ltd ("the Manager").
2. By using The Riverview MS App, the user shall be deemed to have read, understood and agreed with the terms and conditions of The Riverview MS App. Applicants for The Riverview MS App are required to provide correct and true information in the application form.
3. Payment shall be in Singapore Dollars.
4. All prepaid value does not include goods and services tax or any other taxes/charges imposed from time to time by regulatory or governmental bodies.
5. Payment made for The Riverview MS App wallet will be considered as a complete transaction and The Riverview MS App users are not entitled to demand or claim for refunds or reimbursement of whole or any part of the unutilized value of The Riverview MS App wallet under whatever circumstances.
6. The Riverview MS App wallet is not transferable and cannot be shared by other third party.
7. The Riverview MS App wallet with sufficient value will be accepted at all of the outlets of RMS in Singapore with the exception of airport stores. The Riverview MS App users are not entitled to change their order after the relevant transaction has been recorded.
8. Subject to the terms herein contained and basis determined by RMS from time to time, Bean Points (“POINTS”) and/or discounts, benefits, entitlements, privileges, promotions (“Benefits”) may be awarded to the RMS Member when The Riverview MS App wallet is presented at the time of purchasing products/services offered by the RMS.
9. The basis for the enjoyment of The Riverview MS App, POINTS and/or the Benefits shall be determined by the RMS at its absolute discretion and may be changed from time to time without prior notice. The Riverview MS App users are not entitled to request RMS or the Manager to disclose or explain the calculation methods or other matters relating to the POINTS and/or the Benefits for whatever purposes;
10. POINTS will not be awarded for taxes or any charges that are payable by RMS to any regulatory or governmental bodies;
11. POINTS are not transferable, have no monetary value and not exchangeable for cash;
12. Save and except The Riverview MS App user's right to redeem the Rewards as herein set out, The Riverview MS App users shall not have any other right or interest in the value of the POINTS available in their account and are not entitled to claim or take any action against RMS or the Manager for payment of the POINTS' value in cash or in kind or for any benefits, income or profits that may be gained by RMS or the Manager from the management of such POINTS value, if any.
13. Dollar value in the card will automatically expire after 1,200 days from the date of top-up;
14. POINTS will automatically expire on 31st March of the following year irrespective when the POINTS are awarded in the current year;
15. POINTS awarded manually will only be credited after the Manager has been informed of the relevant transaction;
16. RMS and the Manager will not be liable if the POINTS cannot be recorded for whatever reasons (including but not limited to malfunction or interruption of the POINTS recording system, civil commotion, fire, or other force majeure events beyond the control of all parties concerned);
17. RMS or the Manager shall have sole discretion to deduct, cancel or refuse/reject to award POINTS for any suspected transaction as it deems fit or POINTS that have been wrongfully recorded whenever it deems fit;
18. RMS or the Manager shall not be liable for failure to add the POINTS into The Riverview MS App user’s account for whatever reasons; and
19. Any dispute on the POINTS balance shall be notified to RMS or the Manager within 30 days from the date of the relevant transaction.
20. The Riverview MS App wallet with sufficient POINTS may redeem such RMS’s Products that may be determined by RMS from time to time (“Rewards”) subject to the terms and conditions as may be amended or varied at RMS’s sole discretion with or without notice, including but not limited to:
21. &nbsp;
	1. The procedures involved in redemption;
	2. Restrict the quantity of a particular Reward that an app user may exchange for POINTS; 
	3. Classify The Riverview MS App users into different categories;
	4. Offer different Rewards and Benefits for different categories of The Riverview MS App users.
22. POINTS utilized for redemption of the Rewards will be deducted from The Riverview MS Cardholder’s account. No POINTS will be accorded for all redemption transactions.
23. As RMS may determine at the material time, redemption may be made via POINTS only or mixture of the POINTS and The Riverview MS App wallet. For each redemption transaction (irrespective on the mode of redemption) perform at all the RMS’s Outlets:
	1. Number of POINTS that will be deducted is at a minimum of 500 POINTS or more;
24. If Rewards have been redeemed by using any POINTS that have been fraudulently or wrongfully earned, The Riverview MS App user shall be liable to refund to RMS the value of the said POINTS or return the Rewards to RMS or the Manager, as directed by RMS. RMS is entitled to refuse any request for redemption or recall any Rewards redeemed if RMS reasonably suspects the POINTS used by The Riverview MS App user were fraudulently or wrongfully recorded.
25. Once RMS accepts any redemption requests, it cannot be revoked, amended, cancelled, returned or exchanged for other items.
26. Unclaimed Rewards will be forfeited and the POINTS deducted for such redemption will not be reinstated.
27. RMS or the Manager shall not be liable for any POINTS deducted in furtherance to redemption that may be made by persons other than The Riverview MS App user under whatever circumstances.
28. All Rewards are subject to availability and RMS reserves its right to replace other Rewards of approximately the equivalent value without prior notice.
29. Personal database together with information of The Riverview MS App, POINTS and any redemption transactions undertaken by any RMS Member (“the Database”) will be jointly owned by RMS and the Manager, and The Riverview MS App user hereby irrevocably and unconditionally agrees that RMS and its Manager or their respective holding companies, subsidiaries, associates or related corporations to use, process, disclose, transfer or to deal with the Database in whatever manner and for whatever purposes as RMS or the Manager shall deem fit without further notice (either with or without third party) or for the purposes of disclosure under the law or any court order or other governmental or regulatory bodies.
30. The Riverview MS App user shall immediately notify RMS in writing of any change of his/her Database whereupon RMS shall take all practicable steps to update the Database within a reasonable time Provided That RMS shall not be liable for any losses or damages that may be suffered by The Riverview MS App user arising.
31. RMS have the right at any time and at its sole discretion, with or without notice, to limit, add, amend or delete in whole or in part these T&C, rules, conditions, policies of the Program, The Riverview MS App Benefits, Redemption of the Rewards, POINTS awards even though such changes may affect the value of The Riverview MS App, POINTS or Benefits already accumulated or The Riverview MS App users’ rights and interests. The amendment and supplement T&C, rules, conditions and policies shall be binding on The Riverview MS App users as long as his/her The Riverview MS App is valid under these T&C.
32. If RMS or the Manager is found liable by a court of competent jurisdiction, the liabilities of RMS and the Manager to The Riverview MS app user for any cause whatsoever and regardless of the form of action, to the extent permitted by law, shall be limited to restoring or according any the value of The Riverview MS App which RMS has wrongfully deducted, POINTS and/or Benefits that have been mistakenly omitted.
33. Any fees paid under these T&C are not refundable unless otherwise provided.
34. Any notice to be given to The Riverview MS App user shall be deemed to have been properly given if sent by prepaid post to the address recorded in the Database.
35. The Riverview MS App user shall indemnify and hold RMS and the Manager harmless against any liability for loss, penalty, damage, costs and expenses (including but not limited to legal costs) which RMS and/or the Manager may incur by reason of or arising directly or indirectly from any breach on the part of The Riverview MS App user in complying or observing these T&C or in otherwise using of The Riverview MS App or recovery of any outstanding amounts due from The Riverview MS App.
36. RMS and/or the Manager shall be entitled jointly or severally at the time without the consent of or notice to The Riverview MS App user to assign or transfer whole or part of its rights and obligations in relation to The Riverview MS App and/or the Program to any other person.
37. Time shall be of the essence of these terms and conditions but no failure to exercise or delay in or partial exercising of any of RMS’s or the Manager’s rights, power or remedies shall operate as waiver.
38. These terms and conditions shall be governed and construed in accordance with the law of Singapore and the parties hereto agree to submit to the non-exclusive jurisdiction of the courts of Singapore.
39. Any application is subject to fulfillment of any other conditions set by RMS. RMS’s decision to reject any application shall be final. No objection or appeal shall be entertained.
40. No person other than RMS or the App users shall have any rights under the Contracts (Rights of Third Parties) Act (Cap. 53B) to enforce or enjoy the benefit of any of these T&C.

Riverview reserves the right to change, modify or alter these terms and conditions at any time without prior notice

By agreeing to abide by and accept these terms and conditions in the app using the “opt in” upon completion of the in-app registration form, the mobile user is deemed to be and is considered to be in agreement with these terms and conditions as stated herein.


