package com.riverview.harrys.objects;

public class BeanEventLocationRoom {

	private int id, event_location_id;
	private String name, is_unlimited, seating_smoking, seating_non_smoking,
			standing_smoking, standing_non_smoking, waitlist, created_at,
			updated_list;
	private String[] available_place;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getEvent_location_id() {
		return event_location_id;
	}

	public void setEvent_location_id(int event_location_id) {
		this.event_location_id = event_location_id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getIs_unlimited() {
		return is_unlimited;
	}

	public void setIs_unlimited(String is_unlimited) {
		this.is_unlimited = is_unlimited;
	}

	public String getSeating_smoking() {
		return seating_smoking;
	}

	public void setSeating_smoking(String seating_smoking) {
		this.seating_smoking = seating_smoking;
	}

	public String getSeating_non_smoking() {
		return seating_non_smoking;
	}

	public void setSeating_non_smoking(String seating_non_smoking) {
		this.seating_non_smoking = seating_non_smoking;
	}

	public String getStanding_smoking() {
		return standing_smoking;
	}

	public void setStanding_smoking(String standing_smoking) {
		this.standing_smoking = standing_smoking;
	}

	public String getStanding_non_smoking() {
		return standing_non_smoking;
	}

	public void setStanding_non_smoking(String standing_non_smoking) {
		this.standing_non_smoking = standing_non_smoking;
	}

	public String getWaitlist() {
		return waitlist;
	}

	public void setWaitlist(String waitlist) {
		this.waitlist = waitlist;
	}

	public String getCreated_at() {
		return created_at;
	}

	public void setCreated_at(String created_at) {
		this.created_at = created_at;
	}

	public String getUpdated_list() {
		return updated_list;
	}

	public void setUpdated_list(String updated_list) {
		this.updated_list = updated_list;
	}

	public String[] getAvailable_place() {
		return available_place;
	}

	public void setAvailable_place(String[] available_place) {
		this.available_place = available_place;
	}
}
