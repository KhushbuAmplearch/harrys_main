package com.riverview.harrys.objects;

public class BeanTier {

	private String card_name, card_face, tier_type;

	public BeanTier() {
	}

	public String getCard_name() {
		return card_name;
	}

	public void setCard_name(String card_name) {
		this.card_name = card_name;
	}

	public String getCard_face() {
		return card_face;
	}

	public void setCard_face(String card_face) {
		this.card_face = card_face;
	}

	public String getTier_type() {
		return tier_type;
	}

	public void setTier_type(String tier_type) {
		this.tier_type = tier_type;
	}
}
