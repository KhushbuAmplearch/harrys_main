package com.riverview.harrys.dialogfragment;

import android.content.DialogInterface;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.riverview.harrys.MainActivity;
import com.riverview.harrys.R;
import com.riverview.harrys.RCGenericDialogFragment;
import com.riverview.harrys.helpers.Utils;


public class RedeemVoucherDialog extends RCGenericDialogFragment {

	private Button buttonDismiss;
	private Button buttonConfirm;
	private TextView textViewRedeemInfo;
	private TextView textViewTitle;

	private View v;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		v = inflater.inflate(R.layout.dialog_redeem_voucher_fragment_layout,
				container, false);
		setHasOptionsMenu(false);
		((MainActivity) getActivity()).setBackActionBar(getActivity()
				.getResources().getString(R.string.rc_stores),View.INVISIBLE,"");
		buttonDismiss = (Button)v.findViewById (R.id.buttonDismiss);
		buttonConfirm = (Button)v.findViewById (R.id.buttonOk);
		textViewRedeemInfo = (TextView) v.findViewById (R.id.textViewRedeemInfo);
		textViewTitle = (TextView) v.findViewById (R.id.textViewTitle);


		return v;
	}

	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);

		Utils.setTextViewFontSizeBasedOnScreenDensity(getActivity(),
				textViewTitle, Utils.FONT_MEDIUM_DENSITY_SIZE);
		Utils.setTextViewFontSizeBasedOnScreenDensity(getActivity(),
				textViewRedeemInfo, Utils.FONT_SMALL_DENSITY_SIZE);
		Utils.setTextViewFontSizeBasedOnScreenDensity(getActivity(),
				buttonDismiss, Utils.FONT_SMALL_DENSITY_SIZE);
		Utils.setTextViewFontSizeBasedOnScreenDensity(getActivity(),
				buttonConfirm, Utils.FONT_SMALL_DENSITY_SIZE);

		buttonConfirm.setOnClickListener(clickListeners);
		buttonDismiss.setOnClickListener(clickListeners);

	}

	View.OnClickListener clickListeners = new View.OnClickListener() {
		public void onClick(View v) {

			switch (v.getId()) {
			case R.id.buttonDismiss:
				((MainActivity) getActivity()).onBackPressed();

				break;

			case R.id.buttonOk:
				Log.d("TAG", "To Do");
				break;
			default:
				break;
			}
		}
	};

	@Override
	public void onDismiss(DialogInterface dialog) {
		super.onDismiss(dialog);
		try {
			((MainActivity) getActivity()).backToHomeScreen();
		} catch (Exception e) {
		}
	}

}
