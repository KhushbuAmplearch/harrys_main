package com.riverview.harrys;

import uk.co.chrisjenx.calligraphy.CalligraphyConfig;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;
import android.content.Context;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.github.rtyley.android.sherlock.roboguice.activity.RoboSherlockFragmentActivity;
import com.riverview.harrys.helpers.Constants;
import com.riverview.harrys.helpers.Utils;
import com.riverview.harrys.util.AnaylaticsAppender;
import com.riverview.harrys.util.AppUtil;
import com.squareup.picasso.Picasso;

public class CardImageActivity extends FragmentActivity implements
		OnClickListener {

	// implements OnClickListener
	private static final String TAG = CardImageActivity.class.getSimpleName();

	private ImageView imageCard;
	private TextView textCardId;
	private TextView textCardHolderName;
	private Button buttonCloseImage;

	private String cardId;
	private String cardName;
	private String cardFaceUrl;

	boolean isPressed = false;

	private AnaylaticsAppender mAppender = null;

	@Override
	protected void attachBaseContext(Context newBase) {
		super.attachBaseContext(new CalligraphyContextWrapper(newBase));
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		CalligraphyConfig.initDefault(Constants.GLOBAL_FONT_APP);
		setContentView(R.layout.card_image_fragment_layout);
		imageCard = (ImageView)findViewById (R.id.imgCardImage);
		textCardId = (TextView)findViewById (R.id.textCardId);
		textCardHolderName = (TextView)findViewById (R.id.textCardHolderName);
		buttonCloseImage = (Button)findViewById (R.id.buttonCloseImage);
		Bundle bundle = getIntent().getExtras();
		if (bundle != null) {
			cardId = bundle.getString("card_id", " ");
			cardName = bundle.getString("card_name", " ");
			cardFaceUrl = bundle.getString("card_face");
		}
		setUI();
	}

	private void setUI() {

		Utils.setTextViewFontSizeBasedOnScreenDensity(this, textCardId, 20f,
				Typeface.BOLD);
		Utils.setTextViewFontSizeBasedOnScreenDensity(this, textCardHolderName,
				20f);

		textCardId.setText(cardId);
		textCardHolderName.setText(cardName);

		Picasso.with(this).load(cardFaceUrl)
				.placeholder(R.drawable.placeholder).into(imageCard);

		imageCard.setOnClickListener(this);
		buttonCloseImage.setOnClickListener(this);

	}

	@Override
	public void onResume() {
		super.onResume();

		try {
			mAppender = AnaylaticsAppender.getInstance(getApplicationContext());
			mAppender
					.updateAnaylitsData(AppUtil.createAnalyticsJsonEventAction(
							"Harry's Card - Full Screen", cardId, "View", null));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public void onClick(View v) {

		switch (v.getId()) {
		case R.id.buttonCloseImage:
			finish();
			break;

		default:
			break;
		}

	}
}
