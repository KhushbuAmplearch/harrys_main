package com.riverview.harrys.objects;

import android.os.Parcel;
import android.os.Parcelable;

import java.io.Serializable;

public class BeanFeedbackPost implements Parcelable, Serializable {

    public static final String OBJ_NAME = "BeanFeedbackPost";
    private static final long serialVersionUID = 5588254270496437773L;

    private int id;
    private String title;
    private String feedback_type;

    public BeanFeedbackPost(Parcel parcel) {
        super();
        this.id = parcel.readInt();
        this.title = parcel.readString();
        this.feedback_type = parcel.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
        dest.writeString(title);
        dest.writeString(feedback_type);
    }

    public static final Creator<BeanFeedbackPost> CREATOR = new Creator<BeanFeedbackPost>() {

        @Override
        public BeanFeedbackPost createFromParcel(Parcel source) {
            return new BeanFeedbackPost(source);
        }

        @Override
        public BeanFeedbackPost[] newArray(int size) {
            return new BeanFeedbackPost[size];
        }
    };

    @Override
    public int describeContents() {
        // TODO Auto-generated method stub
        return 0;
    }

    public BeanFeedbackPost(){super();}


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

	public String getFeedback_type() {
		return feedback_type;
	}

	public void setFeedback_type(String feedback_type) {
		this.feedback_type = feedback_type;
	}
}
