package com.riverview.harrys.service;

import static com.riverview.harrys.constant.AppConst.ATTR_TAG_UDID;
import static com.riverview.harrys.constant.AppConst.HTTP_METHOD_GET;
import static com.riverview.harrys.constant.AppConst.SERVICE_END_POINT;
import static com.riverview.harrys.constant.AppConst.URL_MOBILE_USERDATA;

import java.io.IOException;
import java.util.ArrayList;

import org.apache.http.NameValuePair;
import org.apache.http.client.utils.URLEncodedUtils;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.type.TypeReference;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.client.ClientHttpResponse;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.DefaultResponseErrorHandler;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import com.riverview.harrys.MainActivity;
import com.riverview.harrys.R;
import com.riverview.harrys.asynctask.AsyncResponse;
import com.riverview.harrys.asynctask.DevicesAsyncTask;
import com.riverview.harrys.asynctask.UserDataAsyncTask;
import com.riverview.harrys.exception.RestServiceException;
import com.riverview.harrys.fragments.HomeFragment;
import com.riverview.harrys.helpers.SharedPref;
import com.riverview.harrys.helpers.UserInfoManager;
import com.riverview.harrys.objects.BeanDevice;
import com.riverview.harrys.objects.BeanMobileUser;
import com.riverview.harrys.objects.ErrorServiceResponse;
import com.riverview.harrys.objects.ServiceResponse;
import com.riverview.harrys.receiver.UserStateCheckerReceiver;
import com.riverview.harrys.util.AppUtil;
import com.riverview.harrys.util.WebServiceUtil;

import fr.castorflex.android.circularprogressbar.CircularProgressDrawable;
import android.app.AlarmManager;
import android.app.IntentService;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

public class UserStatusService extends IntentService {

	private static final String TAG = UserStatusService.class.getSimpleName();

	private int mMobileUserId;

	public UserStatusService() {
		super(TAG);
	}

	public UserStatusService(String name, int userId) {
		super(name);

		this.mMobileUserId = userId;

	}

	@Override
	protected void onHandleIntent(Intent intent) {
		Log.d(TAG, "OnHandleIntent --  user suspend service");

		// call user profile service and check for the suspend flag of the
		// particular user.
		if (!UserInfoManager.getInstance(getApplicationContext()).isSignedIn(
				getApplicationContext())) {

			// if network available
			if (AppUtil.checkNetworkConnection(getApplicationContext())) {

				int mobileUserId = SharedPref.getPref(getApplicationContext())
						.getInt(SharedPref.USERID_KEY, 0);

				UserSuspendCheckerAsyncTask asyncTask = new UserSuspendCheckerAsyncTask(
						getApplicationContext(), mobileUserId);
				asyncTask.execute((Void) null);
			}
		} else {
			stopScheduledUploadReceiver();
		}
	}

	private void stopScheduledUploadReceiver() {
		Log.i(TAG, "Stop scheduled task status receiver");

		AlarmManager service = (AlarmManager) getApplicationContext()
				.getSystemService(Context.ALARM_SERVICE);

		Intent i = new Intent(getApplicationContext(), UserStatusService.class);
		PendingIntent pendingIntent = PendingIntent.getService(
				getApplicationContext(), UserStateCheckerReceiver.REQUEST_CODE,
				i, PendingIntent.FLAG_UPDATE_CURRENT);

		service.cancel(pendingIntent);
		pendingIntent.cancel();

	}

	public class UserSuspendCheckerAsyncTask extends
			AsyncTask<Void, Void, ServiceResponse<BeanMobileUser>> {

		private final String TAG = UserSuspendCheckerAsyncTask.class
				.getSimpleName();

		// public AsyncResponse<ServiceResponse<BeanMobileUser>> asyncResponse;

		private Context mContext;

		String[] commandString = null;
		ArrayList<NameValuePair> httpParams = null;

		private int userId;
		private String userDataApi = null;

		public UserSuspendCheckerAsyncTask(Context context, int mobileUserId) {
			this.mContext = context;

			this.userId = mobileUserId;
		}

		@Override
		protected void onPreExecute() {
			Log.d(TAG, "onPreExecute");
			super.onPreExecute();

			try {
				userDataApi = String.format(URL_MOBILE_USERDATA, userId);
				httpParams = AppUtil.generateAuthCommandString(null,
						userDataApi, HTTP_METHOD_GET);

			} catch (Exception e) {
				e.printStackTrace();
			}

		}

		@Override
		protected void onPostExecute(ServiceResponse<BeanMobileUser> response) {
			Log.d(TAG, "onPostExecute");

			if (response != null) {

				BeanMobileUser user = response.getResponse();

				if (user != null && user.getSuspended() != null
						&& Integer.valueOf(user.getSuspended()) > 0) {
					// log out the user and and go to login

					UserInfoManager.getInstance(getApplicationContext()).logout(getApplicationContext());
					
					
					if (AppUtil.checkNetworkConnection(getApplicationContext())) {
						callDeviceRegApi();
					}
					Toast.makeText(
							getApplicationContext(),
							"Sorry. You account has been suspended. Please contact support for more information",
							Toast.LENGTH_LONG).show();

					try {

						Intent intent = new Intent(mContext, MainActivity.class);
						intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK
								| Intent.FLAG_ACTIVITY_CLEAR_TOP);
						startActivity(intent);

					} catch (Exception e) {
						e.printStackTrace();
					}

					stopScheduledUploadReceiver();
				}

			}
		}

		@Override
		protected ServiceResponse<BeanMobileUser> doInBackground(Void... voids) {
			Log.d(TAG, "doInBackground");

			ServiceResponse<BeanMobileUser> serviceResponse = null;

			RestTemplate restTemplate = WebServiceUtil
					.getRestTemplateInstance();

			restTemplate.setErrorHandler(new DefaultResponseErrorHandler() {
				@Override
				public void handleError(ClientHttpResponse response)
						throws IOException {
					ObjectMapper mapper = WebServiceUtil.getErrorObjMapper();
					ErrorServiceResponse errorRes;
					errorRes = mapper.readValue(response.getBody(),
							new TypeReference<ErrorServiceResponse>() {
							});

					throw new RestServiceException(errorRes, errorRes
							.getError_message());
				}

				protected boolean hasError(HttpStatus statusCode) {
					Log.d(TAG, "Status ----> " + statusCode);
					if (statusCode.value() == 200) {
						return false;
					}
					return true;
				}
			});

			String url = String.format(SERVICE_END_POINT + "%s", userDataApi);

			String urlParams = URLEncodedUtils.format(httpParams, "UTF-8");
			String finalUrl = String.format(url + "?" + "%s", urlParams);

			Log.d(TAG, "Final URL ------> " + finalUrl);

			HttpHeaders requestHeaders = new HttpHeaders();
			requestHeaders
					.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
			requestHeaders.set("Accept", "application/json");
			requestHeaders.add(ATTR_TAG_UDID, AppUtil.getDeviceId(mContext));

			HttpEntity<MultiValueMap<String, String>> requestEntity = new HttpEntity<MultiValueMap<String, String>>(
					null, requestHeaders);

			Log.d(TAG, "Url " + url);
			Log.d(TAG, "Request Entity " + requestEntity.toString());

			try {
				serviceResponse = restTemplate
						.exchange(
								finalUrl,
								HttpMethod.GET,
								requestEntity,
								new ParameterizedTypeReference<ServiceResponse<BeanMobileUser>>() {
								}).getBody();

			} catch (RestClientException e) {
				serviceResponse = new ServiceResponse<BeanMobileUser>();
				serviceResponse.setError(true);
				serviceResponse.setError_message(e.getCause().getMessage());

			} catch (Exception e) {
				e.printStackTrace();
			}
			return serviceResponse;
		}

		@Override
		protected void onCancelled() {
			//
			// if (mProgressDialog != null && mProgressDialog.isShowing()) {
			// mProgressDialog.dismiss();
			// asyncResponse.processFinish(null);
			// }
		}
	}
	
	private void callDeviceRegApi() {

		if (!UserInfoManager.getInstance(this).isSignedIn(this)) {

			BeanDevice beanDevice = AppUtil.calculateDeviceRegData(this, SharedPref.getString(getApplicationContext(), SharedPref.COMPANY_ID_KEY, "1"),
					SharedPref.getString(this, SharedPref.APPID_KEY, "1"),0);

			DevicesAsyncTask mDeviceTask = new DevicesAsyncTask(this,
					beanDevice, true);
			mDeviceTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR,
					(Void) null);
		}
	}

}
