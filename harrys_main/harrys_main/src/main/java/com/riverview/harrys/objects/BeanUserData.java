package com.riverview.harrys.objects;

public class BeanUserData {

	public static final String OBJ_NAME = "BeanUserData";

	private BeanMobileUser mobileuser;

	public BeanMobileUser getMobileuser() {
		return mobileuser;
	}

	public void setMobileuser(BeanMobileUser mobileuser) {
		this.mobileuser = mobileuser;
	}

}
