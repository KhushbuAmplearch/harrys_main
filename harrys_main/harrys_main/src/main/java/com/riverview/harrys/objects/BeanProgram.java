package com.riverview.harrys.objects;

import java.util.ArrayList;

public class BeanProgram {

	private String id, title, description, company_id, expired_in_months,
			start_day, end_day, cardless_redemption, rolling_expiry_date,
			rolling_expiry_cycle, created_at, updated_at, deleted_at;

	private ArrayList<BeanCard> card;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getCompany_id() {
		return company_id;
	}

	public void setCompany_id(String company_id) {
		this.company_id = company_id;
	}

	public String getExpired_in_months() {
		return expired_in_months;
	}

	public void setExpired_in_months(String expired_in_months) {
		this.expired_in_months = expired_in_months;
	}

	public String getStart_day() {
		return start_day;
	}

	public void setStart_day(String start_day) {
		this.start_day = start_day;
	}

	public String getEnd_day() {
		return end_day;
	}

	public void setEnd_day(String end_day) {
		this.end_day = end_day;
	}

	public String getCardless_redemption() {
		return cardless_redemption;
	}

	public void setCardless_redemption(String cardless_redemption) {
		this.cardless_redemption = cardless_redemption;
	}

	public String getRolling_expiry_date() {
		return rolling_expiry_date;
	}

	public void setRolling_expiry_date(String rolling_expiry_date) {
		this.rolling_expiry_date = rolling_expiry_date;
	}

	public String getRolling_expiry_cycle() {
		return rolling_expiry_cycle;
	}

	public void setRolling_expiry_cycle(String rolling_expiry_cycle) {
		this.rolling_expiry_cycle = rolling_expiry_cycle;
	}

	public String getCreated_at() {
		return created_at;
	}

	public void setCreated_at(String created_at) {
		this.created_at = created_at;
	}

	public String getUpdated_at() {
		return updated_at;
	}

	public void setUpdated_at(String updated_at) {
		this.updated_at = updated_at;
	}

	public String getDeleted_at() {
		return deleted_at;
	}

	public void setDeleted_at(String deleted_at) {
		this.deleted_at = deleted_at;
	}

	public ArrayList<BeanCard> getCard() {
		return card;
	}

	public void setCard(ArrayList<BeanCard> card) {
		this.card = card;
	}
}
