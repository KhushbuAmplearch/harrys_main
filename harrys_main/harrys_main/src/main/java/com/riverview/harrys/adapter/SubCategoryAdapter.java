package com.riverview.harrys.adapter;

import java.util.ArrayList;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.riverview.harrys.R;
import com.riverview.harrys.helpers.Utils;
import com.riverview.harrys.objects.BeanSubcategoryItem;

public class SubCategoryAdapter extends BaseAdapter {

	private static final String TAG = SubCategoryAdapter.class.getSimpleName();

	private ArrayList<BeanSubcategoryItem> mItems = new ArrayList<BeanSubcategoryItem>();
	private Activity context;

	public SubCategoryAdapter(Activity context,
			ArrayList<BeanSubcategoryItem> list) {
		this.context = context;

		this.mItems = list;
	}

	@Override
	public int getCount() {
		try {
			return mItems.size();
		} catch (Exception e) {
			return 0;
		}
	}

	@Override
	public BeanSubcategoryItem getItem(int pos) {
		return mItems.get(pos);
	}

	@Override
	public long getItemId(int position) {
		return mItems.get(position).getId();
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ViewHolder holder;

		if (convertView == null) {
			holder = new ViewHolder();
			LayoutInflater inflater = (LayoutInflater) context
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

			convertView = inflater.inflate(R.layout.cell_subcatagory_view,
					parent, false);
			holder.LLRoot = (LinearLayout) convertView
					.findViewById(R.id.LLCatogoryItemRoot);
			holder.title = (TextView) convertView
					.findViewById(R.id.textSubCatagotyName);
			convertView.setTag(holder);

		} else {
			holder = (ViewHolder) convertView.getTag();
		}

		Utils.setMarginByPercent(context, holder.LLRoot, 0.025f, true);
		Utils.setTextViewFontSizeBasedOnScreenDensity(context, holder.title,
				16f);

		BeanSubcategoryItem obj = getItem(position);
		holder.title.setText(obj.getName());

		return convertView;
	}

	public class ViewHolder {

		TextView title;
		LinearLayout LLRoot;
	}
}
