/* AUTO-GENERATED FILE.  DO NOT MODIFY.
 *
 * This class was automatically generated by the
 * gradle plugin from the resource data it found. It
 * should not be modified by hand.
 */
package android.support.v7.recyclerview;

public final class R {
    public static final class attr {
        public static final int fastScrollEnabled = 0x7f0101cf;
        public static final int fastScrollHorizontalThumbDrawable = 0x7f0101d2;
        public static final int fastScrollHorizontalTrackDrawable = 0x7f0101d3;
        public static final int fastScrollVerticalThumbDrawable = 0x7f0101d0;
        public static final int fastScrollVerticalTrackDrawable = 0x7f0101d1;
        public static final int font = 0x7f010162;
        public static final int fontProviderAuthority = 0x7f01015b;
        public static final int fontProviderCerts = 0x7f01015e;
        public static final int fontProviderFetchStrategy = 0x7f01015f;
        public static final int fontProviderFetchTimeout = 0x7f010160;
        public static final int fontProviderPackage = 0x7f01015c;
        public static final int fontProviderQuery = 0x7f01015d;
        public static final int fontStyle = 0x7f010161;
        public static final int fontWeight = 0x7f010163;
        public static final int layoutManager = 0x7f0101cb;
        public static final int reverseLayout = 0x7f0101cd;
        public static final int spanCount = 0x7f0101cc;
        public static final int stackFromEnd = 0x7f0101ce;
    }
    public static final class bool {
        public static final int abc_action_bar_embed_tabs = 0x7f0b0000;
    }
    public static final class color {
        public static final int notification_action_color_filter = 0x7f0d0000;
        public static final int notification_icon_bg_color = 0x7f0d010b;
        public static final int ripple_material_light = 0x7f0d0122;
        public static final int secondary_text_default_material_light = 0x7f0d0124;
    }
    public static final class dimen {
        public static final int compat_button_inset_horizontal_material = 0x7f09007e;
        public static final int compat_button_inset_vertical_material = 0x7f09007f;
        public static final int compat_button_padding_horizontal_material = 0x7f090080;
        public static final int compat_button_padding_vertical_material = 0x7f090081;
        public static final int compat_control_corner_material = 0x7f090082;
        public static final int fastscroll_default_thickness = 0x7f0900b3;
        public static final int fastscroll_margin = 0x7f0900b4;
        public static final int fastscroll_minimum_range = 0x7f0900b5;
        public static final int item_touch_helper_max_drag_scroll_per_frame = 0x7f0900c4;
        public static final int item_touch_helper_swipe_escape_max_velocity = 0x7f0900c5;
        public static final int item_touch_helper_swipe_escape_velocity = 0x7f0900c6;
        public static final int notification_action_icon_size = 0x7f0900cc;
        public static final int notification_action_text_size = 0x7f0900cd;
        public static final int notification_big_circle_margin = 0x7f0900ce;
        public static final int notification_content_margin_start = 0x7f090027;
        public static final int notification_large_icon_height = 0x7f0900cf;
        public static final int notification_large_icon_width = 0x7f0900d0;
        public static final int notification_main_column_padding_top = 0x7f090028;
        public static final int notification_media_narrow_margin = 0x7f090029;
        public static final int notification_right_icon_size = 0x7f0900d1;
        public static final int notification_right_side_padding_top = 0x7f090025;
        public static final int notification_small_icon_background_padding = 0x7f0900d2;
        public static final int notification_small_icon_size_as_large = 0x7f0900d3;
        public static final int notification_subtext_size = 0x7f0900d4;
        public static final int notification_top_pad = 0x7f0900d5;
        public static final int notification_top_pad_large_text = 0x7f0900d6;
    }
    public static final class drawable {
        public static final int notification_action_background = 0x7f02023a;
        public static final int notification_bg = 0x7f02023b;
        public static final int notification_bg_low = 0x7f02023c;
        public static final int notification_bg_low_normal = 0x7f02023d;
        public static final int notification_bg_low_pressed = 0x7f02023e;
        public static final int notification_bg_normal = 0x7f02023f;
        public static final int notification_bg_normal_pressed = 0x7f020240;
        public static final int notification_icon_background = 0x7f020241;
        public static final int notification_template_icon_bg = 0x7f0202d5;
        public static final int notification_template_icon_low_bg = 0x7f0202d6;
        public static final int notification_tile_bg = 0x7f020242;
        public static final int notify_panel_notification_icon_bg = 0x7f020243;
    }
    public static final class id {
        public static final int action_container = 0x7f0f023b;
        public static final int action_divider = 0x7f0f0242;
        public static final int action_image = 0x7f0f023c;
        public static final int action_text = 0x7f0f023d;
        public static final int actions = 0x7f0f024b;
        public static final int async = 0x7f0f0068;
        public static final int blocking = 0x7f0f0069;
        public static final int chronometer = 0x7f0f0247;
        public static final int forever = 0x7f0f006a;
        public static final int icon = 0x7f0f00de;
        public static final int icon_group = 0x7f0f024c;
        public static final int info = 0x7f0f0248;
        public static final int italic = 0x7f0f006b;
        public static final int item_touch_helper_previous_elevation = 0x7f0f0015;
        public static final int line1 = 0x7f0f0016;
        public static final int line3 = 0x7f0f0017;
        public static final int normal = 0x7f0f003c;
        public static final int notification_background = 0x7f0f024a;
        public static final int notification_main_column = 0x7f0f0244;
        public static final int notification_main_column_container = 0x7f0f0243;
        public static final int right_icon = 0x7f0f0249;
        public static final int right_side = 0x7f0f0245;
        public static final int text = 0x7f0f0025;
        public static final int text2 = 0x7f0f0026;
        public static final int time = 0x7f0f0246;
        public static final int title = 0x7f0f002a;
    }
    public static final class integer {
        public static final int status_bar_notification_info_maxnum = 0x7f0c0017;
    }
    public static final class layout {
        public static final int notification_action = 0x7f03007e;
        public static final int notification_action_tombstone = 0x7f03007f;
        public static final int notification_template_custom_big = 0x7f030086;
        public static final int notification_template_icon_group = 0x7f030087;
        public static final int notification_template_part_chronometer = 0x7f03008b;
        public static final int notification_template_part_time = 0x7f03008c;
    }
    public static final class string {
        public static final int status_bar_notification_info_overflow = 0x7f08006d;
    }
    public static final class style {
        public static final int TextAppearance_Compat_Notification = 0x7f0a0091;
        public static final int TextAppearance_Compat_Notification_Info = 0x7f0a0092;
        public static final int TextAppearance_Compat_Notification_Line2 = 0x7f0a012d;
        public static final int TextAppearance_Compat_Notification_Time = 0x7f0a0095;
        public static final int TextAppearance_Compat_Notification_Title = 0x7f0a0097;
        public static final int Widget_Compat_NotificationActionContainer = 0x7f0a0099;
        public static final int Widget_Compat_NotificationActionText = 0x7f0a009a;
    }
    public static final class styleable {
        public static final int[] FontFamily = { 0x7f01015b, 0x7f01015c, 0x7f01015d, 0x7f01015e, 0x7f01015f, 0x7f010160 };
        public static final int FontFamily_fontProviderAuthority = 0;
        public static final int FontFamily_fontProviderPackage = 1;
        public static final int FontFamily_fontProviderQuery = 2;
        public static final int FontFamily_fontProviderCerts = 3;
        public static final int FontFamily_fontProviderFetchStrategy = 4;
        public static final int FontFamily_fontProviderFetchTimeout = 5;
        public static final int[] FontFamilyFont = { 0x7f010161, 0x7f010162, 0x7f010163 };
        public static final int FontFamilyFont_fontStyle = 0;
        public static final int FontFamilyFont_font = 1;
        public static final int FontFamilyFont_fontWeight = 2;
        public static final int[] RecyclerView = { 0x010100c4, 0x010100f1, 0x7f0101cb, 0x7f0101cc, 0x7f0101cd, 0x7f0101ce, 0x7f0101cf, 0x7f0101d0, 0x7f0101d1, 0x7f0101d2, 0x7f0101d3 };
        public static final int RecyclerView_android_orientation = 0;
        public static final int RecyclerView_android_descendantFocusability = 1;
        public static final int RecyclerView_layoutManager = 2;
        public static final int RecyclerView_spanCount = 3;
        public static final int RecyclerView_reverseLayout = 4;
        public static final int RecyclerView_stackFromEnd = 5;
        public static final int RecyclerView_fastScrollEnabled = 6;
        public static final int RecyclerView_fastScrollVerticalThumbDrawable = 7;
        public static final int RecyclerView_fastScrollVerticalTrackDrawable = 8;
        public static final int RecyclerView_fastScrollHorizontalThumbDrawable = 9;
        public static final int RecyclerView_fastScrollHorizontalTrackDrawable = 10;
    }
}
