package com.riverview.harrys.objects;

import java.io.Serializable;

import android.os.Parcel;
import android.os.Parcelable;

public class BeanSubcategoryItem implements Parcelable, Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6083400903668833623L;

	public static final String OBJ_NAME = "BeanSubcategoryItem";

	private int id;
	private String name, description, category_id;

	public BeanSubcategoryItem(int id, String name) {
		super();
		this.id = id;
		this.name = name;

	}

	public BeanSubcategoryItem(Parcel parcel) {
		super();
		this.id = parcel.readInt();
		this.name = parcel.readString();
		this.description = parcel.readString();
		this.category_id = parcel.readString();
	}

	public static final Parcelable.Creator<BeanSubcategoryItem> CREATOR = new Parcelable.Creator<BeanSubcategoryItem>() {

		@Override
		public BeanSubcategoryItem createFromParcel(Parcel source) {
			return new BeanSubcategoryItem(source);
		}

		@Override
		public BeanSubcategoryItem[] newArray(int size) {
			return new BeanSubcategoryItem[size];
		}
	};

	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel out, int flag) {
		out.writeInt(id);
		out.writeString(name);
		out.writeString(description);
		out.writeString(category_id);
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getCategory_id() {
		return category_id;
	}

	public void setCategory_id(String category_id) {
		this.category_id = category_id;
	}

	@Override
	public int hashCode() {
		int hash = 5;
		hash = 89 * hash + (this.name != null ? this.name.hashCode() : 0);
		hash = 89 * hash + (int) (this.id ^ (this.id >>> 32));
		hash = 89 * hash
				+ (this.category_id != null ? this.category_id.hashCode() : 0);
		hash = 89 * hash
				+ (this.description != null ? this.description.hashCode() : 0);
		return hash;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null || this.getClass() != obj.getClass()) {
			return false;
		}
		BeanSubcategoryItem other = (BeanSubcategoryItem) obj;

		return this.id == other.id
				&& (this.name == other.name || (this.name != null && this.name
						.equals(other.name)))
				&& (this.category_id == other.category_id || (this.category_id != null && this.category_id
						.equals(other.category_id)))
				&& (this.description == other.description || (this.description != null && this.description
						.equals(other.description)));
	}

}
