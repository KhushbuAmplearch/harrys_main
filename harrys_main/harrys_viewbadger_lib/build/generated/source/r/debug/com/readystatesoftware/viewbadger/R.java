/* AUTO-GENERATED FILE.  DO NOT MODIFY.
 *
 * This class was automatically generated by the
 * gradle plugin from the resource data it found. It
 * should not be modified by hand.
 */
package com.readystatesoftware.viewbadger;

public final class R {
    public static final class drawable {
        public static int badge_ifaux = 0x7f090001;
        public static int icon = 0x7f090002;
    }
    public static final class id {
        public static int anim1_target = 0x7f0c0001;
        public static int anim2_target = 0x7f0c0002;
        public static int badge = 0x7f0c0003;
        public static int click_target = 0x7f0c0004;
        public static int colour_target = 0x7f0c0005;
        public static int custom_target = 0x7f0c0006;
        public static int default_label = 0x7f0c0007;
        public static int default_target = 0x7f0c0008;
        public static int frame_group_target = 0x7f0c0009;
        public static int frame_target = 0x7f0c000a;
        public static int increment_target = 0x7f0c000b;
        public static int linear_group_target = 0x7f0c000c;
        public static int linear_target = 0x7f0c000d;
        public static int position_target = 0x7f0c000e;
        public static int relative_group_target = 0x7f0c000f;
        public static int relative_label = 0x7f0c0010;
        public static int relative_target = 0x7f0c0011;
        public static int tab1 = 0x7f0c0012;
        public static int tab2 = 0x7f0c0013;
        public static int tab3 = 0x7f0c0014;
        public static int tab_btn = 0x7f0c0015;
        public static int tableLayout1 = 0x7f0c0016;
        public static int tableRow1 = 0x7f0c0017;
        public static int table_target = 0x7f0c0018;
        public static int tablerow_group_target = 0x7f0c0019;
    }
    public static final class layout {
        public static int demos = 0x7f0f0001;
        public static int main = 0x7f0f0002;
        public static int tests = 0x7f0f0003;
    }
    public static final class string {
        public static int app_name = 0x7f150001;
    }
}
