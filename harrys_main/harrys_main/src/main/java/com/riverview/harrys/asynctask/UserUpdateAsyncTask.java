package com.riverview.harrys.asynctask;

import static com.riverview.harrys.constant.AppConst.ATTR_TAG_ADDRESS_LINE_1;
import static com.riverview.harrys.constant.AppConst.ATTR_TAG_ADDRESS_LINE_2;
import static com.riverview.harrys.constant.AppConst.ATTR_TAG_DOB;
import static com.riverview.harrys.constant.AppConst.ATTR_TAG_EMAIL;
import static com.riverview.harrys.constant.AppConst.ATTR_TAG_FIRST_NAME;
import static com.riverview.harrys.constant.AppConst.ATTR_TAG_GENDER;
import static com.riverview.harrys.constant.AppConst.ATTR_TAG_IDENTIFICATION;
import static com.riverview.harrys.constant.AppConst.ATTR_TAG_MOBILE;
import static com.riverview.harrys.constant.AppConst.ATTR_TAG_NATIONALITY;
import static com.riverview.harrys.constant.AppConst.ATTR_TAG_POSTEL_CODE;
import static com.riverview.harrys.constant.AppConst.ATTR_TAG_SUR_NAME;
import static com.riverview.harrys.constant.AppConst.ATTR_TAG_UDID;
import static com.riverview.harrys.constant.AppConst.HTTP_METHOD_POST;
import static com.riverview.harrys.constant.AppConst.SERVICE_END_POINT;
import static com.riverview.harrys.constant.AppConst.URL_MOBILE_USER_UPDATE;

import java.io.IOException;
import java.util.ArrayList;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.type.TypeReference;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.client.ClientHttpResponse;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.DefaultResponseErrorHandler;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.util.Log;

import com.iapps.libs.helpers.BaseHelper;
import com.iapps.libs.helpers.BaseHelper.CancelListener;
import com.iapps.libs.helpers.BaseHelper.ConfirmListener;
import com.riverview.harrys.MainActivity;
import com.riverview.harrys.R;
import com.riverview.harrys.exception.RestServiceException;
import com.riverview.harrys.fragments.VerifyMobileOTPFragment;
import com.riverview.harrys.helpers.SharedPref;
import com.riverview.harrys.objects.BeanLogin;
import com.riverview.harrys.objects.BeanMobileUser;
import com.riverview.harrys.objects.BeanSignUp;
import com.riverview.harrys.objects.BeanUserData;
import com.riverview.harrys.objects.ErrorServiceResponse;
import com.riverview.harrys.objects.ServiceResponse;
import com.riverview.harrys.util.AppUtil;
import com.riverview.harrys.util.WebServiceUtil;

import fr.castorflex.android.circularprogressbar.CircularProgressDrawable;

public class UserUpdateAsyncTask extends
		AsyncTask<Void, Void, ServiceResponse<BeanMobileUser>> {

	private static final String TAG = UserUpdateAsyncTask.class.getSimpleName();

	private ProgressDialog mProgressDialog;

	private Activity mContext;
	private BeanSignUp mBeanSignUp;
	private int userId;
	private String url;
	String[] commandString = null;
	ArrayList<NameValuePair> httpParams = null;

	public UserUpdateAsyncTask(Activity context, BeanSignUp beanSignUp, int id) {
		this.mContext = context;
		this.mBeanSignUp = beanSignUp;
		this.userId = id;
	}

	@Override
	protected void onPreExecute() {
		Log.d(TAG, "onPreExecute");
		super.onPreExecute();

		mProgressDialog = new ProgressDialog(mContext);
		mProgressDialog
				.setIndeterminateDrawable(new CircularProgressDrawable.Builder(
						mContext)
						.colors(mContext.getResources().getIntArray(
								R.array.harrys_colors)).sweepSpeed(1f)
						.strokeWidth(12f)
						.style(CircularProgressDrawable.Style.ROUNDED).build());

		mProgressDialog.setCancelable(false);
		mProgressDialog.setMessage("Updating mobileuser data..");
		mProgressDialog.show();

		// wwww - aaaa
		// wiraj11 - aaaa

		try {
			ArrayList<NameValuePair> arrayList = new ArrayList<NameValuePair>();

			arrayList.add(new BasicNameValuePair(ATTR_TAG_FIRST_NAME,
					mBeanSignUp.getFirstname()));
			
			//if (!BaseHelper.isEmpty(mBeanSignUp.getSurname())) {
				arrayList.add(new BasicNameValuePair(ATTR_TAG_SUR_NAME,
						mBeanSignUp.getSurname()));
			//}

			//if (!BaseHelper.isEmpty(mBeanSignUp.getMobilenumber())) {
				arrayList.add(new BasicNameValuePair(ATTR_TAG_MOBILE,
						mBeanSignUp.getMobilenumber()));
			//}

			//if (!BaseHelper.isEmpty(mBeanSignUp.getEmail())) {
				arrayList.add(new BasicNameValuePair(ATTR_TAG_EMAIL,
						mBeanSignUp.getEmail()));
			//}
			//if (!BaseHelper.isEmpty(mBeanSignUp.getPostalcode())) {
				arrayList.add(new BasicNameValuePair(ATTR_TAG_POSTEL_CODE,
						mBeanSignUp.getPostalcode()));
			//}

			if (!BaseHelper.isEmpty(mBeanSignUp.getGender())) {
				arrayList.add(new BasicNameValuePair(ATTR_TAG_GENDER,
						mBeanSignUp.getGender()));
			}
			//if (!BaseHelper.isEmpty(mBeanSignUp.getIdentification())) {
				arrayList.add(new BasicNameValuePair(ATTR_TAG_IDENTIFICATION,
						mBeanSignUp.getIdentification()));
			//}
			if (!BaseHelper.isEmpty(mBeanSignUp.getNationality())) {
				arrayList.add(new BasicNameValuePair(ATTR_TAG_NATIONALITY,
						mBeanSignUp.getNationality()));
			}
			if (!BaseHelper.isEmpty(mBeanSignUp.getAddressLine1())) {
				arrayList.add(new BasicNameValuePair(ATTR_TAG_ADDRESS_LINE_1,
						mBeanSignUp.getAddressLine1()));
			}
			if (!BaseHelper.isEmpty(mBeanSignUp.getAddressLine2())) {
				arrayList.add(new BasicNameValuePair(ATTR_TAG_ADDRESS_LINE_2,
						mBeanSignUp.getAddressLine2()));
			}
			if (!BaseHelper.isEmpty(mBeanSignUp.getDob())) {
				String convertedDate = AppUtil.convertDateCms(mContext,
						mBeanSignUp.getDob());
				arrayList.add(new BasicNameValuePair(ATTR_TAG_DOB,
						convertedDate));
			}

			url = String.format(URL_MOBILE_USER_UPDATE, userId);

			httpParams = AppUtil.generateAuthCommandString(arrayList, url,
					HTTP_METHOD_POST);

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	@Override
	protected void onPostExecute(ServiceResponse<BeanMobileUser> response) {
		Log.d(TAG, "onPostExecute");

		if (mProgressDialog != null && mProgressDialog.isShowing()) {
			mProgressDialog.dismiss();
		}

		if (response == null) {
			BaseHelper
					.showAlert(mContext, "Harry's ", "Operation unsuccessful");
			return;
		}

		Log.d("Error Message :", ""+ response.getStatus());
		
		if (response.getStatus() == 200 && !response.error) {

			BeanMobileUser details = response.getResponse();

			SharedPref.saveString(mContext, SharedPref.USERFNAME_KEY,
					details.getFirstname());
			SharedPref.saveString(mContext, SharedPref.USERLNAME_KEY,
					details.getSurname());
			SharedPref.saveInteger(mContext, SharedPref.USERID_KEY,
					details.getId());
			SharedPref.saveString(mContext, SharedPref.APPID_KEY,
					details.getApplication_id());
			SharedPref.saveString(mContext, SharedPref.COMPANY_ID_KEY,
					details.getCompany_id());

			// detect mobile number update and if yes verify it from OTP.
//			String verifyMobile = details.getPendingMobileUpdate();
//			Log.d(TAG, "PM " + verifyMobile);
//
//			if (!BaseHelper.isEmpty(verifyMobile)
//					&& !Boolean.valueOf(verifyMobile)) {
//				try {
//					BaseHelper.confirm(mContext,
//							mContext.getString(R.string.app_name),
//							mContext.getString(R.string.rc_verify_request),
//							confirmListener1, new CancelListener() {
//
//								@Override
//								public void onNo() {
//									// getHome().backToHomeScreen();
//
//								}
//							});
//				} catch (Exception e) {
//					e.printStackTrace();
//				}
//			} else {
				BaseHelper.showAlert(mContext, "Harry's ",
						mContext.getString(R.string.rc_profile_update_success));
			//}

		} else {
			
			//Log.d("Error Message :", ""+ );
			
			BaseHelper.showAlert(mContext, "Harry's ", ((response.getError_message() != null) ? response.getError_message()
					: "Operation unsuccessful "));
		}
	}

	ConfirmListener confirmListener1 = new ConfirmListener() {

		@Override
		public void onYes() {
			((MainActivity) mContext)
					.setFragment(new VerifyMobileOTPFragment());
		}
	};

	@Override
	protected ServiceResponse<BeanMobileUser> doInBackground(Void... voids) {
		Log.d(TAG, "doInBackground");

		ServiceResponse<BeanMobileUser> serviceResponse = null;

		//BeanUserData response = null;
		
		RestTemplate restTemplate = WebServiceUtil.getRestTemplateInstance();

		restTemplate.setErrorHandler(new DefaultResponseErrorHandler() {

			@Override
			public void handleError(ClientHttpResponse response)
					throws IOException {
				ObjectMapper mapper = WebServiceUtil.getErrorObjMapper();
				ErrorServiceResponse errorRes;
				errorRes = mapper.readValue(response.getBody(),
						new TypeReference<ErrorServiceResponse>() {
						});

				throw new RestServiceException(errorRes, errorRes
						.getError_message());

			}

			protected boolean hasError(HttpStatus statusCode) {
				Log.d(TAG, "Error ----> " + statusCode);
				if (statusCode.value() == 200) {
					return false;
				}
				return true;
			}
		});

		/*RestTemplate restTemplate = WebServiceUtil.getRestTemplateInstance();

		restTemplate.setErrorHandler(new DefaultResponseErrorHandler() {
			@Override
			public void handleError(ClientHttpResponse response)
					throws IOException {

				ObjectMapper mapper = WebServiceUtil.getErrorObjMapper();
				ErrorServiceResponse errorRes;
				errorRes = mapper.readValue(response.getBody(),
						new TypeReference<ErrorServiceResponse>() {
						});

				throw new RestServiceException(errorRes, errorRes
						.getMessage());
			}

			protected boolean hasError(HttpStatus statusCode) {
				Log.d(TAG, "Status ----> " + statusCode);
				if (statusCode.value() == 200) {
					return false;
				}
				return true;
			}
		});*/

		String finalUrl = String.format(SERVICE_END_POINT + "%s", url);

		HttpHeaders requestHeaders = new HttpHeaders();
		requestHeaders.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
		requestHeaders.set("Accept", "application/json");
		requestHeaders.add(ATTR_TAG_UDID, AppUtil.getDeviceId(mContext));

		MultiValueMap<String, String> map = new LinkedMultiValueMap<String, String>();

		for (NameValuePair a : httpParams) {
			map.add(a.getName(), a.getValue());
		}

		HttpEntity<MultiValueMap<String, String>> requestEntity = new HttpEntity<MultiValueMap<String, String>>(
				map, requestHeaders);

		Log.d(TAG, "Url " + finalUrl);
		Log.d(TAG, "Request Entity " + requestEntity.toString());
		
		try {
            serviceResponse = restTemplate
                    .exchange(
                    		finalUrl,
                            HttpMethod.POST,
                            requestEntity,
                            new ParameterizedTypeReference<ServiceResponse<BeanMobileUser>>() {
                            }).getBody();

            Log.i(TAG, "MEssage -> " + serviceResponse.getError_message());

        } catch (RestClientException e) {

            Log.e(TAG, "Message 11 -> " + e.getLocalizedMessage());

            serviceResponse = new ServiceResponse<BeanMobileUser>();
            serviceResponse.setError(true);
            serviceResponse.setError_message(e.getCause().getMessage());

        } catch (Exception e) {
            Log.e(TAG, "Exception " + e.getLocalizedMessage());
            e.printStackTrace();
        }

		/*try {
			
			response = restTemplate.exchange(finalUrl, HttpMethod.POST,
					requestEntity, BeanUserData.class).getBody();

			// String response1 = restTemplate.postForObject(finalUrl, map,
			// String.class);
			
			Log.d(TAG, "Responce  " + response.getMobileuser());
			
		
			serviceResponse = new ServiceResponse<BeanMobileUser>();
			serviceResponse.setResponse(response.getMobileuser());
			serviceResponse.setError(false);
			serviceResponse.setStatus(200);
			
			 Log.d(TAG, "Response ----> " + serviceResponse.getResponse());

			// create a ServiceResponse separately. Need to change after the
			// API's response
			// format update in next release
			 

		} catch (RestClientException e) {

			Log.e(TAG, "Exception 1 " + e.getLocalizedMessage());

			serviceResponse = new ServiceResponse<BeanMobileUser>();
			serviceResponse.setError(true);
			serviceResponse.setError_message(e.getCause().getMessage());

		} catch (Exception e) {
			Log.e(TAG, "Exception " + e.getLocalizedMessage());
			e.printStackTrace();
		}*/

		return serviceResponse;
	}

	@Override
	protected void onCancelled() {

		if (mProgressDialog != null && mProgressDialog.isShowing()) {
			mProgressDialog.dismiss();
		}
	}
}
