package com.riverview.harrys.asynctask;

import static com.riverview.harrys.constant.AppConst.ATTR_TAG_LIMIT;
import static com.riverview.harrys.constant.AppConst.ATTR_TAG_PAGE;
import static com.riverview.harrys.constant.AppConst.ATTR_TAG_UDID;
import static com.riverview.harrys.constant.AppConst.HTTP_METHOD_GET;
import static com.riverview.harrys.constant.AppConst.SERVICE_END_POINT;
import static com.riverview.harrys.constant.AppConst.URL_UPC_BY_CATAGORY;

import java.io.IOException;
import java.util.ArrayList;

import org.apache.http.NameValuePair;
import org.apache.http.client.utils.URLEncodedUtils;
import org.apache.http.message.BasicNameValuePair;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.type.TypeReference;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.client.ClientHttpResponse;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.DefaultResponseErrorHandler;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.riverview.harrys.R;
import com.riverview.harrys.exception.RestServiceException;
import com.riverview.harrys.objects.BeanUpc;
import com.riverview.harrys.objects.ErrorServiceResponse;
import com.riverview.harrys.objects.ServiceResponse;
import com.riverview.harrys.util.AppUtil;
import com.riverview.harrys.util.WebServiceUtil;

import fr.castorflex.android.circularprogressbar.CircularProgressDrawable;

/**
 * Get upc products by category
 * 
 * @author wiraj
 * 
 */
public class UpcByCatagoryAsyncTask extends
		AsyncTask<Void, Void, ServiceResponse<ArrayList<BeanUpc>>> {

	private static final String TAG = UpcByCatagoryAsyncTask.class
			.getSimpleName();

	public AsyncResponse<ServiceResponse<ArrayList<BeanUpc>>> asyncResponse;

	private ProgressDialog mProgressDialog;
	private Context mContext;
	private int catagoryId;

	private int page;
	private int itemCount;

	private boolean showProgress;

	String[] commandString = null;
	ArrayList<NameValuePair> httpParams = null;

	private String url;

	public UpcByCatagoryAsyncTask(Context context, int id, int page,
			int itemCount, boolean showProgress) {
		this.mContext = context;
		this.catagoryId = id;

		this.page = page;
		this.itemCount = itemCount;

		this.showProgress = showProgress;
	}

	@Override
	protected void onPreExecute() {
		Log.d(TAG, "onPreExecute");
		super.onPreExecute();
		if (showProgress) {
			mProgressDialog = new ProgressDialog(mContext);
			mProgressDialog
					.setIndeterminateDrawable(new CircularProgressDrawable.Builder(
							mContext)
							.colors(mContext.getResources().getIntArray(
									R.array.harrys_colors)).sweepSpeed(1f)
							.strokeWidth(12f)
							.style(CircularProgressDrawable.Style.ROUNDED)
							.build());

			mProgressDialog.setCancelable(false);
			mProgressDialog.setMessage("Retrieving product details.....");
			mProgressDialog.show();

		}

		try {
			url = String.format(URL_UPC_BY_CATAGORY, catagoryId);

			ArrayList<NameValuePair> arrayList = new ArrayList<NameValuePair>();

			arrayList.add(new BasicNameValuePair(ATTR_TAG_PAGE, String
					.valueOf(page)));
			arrayList.add(new BasicNameValuePair(ATTR_TAG_LIMIT, String
					.valueOf(itemCount)));

			httpParams = AppUtil.generateAuthCommandString(arrayList, url,
					HTTP_METHOD_GET);

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	protected void onPostExecute(ServiceResponse<ArrayList<BeanUpc>> response) {
		Log.d(TAG, "onPostExecute");

		if (showProgress && mProgressDialog != null
				&& mProgressDialog.isShowing()) {
			mProgressDialog.dismiss();
		}
		asyncResponse.processFinish(response);
	}

	@Override
	protected ServiceResponse<ArrayList<BeanUpc>> doInBackground(Void... voids) {
		Log.d(TAG, "doInBackground");

		ServiceResponse<ArrayList<BeanUpc>> serviceResponse = null;

		RestTemplate restTemplate = WebServiceUtil.getRestTemplateInstance();

		restTemplate.setErrorHandler(new DefaultResponseErrorHandler() {
			@Override
			public void handleError(ClientHttpResponse response)
					throws IOException {
				ObjectMapper mapper = WebServiceUtil.getErrorObjMapper();
				ErrorServiceResponse errorRes;
				errorRes = mapper.readValue(response.getBody(),
						new TypeReference<ErrorServiceResponse>() {
						});

				throw new RestServiceException(errorRes, errorRes
						.getError_message());
			}

			protected boolean hasError(HttpStatus statusCode) {
				Log.d(TAG, "Status ----> " + statusCode);
				if (statusCode.value() == 200) {
					return false;
				}
				return true;
			}
		});

		String baseUrl = String.format(SERVICE_END_POINT + "%s", url);

		String urlParams = URLEncodedUtils.format(httpParams, "UTF-8");
		String finalUrl = String.format(baseUrl + "?" + "%s", urlParams);

		Log.d(TAG, "Final URL ------> " + finalUrl);

		HttpHeaders requestHeaders = new HttpHeaders();
		requestHeaders.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
		requestHeaders.set("Accept", "application/json");
		requestHeaders.add(ATTR_TAG_UDID, AppUtil.getDeviceId(mContext));

		HttpEntity<MultiValueMap<String, String>> requestEntity = new HttpEntity<MultiValueMap<String, String>>(
				null, requestHeaders);

		Log.d(TAG, "Url " + url);
		Log.d(TAG, "Request Entity " + requestEntity.toString());

		try {
			serviceResponse = restTemplate
					.exchange(
							finalUrl,
							HttpMethod.GET,
							requestEntity,
							new ParameterizedTypeReference<ServiceResponse<ArrayList<BeanUpc>>>() {
							}).getBody();

		} catch (RestClientException e) {
			serviceResponse = new ServiceResponse<ArrayList<BeanUpc>>();
			serviceResponse.setError(true);
			if (e != null && e.getCause() != null) {
				serviceResponse.setError_message(e.getCause().getMessage());
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return serviceResponse;
	}

	@Override
	protected void onCancelled() {
		if (showProgress && mProgressDialog != null
				&& mProgressDialog.isShowing()) {
			mProgressDialog.dismiss();
			asyncResponse.processFinish(null);
		}
	}
}
