package com.riverview.harrys.objects;

import com.iapps.libs.objects.SimpleBean;

public class BeanOption extends SimpleBean {

	public static final String OBJ_NAME = "BeanOption";

	private boolean selected = false;
	private String desc;

	public BeanOption(int id, String name, String desc) {
		super(id, name);
		this.desc = desc;
	}

	public boolean isSelected() {
		return selected;
	}

	public void setSelected(boolean selected) {
		this.selected = selected;
	}

	public String getDesc() {
		return desc;
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}

}
