package com.riverview.harrys.objects;

import com.iapps.libs.objects.SimpleBean;

public class BeanSlideMenu extends SimpleBean {

	public static final String OBJ_NAME = "BeanSlideMenu";

	private int drawableId;
	boolean isEnable;

	public BeanSlideMenu(int id, String name, int drawableId, boolean isEnable) {
		super(id, name);
		this.drawableId = drawableId;
		this.isEnable = isEnable;
	}

	public int getDrawableId() {
		return drawableId;
	}

	public void setDrawableId(int drawableId) {
		this.drawableId = drawableId;
	}

	public boolean isEnable() {
		return isEnable;
	}

	public void setEnable(boolean isEnable) {
		this.isEnable = isEnable;
	}

}
