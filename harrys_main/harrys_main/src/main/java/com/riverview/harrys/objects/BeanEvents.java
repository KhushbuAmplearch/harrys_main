package com.riverview.harrys.objects;

import java.util.ArrayList;

public class BeanEvents {

	private int id;
	private String company_id, category_id, subcategory_id, application_id,
			title, type, description, more_details, start_date, end_date,
			duration, registration_process, target_audience,
			delivery_notification, created_at, updated_at, deleted_at;

	// private BeanImage image;
	private String image;
	private ArrayList<BeanEventLocation> event_location;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getCompany_id() {
		return company_id;
	}

	public void setCompany_id(String company_id) {
		this.company_id = company_id;
	}

	public String getCategory_id() {
		return category_id;
	}

	public void setCategory_id(String category_id) {
		this.category_id = category_id;
	}

	public String getSubcategory_id() {
		return subcategory_id;
	}

	public void setSubcategory_id(String subcategory_id) {
		this.subcategory_id = subcategory_id;
	}

	public String getApplication_id() {
		return application_id;
	}

	public void setApplication_id(String application_id) {
		this.application_id = application_id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getMore_details() {
		return more_details;
	}

	public void setMore_details(String more_details) {
		this.more_details = more_details;
	}

	public String getStart_date() {
		return start_date;
	}

	public void setStart_date(String start_date) {
		this.start_date = start_date;
	}

	public String getEnd_date() {
		return end_date;
	}

	public void setEnd_date(String end_date) {
		this.end_date = end_date;
	}

	public String getDuration() {
		return duration;
	}

	public void setDuration(String duration) {
		this.duration = duration;
	}

	public String getRegistration_process() {
		return registration_process;
	}

	public void setRegistration_process(String registration_process) {
		this.registration_process = registration_process;
	}

	public String getTarget_audience() {
		return target_audience;
	}

	public void setTarget_audience(String target_audience) {
		this.target_audience = target_audience;
	}

	public String getDelivery_notification() {
		return delivery_notification;
	}

	public void setDelivery_notification(String delivery_notification) {
		this.delivery_notification = delivery_notification;
	}

	public String getCreated_at() {
		return created_at;
	}

	public void setCreated_at(String created_at) {
		this.created_at = created_at;
	}

	public String getUpdated_at() {
		return updated_at;
	}

	public void setUpdated_at(String updated_at) {
		this.updated_at = updated_at;
	}

	public String getDeleted_at() {
		return deleted_at;
	}

	public void setDeleted_at(String deleted_at) {
		this.deleted_at = deleted_at;
	}

	public ArrayList<BeanEventLocation> getEvent_location() {
		return event_location;
	}

	public void setEvent_location(ArrayList<BeanEventLocation> event_location) {
		this.event_location = event_location;
	}

	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}

}
