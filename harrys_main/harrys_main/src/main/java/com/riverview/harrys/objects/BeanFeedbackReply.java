package com.riverview.harrys.objects;

import android.os.Parcel;
import android.os.Parcelable;

import java.io.Serializable;

public class BeanFeedbackReply implements Parcelable, Serializable {

    public static final String OBJ_NAME = "BeanFeedback";
    private static final long serialVersionUID = 5588254270496437773L;

    private int id, feedback_post_id, mobileuser_id, cms_user_id;
    private String reply,created_at, updated_at, deleted_at;

    public BeanFeedbackReply() {
    }

    public BeanFeedbackReply(Parcel parcel) {
        super();
        this.id = parcel.readInt();
        this.feedback_post_id = parcel.readInt();
        this.mobileuser_id = parcel.readInt();
        this.cms_user_id = parcel.readInt();
        this.reply = parcel.readString();
        this.created_at = parcel.readString();
        this.updated_at = parcel.readString();
        this.deleted_at = parcel.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
        dest.writeInt(feedback_post_id);
        dest.writeInt(mobileuser_id);
        dest.writeInt(cms_user_id);
        dest.writeString(reply);
        dest.writeString(created_at);
        dest.writeString(updated_at);
        dest.writeString(deleted_at);
    }

    public static final Creator<BeanFeedbackReply> CREATOR = new Creator<BeanFeedbackReply>() {

        @Override
        public BeanFeedbackReply createFromParcel(Parcel source) {
            return new BeanFeedbackReply(source);
        }

        @Override
        public BeanFeedbackReply[] newArray(int size) {
            return new BeanFeedbackReply[size];
        }
    };

    @Override
    public int describeContents() {
        // TODO Auto-generated method stub
        return 0;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getFeedback_post_id() {
        return feedback_post_id;
    }

    public void setFeedback_post_id(int feedback_post_id) {
        this.feedback_post_id = feedback_post_id;
    }

    public int getMobileuser_id() {
        return mobileuser_id;
    }

    public void setMobileuser_id(int mobileuser_id) {
        this.mobileuser_id = mobileuser_id;
    }

    public int getCms_user_id() {
        return cms_user_id;
    }

    public void setCms_user_id(int cms_user_id) {
        this.cms_user_id = cms_user_id;
    }

    public String getReply() {
        return reply;
    }

    public void setReply(String reply) {
        this.reply = reply;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(String updated_at) {
        this.updated_at = updated_at;
    }

    public String getDeleted_at() {
        return deleted_at;
    }

    public void setDeleted_at(String deleted_at) {
        this.deleted_at = deleted_at;
    }
}
