package com.riverview.harrys;

import uk.co.chrisjenx.calligraphy.CalligraphyConfig;
import uk.co.senab.actionbarpulltorefresh.extras.actionbarsherlock.PullToRefreshLayout;
import uk.co.senab.actionbarpulltorefresh.library.ActionBarPullToRefresh;
import uk.co.senab.actionbarpulltorefresh.library.listeners.OnRefreshListener;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;

import com.iapps.libs.generics.GenericDialogFragment;
import com.riverview.harrys.helpers.Api;
import com.riverview.harrys.helpers.Constants;
import com.riverview.harrys.helpers.Preference;

public class RCGenericDialogFragment extends DialogFragment {
	private Api api;
	private Preference pref;
	private ProgressDialog pd;
	private boolean backToHome = false;

	@Override
	public void onResume() {
		super.onResume();
		((MainActivity) getActivity()).invalidateMenuItem();

		if (backToHome)
			((MainActivity) getActivity()).onBackPressed();
	}

	public void toggleBackHome() {
		backToHome = true;
		((MainActivity) getActivity()).onBackPressed();
	}

	public MainActivity getHome() {
		return ((MainActivity) getActivity());
	}

	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState) {
		Dialog dialog = super.onCreateDialog(savedInstanceState);
		dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
		dialog.getWindow().setBackgroundDrawable(new ColorDrawable(0));
		Window window = dialog.getWindow();
		WindowManager.LayoutParams windowParams = window.getAttributes();
		windowParams.dimAmount = 0.50f;
		windowParams.flags |= WindowManager.LayoutParams.FLAG_DIM_BEHIND;
		window.setAttributes(windowParams);

		return dialog;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		CalligraphyConfig.initDefault(Constants.GLOBAL_FONT_APP);
		this.api = new Api(getActivity());
		this.pref = new Preference(getActivity());
		pd = new ProgressDialog(getActivity());
		setStyle(DialogFragment.STYLE_NORMAL, android.R.style.Theme);

	}

	public Api getApi() {
		return this.api;
	}

	public Preference getPref() {
		return this.pref;
	}

	public void showProgressBarCF() {

		try {
			if (!pd.isShowing()) {
				pd.setCancelable(true);
				pd.setMessage(getActivity().getResources().getString(
						R.string.loading));
				pd.setProgressStyle(ProgressDialog.STYLE_SPINNER);
				pd.show();
			}
		} catch (Exception e) {
		}

	}

	public PullToRefreshLayout getPullToRefresh(View view, int resId,
			OnRefreshListener listener) {
		PullToRefreshLayout mPullToRefresh = new PullToRefreshLayout(
				view.getContext());

		ActionBarPullToRefresh.from(getActivity())
				.insertLayoutInto((ViewGroup) view)
				.theseChildrenArePullable(resId).listener(listener)
				.setup(mPullToRefresh);

		return mPullToRefresh;
	}

	public void closeProgressBarCF() {
		try {
			pd.dismiss();
		} catch (Exception e) {
		}
	}

}