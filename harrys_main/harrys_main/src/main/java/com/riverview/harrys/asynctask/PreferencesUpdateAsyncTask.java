package com.riverview.harrys.asynctask;

import static com.riverview.harrys.constant.AppConst.ATTR_TAG_DEVICE_ID;
import static com.riverview.harrys.constant.AppConst.HTTP_METHOD_POST;
import static com.riverview.harrys.constant.AppConst.SERVICE_END_POINT;
import static com.riverview.harrys.constant.AppConst.URL_PREFERENCES;

import java.io.IOException;
import java.util.ArrayList;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.type.TypeReference;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.client.ClientHttpResponse;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.DefaultResponseErrorHandler;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.riverview.harrys.R;
import com.riverview.harrys.exception.RestServiceException;
import com.riverview.harrys.objects.BeanPreference;
import com.riverview.harrys.objects.ErrorServiceResponse;
import com.riverview.harrys.objects.ServiceResponse;
import com.riverview.harrys.util.AppUtil;
import com.riverview.harrys.util.WebServiceUtil;

import fr.castorflex.android.circularprogressbar.CircularProgressDrawable;

public class PreferencesUpdateAsyncTask extends
		AsyncTask<Void, Void, ServiceResponse<ArrayList<BeanPreference>>> {

	private static final String TAG = PreferencesUpdateAsyncTask.class
			.getSimpleName();

	public AsyncResponse<ServiceResponse<ArrayList<BeanPreference>>> asyncResponse;

	private ProgressDialog mProgressDialog;

	private Context mContext;

	String[] commandString = null;
	ArrayList<NameValuePair> httpParams = null;
	ArrayList<NameValuePair> inputParams = null;
	private boolean isShowProgress;

	public PreferencesUpdateAsyncTask(Context context,
			ArrayList<NameValuePair> params, boolean showProgress) {
		this.mContext = context;
		this.inputParams = params;
		this.isShowProgress = showProgress;
	}

	@Override
	protected void onPreExecute() {
		Log.d(TAG, "onPreExecute");
		super.onPreExecute();

		if (isShowProgress) {
			mProgressDialog = new ProgressDialog(mContext);
			mProgressDialog
					.setIndeterminateDrawable(new CircularProgressDrawable.Builder(
							mContext)
							.colors(mContext.getResources().getIntArray(
									R.array.harrys_colors)).sweepSpeed(1f)
							.strokeWidth(12f)
							.style(CircularProgressDrawable.Style.ROUNDED)
							.build());

			mProgressDialog.setCancelable(false);
			mProgressDialog.setMessage("Updating user preferences.....");
			mProgressDialog.show();
		}

		try {

			httpParams = AppUtil.generateAuthCommandString(inputParams,
					URL_PREFERENCES, HTTP_METHOD_POST);

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	@Override
	protected void onPostExecute(
			ServiceResponse<ArrayList<BeanPreference>> response) {
		Log.d(TAG, "onPostExecute");

		if (isShowProgress && mProgressDialog != null
				&& mProgressDialog.isShowing()) {
			mProgressDialog.dismiss();
			asyncResponse.processFinish(response);
		}

	}

	@Override
	protected ServiceResponse<ArrayList<BeanPreference>> doInBackground(
			Void... voids) {
		Log.d(TAG, "doInBackground");

		ServiceResponse<ArrayList<BeanPreference>> serviceResponse = null;

		RestTemplate restTemplate = WebServiceUtil.getRestTemplateInstance();

		restTemplate.setErrorHandler(new DefaultResponseErrorHandler() {

			@Override
			public void handleError(ClientHttpResponse response)
					throws IOException {
				ObjectMapper mapper = WebServiceUtil.getErrorObjMapper();
				ErrorServiceResponse errorRes;
				errorRes = mapper.readValue(response.getBody(),
						new TypeReference<ErrorServiceResponse>() {
						});

				throw new RestServiceException(errorRes, errorRes
						.getError_message());

			}

			protected boolean hasError(HttpStatus statusCode) {
				Log.d(TAG, "Error ----> " + statusCode);
				if (statusCode.value() == 200) {
					return false;
				}
				return true;
			}
		});

		String url = String.format(SERVICE_END_POINT + "%s", URL_PREFERENCES);

		HttpHeaders requestHeaders = new HttpHeaders();
		requestHeaders.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
		requestHeaders.set("Accept", "application/json");
		requestHeaders.add(ATTR_TAG_DEVICE_ID, AppUtil.getDeviceId(mContext));

		Log.d(TAG, "Device Id ----> " + AppUtil.getDeviceId(mContext));

		MultiValueMap<String, String> map = new LinkedMultiValueMap<String, String>();
		for (NameValuePair a : httpParams) {
			map.add(a.getName(), a.getValue());
		}

		HttpEntity<MultiValueMap<String, String>> requestEntity = new HttpEntity<MultiValueMap<String, String>>(
				map, requestHeaders);

		Log.d(TAG, "Url " + url);
		Log.d(TAG, "Request Entity " + requestEntity.toString());

		try {
			serviceResponse = restTemplate
					.exchange(
							url,
							HttpMethod.POST,
							requestEntity,
							new ParameterizedTypeReference<ServiceResponse<ArrayList<BeanPreference>>>() {
							}).getBody();

		} catch (RestClientException e) {
			serviceResponse = new ServiceResponse<ArrayList<BeanPreference>>();
			serviceResponse.setError(true);
			serviceResponse.setError_message(e.getCause().getMessage());

		} catch (Exception e) {
			e.printStackTrace();
		}
		return serviceResponse;
	}

	@Override
	protected void onCancelled() {

		if (isShowProgress && mProgressDialog != null
				&& mProgressDialog.isShowing()) {
			mProgressDialog.dismiss();
			asyncResponse.processFinish(null);
		}

	}
}
