package com.riverview.harrys.fragments;

import android.graphics.Typeface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;


import com.iapps.libs.helpers.BaseUIHelper;
import com.riverview.harrys.MainActivity;
import com.riverview.harrys.R;
import com.riverview.harrys.RCGenericFragment;
import com.riverview.harrys.helpers.Utils;
import com.riverview.harrys.objects.BeanProductItem;

public class ProductsDetailsFragment extends RCGenericFragment {

	private ImageView imgStoreDetail;
	private TextView textViewContent;
	private TextView textViewTitle;
	private LinearLayout LLSpaceContainer;
	private View v;
	private BeanProductItem productItem;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		// setHasOptionsMenu(true);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
							 Bundle savedInstanceState) {
		v = inflater.inflate(R.layout.product_detail_fragment_layout,
				container, false);

		imgStoreDetail = (ImageView)v.findViewById(R.id.imgStoreDetail);
		textViewContent = (TextView)v.findViewById(R.id.textViewContent);
		textViewTitle=(TextView)v.findViewById(R.id.textViewTitle);
		LLSpaceContainer = (LinearLayout)v.findViewById(R.id.LLSpaceContainer);

		productItem = (BeanProductItem) getArguments().getParcelable("details");
		setHasOptionsMenu(false);
		((MainActivity) getActivity()).setBackActionBar(productItem.getName(),View.INVISIBLE,"");

		return v;
	}

	public void onPrepareOptionsMenu(Menu menu) {
		// getSherlockActivity().getSupportMenuInflater().inflate(
		// R.menu.menu_product_detail, menu);
	}

	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);

		BaseUIHelper
				.loadImageWithPlaceholder(getActivity(),
						productItem.getOrgUrl(), imgStoreDetail,
						R.drawable.placeholder);

		Utils.setMarginByPercent(getActivity(), LLSpaceContainer, .02f);

		Utils.setTextViewFontSizeBasedOnScreenDensity(getActivity(),
				textViewContent, Utils.FONT_SMALLER_DENSITY_SIZE);
		Utils.setTextViewFontSizeBasedOnScreenDensity(getActivity(),
				textViewTitle, Utils.FONT_SMALL_DENSITY_SIZE, Typeface.BOLD);

		textViewTitle.setText(productItem.getName());
		textViewContent.setText(productItem.getDescription());

	}
}
