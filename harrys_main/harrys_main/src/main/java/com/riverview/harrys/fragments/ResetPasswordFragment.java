package com.riverview.harrys.fragments;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import org.joda.time.DateTime;


import android.app.DatePickerDialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.text.SpannableString;
import android.text.method.LinkMovementMethod;
import android.text.util.Linkify;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.iapps.libs.helpers.BaseHelper;
import com.iapps.libs.helpers.BaseHelper.ConfirmListener;
import com.riverview.harrys.MainActivity;
import com.riverview.harrys.R;
import com.riverview.harrys.RCGenericFragment;
import com.riverview.harrys.asynctask.AsyncResponse;
import com.riverview.harrys.asynctask.ResetPasswordAsyncTask;
import com.riverview.harrys.helpers.Constants;
import com.riverview.harrys.helpers.Utils;
import com.riverview.harrys.objects.ServiceResponse;
import com.riverview.harrys.util.AnaylaticsAppender;
import com.riverview.harrys.util.AppUtil;

public class ResetPasswordFragment extends RCGenericFragment implements
		AsyncResponse<ServiceResponse<Object>> {

	private static final String TAG = ResetPasswordFragment.class
			.getSimpleName();

	private LinearLayout LLSignIn;
	private EditText editTextUsername;
	private EditText editTextDob;
	private Button buttonSignIn;
	Calendar myCalendar;
	private View v;

	private AnaylaticsAppender mAppender = null;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		v = inflater.inflate(R.layout.reset_password_fragment_layout,
				container, false);
		LLSignIn = (LinearLayout)v.findViewById(R.id.LLSignIn);
		editTextUsername =(EditText)v.findViewById(R.id.editTextUsername);
		editTextDob = (EditText)v.findViewById(R.id.editTextDob);
		buttonSignIn = (Button)v.findViewById(R.id.buttonSignIn);
		Date date = new Date();
		myCalendar = new GregorianCalendar();
		myCalendar.setTime(date);
		myCalendar.add(Calendar.YEAR, -18);

		setHasOptionsMenu(false);
		((MainActivity) getActivity()).setBackActionBar(getHome()
				.getResources().getString(R.string.rc_reset_password),View.INVISIBLE,"");

		hideDefaultKeyboard();

		return v;
	}

	@Override
	public void onResume() {
		super.onResume();

		try {
			mAppender = AnaylaticsAppender.getInstance(getActivity());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void hideDefaultKeyboard() {
		try {
			getActivity().getWindow().setSoftInputMode(
					WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

			getActivity();
			InputMethodManager imm = (InputMethodManager) getActivity()
					.getSystemService(Context.INPUT_METHOD_SERVICE);
			imm.hideSoftInputFromWindow(editTextUsername.getWindowToken(), 0);

		} catch (Exception e) {
		}

	}

	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);

		LLSignIn.setOnClickListener(clickListeners);

		buttonSignIn.setText("Submit");
		buttonSignIn.setOnClickListener(clickListeners);

		Utils.setEditTextFontSizeBasedOnScreenDensity(getActivity(),
				editTextUsername, Utils.FONT_SMALL_DENSITY_SIZE);
		Utils.setEditTextFontSizeBasedOnScreenDensity(getActivity(),
				editTextDob, Utils.FONT_SMALL_DENSITY_SIZE);
		Utils.setButtonFontSizeBasedOnScreenDensity(getActivity(),
				buttonSignIn, Utils.FONT_SMALL_DENSITY_SIZE, Typeface.BOLD);
		
		setupCalendar();
	}

	View.OnClickListener clickListeners = new View.OnClickListener() {
		public void onClick(View v) {

			switch (v.getId()) {
			case R.id.textViewSignUp:
				getHome().setFragment(new SignUpFragment());
				break;

			case R.id.buttonSignIn:
				if (BaseHelper.isEmpty(editTextUsername)) {
					BaseHelper.showAlert(getActivity(),
							"Please fill all the blank fields.");
				} else {
					if (AppUtil.checkNetworkConnection(getActivity())) {
						resetPassword();
					} else {
						BaseHelper
								.confirm(
										getActivity(),
										"Network Unavaliable",
										"Please turn on network to connect with server",
										new ConfirmListener() {

											@Override
											public void onYes() {
												AppUtil.showSystemSettingsDialog(getActivity());
											}
										});
					}
					hideDefaultKeyboard();
				}
				break;

			default:
				break;
			}
		}
	};
	
	public void setupCalendar() {

		editTextDob.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				new DatePickerDialog(getActivity(), date, myCalendar
						.get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
						myCalendar.get(Calendar.DAY_OF_MONTH)).show();
			}
		});

	}
	DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {

		@Override
		public void onDateSet(DatePicker view, int year, int monthOfYear,
							  int dayOfMonth) {
			// TODO Auto-generated method stub
			myCalendar.set(Calendar.YEAR, year);
			myCalendar.set(Calendar.MONTH, monthOfYear);
			myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);

			DateTime dt = DateTime.now().withYear(myCalendar.get(Calendar.YEAR))
					.withMonthOfYear(myCalendar.get(Calendar.MONTH) + 1)
					.withDayOfMonth(myCalendar.get(Calendar.DAY_OF_MONTH));
			editTextDob.setText(dt.toString(Constants.DATE_JSON));
		}

	};


	private void resetPassword() {

		ResetPasswordAsyncTask mAuthTask = new ResetPasswordAsyncTask(
				getHome(), editTextUsername.getText().toString().trim(), editTextDob.getText().toString().trim());
		mAuthTask.asyncResponse = ResetPasswordFragment.this;
		mAuthTask.execute((Void) null);

	}

	@Override
	public void onDestroy() {

		super.onDestroy();
		hideDefaultKeyboard();
	}

	@Override
	public void onDetach() {
		super.onDetach();
		hideDefaultKeyboard();
	}

	@Override
	public void processFinish(ServiceResponse<Object> details) {

		if (details == null) {
			try {
				BaseHelper.showAlert(getHome(),
						"Error when resetting user password.");
			} catch (Exception e) {
			}
			return;
		}

		if (details != null && details.getStatus() == 200 && !details.error) {
			Log.d(TAG, "Error ---> "  + details.error);
			try {
				TextView message = new TextView(getActivity());
				message.setTypeface(Typeface.createFromAsset(getActivity()
						.getAssets(), "Georgia.ttf"));
				message.setTextSize(TypedValue.COMPLEX_UNIT_SP, 18f);
				message.setPadding(40, 40, 0, 40);
				final SpannableString s = new SpannableString(getActivity()
						.getString(R.string.rc_reset_password_notification));
				Linkify.addLinks(s, Linkify.EMAIL_ADDRESSES
						| Linkify.PHONE_NUMBERS);

				message.setText(s);
				message.setMovementMethod(LinkMovementMethod.getInstance());
				BaseHelper.showAlert(getHome(), "Harry's ", message);

				mAppender.updateAnaylitsData(AppUtil
						.createAnalyticsJsonEventAction("password_change",
								editTextUsername.getText().toString(), "Reset",
								null));
			} catch (Exception e) {
				e.printStackTrace();
			}

		} else {
			try {
				BaseHelper
						.showAlert(
								getHome(),
								"Harry's ",
								(BaseHelper.isEmpty(details.getError_message()) ? "Unable to reset user password."
										: details.getError_message()));
			} catch (Exception e) {
				e.printStackTrace();
			}

		}
	}
}
