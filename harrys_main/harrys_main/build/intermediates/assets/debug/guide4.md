######Using a Coupon or Voucher

Coupons & Vouchers stored in your wallet can be used when paying for your purchases :
1.  Check your App Wallet for available Vouchers2.  Select the Coupon or Voucher you want to use3.  Tell the Cashier you want to use a Coupon or Voucher4.  Scan your QR Code at the counter5.  The Cashier will check the validity6.  The Coupon or Voucher will be applied to your purchase & removed from your wallet after use