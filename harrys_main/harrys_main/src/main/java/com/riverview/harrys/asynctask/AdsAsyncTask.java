package com.riverview.harrys.asynctask;

import static com.riverview.harrys.constant.AppConst.API_GET_ADS;
import static com.riverview.harrys.constant.AppConst.ATTR_TAG_UDID;
import static com.riverview.harrys.constant.AppConst.HTTP_METHOD_GET;
import static com.riverview.harrys.constant.AppConst.SERVICE_END_POINT;

import java.io.IOException;
import java.util.ArrayList;

import org.apache.http.NameValuePair;
import org.apache.http.client.utils.URLEncodedUtils;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.type.TypeReference;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.client.ClientHttpResponse;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.DefaultResponseErrorHandler;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import android.app.Fragment;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v4.app.FragmentManager;
import android.util.Log;
import android.widget.Toast;

import com.riverview.harrys.MainActivity;
import com.riverview.harrys.R;
import com.riverview.harrys.SplashAdActivity;
import com.riverview.harrys.exception.RestServiceException;
import com.riverview.harrys.helpers.SharedPref;
import com.riverview.harrys.objects.BeanAdZone;
import com.riverview.harrys.objects.BeanAds;
import com.riverview.harrys.objects.BeanCompanyData;
import com.riverview.harrys.objects.ErrorServiceResponse;
import com.riverview.harrys.objects.ServiceResponse;
import com.riverview.harrys.util.AppUtil;
import com.riverview.harrys.util.WebServiceUtil;

public class AdsAsyncTask extends
		AsyncTask<Void, Void, ServiceResponse<ArrayList<BeanAds>>> {

	private static final String TAG = AdsAsyncTask.class.getSimpleName();

	public AsyncResponse<ServiceResponse<ArrayList<BeanAds>>> asyncResponse;
	private Context mContext;
	private String addDateURL;

	ArrayList<NameValuePair> httpParams = null;

	public AdsAsyncTask(Context context) {
		this.mContext = context;
	}

	@Override
	protected void onPreExecute() {
		// TODO Auto-generated method stub
		super.onPreExecute();
		try {
			addDateURL = String.format(API_GET_ADS);
			httpParams = AppUtil.generateAuthCommandString(null, addDateURL,
					HTTP_METHOD_GET);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	protected ServiceResponse<ArrayList<BeanAds>> doInBackground(Void... arg0) {
		ServiceResponse<ArrayList<BeanAds>> serviceResponse = null;

		RestTemplate restTemplate = WebServiceUtil.getRestTemplateInstance();

		restTemplate.setErrorHandler(new DefaultResponseErrorHandler() {
			@Override
			public void handleError(ClientHttpResponse response)
					throws IOException {
				ObjectMapper mapper = WebServiceUtil.getErrorObjMapper();
				ErrorServiceResponse errorRes;
				errorRes = mapper.readValue(response.getBody(),
						new TypeReference<ErrorServiceResponse>() {
						});

				throw new RestServiceException(errorRes, errorRes
						.getError_message());
			}

			protected boolean hasError(HttpStatus statusCode) {
				Log.d(TAG, "Status ----> " + statusCode);
				if (statusCode.value() == 200) {
					return false;
				}
				return true;
			}
		});

		String url = String.format(SERVICE_END_POINT + "%s", addDateURL);

		String urlParams = URLEncodedUtils.format(httpParams, "UTF-8");
		String finalUrl = String.format(url + "?" + "%s", urlParams);

		Log.d(TAG, "Final URL ------> " + finalUrl);

		HttpHeaders requestHeaders = new HttpHeaders();
		requestHeaders.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
		requestHeaders.set("Accept", "application/json");
		requestHeaders.add(ATTR_TAG_UDID, AppUtil.getDeviceId(mContext));

		HttpEntity<MultiValueMap<String, String>> requestEntity = new HttpEntity<MultiValueMap<String, String>>(
				null, requestHeaders);

		try {
			serviceResponse = restTemplate
					.exchange(
							finalUrl,
							HttpMethod.GET,
							requestEntity,
							new ParameterizedTypeReference<ServiceResponse<ArrayList<BeanAds>>>() {
							}).getBody();

		} catch (RestClientException e) {
			serviceResponse = new ServiceResponse<ArrayList<BeanAds>>();
			serviceResponse.setError(true);
			//Toast.makeText(mContext, "Exception Error", Toast.LENGTH_LONG).show();
			// serviceResponse.setError_message(e.getCause().getMessage());

		} catch (Exception e) {
			e.printStackTrace();
			//Toast.makeText(mContext, "Exception", Toast.LENGTH_LONG).show();
		}
		return serviceResponse;
	}

	@Override
	protected void onPostExecute(ServiceResponse<ArrayList<BeanAds>> response) {
		if (response != null && response.getStatus() == 200) {
			//Toast.makeText(mContext, "postExecute() -- ", Toast.LENGTH_LONG).show();
			ArrayList<BeanAds> adsDataList = response.getResponse();
			
			
			if(adsDataList != null && !adsDataList.isEmpty()){
				//Toast.makeText(mContext, "postExecute(HEE)", Toast.LENGTH_LONG).show();
				BeanAds adObject = null;
				BeanAdZone adZone = null;
				String time = "";
				String imageurl = "";
				for(int i=0;i<adsDataList.size();i++){
					BeanAds adObjectTemp = adsDataList.get(i);
					BeanAdZone adZoneTemp = adObjectTemp.getAdszone();
					if(adZoneTemp.getName().equals("SPLASH")){
						adObject = adObjectTemp;
						adZone = adZoneTemp;
						
						break;
					}
				}
				if(adZone != null && adZone.getName().equals("SPLASH")){
					imageurl = adObject.getImageurl();
					time = adObject.getDisplay_time();
					if(SharedPref.getString((MainActivity)mContext, "SPLASH_AD_IMAGE_URL", "").equals("")){
						SharedPref.saveString((MainActivity)mContext, "SPLASH_AD_IMAGE_URL", imageurl);
						
						if (SharedPref.getString((MainActivity)mContext,
								"SPLASH_AD_IMAGE_SHOW_TIME", "").equals("")) {

							Intent intent = new Intent(mContext, SplashAdActivity.class);
							intent.putExtra("time", String.valueOf(1000*Integer.parseInt(time)));
							intent.putExtra("imgPath", imageurl);
							
							mContext.startActivity(intent);
						}else{
							SharedPref.saveString((MainActivity)mContext, "SPLASH_AD_IMAGE_SHOW_TIME", "");
						}
					}else{
						SharedPref.saveString((MainActivity)mContext, "SPLASH_AD_IMAGE_URL", imageurl);
					}
					
					
					//Toast.makeText(mContext, "ImageURL: "+imageurl+", time: "+time, Toast.LENGTH_LONG).show();
					
//					
//					Intent intent = new Intent(mContext, SplashAdActivity.class);
//					intent.putExtra("time", String.valueOf(1000*Integer.parseInt(time)));
//					intent.putExtra("imgPath", imageurl);
//					
//					mContext.startActivity(intent);
				}
			}else{
				//Toast.makeText(mContext, "Bean Ads Array Empty", Toast.LENGTH_LONG).show();
				}
		}else{
		//	Toast.makeText(mContext, "null responce", Toast.LENGTH_LONG).show();
		}
	}
	
	@Override
    protected void onCancelled() {
        asyncResponse.processFinish(null);
    }

}
