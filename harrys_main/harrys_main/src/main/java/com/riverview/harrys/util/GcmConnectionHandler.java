package com.riverview.harrys.util;

import static com.riverview.harrys.constant.AppConst.GOOGLE_PROJECT_NUMBER;

import java.io.IOException;

import android.content.Context;
import android.util.Log;

import com.google.android.gms.gcm.GoogleCloudMessaging;

public class GcmConnectionHandler {

	private static final String TAG = GcmConnectionHandler.class
			.getSimpleName();

	private Context mContext;

	public GcmConnectionHandler(Context mContext) {
		super();
		this.mContext = mContext;
	}


	public String getGcmRegistrationId() {
		Log.d(TAG, "getGcmRegistrationId");

		int currentAppVersion = AppUtil.getAppVersion(mContext);

		// Check GCM registration process. If the application is upgraded, so
		// the GCm id could be cleared or not compatible.
		// So it should be generated again.
		String mCloudMessagingId = null;

		// Device isn't register with GCM or not contain latest value
		GoogleCloudMessaging mCloudMessaging = GoogleCloudMessaging
				.getInstance(mContext);
		try {
			mCloudMessagingId = mCloudMessaging.register(GOOGLE_PROJECT_NUMBER);
			Log.d(TAG, "GCM Id " + mCloudMessagingId);
		} catch (IOException e) {
			e.printStackTrace();
		}

		return mCloudMessagingId;
	}
}
