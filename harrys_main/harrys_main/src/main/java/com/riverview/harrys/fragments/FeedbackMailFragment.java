package com.riverview.harrys.fragments;

import java.io.File;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager.NameNotFoundException;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.animation.AccelerateInterpolator;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.iapps.libs.helpers.BaseHelper;
import com.iapps.libs.helpers.BaseUIHelper;
import com.nineoldandroids.animation.ValueAnimator;
import com.riverview.harrys.MainActivity;
import com.riverview.harrys.R;
import com.riverview.harrys.RCGenericFragment;
import com.riverview.harrys.helpers.HeightEvaluator;
import com.riverview.harrys.helpers.SharedPref;
import com.riverview.harrys.helpers.Utils;
import com.riverview.harrys.util.AppUtil;

import static com.riverview.harrys.MainActivity.result_main_image;

public class FeedbackMailFragment extends RCGenericFragment {

	private static final String TAG = FeedbackMailFragment.class.getSimpleName();

	private TextView textSelectFeedback;
	private TextView textTopicData;
	private TextView textDeviceData;
	private TextView textOsData;
	private TextView textAppNameData;
	private TextView textVersionData;
	private TextView textBuildData;
	private ImageView imageData;
	private EditText editTextDescription;
	private LinearLayout LLMainContainer;
	private LinearLayout LLMenuCamara;
	private LinearLayout LLMenuCancel;
	private LinearLayout LLMenuGallary;
	private LinearLayout LLCardOptionSpace;
	private TextView textMenuTransactions;
	private TextView textMenuAddCard;
	private TextView textMenuCancel;
	private FrameLayout slideDownView;

	private static final int VIEW_ANIMATION_INTERVAL = 300;
	private static float height = 0f;

	private static final int RESULT_LOAD_IMAGE = 1;
	private static final int RESULT_LOAD_CAMARA = 2;
	private static final int RESULT_LOAD_EMAIL = 3;
	private String path = null;
	Uri uriLatest;

	private Uri fileUri = null;

	private Dialog dialog;

	private View v;
	
	private final String[] natArray = new String[] { "Service Quality",
			"Menu Quality", "Store Facility", "Other Issue" };

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		v = inflater.inflate(R.layout.feedback_mail_layout, container,
				false);


		textSelectFeedback = (TextView)v.findViewById(R.id.textSelectFeedback);
		textTopicData = (TextView)v.findViewById(R.id.textTopicData);
		textDeviceData = (TextView)v.findViewById(R.id.textDeviceData);
		textOsData = (TextView)v.findViewById(R.id.textOsData);
		textAppNameData = (TextView)v.findViewById(R.id.textAppNameData);
		textVersionData = (TextView)v.findViewById(R.id.textVersionData);
		textBuildData = (TextView)v.findViewById(R.id.textBuildData);


		imageData = (ImageView)v.findViewById (R.id.imageData);
		editTextDescription = (EditText)v.findViewById(R.id.editTextDescription);
		LLMainContainer = (LinearLayout)v.findViewById (R.id.LLMainContainer);

		LLMenuCamara = (LinearLayout)v.findViewById (R.id.LLMenuCamara);
		LLMenuCancel = (LinearLayout)v.findViewById (R.id.LLMenuCancel);
		LLMenuGallary = (LinearLayout)v.findViewById (R.id.LLMenuGallary);
		LLCardOptionSpace = (LinearLayout)v.findViewById (R.id.cardOptionSpace);

		textMenuTransactions = (TextView)v.findViewById(R.id.textMenuTransactions);
		textMenuAddCard = (TextView)v.findViewById(R.id.textMenuAddCard);
		textMenuCancel = (TextView)v.findViewById(R.id.textMenuCancel);
		slideDownView = (FrameLayout)v.findViewById(R.id.feedback_options_slideup_layout);

		setHasOptionsMenu(false);
		((MainActivity) getActivity()).setBackActionBar(getHome()
				.getResources().getString(R.string.rc_menu_feedback),View.VISIBLE,"mail");
		hideDefaultKeyboard();
		result_main_image.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				sendMail();
			}
		});

		return v;
	}

	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
		inflater.inflate(R.menu.menu_feedback, menu);
	}

	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.rv_menu_feedback:

			sendMail();
			
			break;

		default:
			return true;
		}
		return true;
	}

	private void hideDefaultKeyboard() {
		try {
			getActivity().getWindow().setSoftInputMode(
					WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

			getActivity();
			InputMethodManager imm = (InputMethodManager) getActivity()
					.getSystemService(Context.INPUT_METHOD_SERVICE);

			imm.hideSoftInputFromWindow(editTextDescription.getWindowToken(), 0);
		} catch (Exception e) {
		}

	}

	private PackageInfo getPackageData() {
		PackageInfo pInfo;
		try {
			pInfo = getHome().getPackageManager().getPackageInfo(
					getHome().getPackageName(), 0);
			return pInfo;
		} catch (NameNotFoundException e) {
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);

		textSelectFeedback.setOnClickListener(clickListeners);
		textTopicData.setOnClickListener(clickListeners);

		LLMenuCancel.setOnClickListener(clickListeners);
		LLMenuGallary.setOnClickListener(clickListeners);
		LLMenuCamara.setOnClickListener(clickListeners);
		LLCardOptionSpace.setOnClickListener(clickListeners);

		Utils.setTextViewFontSizeBasedOnScreenDensity(getActivity(),
				textMenuTransactions, 18f);
		Utils.setTextViewFontSizeBasedOnScreenDensity(getActivity(),
				textMenuAddCard, 18f);
		Utils.setTextViewFontSizeBasedOnScreenDensity(getActivity(), editTextDescription, 25f);
		Utils.setTextViewFontSizeBasedOnScreenDensity(getActivity(),
				textMenuCancel, 14f, Typeface.BOLD);

		textTopicData.setText(natArray[0]);
		textDeviceData.setText(AppUtil.getDeviceName());
		textOsData.setText(String.valueOf(Build.VERSION.SDK_INT));

		try {

			PackageInfo pInfo = getPackageData();
			if (pInfo != null) {
				textVersionData.setText(pInfo.versionName);
				textBuildData.setText(String.valueOf(pInfo.versionCode));
				textAppNameData
						.setText(getString(pInfo.applicationInfo.labelRes));

			}
		} catch (Exception e) {
			
		}
	}

	public void showTopicsSpinnerDialog() {
		AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

		try {
			builder.setTitle("Topics");
			builder.setItems(natArray, new DialogInterface.OnClickListener() {

				@Override
				public void onClick(DialogInterface dialog, int pos) {
					textTopicData.setText(natArray[pos]);
				}
			});

			builder.setNegativeButton("Cancel",
					new DialogInterface.OnClickListener() {

						@Override
						public void onClick(DialogInterface dialog, int which) {

						}
					});

			try {
				if (!dialog.isShowing()) {
					dialog = builder.create();
					dialog.show();
				}
			} catch (Exception e) {
				try {
					dialog = builder.create();
					dialog.show();
				} catch (Exception e1) {
				}
			}

		} catch (Exception e) {
		}
	}

	View.OnClickListener clickListeners = new View.OnClickListener() {
		public void onClick(View v) {

			switch (v.getId()) {
			case R.id.textSelectFeedback:
				hideDefaultKeyboard();
				toggle(slideDownView);

				break;

			case R.id.textTopicData:
				showTopicsSpinnerDialog();
				break;

			case R.id.LLMenuCancel:
				Log.d(TAG, "Menu Cancel");
				closeToggle();
				break;

			case R.id.LLMenuGallary:
				Intent i = new Intent(
						Intent.ACTION_PICK,
						android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
				if (i.resolveActivity(getHome().getPackageManager()) != null) {
					startActivityForResult(i, RESULT_LOAD_IMAGE);
				} else {
					Toast.makeText(getHome(),
							"No application avaliable to complete this action",
							Toast.LENGTH_SHORT).show();
				}

				// Close toggle menu
				closeToggle();
				break;

			case R.id.LLMenuCamara:

				startCamara();

				// Close toggle menu
				closeToggle();
				break;
			case R.id.cardOptionSpace:
				Log.d(TAG, "Menu Cancel");
				closeToggle();
				break;

			default:
				break;
			}

		}
	};

	public void startCamara() {

		ContentValues values = new ContentValues();
		values.put(MediaStore.Images.Media.TITLE,
				"IMG_" + String.valueOf(System.currentTimeMillis()) + ".jpg");

		Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

		// to save the image (this doesn't work at all for images)
		fileUri = getHome().getContentResolver().insert(
				MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values);
		intent.putExtra(MediaStore.EXTRA_OUTPUT, fileUri);

		// start the image capture Intent
		if (intent.resolveActivity(getHome().getPackageManager()) != null) {
			startActivityForResult(intent, RESULT_LOAD_CAMARA);
		} else {
			Toast.makeText(getHome(),
					"No application avaliable to complete this action",
					Toast.LENGTH_SHORT).show();
		}
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {

		super.onActivityResult(requestCode, resultCode, data);

		if (requestCode == RESULT_LOAD_IMAGE
				&& resultCode == Activity.RESULT_OK && data != null) {

			loadImage(data.getData());

		} else if (requestCode == RESULT_LOAD_CAMARA
				&& resultCode == Activity.RESULT_OK) {

			if (fileUri != null)
				loadImage(fileUri);
		}
	}

	private void loadImage(Uri uri) {
		uriLatest = uri;
		String[] filePathColumn = { MediaStore.Images.Media.DATA };

		Cursor cursor = getHome().getContentResolver().query(uri,
				filePathColumn, null, null, null);
		cursor.moveToFirst();

		int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
		path = cursor.getString(columnIndex);

		try {

			File imgFile = new File(path);

			if (imgFile.exists()) {
				Bitmap myBitmap = BitmapFactory.decodeFile(imgFile
						.getAbsolutePath());
				imageData.setImageBitmap(myBitmap);
			}

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (cursor != null) {
				cursor.close();
			}
		}

	}

	private void toggle(final FrameLayout v) {

		LLMainContainer.setEnabled(false);

		height = BaseUIHelper.getScreenHeight(getHome());
		height = (float) (height * 1);

		v.setVisibility(View.VISIBLE);
		ValueAnimator va = ValueAnimator.ofFloat(0f, height).setDuration(
				VIEW_ANIMATION_INTERVAL);
		va.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
			public void onAnimationUpdate(ValueAnimator animation) {
				Integer value = (Integer) Math.round((float) animation
						.getAnimatedValue());
				v.getLayoutParams().height = value.intValue();
				v.invalidate();
				v.requestLayout();

			}
		});
		va.setInterpolator(new AccelerateInterpolator(8));
		va.start();

	}

	private void closeToggle() {

		LLMainContainer.setEnabled(true);

		int startHeight = slideDownView.getHeight();
		ValueAnimator animation = ValueAnimator.ofObject(
				new HeightEvaluator(slideDownView), startHeight, (int) 0)
				.setDuration(VIEW_ANIMATION_INTERVAL);
		animation.setInterpolator(new AccelerateInterpolator(8));
		animation.start();
	}

	public void sendMail() {
		Intent emailIntent = new Intent(android.content.Intent.ACTION_SEND);
		emailIntent.setType("message/rfc822");
		
		StringBuilder sb = new StringBuilder();
		sb.append("Harry's : ");
		sb.append(textTopicData.getText());

		emailIntent.putExtra(android.content.Intent.EXTRA_EMAIL,
				new String[] { "marketing@harrys.com.sg" });
		emailIntent.putExtra(android.content.Intent.EXTRA_SUBJECT,
				sb.toString());

		// Create description data
		sb.setLength(0);
		sb.append("Feedback Category : \n");
		sb.append(textTopicData.getText());
		sb.append("\n\n");
		sb.append("Description : \n");
		sb.append((BaseHelper.isEmpty(editTextDescription.getText().toString())) ? "Description not avaliable"
				: editTextDescription.getText().toString());
		sb.append("\n\n");
		sb.append("Application Data : \n");
		sb.append("Name - ").append(textAppNameData.getText());
		sb.append("\n");
		sb.append("Version - ").append(textVersionData.getText());
		sb.append("\n");
		sb.append("Build - ").append(textBuildData.getText());
		sb.append("\n\n");

		sb.append("Device Data : \n");
		sb.append("Device Type - ").append(textDeviceData.getText());
		sb.append("\n");
		sb.append("OS version - ").append(textOsData.getText());
		sb.append("\n\n");
		
		sb.append("User Data : \n");
		sb.append("First name - ").append(SharedPref
				.getString(getHome(), SharedPref.USERFNAME_KEY, ""));
		sb.append("\n");
		sb.append("Last Name - ").append(SharedPref
				.getString(getHome(), SharedPref.USERLNAME_KEY, ""));
		sb.append("\n");
		sb.append("Mobile Number - ").append(SharedPref
				.getString(getHome(), SharedPref.MOBILE_NO_KEY, ""));
		sb.append("\n");
		sb.append("Email - ").append(SharedPref
				.getString(getHome(), SharedPref.EMAIL_KEY, ""));
		sb.append("\n\n");
		
		emailIntent.putExtra(android.content.Intent.EXTRA_TEXT, sb.toString());
		if (path != null) {
			emailIntent.setType("image/jpeg");
			emailIntent.setFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
			Uri myUri = uriLatest;
			emailIntent.putExtra(Intent.EXTRA_STREAM, myUri);
		}
		// startActivityForResult(Intent.createChooser(emailIntent,
		// "Sending feedback..."), RESULT_LOAD_EMAIL);
		if (emailIntent.resolveActivity(getHome().getPackageManager()) != null) {
			startActivityForResult(emailIntent, RESULT_LOAD_EMAIL);
		} else {
			Toast.makeText(getHome(),
					"No application avaliable to complete this action",
					Toast.LENGTH_SHORT).show();
		}
	}


}
