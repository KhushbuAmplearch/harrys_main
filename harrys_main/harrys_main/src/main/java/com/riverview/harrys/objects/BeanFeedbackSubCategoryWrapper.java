package com.riverview.harrys.objects;

import java.util.ArrayList;

public class BeanFeedbackSubCategoryWrapper {

    public static final String OBJ_NAME = "BeanFeedbackCategoryWrapper";

    private ArrayList<BeanFeedbackCatagory> sub_categories;

    public ArrayList<BeanFeedbackCatagory> getSub_categories() {
        return sub_categories;
    }

    public void setSub_categories(ArrayList<BeanFeedbackCatagory> sub_categories) {
        this.sub_categories = sub_categories;
    }

    @Override
    public String toString() {
        return "BeanFeedbackSubCategoryWrapper{" +
                "sub_categories=" + sub_categories +
                '}';
    }
}
