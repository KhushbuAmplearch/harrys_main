package com.riverview.harrys;

import uk.co.chrisjenx.calligraphy.CalligraphyConfig;
import uk.co.senab.actionbarpulltorefresh.extras.actionbarsherlock.PullToRefreshLayout;
import uk.co.senab.actionbarpulltorefresh.library.ActionBarPullToRefresh;
import uk.co.senab.actionbarpulltorefresh.library.listeners.OnRefreshListener;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.iapps.libs.generics.GenericFragment;
import com.riverview.harrys.helpers.Api;
import com.riverview.harrys.helpers.Constants;
import com.riverview.harrys.helpers.Preference;

public class RCGenericFragment extends GenericFragment {

	private static final String TAG = RCGenericFragment.class.getSimpleName();

	private Api api;
	private Preference pref;
	private ProgressDialog pd;
	private boolean backToHome = false;

	@Override
	public void onResume() {
		Log.d(TAG, "onResume");
		super.onResume();
		((MainActivity) getActivity()).invalidateMenuItem();

		if (backToHome)
			((MainActivity) getActivity()).onBackPressed();
	}

	public void toggleBackHome() {
		Log.d(TAG, "toggleBackHome()");
		backToHome = true;
		((MainActivity) getActivity()).onBackPressed();
	}

	public MainActivity getHome() {
		return ((MainActivity) getActivity());
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		CalligraphyConfig.initDefault(Constants.GLOBAL_FONT_APP);
		this.api = new Api(getActivity());
		this.pref = new Preference(getActivity());
		pd = new ProgressDialog(getActivity());
		setHasOptionsMenu(false);
	}

	public Api getApi() {
		return this.api;
	}

	public Preference getPref() {
		return this.pref;
	}

	public void showProgressBarCF() {

		try {
			if (!pd.isShowing()) {
				pd.setCancelable(true);
				pd.setMessage(getActivity().getResources().getString(
						R.string.loading));
				pd.setProgressStyle(ProgressDialog.STYLE_SPINNER);
				pd.show();
			}
		} catch (Exception e) {
		}

	}

	/*
	 * 
	 * Added to recover from calligraphy error and can fix when lib version 2.0
	 * available as jar. Then we can remove below overridden method.
	 * 
	 * (non-Javadoc)
	 * 
	 * @see android.support.v4.app.Fragment#getLayoutInflater(android.os.Bundle)
	 */

	@Override
	public LayoutInflater getLayoutInflater(Bundle savedInstanceState) {
		return getActivity().getLayoutInflater();
	}

	public PullToRefreshLayout getPullToRefresh(View view, int resId,
			OnRefreshListener listener) {
		PullToRefreshLayout mPullToRefresh = new PullToRefreshLayout(
				view.getContext());

		ActionBarPullToRefresh.from(getActivity())
				.insertLayoutInto((ViewGroup) view)
				.theseChildrenArePullable(resId).listener(listener)
				.setup(mPullToRefresh);

		return mPullToRefresh;
	}

	public void closeProgressBarCF() {
		try {
			pd.dismiss();
		} catch (Exception e) {
		}
	}

}