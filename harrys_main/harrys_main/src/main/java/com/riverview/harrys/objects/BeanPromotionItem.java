package com.riverview.harrys.objects;

import java.io.Serializable;

import android.os.Parcel;
import android.os.Parcelable;

public class BeanPromotionItem implements Parcelable, Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2686762857860641551L;

	public static final String OBJ_NAME = "BeanStoreItem";

	private int id;
	private String name, description, startdate, enddate, imageurl, terms,
			coupon_title;

	public BeanPromotionItem(int id, String name) {
		super();
		this.id = id;
		this.name = name;

	}

	public BeanPromotionItem(Parcel parcel) {
		super();
		this.id = parcel.readInt();
		this.name = parcel.readString();
		this.description = parcel.readString();
		this.startdate = parcel.readString();
		this.enddate = parcel.readString();
		this.imageurl = parcel.readString();
		this.terms = parcel.readString();
		this.coupon_title = parcel.readString();
	}

	public static final Parcelable.Creator<BeanPromotionItem> CREATOR = new Parcelable.Creator<BeanPromotionItem>() {

		@Override
		public BeanPromotionItem createFromParcel(Parcel source) {
			return new BeanPromotionItem(source);
		}

		@Override
		public BeanPromotionItem[] newArray(int size) {
			return new BeanPromotionItem[size];
		}
	};

	@Override
	public int describeContents() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public void writeToParcel(Parcel out, int flag) {
		out.writeInt(id);
		out.writeString(name);
		out.writeString(description);
		out.writeString(startdate);
		out.writeString(enddate);
		out.writeString(imageurl);
		out.writeString(terms);
		out.writeString(coupon_title);
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getStartdate() {
		return startdate;
	}

	public void setStartdate(String startdate) {
		this.startdate = startdate;
	}

	public String getEnddate() {
		return enddate;
	}

	public void setEnddate(String enddate) {
		this.enddate = enddate;
	}

	public String getImageurl() {
		return imageurl;
	}

	public void setImageurl(String imageurl) {
		this.imageurl = imageurl;
	}

	public String getTerms() {
		return terms;
	}

	public void setTerms(String terms) {
		this.terms = terms;
	}

	public String getCoupon_title() {
		return coupon_title;
	}

	public void setCoupon_title(String coupon_title) {
		this.coupon_title = coupon_title;
	}
}
