package com.riverview.harrys.helpers;

import android.app.Activity;
import android.util.Log;
import android.widget.Button;

public class Waiter extends Thread {
	private static final String TAG = Waiter.class.getName();
	private long lastUsed;
	private long period;
	private boolean stop;
	private Activity context;
	private int mode; // mode 1 - check idle, mode 2 - check continue or not
	private Button tvCount;

	public Waiter(long period, Activity ctx, int mode) {
		context = ctx;
		this.period = period;
		stop = false;
		this.mode = mode;

	}

	public Waiter(long period, Activity ctx, int mode, Button tvCount) {
		context = ctx;
		this.period = period;
		stop = false;
		this.mode = mode;
		this.tvCount = tvCount;
		tvCount.setEnabled(false);
	}

	long idle;

	public void run() {
		idle = 0;
		this.touch();
		do {
			idle = System.currentTimeMillis() - lastUsed;
			if (mode == 1)
				Log.d(TAG, "Application is idle for " + idle + " ms");
			else
				Log.d(TAG, "Waiting continue or not for " + idle + " ms");

			try {
				context.runOnUiThread(new Runnable() {

					public void run() {

						try {
							tvCount.setText("Resend code in "
									+ String.valueOf(Math
											.round((60 - (idle / 1000)))));
						} catch (Exception e) {

						}
					}
				});

				Thread.sleep(1000); // check every 1 seconds
			} catch (InterruptedException e) {
				Log.d(TAG, "Waiter interrupted!");
			}
			if (idle > period) {
				stop = true;
				idle = 0;

				// do something here - e.g. call popup or so

				if (mode == 1) {

					context.runOnUiThread(new Runnable() {

						public void run() {

							try {
								tvCount.setEnabled(true);
								tvCount.setText("Resend code");
							} catch (Exception e) {

							}
						}
					});

				} else {

				}

			}
		} while (!stop);
		Log.d(TAG, "Finishing Waiter thread");
	}

	public synchronized void touch() {
		lastUsed = System.currentTimeMillis();
	}

	public synchronized void forceInterrupt() {
		this.interrupt();
	}

	// soft stopping of thread
	public synchronized void stopIT() {
		stop = true;
	}

	public synchronized void setPeriod(long period) {
		this.period = period;
	}

}