package com.riverview.harrys.objects;

import android.os.Parcel;
import android.os.Parcelable;

import java.io.Serializable;

public class BeanFeedbackCatagory implements Parcelable, Serializable {

    public static final String OBJ_NAME = "BeanAds";
    private static final long serialVersionUID = 5588254270496437773L;

    private int id;
    private String title;

    public BeanFeedbackCatagory(Parcel parcel) {
        super();
        this.id = parcel.readInt();
        this.title = parcel.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
        dest.writeString(title);
    }

    public static final Creator<BeanFeedbackCatagory> CREATOR = new Creator<BeanFeedbackCatagory>() {

        @Override
        public BeanFeedbackCatagory createFromParcel(Parcel source) {
            return new BeanFeedbackCatagory(source);
        }

        @Override
        public BeanFeedbackCatagory[] newArray(int size) {
            return new BeanFeedbackCatagory[size];
        }
    };

    @Override
    public int describeContents() {
        // TODO Auto-generated method stub
        return 0;
    }

    public BeanFeedbackCatagory(){super();}


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
