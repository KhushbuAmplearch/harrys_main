package com.riverview.harrys.fragments;

import java.util.ArrayList;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.ListFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.iapps.libs.helpers.BaseHelper;
import com.iapps.libs.helpers.BaseHelper.CancelListener;
import com.iapps.libs.helpers.BaseHelper.ConfirmListener;
import com.iapps.libs.helpers.BaseUIHelper;
import com.riverview.harrys.MainActivity;
import com.riverview.harrys.R;
import com.riverview.harrys.adapter.SlideMenuAdapter;
import com.riverview.harrys.asynctask.DevicesAsyncTask;
import com.riverview.harrys.helpers.SharedPref;
import com.riverview.harrys.helpers.UserInfoManager;
import com.riverview.harrys.helpers.Utils;
import com.riverview.harrys.helpers.menuListener;
import com.riverview.harrys.objects.BeanDevice;
import com.riverview.harrys.objects.BeanSlideMenu;
import com.riverview.harrys.util.AppUtil;

public class SlideMenuFragment extends ListFragment implements menuListener {

	private ArrayList<BeanSlideMenu> mMenu;
	private View headerUserProfileView;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		// CalligraphyConfig.initDefault(Constants.GLOBAL_FONT_APP);
		return inflater.inflate(R.layout.list, null);
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);

		// CalligraphyConfig.initDefault(Constants.GLOBAL_FONT_APP);
		populateSlideMenuItem();
	}

	public void addHeaderView() {

		if (UserInfoManager.getInstance(getActivity())
				.isSignedIn(getActivity())) {

			headerUserProfileView = getActivity().getLayoutInflater().inflate(
					R.layout.header_slidingmenu, null);
			LinearLayout LLRootHEader = (LinearLayout) headerUserProfileView
					.findViewById(R.id.LLRoot);
			Utils.setHeightByPercent(getActivity(), LLRootHEader, 0.0f);
			TextView userName = (TextView) headerUserProfileView
					.findViewById(R.id.textViewUserProfile);
			userName.setVisibility(View.GONE);
			ImageView userImageProfile = (ImageView) headerUserProfileView
					.findViewById(R.id.imgUserProfile);
			userImageProfile.setVisibility(View.GONE);

		} else {

			headerUserProfileView = getActivity().getLayoutInflater().inflate(
					R.layout.header_slidingmenu, null);
			LinearLayout LLRootHEader = (LinearLayout) headerUserProfileView
					.findViewById(R.id.LLRoot);
			Utils.setHeightByPercent(getActivity(), LLRootHEader, 0.22f);
			Utils.setMarginByPercent(getActivity(), LLRootHEader, 0.01f);
			TextView userName = (TextView) headerUserProfileView
					.findViewById(R.id.textViewUserProfile);
			ImageView userImageProfile = (ImageView) headerUserProfileView
					.findViewById(R.id.imgUserProfile);
			BaseUIHelper
					.loadImageRounded(
							getActivity(),
							"http://www.cottagecountry.com/_assets/images/user-placeholder.png",
							userImageProfile, R.drawable.placeholder);
			// Utils.procTextsizeBasedScreen(getActivity(), userName, 1f);
			Utils.setTextViewFontSizeBasedOnScreenDensity(getActivity(),
					userName, Utils.FONT_SMALL_DENSITY_SIZE);
			userName.setText(SharedPref.getString(getActivity(),
					SharedPref.USERNAME_KEY, "Unknown"));

		}
	}

	public void populateSlideMenuItem() {
		mMenu = new ArrayList<BeanSlideMenu>();
		SlideMenuAdapter mAdapter = new SlideMenuAdapter(getActivity(), mMenu);

		if (UserInfoManager.getInstance(getActivity())
				.isSignedIn(getActivity())) {

			mMenu.add(new BeanSlideMenu(0, getActivity().getResources()
					.getString(R.string.rc_menu_sign_in),
					R.drawable.harrys_card_icon, true));
			mMenu.add(new BeanSlideMenu(1, getActivity().getResources()
					.getString(R.string.rc_menu_home), R.drawable.home_icon,
					true));
			mMenu.add(new BeanSlideMenu(2, getActivity().getResources()
					.getString(R.string.rc_menu_preferences),
					R.drawable.preferences_icon, true));
			mMenu.add(new BeanSlideMenu(3, getActivity().getResources()
					.getString(R.string.rc_menu_feedback),
					R.drawable.feedback_icon, true));
			mMenu.add(new BeanSlideMenu(4, getActivity().getResources()
					.getString(R.string.rc_menu_user_guide),
					R.drawable.userguide_icon, true));
			mMenu.add(new BeanSlideMenu(5, getActivity().getResources()
					.getString(R.string.rc_menu_pp), R.drawable.privacy_icon,
					true));
			mMenu.add(new BeanSlideMenu(6, getActivity().getResources()
					.getString(R.string.rc_menu_tnc), R.drawable.tnc_icon, true));
			mMenu.add(new BeanSlideMenu(7, getActivity().getResources()
					.getString(R.string.rc_menu_about), R.drawable.about_icon,
					true));

		} else {

			mMenu.add(new BeanSlideMenu(0, getActivity().getResources()
					.getString(R.string.rc_menu_sign_out),
					R.drawable.harrys_card_icon, true));
			mMenu.add(new BeanSlideMenu(1, getActivity().getResources()
					.getString(R.string.rc_menu_home), R.drawable.home_icon,
					true));
			mMenu.add(new BeanSlideMenu(2, getActivity().getResources()
					.getString(R.string.rc_menu_preferences),
					R.drawable.preferences_icon, true));
			mMenu.add(new BeanSlideMenu(3, getActivity().getResources()
					.getString(R.string.rc_menu_feedback),
					R.drawable.feedback_icon, true));
			mMenu.add(new BeanSlideMenu(4, getActivity().getResources()
					.getString(R.string.rc_menu_user_guide),
					R.drawable.userguide_icon, true));
			mMenu.add(new BeanSlideMenu(5, getActivity().getResources()
					.getString(R.string.rc_menu_pp), R.drawable.privacy_icon,
					true));
			mMenu.add(new BeanSlideMenu(6, getActivity().getResources()
					.getString(R.string.rc_menu_tnc), R.drawable.tnc_icon, true));
			mMenu.add(new BeanSlideMenu(7, getActivity().getResources()
					.getString(R.string.rc_menu_about), R.drawable.about_icon,
					true));

		}

		setListAdapter(mAdapter);
		mAdapter.notifyDataSetChanged();
		getListView().invalidate();
		getListView().invalidateViews();
	}

	@Override
	public void onListItemClick(ListView lv, View v, int position, long id) {
		Fragment newContent = null;

		if (position > 0) {
			if (!mMenu.get(position - 1).isEnable())
				return;
		}

		switch (position) {
		case 0:
			if (UserInfoManager.getInstance(getActivity()).isSignedIn(
					getActivity())) {
				newContent = new LoginFragment();
			} else {

				try {
					UserInfoManager.getInstance(
							getActivity().getApplicationContext()).logout(
							getActivity());
					newContent = new HomeFragment();
					// }
					//
					if (AppUtil.checkNetworkConnection(getActivity())) {
						callDeviceRegApi();
					}

				} catch (Exception e) {
					e.printStackTrace();
				}
			}
			break;
		case 2:
			if (!UserInfoManager.getInstance(getActivity()).isSignedIn(
					getActivity())) {

				newContent = new PreferencesFragment();
			} else {

				BaseHelper.confirm(getActivity(), null,
						"You need to login first", confirmListener,
						new CancelListener() {

							@Override
							public void onNo() {
								getHome().backToHomeScreen();

							}

						});
			}

			break;
		case 1:
			newContent = new HomeFragment();
			break;
		case 3:
//			if (!UserInfoManager.getInstance(getActivity()).isSignedIn(
//					getActivity())) {

				//newContent = new FeedbackListFragment();
				newContent = new FeedbackMailFragment();
//			} else {
//
//				BaseHelper.confirm(getActivity(), null,
//						"You need to login first", confirmListener,
//						new CancelListener() {
//
//							@Override
//							public void onNo() {
//								getHome().backToHomeScreen();
//
//							}
//
//						});
//			}	
			break;
		case 4:
			UserGuideDetailFragment wf = new UserGuideDetailFragment();
			Bundle bundle3 = new Bundle();
			bundle3.putString(
					"title",
					getHome().getResources().getString(
							R.string.rc_menu_user_guide));
			bundle3.putString("url", "file:///android_asset/guide6.html");
			wf.setArguments(bundle3);
			newContent = wf;
			break;
		case 5:
			WebFragment privacyPolicyFragment = new WebFragment();
			Bundle bundle = new Bundle();
			bundle.putString("title",
					getHome().getResources().getString(R.string.rc_menu_pp));
			bundle.putInt("whichpage", 0);
			privacyPolicyFragment.setArguments(bundle);
			newContent = privacyPolicyFragment;
			break;
		case 6:
			WebFragment termsConditionsFragment = new WebFragment();
			Bundle bundle1 = new Bundle();
			bundle1.putString("title",
					getHome().getResources().getString(R.string.rc_menu_tnc));
			bundle1.putInt("whichpage", 1);
			termsConditionsFragment.setArguments(bundle1);
			newContent = termsConditionsFragment;
			break;
		case 7:
			newContent = new AboutFragment();
			break;
		}
		if (newContent != null)
			switchFragment(newContent);
	}

	ConfirmListener confirmListener = new ConfirmListener() {

		@Override
		public void onYes() {
			((MainActivity) getActivity()).closeDrawer();
			((MainActivity) getActivity()).setFragment(new LoginFragment());

		}
	};

	// the meat of switching the above fragment
	private void switchFragment(Fragment fragment) {
		if (getActivity() == null)
			return;

		if (getActivity() instanceof MainActivity) {
			MainActivity mainAct = (MainActivity) getActivity();
			mainAct.setFragment(fragment);
			mainAct.closeDrawer();
		}
	}

	@Override
	public void refreshSlidingMenu() {
		populateSlideMenuItem();

	}

	public MainActivity getHome() {
		return ((MainActivity) getActivity());
	}

	private void callDeviceRegApi() {

		BeanDevice beanDevice = AppUtil.calculateDeviceRegData(getActivity(),
				SharedPref.getString(getActivity(), SharedPref.COMPANY_ID_KEY,
						"1"), SharedPref.getString(getActivity(),
						SharedPref.APPID_KEY, "1"), 0);

		DevicesAsyncTask mDeviceTask = new DevicesAsyncTask(getActivity(),
				beanDevice, false);
		mDeviceTask.execute((Void) null);
	}

}
