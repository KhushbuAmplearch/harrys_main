package com.riverview.harrys.objects;

import java.util.ArrayList;

public class BeanUpcProduct {

	private String id, company_id, brand_id, category_id, subcategory_id,
			ean_code, universal_product_code, unit_of_measure, description,
			additional_description, title, merchant_id, delivery,
			self_collection, overridable, tax_amount, created_at, updated_at,
			deleted_at, base_price, suspended;

	private BeanImage image;
	private BeanBrand product_brand;
	private BeanCategory product_category;
	private BeanSubCategory product_subcategory;
	private ArrayList<BeanPhoto> photos;
	private ArrayList<BeanProducts> products;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getCompany_id() {
		return company_id;
	}

	public void setCompany_id(String company_id) {
		this.company_id = company_id;
	}

	public String getBrand_id() {
		return brand_id;
	}

	public void setBrand_id(String brand_id) {
		this.brand_id = brand_id;
	}

	public String getCategory_id() {
		return category_id;
	}

	public void setCategory_id(String category_id) {
		this.category_id = category_id;
	}

	public String getSubcategory_id() {
		return subcategory_id;
	}

	public void setSubcategory_id(String subcategory_id) {
		this.subcategory_id = subcategory_id;
	}

	public String getEan_code() {
		return ean_code;
	}

	public void setEan_code(String ean_code) {
		this.ean_code = ean_code;
	}

	public String getUniversal_product_code() {
		return universal_product_code;
	}

	public void setUniversal_product_code(String universal_product_code) {
		this.universal_product_code = universal_product_code;
	}

	public String getUnit_of_measure() {
		return unit_of_measure;
	}

	public void setUnit_of_measure(String unit_of_measure) {
		this.unit_of_measure = unit_of_measure;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getAdditional_description() {
		return additional_description;
	}

	public void setAdditional_description(String additional_description) {
		this.additional_description = additional_description;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getMerchant_id() {
		return merchant_id;
	}

	public void setMerchant_id(String merchant_id) {
		this.merchant_id = merchant_id;
	}

	public String getDelivery() {
		return delivery;
	}

	public void setDelivery(String delivery) {
		this.delivery = delivery;
	}

	public String getSelf_collection() {
		return self_collection;
	}

	public void setSelf_collection(String self_collection) {
		this.self_collection = self_collection;
	}

	public String getOverridable() {
		return overridable;
	}

	public void setOverridable(String overridable) {
		this.overridable = overridable;
	}

	public String getTax_amount() {
		return tax_amount;
	}

	public void setTax_amount(String tax_amount) {
		this.tax_amount = tax_amount;
	}

	public String getCreated_at() {
		return created_at;
	}

	public void setCreated_at(String created_at) {
		this.created_at = created_at;
	}

	public String getUpdated_at() {
		return updated_at;
	}

	public void setUpdated_at(String updated_at) {
		this.updated_at = updated_at;
	}

	public String getDeleted_at() {
		return deleted_at;
	}

	public void setDeleted_at(String deleted_at) {
		this.deleted_at = deleted_at;
	}

	public String getBase_price() {
		return base_price;
	}

	public void setBase_price(String base_price) {
		this.base_price = base_price;
	}

	public String getSuspended() {
		return suspended;
	}

	public void setSuspended(String suspended) {
		this.suspended = suspended;
	}

	public BeanImage getImage() {
		return image;
	}

	public void setImage(BeanImage image) {
		this.image = image;
	}

	public BeanBrand getProduct_brand() {
		return product_brand;
	}

	public void setProduct_brand(BeanBrand product_brand) {
		this.product_brand = product_brand;
	}

	public BeanCategory getProduct_category() {
		return product_category;
	}

	public void setProduct_category(BeanCategory product_category) {
		this.product_category = product_category;
	}

	public BeanSubCategory getProduct_subcategory() {
		return product_subcategory;
	}

	public void setProduct_subcategory(BeanSubCategory product_subcategory) {
		this.product_subcategory = product_subcategory;
	}

	public ArrayList<BeanPhoto> getPhotos() {
		return photos;
	}

	public void setPhotos(ArrayList<BeanPhoto> photos) {
		this.photos = photos;
	}

	public ArrayList<BeanProducts> getProducts() {
		return products;
	}

	public void setProducts(ArrayList<BeanProducts> products) {
		this.products = products;
	}
}
