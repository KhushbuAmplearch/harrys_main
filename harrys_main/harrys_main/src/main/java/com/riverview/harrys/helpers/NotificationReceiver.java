package com.riverview.harrys.helpers;

import android.app.NotificationManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.riverview.harrys.constant.AppConst;

public class NotificationReceiver extends BroadcastReceiver {

	private static final String TAG = NotificationReceiver.class
			.getSimpleName();

	@Override
	public void onReceive(Context context, Intent intent) {
		Log.d(TAG, "onReceive ddd");
		int notificationId = intent.getIntExtra(AppConst.NOTIFICATON_ID, 0);

		// if you want cancel notification
		NotificationManager manager = (NotificationManager) context
				.getSystemService(Context.NOTIFICATION_SERVICE);
		manager.cancel(notificationId);

	}

}
