package com.riverview.harrys.fragments;

import static com.riverview.harrys.constant.AppConst.OTP_VERIFIED;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import org.joda.time.DateTime;
import org.jraf.android.backport.switchwidget.Switch;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.SpannableString;
import android.text.style.UnderlineSpan;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.iapps.libs.helpers.BaseHelper;
import com.iapps.libs.helpers.BaseHelper.ConfirmListener;
import com.riverview.harrys.MainActivity;
import com.riverview.harrys.R;
import com.riverview.harrys.RCGenericFragment;
import com.riverview.harrys.asynctask.AsyncResponse;
import com.riverview.harrys.asynctask.DevicesAsyncTask;
import com.riverview.harrys.asynctask.SignUpAsyncTask;
import com.riverview.harrys.helpers.Constants;
import com.riverview.harrys.helpers.SharedPref;
import com.riverview.harrys.helpers.Utils;
import com.riverview.harrys.objects.BeanDevice;
import com.riverview.harrys.objects.BeanLogin;
import com.riverview.harrys.objects.BeanSignUp;
import com.riverview.harrys.objects.ServiceResponse;
import com.riverview.harrys.util.AnaylaticsAppender;
import com.riverview.harrys.util.AppUtil;

public class SignUpFragment extends RCGenericFragment implements
		AsyncResponse<ServiceResponse<BeanLogin>>, OnCheckedChangeListener {

	private static final String TAG = SignUpFragment.class.getSimpleName();

	private LinearLayout LLSubmit;
	private LinearLayout LLAgreeMembership;
	private LinearLayout LLAgreeReceiveCard;
	private LinearLayout LLSignUpSubmit;
	private LinearLayout LLReceiveCardDetails,llterms;
	private Switch switch1;
	private Switch switch2;
	private TextView tv3;
	private TextView tv5;
	private EditText etUserID;
	private EditText etPassword;
	private EditText etRetypePassword;
	private EditText etEmail;
	private EditText etFirstName;
	private EditText etFamilyName;
	private EditText etDob;
	private EditText etMobileNo;
	private TextView tvDobNotfication;
	private TextView tvAddressNotfication,textViewDob;
	private EditText etPostelCode;
	private EditText etReferralCode;
	private EditText etPromoCode;
	private Button buttonSignUp;
	private EditText etAddressLine1;
	private EditText etAddressLine2;
	Calendar myCalendar;
	// private Calendar calendar;
	// private DatePickerDialog datePickerDialog;
	private Dialog dialog;

	View v;

	private AnaylaticsAppender mAppender = null;

	private final String DATEPICKER_TAG = "datepicker";

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
							 Bundle savedInstanceState) {
		v = inflater.inflate(R.layout.signup_fragment_layout, container, false);
		LLSubmit = (LinearLayout)v.findViewById(R.id.LLSubmit);
		LLAgreeMembership = (LinearLayout)v.findViewById(R.id.LLAgreeMembership);
		LLAgreeReceiveCard = (LinearLayout)v.findViewById(R.id.LLAgreeReceiveCard);
		LLSignUpSubmit = (LinearLayout)v.findViewById(R.id.LLSignUpSubmit);
		LLReceiveCardDetails = (LinearLayout)v.findViewById(R.id.LLReceiveCardDetails);
		textViewDob = (TextView)v.findViewById(R.id.textViewDob);
		switch1 = (Switch)v.findViewById (R.id.switch1);
		llterms = (LinearLayout)v.findViewById(R.id.llterms);
		switch2 = (Switch)v.findViewById (R.id.switch2);
		tv3 = (TextView) v.findViewById (R.id.tv3);
		tv5 = (TextView)v.findViewById (R.id.tv5);
		etUserID = (EditText)v.findViewById(R.id.etUserID);
		etPassword = (EditText)v.findViewById(R.id.etPassword);
		etRetypePassword = (EditText)v.findViewById(R.id.etRetypePassword);
		etEmail = (EditText)v.findViewById(R.id.etEmail);
		etFirstName = (EditText)v.findViewById(R.id.etFirstName);

		etFamilyName = (EditText)v.findViewById(R.id.etFamilyName);
		etDob = (EditText)v.findViewById(R.id.etDob);
		etMobileNo = (EditText)v.findViewById(R.id.etMobileNo);
		tvDobNotfication = (TextView)v.findViewById(R.id.tvDobNotfication);

		tvAddressNotfication = (TextView)v.findViewById (R.id.tvAddressNotfication);
		etPostelCode = (EditText)v.findViewById(R.id.etPostelCode);
		etReferralCode = (EditText)v.findViewById(R.id.etReferralCode);
		etPromoCode = (EditText)v.findViewById(R.id.etPromoCode);
		buttonSignUp = (Button)v.findViewById(R.id.buttonSignUp);
		etAddressLine1 = (EditText)v.findViewById(R.id.etAddressLine1);
		etAddressLine2 = (EditText)v.findViewById(R.id.etAddressLine2);

		setHasOptionsMenu(false);
		((MainActivity) getActivity()).setBackActionBar(getHome()
				.getResources().getString(R.string.rc_login_signup),View.INVISIBLE,"");
		hideDefaultKeyboard();
		String mystring=getResources().getString(R.string.rc_i_agree_with_membership);
		SpannableString content = new SpannableString(mystring);
		content.setSpan(new UnderlineSpan(), 0, mystring.length(), 0);
		tv3.setText(content);
		llterms.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				((MainActivity) getActivity())
						.setFragment(new Web1Fragment());
			}
		});
		return v;
	}

	private void hideDefaultKeyboard() {
		getActivity().getWindow().setSoftInputMode(
				WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

	}

	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);

		Utils.setTextViewFontSizeBasedOnScreenDensity(getActivity(), tv3, 30f);
		Utils.setTextViewFontSizeBasedOnScreenDensity(getActivity(), tv5, 30f);
		Utils.setTextViewFontSizeBasedOnScreenDensity(getActivity(),
				tvDobNotfication, 30f);
		Utils.setTextViewFontSizeBasedOnScreenDensity(getActivity(),
				tvAddressNotfication, 30f);

		// switch1.setOnCheckedChangeListener(this);
		switch2.setOnCheckedChangeListener(this);

		Utils.setButtonFontSizeBasedOnScreenDensity(getActivity(),
				buttonSignUp, Utils.FONT_SMALL_DENSITY_SIZE, Typeface.BOLD);
		Utils.setTextViewFontSizeBasedOnScreenDensity(getActivity(), etUserID,
				Utils.FONT_SMALL_DENSITY_SIZE);
		Utils.setTextViewFontSizeBasedOnScreenDensity(getActivity(),
				etPassword, Utils.FONT_SMALL_DENSITY_SIZE);
		Utils.setTextViewFontSizeBasedOnScreenDensity(getActivity(),
				etRetypePassword, Utils.FONT_SMALL_DENSITY_SIZE);
		Utils.setTextViewFontSizeBasedOnScreenDensity(getActivity(), etEmail,
				Utils.FONT_SMALL_DENSITY_SIZE);
		Utils.setTextViewFontSizeBasedOnScreenDensity(getActivity(),
				etFirstName, Utils.FONT_SMALL_DENSITY_SIZE);
		Utils.setTextViewFontSizeBasedOnScreenDensity(getActivity(),
				etFamilyName, Utils.FONT_SMALL_DENSITY_SIZE);
		Utils.setTextViewFontSizeBasedOnScreenDensity(getActivity(), textViewDob,
				Utils.FONT_SMALL_DENSITY_SIZE);
		Utils.setTextViewFontSizeBasedOnScreenDensity(getActivity(),
				etMobileNo, Utils.FONT_SMALL_DENSITY_SIZE);
		Utils.setTextViewFontSizeBasedOnScreenDensity(getActivity(),
				etPostelCode, Utils.FONT_SMALL_DENSITY_SIZE);

		Utils.setTextViewFontSizeBasedOnScreenDensity(getActivity(),
				etAddressLine1, Utils.FONT_SMALL_DENSITY_SIZE);
		Utils.setTextViewFontSizeBasedOnScreenDensity(getActivity(),
				etAddressLine2, Utils.FONT_SMALL_DENSITY_SIZE);

		buttonSignUp.setOnClickListener(clickListeners);

		Utils.setHeightByPercent(getActivity(), LLAgreeMembership, 0.1f);
		Utils.setHeightByPercent(getActivity(), LLAgreeReceiveCard, 0.1f);
		Utils.setHeightByPercent(getActivity(), LLSignUpSubmit, 0.08f);
		Date date = new Date();
		myCalendar = new GregorianCalendar();
		myCalendar.setTime(date);
		myCalendar.add(Calendar.YEAR, -18);
		setupCalendar();
	}

	@Override
	public void onResume() {
		super.onResume();

		try {
			mAppender = AnaylaticsAppender.getInstance(getActivity());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void setupCalendar() {
		// birth day restrictions


		etDob.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				new DatePickerDialog(getActivity(), date, myCalendar
						.get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
						myCalendar.get(Calendar.DAY_OF_MONTH)).show();

			}
		});

	}

	View.OnClickListener clickListeners = new View.OnClickListener() {
		public void onClick(View v) {

			if (v.getId() == R.id.buttonSignUp) {

				if (switch2.isChecked()) {
					if (BaseHelper.isEmpty(etAddressLine1)) {
						BaseHelper.showAlert(getActivity(),
								getString(R.string.rc_i_invalid_address_line));
						return;
					}
				}

				if (!switch1.isChecked()) {
					BaseHelper
							.showAlert(getActivity(),
									"Please accept the terms and conditions to proceed.");
					return;
				}

				if (AppUtil.checkNetworkConnection(getActivity())) {

					BeanSignUp beanSignUp = new BeanSignUp();
					beanSignUp
							.setUsername(etUserID.getText().toString().trim());
					beanSignUp.setPassword(etPassword.getText().toString()
							.trim());
					beanSignUp.setPassword_confirmation(etRetypePassword
							.getText().toString().trim());
					beanSignUp.setEmail(etEmail.getText().toString().trim());
					beanSignUp.setFirstname(etFirstName.getText().toString()
							.trim());
					beanSignUp.setSurname(etFamilyName.getText().toString()
							.trim());
					beanSignUp.setMobilenumber(etMobileNo.getText().toString()
							.trim());
					beanSignUp.setPostalcode(etPostelCode.getText().toString()
							.trim());
					beanSignUp.setDob(etDob.getText().toString().trim());
					beanSignUp.setCode(etPromoCode.getText().toString());

					if (switch2.isChecked()) {
						beanSignUp.setAddressLine1(etAddressLine1.getText()
								.toString().trim());
						beanSignUp.setAddressLine2(etAddressLine2.getText()
								.toString().trim());
					}

					signUp(beanSignUp);
				} else {
					BaseHelper.confirm(getActivity(), "Network Unavaliable",
							"Please turn on network to connect with server",
							new ConfirmListener() {

								@Override
								public void onYes() {
									AppUtil.showSystemSettingsDialog(getActivity());
								}
							});
				}

				hideDefaultKeyboard();
			}
		}
	};


	DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {

		@Override
		public void onDateSet(DatePicker view, int year, int monthOfYear,
							  int dayOfMonth) {
			// TODO Auto-generated method stub
			myCalendar.set(Calendar.YEAR, year);
			myCalendar.set(Calendar.MONTH, monthOfYear);
			myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);

			DateTime dt = DateTime.now().withYear(myCalendar.get(Calendar.YEAR))
					.withMonthOfYear(myCalendar.get(Calendar.MONTH) + 1)
					.withDayOfMonth(myCalendar.get(Calendar.DAY_OF_MONTH));
			etDob.setText(dt.toString(Constants.DATE_JSON));
		}

	};
	/*private SlideDateTimeListener listener = new SlideDateTimeListener() {

		@Override
		public void onDateTimeSet(Date date) {
			// Toast.makeText(getActivity(),
			// date.toString(), Toast.LENGTH_SHORT).show();
			Calendar calendar = Calendar.getInstance();
			calendar.setTime(date);
			DateTime dt = DateTime.now().withYear(calendar.get(Calendar.YEAR))
					.withMonthOfYear(calendar.get(Calendar.MONTH) + 1)
					.withDayOfMonth(calendar.get(Calendar.DAY_OF_MONTH));
			etDob.setText(dt.toString(Constants.DATE_JSON));
		}


		@Override
		public void onDateTimeCancel() {
			// Toast.makeText(getActivity(),
			// "Canceled", Toast.LENGTH_SHORT).show();
		}
	};*/

	private void signUp(BeanSignUp beanSignUp) {

		SignUpAsyncTask mAuthTask = new SignUpAsyncTask(getActivity(),
				beanSignUp);
		mAuthTask.asyncResponse = SignUpFragment.this;
		mAuthTask.execute((Void) null);
	}

	@Override
	public void processFinish(ServiceResponse<BeanLogin> details) {
		if (details == null) {
			BaseHelper.showAlert(getActivity(), getString(R.string.app_name),
					"SignUp Error");
			return;
		}

		if (details.getStatus() == 200 && !details.error) {
			Log.i(TAG, "Signup Success");

			try {
				SharedPref.saveBoolean(getActivity(), SharedPref.VERIFY_KEY,
						false);
				SharedPref.saveString(getActivity(), SharedPref.USERNAME_KEY,
						details.getResponse().getFirstname());
				SharedPref.saveString(getActivity(), SharedPref.USERFNAME_KEY,
						details.getResponse().getFirstname());
				SharedPref.saveString(getActivity(), SharedPref.USERLNAME_KEY,
						details.getResponse().getSurname());
				SharedPref.saveString(getActivity(), SharedPref.MOBILE_NO_KEY,
						details.getResponse().getMobilenumber());
				SharedPref.saveString(getActivity(), SharedPref.EMAIL_KEY,
						details.getResponse().getEmail());
				SharedPref.saveInteger(getActivity(), SharedPref.USERID_KEY,
						details.getResponse().getId());
				SharedPref.saveString(getActivity(), SharedPref.APPID_KEY,
						details.getResponse().getApplication_id());
				SharedPref.saveString(getActivity(), SharedPref.COMPANY_ID_KEY,
						details.getResponse().getCompany_id());

				Log.d(TAG, "Confirmation code "
						+ details.getResponse().getConfirmation_code());

				callDeviceRegApi();

				if (Integer.valueOf(details.getResponse().getConfirmed()) == OTP_VERIFIED) {
					SharedPref.saveBoolean(getActivity(),
							SharedPref.VERIFY_KEY, true);
					getHome().backToHomeScreen();

				} else {

					// Go to OTP Verify fragment
					VerifyOTPFragment frag = new VerifyOTPFragment();
					Bundle bundle = new Bundle();

					bundle.putBoolean("from_signup", true);

					frag.setArguments(bundle);
					getHome().setFragment(frag);

				}

				mAppender.updateAnaylitsData(AppUtil
						.createAnalyticsJsonEventAction("registration",
								String.valueOf(details.getResponse().getId()),
								"Signed up", null));

			} catch (Exception e) {
				e.printStackTrace();
			}

		} else {
			BaseHelper
					.showAlert(
							getActivity(),
							getString(R.string.app_name),
							(details.getError_message() == null | details
									.getError_message() == "") ? getString(R.string.sign_up_general_error)
									: details.getError_message());
		}

	}

	private void callDeviceRegApi() {

		BeanDevice beanDevice = AppUtil.calculateDeviceRegData(getActivity(),
				SharedPref.getString(getActivity(), SharedPref.COMPANY_ID_KEY,
						"1"), SharedPref.getString(getActivity(),
						SharedPref.APPID_KEY, "1"), SharedPref.getInteger(
						getActivity(), SharedPref.USERID_KEY, 0));

		DevicesAsyncTask mDeviceTask = new DevicesAsyncTask(getActivity(),
				beanDevice, true, etReferralCode.getText().toString());
		mDeviceTask.execute((Void) null);
	}

	@Override
	public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
		switch (buttonView.getId()) {

			case R.id.switch2:
				if (switch2.isChecked()) {
					LLReceiveCardDetails.setVisibility(View.VISIBLE);
				} else {
					LLReceiveCardDetails.setVisibility(View.GONE);
				}
				break;

			default:
				break;
		}
	}

	@Override
	public void onDestroy() {

		super.onDestroy();
		//hideDefaultKeyboard();
		Utils.hideKeyboard(getActivity());
	}

	@Override
	public void onDetach() {
		super.onDetach();
		//hideDefaultKeyboard();
		Utils.hideKeyboard(getActivity());
	}
}
