package com.riverview.harrys.fragments;

import static com.riverview.harrys.constant.AppConst.ATTR_TAG_DEVICE_ID;
import static com.riverview.harrys.constant.AppConst.HTTP_METHOD_GET;
import static com.riverview.harrys.constant.AppConst.SERVICE_END_POINT;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;

import org.apache.http.NameValuePair;
import org.apache.http.client.utils.URLEncodedUtils;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.type.TypeReference;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.client.ClientHttpResponse;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.DefaultResponseErrorHandler;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.media.MediaMetadataRetriever;
import android.media.MediaRecorder;
import android.media.ThumbnailUtils;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.animation.AccelerateInterpolator;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.iapps.libs.helpers.BaseHelper;
import com.iapps.libs.helpers.BaseUIHelper;
import com.iapps.libs.helpers.BaseHelper.CancelListener;
import com.iapps.libs.helpers.BaseHelper.ConfirmListener;
import com.nineoldandroids.animation.ValueAnimator;
import com.riverview.harrys.MainActivity;
import com.riverview.harrys.R;
import com.riverview.harrys.RCGenericFragment;
import com.riverview.harrys.asynctask.AsyncResponse;
import com.riverview.harrys.asynctask.CreateFeedbackAsyncTask;
import com.riverview.harrys.constant.AppConst;
import com.riverview.harrys.exception.RestServiceException;
import com.riverview.harrys.helpers.HeightEvaluator;
import com.riverview.harrys.helpers.SharedPref;
import com.riverview.harrys.helpers.Utils;
import com.riverview.harrys.objects.BeanFeedbackCatagory;
import com.riverview.harrys.objects.BeanFeedbackCategoryWrapper;
import com.riverview.harrys.objects.BeanFeedbackResponseWrapper;
import com.riverview.harrys.objects.BeanFeedbackSubCategoryWrapper;
import com.riverview.harrys.objects.BeanFeedbackType;
import com.riverview.harrys.objects.BeanFeedbackTypeOuter;
import com.riverview.harrys.objects.ErrorServiceResponse;
import com.riverview.harrys.objects.ServiceResponse;
import com.riverview.harrys.util.AppUtil;
import com.riverview.harrys.util.WebServiceUtil;

import fr.castorflex.android.circularprogressbar.CircularProgressDrawable;

public class FeedbackFragment extends RCGenericFragment implements
		AsyncResponse<ServiceResponse<BeanFeedbackResponseWrapper>> {

	private static final String TAG = FeedbackFragment.class.getSimpleName();

	private TextView textSelectFeedback;
	private TextView textSelectVideoFeedback;
	private TextView textSelectAudioFeedback;
	private TextView textTypeData;
	private TextView textSubCatData;
	private TextView textCatData;
	private ImageView imageData;
	private ImageView videoData;
	private EditText editTextDescription;
	private EditText editTextTitle;
	private LinearLayout LLMainContainer;
	private LinearLayout LLMenuCamara;
	private LinearLayout LLMenuCancel;
	private LinearLayout LLMenuGallary;
	private LinearLayout LLCardOptionSpace;
	private TextView textMenuTransactions;
	private TextView textMenuAddCard;
	private TextView textMenuCancel;
	private Button audioButton;
	private LinearLayout LLSubCategory;
	private FrameLayout slideDownView;

	private static final int VIEW_ANIMATION_INTERVAL = 300;
	private static float height = 0f;

	private static final int RESULT_LOAD_IMAGE = 1;
	private static final int RESULT_LOAD_VIDEO = 5;
	private static final int RESULT_IMAGE_CAPTURE = 2;
	private static final int RESULT_VIDEO_CAPTURE = 4;

	private String path = null;
	private String videoPath = null;
	private String audioPath = null;

	private MediaRecorder myAudioRecorder;

	private Uri fileUri = null;
	private Uri fileAudioUri = null;

	private boolean recordStatus = false;

	private Dialog dialog;

	private View v;

	private CreateFeedbackAsyncTask mCreateFeedbackAsyncTask;
	private FeedbackTypeAsyncTask mFeedbackTypeAsyncTask;
	private FeedbackCategoryAsyncTask mFeedbackCategoryAsyncTask;
	private FeedbackSubCategoryAsyncTask mFeedbackSubCategoryAsyncTask;

	private boolean CAPTURE_TYPE_VIDEO = false;

	private String[] mFeedbackType;
	private String[] mFeedbackCategoryType;
	private String[] mFeedbackSubCategoryType;

	private ArrayList<BeanFeedbackType> mFeedbackTypeList;
	private ArrayList<BeanFeedbackCatagory> mFeedbackCategoryList;
	private ArrayList<BeanFeedbackCatagory> mFeedbackSubCategoryList;

	private BeanFeedbackType mCurrentType = null;
	private BeanFeedbackCatagory mCurrentCategory = null;
	private BeanFeedbackCatagory mCurrentSubCategory = null;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		v = inflater.inflate(R.layout.feedback_fragment_layout, container,
				false);

		textSelectFeedback = (TextView)v.findViewById(R.id.textSelectFeedback);
		textSelectVideoFeedback = (TextView)v.findViewById(R.id.textSelectVideoFeedback);
		textSelectAudioFeedback = (TextView)v.findViewById(R.id.textSelectAudioFeedback);
		textTypeData = (TextView)v.findViewById(R.id.textTypeData);
		textSubCatData = (TextView)v.findViewById(R.id.textSubCatData);
		textCatData = (TextView)v.findViewById(R.id.textCatData);
		imageData = (ImageView)v.findViewById(R.id.imageData);
		videoData = (ImageView)v.findViewById(R.id.videoData);
		editTextDescription = (EditText)v.findViewById(R.id.editTextDescription);
		editTextTitle = (EditText)v.findViewById(R.id.editTextTitle);
		LLMainContainer = (LinearLayout)v.findViewById(R.id.LLMainContainer);
		LLMenuCamara = (LinearLayout)v.findViewById(R.id.LLMenuCamara);
		LLMenuCancel = (LinearLayout)v.findViewById(R.id.LLMenuCancel);
		LLMenuGallary = (LinearLayout)v.findViewById(R.id.LLMenuGallary);
		LLCardOptionSpace = (LinearLayout)v.findViewById(R.id.cardOptionSpace);
		textMenuTransactions = (TextView)v.findViewById(R.id.textMenuTransactions);
		textMenuAddCard = (TextView)v.findViewById(R.id.textMenuAddCard);
		textMenuCancel = (TextView)v.findViewById(R.id.textMenuCancel);

		audioButton = (Button)v.findViewById(R.id.audioButton);
		LLSubCategory = (LinearLayout)v.findViewById(R.id.LLSubCategory);
		slideDownView = (FrameLayout)v.findViewById(R.id.feedback_options_slideup_layout);

		setHasOptionsMenu(false);
		((MainActivity) getActivity()).setBackActionBar(getHome()
				.getResources().getString(R.string.rc_menu_feedback),View.INVISIBLE,"");
		hideDefaultKeyboard();

		return v;
	}

	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
		inflater.inflate(R.menu.menu_feedback, menu);
	}

	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.rv_menu_feedback:

			createFeedbackAsyncTask();
			break;

		default:
			return true;
		}
		return true;
	}

	private void hideDefaultKeyboard() {
		try {
			getActivity().getWindow().setSoftInputMode(
					WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

			getActivity();
			InputMethodManager imm = (InputMethodManager) getActivity()
					.getSystemService(Context.INPUT_METHOD_SERVICE);

			imm.hideSoftInputFromWindow(editTextDescription.getWindowToken(), 0);
		} catch (Exception e) {
		}

	}

	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);

		textSelectFeedback.setOnClickListener(clickListeners);
		textSelectVideoFeedback.setOnClickListener(clickListeners);
		textSelectAudioFeedback.setOnClickListener(clickListeners);
		textTypeData.setOnClickListener(clickListeners);
		textCatData.setOnClickListener(clickListeners);
		textSubCatData.setOnClickListener(clickListeners);
		LLSubCategory.setOnClickListener(clickListeners);
		audioButton.setOnClickListener(clickListeners);

		LLMenuCancel.setOnClickListener(clickListeners);
		LLMenuGallary.setOnClickListener(clickListeners);
		LLMenuCamara.setOnClickListener(clickListeners);
		LLCardOptionSpace.setOnClickListener(clickListeners);

		Utils.setTextViewFontSizeBasedOnScreenDensity(getActivity(),
				textMenuTransactions, 18f);
		Utils.setTextViewFontSizeBasedOnScreenDensity(getActivity(),
				textMenuAddCard, 18f);
		Utils.setTextViewFontSizeBasedOnScreenDensity(getActivity(),
				editTextDescription, 25f);
		Utils.setTextViewFontSizeBasedOnScreenDensity(getActivity(),
				editTextTitle, 25f);
		Utils.setTextViewFontSizeBasedOnScreenDensity(getActivity(),
				textMenuCancel, 14f, Typeface.BOLD);

	}

	private void showTopicsSpinnerDialog() {
		AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

		try {
			builder.setTitle("Feedback Type");
			builder.setItems(mFeedbackType,
					new DialogInterface.OnClickListener() {

						@Override
						public void onClick(DialogInterface dialog, int pos) {
							textTypeData.setText(mFeedbackType[pos]);
							mCurrentType = mFeedbackTypeList.get(pos);
						}
					});

			builder.setNegativeButton("Cancel",
					new DialogInterface.OnClickListener() {

						@Override
						public void onClick(DialogInterface dialog, int which) {

						}
					});

			try {
				if (!dialog.isShowing()) {
					dialog = builder.create();
					dialog.show();
				}
			} catch (Exception e) {
				try {
					dialog = builder.create();
					dialog.show();
				} catch (Exception e1) {
					e.printStackTrace();
				}
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void showCategorySpinnerDialog() {
		AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

		try {
			builder.setTitle("Feedback Category");
			builder.setItems(mFeedbackCategoryType,
					new DialogInterface.OnClickListener() {

						@Override
						public void onClick(DialogInterface dialog, int pos) {
							textCatData.setText(mFeedbackCategoryType[pos]);
							mCurrentCategory = mFeedbackCategoryList.get(pos);

							// clear sub category data when changing the
							// category index
							mCurrentSubCategory = null;
							textSubCatData.setText("");
						}
					});

			builder.setNegativeButton("Cancel",
					new DialogInterface.OnClickListener() {

						@Override
						public void onClick(DialogInterface dialog, int which) {

						}
					});

			try {
				if (!dialog.isShowing()) {
					dialog = builder.create();
					dialog.show();
				}
			} catch (Exception e) {
				try {
					dialog = builder.create();
					dialog.show();
				} catch (Exception e1) {
					e.printStackTrace();
				}
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void showSubCategorySpinnerDialog() {
		AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

		try {
			builder.setTitle("Feedback Sub Category");
			builder.setItems(mFeedbackSubCategoryType,
					new DialogInterface.OnClickListener() {

						@Override
						public void onClick(DialogInterface dialog, int pos) {
							textSubCatData
									.setText(mFeedbackSubCategoryType[pos]);
							mCurrentSubCategory = mFeedbackSubCategoryList
									.get(pos);
						}
					});

			builder.setNegativeButton("Cancel",
					new DialogInterface.OnClickListener() {

						@Override
						public void onClick(DialogInterface dialog, int which) {

						}
					});

			try {
				if (!dialog.isShowing()) {
					dialog = builder.create();
					dialog.show();
				}
			} catch (Exception e) {
				try {
					dialog = builder.create();
					dialog.show();
				} catch (Exception e1) {
					e.printStackTrace();
				}
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void startRecording() {

		try {

			audioButton.setEnabled(false);

			audioPath = Environment.getExternalStorageDirectory()
					.getAbsolutePath()
					+ "/"
					+ "AUD_"
					+ String.valueOf(System.currentTimeMillis()) + ".3gp";

			myAudioRecorder = new MediaRecorder();
			myAudioRecorder.setAudioSource(MediaRecorder.AudioSource.MIC);
			myAudioRecorder
					.setOutputFormat(MediaRecorder.OutputFormat.THREE_GPP);
			myAudioRecorder.setAudioEncoder(MediaRecorder.OutputFormat.AMR_NB);
			myAudioRecorder.setOutputFile(audioPath);

			// start recording
			myAudioRecorder.prepare();
			myAudioRecorder.start();

			Log.d(TAG, "Audio URL " + audioPath);

			audioButton.setEnabled(true);

		} catch (Exception e) {
			e.printStackTrace();

			audioButton.setEnabled(true);
		}
	}

	private void endRecording() {
		try {

			myAudioRecorder.stop();
			myAudioRecorder.release();
			myAudioRecorder = null;

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	View.OnClickListener clickListeners = new View.OnClickListener() {
		public void onClick(View v) {

			switch (v.getId()) {
			case R.id.textSelectFeedback:
				hideDefaultKeyboard();

				CAPTURE_TYPE_VIDEO = false;
				toggle(slideDownView);

				break;

			case R.id.textSelectVideoFeedback:
				hideDefaultKeyboard();

				CAPTURE_TYPE_VIDEO = true;
				toggle(slideDownView);
				break;

			case R.id.audioButton:
				if (!recordStatus) {
					// no recording started
					recordStatus = true;
					audioButton.setText("Recording. Tap to save.");

					startRecording();

				} else if (recordStatus) {
					recordStatus = false;
					audioButton.setText("RECORD AUDIO");

					endRecording();
				}

				break;

			case R.id.textTypeData:
				// Load feedback types from Async task
				if (mFeedbackType != null && mFeedbackType.length > 0) {
					showTopicsSpinnerDialog();
				} else {
					startFeedbackTypeAsyncTask();
				}
				break;

			case R.id.textCatData:
				// Load feedback types from Async task
				if (mFeedbackCategoryType != null
						&& mFeedbackCategoryType.length > 0) {
					showCategorySpinnerDialog();
				} else {
					startFeedbackCategoryAsyncTask();
				}
				break;

			case R.id.LLSubCategory:
				// Load feedback types from Async task
				if (mCurrentCategory != null) {
					startFeedbackSubCategoryAsyncTask(mCurrentCategory.getId());
				} else {
					// no category selected
					Toast.makeText(getActivity(),
							"Please selecte a category to continue",
							Toast.LENGTH_SHORT).show();
				}
				break;

			case R.id.LLMenuCancel:
				closeToggle();
				break;

			case R.id.LLMenuGallary:
				if (CAPTURE_TYPE_VIDEO) {
					Intent i = new Intent(Intent.ACTION_PICK);
					i.setType("video/*");

					if (i.resolveActivity(getHome().getPackageManager()) != null) {
						startActivityForResult(i, RESULT_LOAD_VIDEO);
					} else {
						Toast.makeText(
								getHome(),
								"No application avaliable to complete this action",
								Toast.LENGTH_SHORT).show();
					}
				} else {
					Intent i = new Intent(Intent.ACTION_PICK);
					i.setType("image/*");

					if (i.resolveActivity(getHome().getPackageManager()) != null) {
						startActivityForResult(i, RESULT_LOAD_IMAGE);
					} else {
						Toast.makeText(
								getHome(),
								"No application avaliable to complete this action",
								Toast.LENGTH_SHORT).show();
					}
				}

				closeToggle();
				break;

			case R.id.LLMenuCamara:
				startCamara();

				closeToggle();
				break;
			case R.id.cardOptionSpace:
				closeToggle();
				break;

			default:
				break;
			}

		}
	};

	public void startCamara() {

		if (CAPTURE_TYPE_VIDEO) {
			// recording a video
			// ContentValues values = new ContentValues();
			// values.put(MediaStore.Video.Media.TITLE,
			// "VID_" + String.valueOf(System.currentTimeMillis())
			// + ".mp4");

			Intent intent = new Intent(MediaStore.ACTION_VIDEO_CAPTURE);

			// // to save the image (this doesn't work at all for images)
			// fileVideoUri = getHome().getContentResolver().insert(
			// MediaStore.Video.Media.EXTERNAL_CONTENT_URI, values);
			//
			// intent.putExtra(MediaStore.EXTRA_OUTPUT, fileVideoUri);

			// start the image capture Intent
			if (intent.resolveActivity(getHome().getPackageManager()) != null) {
				startActivityForResult(intent, RESULT_VIDEO_CAPTURE);
			} else {
				Toast.makeText(getHome(),
						"No application avaliable to complete this action",
						Toast.LENGTH_SHORT).show();
			}
		} else {
			// Capturing an image
			ContentValues values = new ContentValues();
			values.put(MediaStore.Images.Media.TITLE,
					"IMG_" + String.valueOf(System.currentTimeMillis())
							+ ".jpg");

			Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

			// to save the image (this doesn't work at all for images)
			fileUri = getHome().getContentResolver().insert(
					MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values);
			intent.putExtra(MediaStore.EXTRA_OUTPUT, fileUri);

			// start the image capture Intent
			if (intent.resolveActivity(getHome().getPackageManager()) != null) {
				startActivityForResult(intent, RESULT_IMAGE_CAPTURE);
			} else {
				Toast.makeText(getHome(),
						"No application avaliable to complete this action",
						Toast.LENGTH_SHORT).show();
			}
		}
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {

		super.onActivityResult(requestCode, resultCode, data);

		if (requestCode == RESULT_LOAD_IMAGE
				&& resultCode == Activity.RESULT_OK && data != null) {

			loadImage(data.getData());

		} else if (requestCode == RESULT_IMAGE_CAPTURE
				&& resultCode == Activity.RESULT_OK) {

			if (fileUri != null) {
				loadImage(fileUri);
			}
		} else if (requestCode == RESULT_VIDEO_CAPTURE
				&& resultCode == Activity.RESULT_OK) {

			Log.d(TAG, "Video URL " + data.getData().toString());
			loadVideo(data.getData());
		} else if (requestCode == RESULT_LOAD_VIDEO
				&& resultCode == Activity.RESULT_OK && data != null) {
			loadVideo(data.getData());
		}
	}

	private void loadVideo(Uri uri) {

		String[] filePathColumn = { MediaStore.Video.Media.DATA };

		Cursor cursor = getHome().getContentResolver().query(uri,
				filePathColumn, null, null, null);
		cursor.moveToFirst();

		int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
		videoPath = cursor.getString(columnIndex);

		try {
			MediaMetadataRetriever retriever = new MediaMetadataRetriever();
			retriever.setDataSource(videoPath);

			Log.d(TAG, "Video PATH " + videoPath);

			Bitmap thumb = retriever.getFrameAtTime(10000,
					MediaMetadataRetriever.OPTION_CLOSEST_SYNC);
			videoData.setImageBitmap(thumb);

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (cursor != null) {
				cursor.close();
			}
		}

	}

	private void loadImage(Uri uri) {

		String[] filePathColumn = { MediaStore.Images.Media.DATA };

		Cursor cursor = getHome().getContentResolver().query(uri,
				filePathColumn, null, null, null);
		cursor.moveToFirst();

		int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
		path = cursor.getString(columnIndex);
		Log.d(TAG, "IMAGE PATH " + path);

		try {

			File imgFile = new File(path);
			Bitmap myBitmap;
			Bitmap finalBitmap;

			if (imgFile.exists()) {

				if ((imgFile.length() / (1024 * 1024)) < 1.45) {
					finalBitmap = BaseUIHelper.getThumbnail(getActivity(), uri);
				} else {
					myBitmap = decodeFile(imgFile);

					ByteArrayOutputStream out = new ByteArrayOutputStream();
					myBitmap.compress(Bitmap.CompressFormat.JPEG, 60, out);

					finalBitmap = BitmapFactory
							.decodeStream(new ByteArrayInputStream(out
									.toByteArray()));

				}
				saveScaledImage(finalBitmap);

				// Bitmap myBitmap = BitmapFactory.decodeFile(imgFile
				// .getAbsolutePath());
				imageData.setImageBitmap(finalBitmap);
			}

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (cursor != null) {
				cursor.close();
			}
		}

	}

	private void saveScaledImage(Bitmap bmp) {
		String file_path = Environment.getExternalStorageDirectory()
				.getAbsolutePath() + "/harrysimages";
		String name = "HAR_" + System.currentTimeMillis() + ".jpeg";

		File dir = new File(file_path);
		if (!dir.exists())
			dir.mkdirs();
		File file = new File(dir, name);
		FileOutputStream fOut;
		try {
			fOut = new FileOutputStream(file);
			bmp.compress(Bitmap.CompressFormat.JPEG, 85, fOut);

			path = file_path + "/" + name;

			fOut.flush();
			fOut.close();

		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private Bitmap decodeFile(File f) {
		try {
			// Decode image size
			BitmapFactory.Options o = new BitmapFactory.Options();
			o.inJustDecodeBounds = true;
			BitmapFactory.decodeStream(new FileInputStream(f), null, o);

			// The new size we want to scale to
			final int REQUIRED_SIZE = 50;

			// Find the correct scale value. It should be the power of 2.
			int scale = 1;
			while (o.outWidth / scale / 2 >= REQUIRED_SIZE
					&& o.outHeight / scale / 2 >= REQUIRED_SIZE) {
				scale *= 2;
			}

			// Decode with inSampleSize
			BitmapFactory.Options o2 = new BitmapFactory.Options();
			o2.inSampleSize = scale;
			return BitmapFactory.decodeStream(new FileInputStream(f), null, o2);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		return null;
	}

	private void toggle(final FrameLayout v) {

		LLMainContainer.setEnabled(false);

		height = BaseUIHelper.getScreenHeight(getHome());
		height = (float) (height * 1);

		v.setVisibility(View.VISIBLE);
		ValueAnimator va = ValueAnimator.ofFloat(0f, height).setDuration(
				VIEW_ANIMATION_INTERVAL);
		va.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
			public void onAnimationUpdate(ValueAnimator animation) {
				Integer value = (Integer) Math.round((float) animation
						.getAnimatedValue());
				v.getLayoutParams().height = value.intValue();
				v.invalidate();
				v.requestLayout();

			}
		});
		va.setInterpolator(new AccelerateInterpolator(8));
		va.start();

	}

	private void closeToggle() {

		LLMainContainer.setEnabled(true);

		int startHeight = slideDownView.getHeight();
		ValueAnimator animation = ValueAnimator.ofObject(
				new HeightEvaluator(slideDownView), startHeight, (int) 0)
				.setDuration(VIEW_ANIMATION_INTERVAL);
		animation.setInterpolator(new AccelerateInterpolator(8));
		animation.start();
	}

	@Override
	public void processFinish(
			ServiceResponse<BeanFeedbackResponseWrapper> details) {
		if (details == null) {
			Log.d(TAG, "Null details");
			try {
				BaseHelper.showAlert(getActivity(), "Harry's",
						"Operation fail.");
			} catch (Exception e) {
				e.printStackTrace();
			}
			return;
		}

		if (details.getStatus() == 200 && !details.error) {
			Log.d(TAG, "Create feedback success");

			try {
				BaseHelper
						.showAlert(getActivity(), "Harry's",
								"Your Inquiry has been recorded. We will contact you shortly.");
			} catch (Exception e) {
				e.printStackTrace();
			}

		} else {
			BaseHelper.showAlert(getActivity(), "Error", (details
					.getError_message() != null) ? details.getError_message()
					: "Unable to create feedback. Please try again.");
		}

	}

	// Async tasks to load feedback quires
	public class FeedbackTypeAsyncTask extends
			AsyncTask<Void, Void, ServiceResponse<BeanFeedbackTypeOuter>> {

		private final String TAG = FeedbackTypeAsyncTask.class.getSimpleName();

		public AsyncResponse<ServiceResponse<BeanFeedbackTypeOuter>> asyncResponse;

		// private ProgressDialog mProgressDialog;
		private Context mContext;

		String[] commandString = null;
		ArrayList<NameValuePair> httpParams = null;
		private String urlApi = null;

		private ProgressDialog mProgressDialog;

		public FeedbackTypeAsyncTask(Context context) {
			this.mContext = context;
		}

		@Override
		protected void onPreExecute() {
			Log.d(TAG, "onPreExecute");
			super.onPreExecute();

			mProgressDialog = new ProgressDialog(mContext);
			mProgressDialog
					.setIndeterminateDrawable(new CircularProgressDrawable.Builder(
							mContext)
							.colors(mContext.getResources().getIntArray(
									R.array.harrys_colors)).sweepSpeed(1f)
							.strokeWidth(12f)
							.style(CircularProgressDrawable.Style.ROUNDED)
							.build());

			mProgressDialog.setCancelable(true);
			mProgressDialog.setMessage("Loading feedback types..");
			mProgressDialog.show();

			try {

				urlApi = AppConst.URL_GET_FEEDBACK_TYPES;
				httpParams = AppUtil.generateAuthCommandString(null, urlApi,
						HTTP_METHOD_GET);

			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		@Override
		protected void onPostExecute(
				ServiceResponse<BeanFeedbackTypeOuter> response) {

			if (mProgressDialog != null && mProgressDialog.isShowing()) {
				mProgressDialog.dismiss();
			}

			if (response != null
					&& response.getStatus() == HttpStatus.OK.value()) {
				mFeedbackTypeList = response.getResponse().getFeedback_types();

				if (mFeedbackTypeList != null && mFeedbackTypeList.size() > 0) {
					// add to list
					mFeedbackType = new String[mFeedbackTypeList.size()];
					for (int i = 0; i < mFeedbackTypeList.size(); i++) {
						mFeedbackType[i] = mFeedbackTypeList.get(i).getType();
					}

					// populate it as list
					showTopicsSpinnerDialog();

				} else {
					// relevent message
				}
			}
		}

		@Override
		protected ServiceResponse<BeanFeedbackTypeOuter> doInBackground(
				Void... voids) {
			Log.d(TAG, "doInBackground");

			ServiceResponse<BeanFeedbackTypeOuter> serviceResponse = null;

			RestTemplate restTemplate = WebServiceUtil
					.getRestTemplateInstance();

			restTemplate.setErrorHandler(new DefaultResponseErrorHandler() {
				@Override
				public void handleError(ClientHttpResponse response)
						throws IOException {
					ObjectMapper mapper = WebServiceUtil.getErrorObjMapper();
					ErrorServiceResponse errorRes;
					errorRes = mapper.readValue(response.getBody(),
							new TypeReference<ErrorServiceResponse>() {
							});

					throw new RestServiceException(errorRes, errorRes
							.getMessage());
				}

				protected boolean hasError(HttpStatus statusCode) {
					Log.d(TAG, "Status ----> " + statusCode);
					if (statusCode.value() == 200) {
						return false;
					}
					return true;
				}
			});

			String url = String.format(SERVICE_END_POINT + "%s", urlApi);

			Log.d(TAG, "Final URL ------> " + url);

			String urlParams = URLEncodedUtils.format(httpParams, "UTF-8");
			String finalUrl = String.format(url + "?" + "%s", urlParams);

			HttpHeaders requestHeaders = new HttpHeaders();
			requestHeaders
					.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
			requestHeaders.set("Accept", "application/json");
			requestHeaders.add(ATTR_TAG_DEVICE_ID,
					AppUtil.getDeviceId(mContext));

			MultiValueMap<String, String> map = new LinkedMultiValueMap<String, String>();
			for (NameValuePair a : httpParams) {
				map.add(a.getName(), a.getValue());
			}

			HttpEntity<MultiValueMap<String, String>> requestEntity = new HttpEntity<>(
					map, requestHeaders);

			Log.d(TAG, "Url " + url);
			Log.d(TAG, "Request Entity " + requestEntity.toString());

			try {
				serviceResponse = restTemplate
						.exchange(
								finalUrl,
								HttpMethod.GET,
								requestEntity,
								new ParameterizedTypeReference<ServiceResponse<BeanFeedbackTypeOuter>>() {
								}).getBody();

			} catch (RestClientException e) {
				serviceResponse = new ServiceResponse<>();
				serviceResponse.setError(true);
				serviceResponse.setError_message(e.getCause().getMessage());

			} catch (Exception e) {
				e.printStackTrace();
			}
			return serviceResponse;
		}

		@Override
		protected void onCancelled() {
			asyncResponse.processFinish(null);
		}
	}

	// feedback category Async task
	public class FeedbackCategoryAsyncTask extends
			AsyncTask<Void, Void, ServiceResponse<BeanFeedbackCategoryWrapper>> {

		private final String TAG = FeedbackCategoryAsyncTask.class
				.getSimpleName();

		public AsyncResponse<ServiceResponse<BeanFeedbackCategoryWrapper>> asyncResponse;

		// private ProgressDialog mProgressDialog;
		private Context mContext;

		String[] commandString = null;
		ArrayList<NameValuePair> httpParams = null;
		private String urlApi = null;

		private ProgressDialog mProgressDialog;

		public FeedbackCategoryAsyncTask(Context context) {
			this.mContext = context;
		}

		@Override
		protected void onPreExecute() {
			Log.d(TAG, "onPreExecute");
			super.onPreExecute();

			mProgressDialog = new ProgressDialog(mContext);
			mProgressDialog
					.setIndeterminateDrawable(new CircularProgressDrawable.Builder(
							mContext)
							.colors(mContext.getResources().getIntArray(
									R.array.harrys_colors)).sweepSpeed(1f)
							.strokeWidth(12f)
							.style(CircularProgressDrawable.Style.ROUNDED)
							.build());

			mProgressDialog.setCancelable(true);
			mProgressDialog.setMessage("Loading feedback Categories..");
			mProgressDialog.show();

			try {

				urlApi = AppConst.URL_GET_FEEDBACK_CATEGORIES;
				httpParams = AppUtil.generateAuthCommandString(null, urlApi,
						HTTP_METHOD_GET);

			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		@Override
		protected void onPostExecute(
				ServiceResponse<BeanFeedbackCategoryWrapper> response) {

			if (mProgressDialog != null && mProgressDialog.isShowing()) {
				mProgressDialog.dismiss();
			}

			if (response != null
					&& response.getStatus() == HttpStatus.OK.value()) {
				mFeedbackCategoryList = response.getResponse().getCategories();

				if (mFeedbackCategoryList != null
						&& mFeedbackCategoryList.size() > 0) {
					// add to list
					mFeedbackCategoryType = new String[mFeedbackCategoryList
							.size()];
					for (int i = 0; i < mFeedbackCategoryList.size(); i++) {
						mFeedbackCategoryType[i] = mFeedbackCategoryList.get(i)
								.getTitle();
					}

					// populate it as list
					showCategorySpinnerDialog();

				} else {
					// relevent message
				}
			}
		}

		@Override
		protected ServiceResponse<BeanFeedbackCategoryWrapper> doInBackground(
				Void... voids) {
			Log.d(TAG, "doInBackground");

			ServiceResponse<BeanFeedbackCategoryWrapper> serviceResponse = null;

			RestTemplate restTemplate = WebServiceUtil
					.getRestTemplateInstance();

			restTemplate.setErrorHandler(new DefaultResponseErrorHandler() {
				@Override
				public void handleError(ClientHttpResponse response)
						throws IOException {
					ObjectMapper mapper = WebServiceUtil.getErrorObjMapper();
					ErrorServiceResponse errorRes;
					errorRes = mapper.readValue(response.getBody(),
							new TypeReference<ErrorServiceResponse>() {
							});

					throw new RestServiceException(errorRes, errorRes
							.getMessage());
				}

				protected boolean hasError(HttpStatus statusCode) {
					Log.d(TAG, "Status ----> " + statusCode);
					if (statusCode.value() == 200) {
						return false;
					}
					return true;
				}
			});

			String url = String.format(SERVICE_END_POINT + "%s", urlApi);

			Log.d(TAG, "Final URL ------> " + url);

			String urlParams = URLEncodedUtils.format(httpParams, "UTF-8");
			String finalUrl = String.format(url + "?" + "%s", urlParams);

			HttpHeaders requestHeaders = new HttpHeaders();
			requestHeaders
					.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
			requestHeaders.set("Accept", "application/json");
			requestHeaders.add(ATTR_TAG_DEVICE_ID,
					AppUtil.getDeviceId(mContext));

			MultiValueMap<String, String> map = new LinkedMultiValueMap<String, String>();
			for (NameValuePair a : httpParams) {
				map.add(a.getName(), a.getValue());
			}

			HttpEntity<MultiValueMap<String, String>> requestEntity = new HttpEntity<>(
					map, requestHeaders);

			Log.d(TAG, "Url " + url);
			Log.d(TAG, "Request Entity " + requestEntity.toString());

			try {
				serviceResponse = restTemplate
						.exchange(
								finalUrl,
								HttpMethod.GET,
								requestEntity,
								new ParameterizedTypeReference<ServiceResponse<BeanFeedbackCategoryWrapper>>() {
								}).getBody();

			} catch (RestClientException e) {
				serviceResponse = new ServiceResponse<>();
				serviceResponse.setError(true);
				serviceResponse.setError_message(e.getCause().getMessage());

			} catch (Exception e) {
				e.printStackTrace();
			}
			return serviceResponse;
		}

		@Override
		protected void onCancelled() {
			asyncResponse.processFinish(null);
		}
	}

	// feedback sub category Async task
	public class FeedbackSubCategoryAsyncTask
			extends
			AsyncTask<Void, Void, ServiceResponse<BeanFeedbackSubCategoryWrapper>> {

		private final String TAG = FeedbackCategoryAsyncTask.class
				.getSimpleName();

		public AsyncResponse<ServiceResponse<BeanFeedbackSubCategoryWrapper>> asyncResponse;

		// private ProgressDialog mProgressDialog;
		private Context mContext;

		String[] commandString = null;
		ArrayList<NameValuePair> httpParams = null;
		private String urlApi = null;

		private ProgressDialog mProgressDialog;
		private int mCategoryId;

		public FeedbackSubCategoryAsyncTask(Context context, int id) {
			this.mContext = context;
			this.mCategoryId = id;
		}

		@Override
		protected void onPreExecute() {
			Log.d(TAG, "onPreExecute");
			super.onPreExecute();

			mProgressDialog = new ProgressDialog(mContext);
			mProgressDialog
					.setIndeterminateDrawable(new CircularProgressDrawable.Builder(
							mContext)
							.colors(mContext.getResources().getIntArray(
									R.array.harrys_colors)).sweepSpeed(1f)
							.strokeWidth(12f)
							.style(CircularProgressDrawable.Style.ROUNDED)
							.build());

			mProgressDialog.setCancelable(true);
			mProgressDialog.setMessage("Loading feedback Sub Categories..");
			mProgressDialog.show();

			try {

				urlApi = String.format(
						AppConst.URL_GET_FEEDBACK_SUB_CATEGORIES,
						String.valueOf(mCategoryId));
				httpParams = AppUtil.generateAuthCommandString(null, urlApi,
						HTTP_METHOD_GET);

			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		@Override
		protected void onPostExecute(
				ServiceResponse<BeanFeedbackSubCategoryWrapper> response) {

			if (mProgressDialog != null && mProgressDialog.isShowing()) {
				mProgressDialog.dismiss();
			}

			if (response != null
					&& response.getStatus() == HttpStatus.OK.value()) {
				mFeedbackSubCategoryList = response.getResponse()
						.getSub_categories();

				if (mFeedbackSubCategoryList != null
						&& mFeedbackSubCategoryList.size() > 0) {
					// add to list
					mFeedbackSubCategoryType = new String[mFeedbackSubCategoryList
							.size()];
					for (int i = 0; i < mFeedbackSubCategoryList.size(); i++) {
						mFeedbackSubCategoryType[i] = mFeedbackSubCategoryList
								.get(i).getTitle();
					}
					// populate it as list
					showSubCategorySpinnerDialog();

				} else {
					// relevent message
					Toast.makeText(getActivity(), "No sub categories found",
							Toast.LENGTH_SHORT).show();
				}
			} else {
				Toast.makeText(
						getActivity(),
						(response.getError_message() != null) ? response
								.getError_message() : "No sub categories found",
						Toast.LENGTH_SHORT).show();
			}
		}

		@Override
		protected ServiceResponse<BeanFeedbackSubCategoryWrapper> doInBackground(
				Void... voids) {
			Log.d(TAG, "doInBackground");

			ServiceResponse<BeanFeedbackSubCategoryWrapper> serviceResponse = null;

			RestTemplate restTemplate = WebServiceUtil
					.getRestTemplateInstance();

			restTemplate.setErrorHandler(new DefaultResponseErrorHandler() {
				@Override
				public void handleError(ClientHttpResponse response)
						throws IOException {
					ObjectMapper mapper = WebServiceUtil.getErrorObjMapper();
					ErrorServiceResponse errorRes;
					errorRes = mapper.readValue(response.getBody(),
							new TypeReference<ErrorServiceResponse>() {
							});

					throw new RestServiceException(errorRes, errorRes
							.getError_message());
				}

				protected boolean hasError(HttpStatus statusCode) {
					Log.d(TAG, "Status ----> " + statusCode);
					if (statusCode.value() == 200) {
						return false;
					}
					return true;
				}
			});

			String url = String.format(SERVICE_END_POINT + "%s", urlApi);

			Log.d(TAG, "Final URL ------> " + url);

			String urlParams = URLEncodedUtils.format(httpParams, "UTF-8");
			String finalUrl = String.format(url + "?" + "%s", urlParams);

			HttpHeaders requestHeaders = new HttpHeaders();
			requestHeaders
					.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
			requestHeaders.set("Accept", "application/json");
			requestHeaders.add(ATTR_TAG_DEVICE_ID,
					AppUtil.getDeviceId(mContext));

			MultiValueMap<String, String> map = new LinkedMultiValueMap<String, String>();
			for (NameValuePair a : httpParams) {
				map.add(a.getName(), a.getValue());
			}

			HttpEntity<MultiValueMap<String, String>> requestEntity = new HttpEntity<>(
					map, requestHeaders);

			Log.d(TAG, "Url " + url);
			Log.d(TAG, "Request Entity " + requestEntity.toString());

			try {
				serviceResponse = restTemplate
						.exchange(
								finalUrl,
								HttpMethod.GET,
								requestEntity,
								new ParameterizedTypeReference<ServiceResponse<BeanFeedbackSubCategoryWrapper>>() {
								}).getBody();

			} catch (RestClientException e) {
				serviceResponse = new ServiceResponse<>();
				serviceResponse.setError(true);
				serviceResponse.setError_message(e.getCause().getMessage());

			} catch (Exception e) {
				e.printStackTrace();
			}
			return serviceResponse;
		}

		@Override
		protected void onCancelled() {
			asyncResponse.processFinish(null);
		}
	}

	private void createFeedbackAsyncTask() {

		if (AppUtil.checkNetworkConnection(getActivity())) {

			// get data
			String feedbackType = (mCurrentType != null ? mCurrentType
					.getType().toUpperCase() : "");
			String title = editTextTitle.getText().toString().trim();
			String post = editTextDescription.getText().toString().trim();
			int catId = (mCurrentCategory != null ? mCurrentCategory.getId()
					: 0);
			int subCatId = (mCurrentSubCategory != null ? mCurrentSubCategory
					.getId() : 0);
			int mobileUserId = SharedPref.getInteger(getActivity(),
					SharedPref.USERID_KEY, 0);

			ArrayList<String> feedbackList = new ArrayList<>();
			if (path != null) {
				feedbackList.add(path);
			}

			if (videoPath != null) {
				feedbackList.add(videoPath);
			}

			if (audioPath != null) {
				feedbackList.add(audioPath);
			}

			mCreateFeedbackAsyncTask = new CreateFeedbackAsyncTask(
					getActivity(), feedbackType, title, post, catId, subCatId,
					mobileUserId, feedbackList);
			mCreateFeedbackAsyncTask.asyncResponse = FeedbackFragment.this;
			mCreateFeedbackAsyncTask.executeOnExecutor(
					AsyncTask.THREAD_POOL_EXECUTOR, (Void) null);

		} else {

			BaseHelper.confirm(getActivity(), "Network Unavaliable",
					"Please turn on network to connect with server",
					new ConfirmListener() {

						@Override
						public void onYes() {
							AppUtil.showSystemSettingsDialog(getActivity());
						}
					}, new CancelListener() {

						@Override
						public void onNo() {
							getHome().backToHomeScreen();

						}
					});
		}

	}

	private void startFeedbackTypeAsyncTask() {

		if (AppUtil.checkNetworkConnection(getActivity())) {

			mFeedbackTypeAsyncTask = new FeedbackTypeAsyncTask(getActivity());
			mFeedbackTypeAsyncTask.executeOnExecutor(
					AsyncTask.THREAD_POOL_EXECUTOR, (Void) null);

		} else {

			BaseHelper.confirm(getActivity(), "Network Unavaliable",
					"Please turn on network to connect with server",
					new ConfirmListener() {

						@Override
						public void onYes() {
							AppUtil.showSystemSettingsDialog(getActivity());
						}
					}, new CancelListener() {

						@Override
						public void onNo() {
							getHome().backToHomeScreen();

						}
					});
		}

	}

	private void startFeedbackCategoryAsyncTask() {

		if (AppUtil.checkNetworkConnection(getActivity())) {

			mFeedbackCategoryAsyncTask = new FeedbackCategoryAsyncTask(
					getActivity());
			mFeedbackCategoryAsyncTask.executeOnExecutor(
					AsyncTask.THREAD_POOL_EXECUTOR, (Void) null);

		} else {

			BaseHelper.confirm(getActivity(), "Network Unavaliable",
					"Please turn on network to connect with server",
					new ConfirmListener() {

						@Override
						public void onYes() {
							AppUtil.showSystemSettingsDialog(getActivity());
						}
					}, new CancelListener() {

						@Override
						public void onNo() {
							getHome().backToHomeScreen();

						}
					});
		}

	}

	private void startFeedbackSubCategoryAsyncTask(int categoryId) {
		if (AppUtil.checkNetworkConnection(getActivity())) {

			mFeedbackSubCategoryAsyncTask = new FeedbackSubCategoryAsyncTask(
					getActivity(), categoryId);
			mFeedbackSubCategoryAsyncTask.executeOnExecutor(
					AsyncTask.THREAD_POOL_EXECUTOR, (Void) null);

		} else {

			BaseHelper.confirm(getActivity(), "Network Unavaliable",
					"Please turn on network to connect with server",
					new ConfirmListener() {

						@Override
						public void onYes() {
							AppUtil.showSystemSettingsDialog(getActivity());
						}
					}, new CancelListener() {

						@Override
						public void onNo() {
							getHome().backToHomeScreen();

						}
					});
		}
	}

}
