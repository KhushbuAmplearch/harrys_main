package com.riverview.harrys.asynctask;

import static com.riverview.harrys.constant.AppConst.ATTR_TAG_UDID;
import static com.riverview.harrys.constant.AppConst.HTTP_METHOD_GET;
import static com.riverview.harrys.constant.AppConst.SERVICE_END_POINT;
import static com.riverview.harrys.constant.AppConst.URL_GET_COMPANY_DATA;

import java.io.IOException;
import java.io.ObjectInputStream.GetField;
import java.util.ArrayList;
import java.util.Iterator;

import org.apache.http.NameValuePair;
import org.apache.http.client.utils.URLEncodedUtils;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.type.TypeReference;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.client.ClientHttpResponse;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.DefaultResponseErrorHandler;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.net.Uri;
import android.os.AsyncTask;
import android.support.v4.app.FragmentManager;
import android.util.Log;

import com.iapps.libs.helpers.BaseHelper;
import com.riverview.harrys.R;
import com.riverview.harrys.adapter.NewsAdapter;
import com.riverview.harrys.dialogfragment.ForceUpdateDialog;
import com.riverview.harrys.dialogfragment.SignUpSuccessDialog;
import com.riverview.harrys.exception.RestServiceException;
import com.riverview.harrys.helpers.Keys;
import com.riverview.harrys.objects.BeanCompanyData;
import com.riverview.harrys.objects.BeanNews;
import com.riverview.harrys.objects.BeanPromotionItem;
import com.riverview.harrys.objects.ErrorServiceResponse;
import com.riverview.harrys.objects.ServiceResponse;
import com.riverview.harrys.util.AppUtil;
import com.riverview.harrys.util.WebServiceUtil;

public class CompanyDataAsyncTask extends
		AsyncTask<Void, Void, ServiceResponse<ArrayList<BeanCompanyData>>> {

	private static final String TAG = CompanyDataAsyncTask.class
			.getSimpleName();

	public AsyncResponse<ServiceResponse<ArrayList<BeanCompanyData>>> asyncResponse;

	// private ProgressDialog mProgressDialog;
	private Context mContext;
	private int companyId;

	String[] commandString = null;
	ArrayList<NameValuePair> httpParams = null;

	private String companyDataUrl;

	int cmsBuildNumber;
	int appBuildNumber;

	FragmentManager mFragmentManager;

	public CompanyDataAsyncTask(Context context, int id,
			FragmentManager fragmentManager) {
		this.mContext = context;
		this.companyId = id;
		this.mFragmentManager = fragmentManager;
	}

	@Override
	protected void onPreExecute() {
		Log.d(TAG, "onPreExecute");
		super.onPreExecute();

		try {
			companyDataUrl = String.format(URL_GET_COMPANY_DATA, companyId);
			httpParams = AppUtil.generateAuthCommandString(null,
					companyDataUrl, HTTP_METHOD_GET);

		} catch (Exception e) {

			e.printStackTrace();
		}

	}

	@Override
	protected void onPostExecute(
			ServiceResponse<ArrayList<BeanCompanyData>> response) {
		Log.d(TAG, "onPostExecute");

		if (response != null && response.getStatus() == 200 && !response.error) {
			Log.i(TAG, "News feed download Success ");

			ArrayList<BeanCompanyData> companyDataList = response.getResponse();
			if (companyDataList != null && companyDataList.size() > 0) {

				BeanCompanyData latestAppVersion = companyDataList.get(0);

				try {
					cmsBuildNumber = Double.valueOf(latestAppVersion
							.getVersion()).intValue();
					
					 String message = latestAppVersion.getMessage();

					// get local build number
					PackageInfo pInfo = mContext.getPackageManager()
							.getPackageInfo(mContext.getPackageName(), 0);
					appBuildNumber = Double.valueOf(pInfo.versionName).intValue();

					// check the version data
					if (cmsBuildNumber > appBuildNumber) {
						// show update dialog
						try {
							
							if(message == null || message.isEmpty()){

								message = mContext.getString(R.string.rc_force_update_msg);
							}
						/*	
							AppUtil.confirmForceUpdate(mContext, mContext.getString(R.string.app_name), message,
									new AppUtil.ConfirmListener() {
										@Override
										public void onYes() {
											
											String appPackageName = "com.riverview.harrys";
											try {
												mContext.startActivity(
														new Intent(Intent.ACTION_VIEW, Uri
																.parse("market://details?id="
																		+ appPackageName)));
											} catch (android.content.ActivityNotFoundException anfe) {
												mContext.startActivity(new Intent(
														Intent.ACTION_VIEW,
														Uri.parse("https://play.google.com/store/apps/details?id="
																+ appPackageName)));
											}					
										}
									});*/
							
							ForceUpdateDialog df = new ForceUpdateDialog(message);
							df.show(mFragmentManager, Keys.DIALOG);
						} catch (Exception e) {
							e.printStackTrace();
						}

					} else {
						Log.d(TAG, "App have latest version");
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}
	}

	@Override
	protected ServiceResponse<ArrayList<BeanCompanyData>> doInBackground(
			Void... voids) {
		Log.d(TAG, "doInBackground");

		ServiceResponse<ArrayList<BeanCompanyData>> serviceResponse = null;

		RestTemplate restTemplate = WebServiceUtil.getRestTemplateInstance();

		restTemplate.setErrorHandler(new DefaultResponseErrorHandler() {
			@Override
			public void handleError(ClientHttpResponse response)
					throws IOException {
				ObjectMapper mapper = WebServiceUtil.getErrorObjMapper();
				ErrorServiceResponse errorRes;
				errorRes = mapper.readValue(response.getBody(),
						new TypeReference<ErrorServiceResponse>() {
						});

				throw new RestServiceException(errorRes, errorRes
						.getError_message());
			}

			protected boolean hasError(HttpStatus statusCode) {
				Log.d(TAG, "Status ----> " + statusCode);
				if (statusCode.value() == 200) {
					return false;
				}
				return true;
			}
		});

		String url = String.format(SERVICE_END_POINT + "%s", companyDataUrl);

		String urlParams = URLEncodedUtils.format(httpParams, "UTF-8");
		String finalUrl = String.format(url + "?" + "%s", urlParams);

		Log.d(TAG, "Final URL ------> " + finalUrl);

		HttpHeaders requestHeaders = new HttpHeaders();
		requestHeaders.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
		requestHeaders.set("Accept", "application/json");
		requestHeaders.add(ATTR_TAG_UDID, AppUtil.getDeviceId(mContext));

		HttpEntity<MultiValueMap<String, String>> requestEntity = new HttpEntity<MultiValueMap<String, String>>(
				null, requestHeaders);

		Log.d(TAG, "Url " + url);
		Log.d(TAG, "Request Entity " + requestEntity.toString());

		try {
			serviceResponse = restTemplate
					.exchange(
							finalUrl,
							HttpMethod.GET,
							requestEntity,
							new ParameterizedTypeReference<ServiceResponse<ArrayList<BeanCompanyData>>>() {
							}).getBody();

		} catch (RestClientException e) {
			serviceResponse = new ServiceResponse<ArrayList<BeanCompanyData>>();
			serviceResponse.setError(true);
			// serviceResponse.setError_message(e.getCause().getMessage());

		} catch (Exception e) {
			e.printStackTrace();
		}
		return serviceResponse;
	}

	@Override
	protected void onCancelled() {

		// if (mProgressDialog != null && mProgressDialog.isShowing()) {
		// mProgressDialog.dismiss();
		asyncResponse.processFinish(null);
		// }
	}
}
