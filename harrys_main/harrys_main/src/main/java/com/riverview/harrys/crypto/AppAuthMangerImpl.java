package com.riverview.harrys.crypto;

import static com.riverview.harrys.constant.AppConst.SERVICE_PRIVATE_KEY;
import static com.riverview.harrys.constant.AppConst.SHA_ALGORITHM_1;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.nio.charset.Charset;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;

import android.util.Base64;

import com.riverview.harrys.util.ByteUtils;

public class AppAuthMangerImpl implements AppAuthManager {

	private static final String TAG = AppAuthMangerImpl.class.getSimpleName();

	@Override
	public String getHMACHash(String data) throws AuthException {

		try {
			final Charset charSet = Charset.forName("US-ASCII");

			SecretKeySpec secretKeySpec = new SecretKeySpec(charSet.encode(
					SERVICE_PRIVATE_KEY).array(), SHA_ALGORITHM_1);
			Mac hMac = Mac.getInstance(SHA_ALGORITHM_1);
			hMac.init(secretKeySpec);

			byte[] hashVal = hMac.doFinal(charSet.encode(data).array());

			String s = URLEncoder.encode(
					Base64.encodeToString(hashVal, Base64.URL_SAFE), "UTF-8");

			return ByteUtils.hexToString(hashVal);

			// final Charset asciiCs = Charset.forName("US-ASCII");
			// final Mac sha256_HMAC = Mac.getInstance("HmacSHA256");
			// final SecretKeySpec secret_key = new
			// javax.crypto.spec.SecretKeySpec(asciiCs.encode(SERVICE_PRIVATE_KEY).array(),
			// "HmacSHA256");
			// try {
			// sha256_HMAC.init(secret_key);
			// } catch (Exception e) {}
			// final byte[] mac_data =
			// sha256_HMAC.doFinal(asciiCs.encode(data).array());
			// String result = "";
			// for (final byte element : mac_data)
			// {
			// result += Integer.toString((element & 0xff) + 0x100,
			// 16).substring(1);
			// }
			//
			// return result;

		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
			throw new AuthException(e.getMessage(),
					AuthException.BAD_PARAMETERS, e.getLocalizedMessage());
		} catch (InvalidKeyException e) {
			e.printStackTrace();
			throw new AuthException(e.getMessage(),
					AuthException.BAD_PARAMETERS, e.getLocalizedMessage());
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
			throw new AuthException(e.getMessage(),
					AuthException.BAD_PARAMETERS, e.getLocalizedMessage());
		}

	}
}
