package com.riverview.harrys.objects;

public class BeanTransaction {

	private int id;
	private String loyalty_user_id, txn_type, loyalty_type, amount,
			loyalty_transaction_id, comment, outlet_id, deleted_at, created_at,
			updated_at, reversal_id, outletid, outlet_name;

	private BeanOutlet outlet;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getLoyalty_user_id() {
		return loyalty_user_id;
	}

	public void setLoyalty_user_id(String loyalty_user_id) {
		this.loyalty_user_id = loyalty_user_id;
	}

	public String getTxn_type() {
		return txn_type;
	}

	public void setTxn_type(String txn_type) {
		this.txn_type = txn_type;
	}

	public String getLoyalty_type() {
		return loyalty_type;
	}

	public void setLoyalty_type(String loyalty_type) {
		this.loyalty_type = loyalty_type;
	}

	public String getAmount() {
		return amount;
	}

	public void setAmount(String amount) {
		this.amount = amount;
	}

	public String getLoyalty_transaction_id() {
		return loyalty_transaction_id;
	}

	public void setLoyalty_transaction_id(String loyalty_transaction_id) {
		this.loyalty_transaction_id = loyalty_transaction_id;
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	public String getOutlet_id() {
		return outlet_id;
	}

	public void setOutlet_id(String outlet_id) {
		this.outlet_id = outlet_id;
	}

	public String getDeleted_at() {
		return deleted_at;
	}

	public void setDeleted_at(String deleted_at) {
		this.deleted_at = deleted_at;
	}

	public String getCreated_at() {
		return created_at;
	}

	public void setCreated_at(String created_at) {
		this.created_at = created_at;
	}

	public String getUpdated_at() {
		return updated_at;
	}

	public void setUpdated_at(String updated_at) {
		this.updated_at = updated_at;
	}

	public String getReversal_id() {
		return reversal_id;
	}

	public void setReversal_id(String reversal_id) {
		this.reversal_id = reversal_id;
	}

	public String getOutletid() {
		return outletid;
	}

	public void setOutletid(String outletid) {
		this.outletid = outletid;
	}

	public String getOutlet_name() {
		return outlet_name;
	}

	public void setOutlet_name(String outlet_name) {
		this.outlet_name = outlet_name;
	}

	public BeanOutlet getOutlet() {
		return outlet;
	}

	public void setOutlet(BeanOutlet outlet) {
		this.outlet = outlet;
	}
}
