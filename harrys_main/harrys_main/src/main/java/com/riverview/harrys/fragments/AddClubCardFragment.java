package com.riverview.harrys.fragments;

import android.app.Activity;
import android.content.Context;
import android.graphics.Typeface;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.iapps.libs.helpers.BaseHelper;
import com.iapps.libs.helpers.BaseHelper.ConfirmListener;
import com.riverview.harrys.MainActivity;
import com.riverview.harrys.R;
import com.riverview.harrys.RCGenericFragment;
import com.riverview.harrys.asynctask.AsyncResponse;
import com.riverview.harrys.asynctask.CreateLoyaltyUserAsyncTask;
import com.riverview.harrys.helpers.SharedPref;
import com.riverview.harrys.helpers.Utils;
import com.riverview.harrys.objects.BeanLoyaltyCard;
import com.riverview.harrys.objects.ServiceResponse;
import com.riverview.harrys.util.AppUtil;

public class AddClubCardFragment extends RCGenericFragment implements
		AsyncResponse<ServiceResponse<BeanLoyaltyCard>> {

	private static final String TAG = AddClubCardFragment.class.getSimpleName();

	private LinearLayout LLAddCard;
	private EditText editTextCardId;
	private Button buttonAddClubCard;
	private TextView textViewHeader;

	private View v;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setHasOptionsMenu(false);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		v = inflater.inflate(R.layout.add_clubcard_fragment_layout, container,
				false);
		LLAddCard = (LinearLayout)v.findViewById(R.id.LLAddCard);
		editTextCardId = (EditText)v.findViewById(R.id.editTextCardId);
		buttonAddClubCard = (Button)v.findViewById(R.id.buttonAddClubCard);
		textViewHeader = (TextView)v.findViewById(R.id.tv1);

		getActivity().getActionBar().show();
		((MainActivity) getActivity()).setBackActionBar(getHome()
				.getResources().getString(R.string.rc_addcard_header),View.INVISIBLE,"");

		hideDefaultKeyboard();

		return v;
	}

	private void hideDefaultKeyboard() {
		try {
			getActivity().getWindow().setSoftInputMode(
					WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

			getActivity();
			InputMethodManager imm = (InputMethodManager) getActivity()
					.getSystemService(Context.INPUT_METHOD_SERVICE);

			imm.hideSoftInputFromWindow(editTextCardId.getWindowToken(), 0);

		} catch (Exception e) {
		}

	}

	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);

		buttonAddClubCard.setOnClickListener(clickListeners);

		Utils.setEditTextFontSizeBasedOnScreenDensity(getActivity(),
				editTextCardId, Utils.FONT_SMALL_DENSITY_SIZE);
		Utils.setButtonFontSizeBasedOnScreenDensity(getActivity(),
				buttonAddClubCard, Utils.FONT_SMALL_DENSITY_SIZE, Typeface.BOLD);
		Utils.setTextViewFontSizeBasedOnScreenDensity(getActivity(),
				textViewHeader, Utils.FONT_SMALL_DENSITY_SIZE);

	}

	View.OnClickListener clickListeners = new View.OnClickListener() {
		public void onClick(View v) {

			if (v.getId() == R.id.buttonAddClubCard) {

				if (AppUtil.checkNetworkConnection(getActivity())) {
					addClubCard();
				} else {
					BaseHelper.confirm(getActivity(), "Network Unavaliable",
							"Please turn on network to connect with server",
							new ConfirmListener() {

								@Override
								public void onYes() {
									AppUtil.showSystemSettingsDialog(getActivity());
								}
							});
				}
				hideDefaultKeyboard();
			}
		}
	};

	private void addClubCard() {

		int mobileUserId = SharedPref.getInteger(getHome(),
				SharedPref.USERID_KEY, 0);

		CreateLoyaltyUserAsyncTask mAuthTask = new CreateLoyaltyUserAsyncTask(
				getActivity(), String.valueOf(mobileUserId), editTextCardId
						.getText().toString());
		mAuthTask.asyncResponse = AddClubCardFragment.this;
		mAuthTask.execute((Void) null);

	}

	@Override
	public void onDestroy() {

		super.onDestroy();
		//hideDefaultKeyboard();
		Utils.hideKeyboard(getActivity());
	}

	@Override
	public void onDetach() {
		super.onDetach();
		//hideDefaultKeyboard();
		Utils.hideKeyboard(getActivity());
	}

	@Override
	public void processFinish(ServiceResponse<BeanLoyaltyCard> details) {
		if (details == null) {
			BaseHelper.showAlert(getActivity(), "Add club card Error");
			return;
		}

		if (details.getStatus() == 200 && !details.error) {
			Log.i(TAG, "Add club card Success ");

			Toast.makeText(getHome(), "Your club card is succesfully added",
					Toast.LENGTH_SHORT).show();

			 getHome().setFragmentClearStack(new CardsFragment());
		} else {
			BaseHelper.showAlert(getActivity(), "Harry's",
					details.getError_message());
		}

	}

}
