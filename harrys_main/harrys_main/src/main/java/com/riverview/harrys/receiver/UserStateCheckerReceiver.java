package com.riverview.harrys.receiver;

import java.util.Calendar;

import com.riverview.harrys.service.UserStatusService;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

public class UserStateCheckerReceiver extends BroadcastReceiver {

	private static final String TAG = UserStateCheckerReceiver.class
			.getSimpleName();
	public static final String ACTION_START_USER_STATUS_CHECKER = "com.riverview.harrys.user.checker";

	private static final int SERVICE_REPEAT_TIME_MINUTES = 2 * 60; // 2 mins
	private static final int SERVICE_START_TIME_SECONDS = 2; // 2 seconds
	private static final long SERVICE_REPEAT_TIME_MS = 1000 * SERVICE_REPEAT_TIME_MINUTES;
	public static final int REQUEST_CODE = 100;

	@Override
	public void onReceive(Context context, Intent intent) {

		if (intent.getAction().equals(ACTION_START_USER_STATUS_CHECKER)) {
			Log.d(TAG, "onReceive");

			AlarmManager alarmManager = (AlarmManager) context
					.getSystemService(Context.ALARM_SERVICE);
			Intent i = new Intent(context, UserStatusService.class);
			PendingIntent pendingIntent = PendingIntent.getService(context,
					REQUEST_CODE, i, PendingIntent.FLAG_UPDATE_CURRENT);

			// Create calendar instance to handle start time
			Calendar calendar = Calendar.getInstance();
			calendar.add(Calendar.SECOND, SERVICE_START_TIME_SECONDS);

			// start pending intent in 2 minute intervals until operation get
			// succeeded. This service will not delivered the async task
			// when device remain in sleep mode and start once device wake up.
			alarmManager.setInexactRepeating(AlarmManager.RTC,
					calendar.getTimeInMillis(), SERVICE_REPEAT_TIME_MS,
					pendingIntent);
		}
	}

}
