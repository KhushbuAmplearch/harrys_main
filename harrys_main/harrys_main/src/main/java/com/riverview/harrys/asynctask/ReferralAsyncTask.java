package com.riverview.harrys.asynctask;

import static com.riverview.harrys.constant.AppConst.ATTR_TAG_UDID;
import static com.riverview.harrys.constant.AppConst.HTTP_METHOD_GET;
import static com.riverview.harrys.constant.AppConst.SERVICE_END_POINT;
import static com.riverview.harrys.constant.AppConst.URL_GET_REFERRALS;

import java.io.IOException;
import java.util.ArrayList;

import org.apache.http.NameValuePair;
import org.apache.http.client.utils.URLEncodedUtils;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.type.TypeReference;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.client.ClientHttpResponse;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.DefaultResponseErrorHandler;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.riverview.harrys.exception.RestServiceException;
import com.riverview.harrys.objects.BeanReferralCampaigns;
import com.riverview.harrys.objects.ErrorServiceResponse;
import com.riverview.harrys.objects.ServiceResponse;
import com.riverview.harrys.util.AppUtil;
import com.riverview.harrys.util.WebServiceUtil;

public class ReferralAsyncTask extends
		AsyncTask<Void, Void, ServiceResponse<ArrayList<BeanReferralCampaigns>>> {

	private static final String TAG = ReferralAsyncTask.class.getSimpleName();

	public AsyncResponse<ServiceResponse<ArrayList<BeanReferralCampaigns>>> asyncResponse;

	// private ProgressDialog mProgressDialog;
	private Context mContext;
    private int mobileUserId;
    private String type;

	String[] commandString = null;
	ArrayList<NameValuePair> httpParams = null;

	private String referralUrl;

	public ReferralAsyncTask(Context context, int id, String type) {
		this.mContext = context;
        this.mobileUserId = id;
        this.type = type;
	}

	@Override
	protected void onPreExecute() {
		Log.d(TAG, "onPreExecute");
		super.onPreExecute();

		try {
			referralUrl = String.format(URL_GET_REFERRALS, String.valueOf(mobileUserId), type);
			Log.d(TAG, "onPreExecute " + referralUrl);
			httpParams = AppUtil.generateAuthCommandString(null, referralUrl,
					HTTP_METHOD_GET);

		} catch (Exception e) {

			e.printStackTrace();
		}

	}

	@Override
	protected void onPostExecute(ServiceResponse<ArrayList<BeanReferralCampaigns>> response) {
		Log.d(TAG, "onPostExecute");

		// if (mProgressDialog != null && mProgressDialog.isShowing()) {
		// mProgressDialog.dismiss();
		// }
		asyncResponse.processFinish(response);
	}

	@Override
	protected ServiceResponse<ArrayList<BeanReferralCampaigns>> doInBackground(Void... voids) {
		Log.d(TAG, "doInBackground");

		ServiceResponse<ArrayList<BeanReferralCampaigns>> serviceResponse = null;

		RestTemplate restTemplate = WebServiceUtil.getRestTemplateInstance();

		restTemplate.setErrorHandler(new DefaultResponseErrorHandler() {
			@Override
			public void handleError(ClientHttpResponse response)
					throws IOException {
				ObjectMapper mapper = WebServiceUtil.getErrorObjMapper();
				ErrorServiceResponse errorRes;
				errorRes = mapper.readValue(response.getBody(),
						new TypeReference<ErrorServiceResponse>() {
						});

				throw new RestServiceException(errorRes, errorRes
						.getError_message());
			}

			protected boolean hasError(HttpStatus statusCode) {
				Log.d(TAG, "Status ----> " + statusCode);
				if (statusCode.value() == 200) {
					return false;
				}
				return true;
			}
		});

		String url = String.format(SERVICE_END_POINT + "%s", referralUrl);

		String urlParams = URLEncodedUtils.format(httpParams, "UTF-8");
		String finalUrl = String.format(url + "?" + "%s", urlParams);

		Log.d(TAG, "Final URL ------> " + finalUrl);

		HttpHeaders requestHeaders = new HttpHeaders();
		requestHeaders.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
		requestHeaders.set("Accept", "application/json");
		requestHeaders.add(ATTR_TAG_UDID, AppUtil.getDeviceId(mContext));

		HttpEntity<MultiValueMap<String, String>> requestEntity = new HttpEntity<MultiValueMap<String, String>>(
				null, requestHeaders);

		Log.d(TAG, "Url " + url);
		Log.d(TAG, "Request Entity " + requestEntity.toString());

		try {
			serviceResponse = restTemplate
					.exchange(
							finalUrl,
							HttpMethod.GET,
							requestEntity,
							new ParameterizedTypeReference<ServiceResponse<ArrayList<BeanReferralCampaigns>>>() {
							}).getBody();

		} catch (RestClientException e) {
			serviceResponse = new ServiceResponse<ArrayList<BeanReferralCampaigns>>();
			serviceResponse.setError(true);
			serviceResponse.setError_message(e.getCause().getMessage());

		} catch (Exception e) {
			e.printStackTrace();
		}
		return serviceResponse;
	}

	@Override
	protected void onCancelled() {

		// if (mProgressDialog != null && mProgressDialog.isShowing()) {
		// mProgressDialog.dismiss();
		asyncResponse.processFinish(null);
		// }
	}
}
