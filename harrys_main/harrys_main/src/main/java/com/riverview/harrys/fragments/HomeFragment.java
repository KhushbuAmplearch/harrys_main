package com.riverview.harrys.fragments;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.client.utils.URLEncodedUtils;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONObject;


import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStates;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.vision.text.Line;
import com.google.zxing.client.android.common.executor.AsyncTaskExecInterface;
import com.iapps.libs.helpers.BaseConstants;
import com.iapps.libs.helpers.BaseHelper;
import com.iapps.libs.helpers.BaseHelper.CancelListener;
import com.iapps.libs.helpers.BaseHelper.ConfirmListener;
import com.iapps.libs.helpers.HTTPAsyncTask;
import com.iapps.libs.objects.Response;
import com.readystatesoftware.viewbadger.BadgeView;
import com.riverview.harrys.MainActivity;
import com.riverview.harrys.R;
import com.riverview.harrys.RCGenericFragment;
import com.riverview.harrys.SplashAdActivity;
import com.riverview.harrys.asynctask.AdsAsyncTask;
import com.riverview.harrys.asynctask.AsyncResponse;
import com.riverview.harrys.asynctask.CompanyDataAsyncTask;
import com.riverview.harrys.asynctask.PromotionsAsyncTask;
import com.riverview.harrys.asynctask.OffersAsyncTask;
import com.riverview.harrys.constant.AppConst;
import com.riverview.harrys.dialogfragment.SignUpSuccessDialog;
import com.riverview.harrys.helpers.Api;
import com.riverview.harrys.helpers.Constants;
import com.riverview.harrys.helpers.Converter;
import com.riverview.harrys.helpers.Keys;
import com.riverview.harrys.helpers.SharedPref;
import com.riverview.harrys.helpers.UserInfoManager;
import com.riverview.harrys.helpers.Utils;
import com.riverview.harrys.helpers.Waiter;
import com.riverview.harrys.objects.BeanAds;
import com.riverview.harrys.objects.BeanOffers;
import com.riverview.harrys.objects.ServiceResponse;
import com.riverview.harrys.util.AppUtil;

import static com.riverview.harrys.MainActivity.loc_permission;

public class HomeFragment extends RCGenericFragment implements
        LocationListener, GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, AsyncResponse<ServiceResponse<ArrayList<BeanOffers>>> {
    private final static int PLAY_SERVICES_REQUEST = 1000;
    private static final String TAG = HomeFragment.class.getSimpleName();

    public static final int TAG_GET_LIST_VENUES = 0, TAG_GO_LOGIN = 1,
            TAG_GET_LIST_SWIM_VENUES = 2;

    private LinearLayout LLMenuOne;
    private LinearLayout LLMenuTwo;
    private LinearLayout LLMenuThree;
    private LinearLayout LLMenuFour;
    private LinearLayout LLMenuFive;
    private LinearLayout LLMenuSix;
    private LinearLayout LLMenuSeven;
    private LinearLayout LLMenuEight;
    private LinearLayout LLMenuNine;
    private LinearLayout LLSharing;
    private LinearLayout LLHeader;
    private LinearLayout LLB;

    private TextView imgMenuIconRow2_1;
    private TextView imgMenuIconRow3_2;

    private TextView textViewHomeHeaderInfo;
    private TextView tvMenu1_1;
    private TextView tvMenu1_2;
    private TextView tvMenu1_3;
    private TextView tvMenu2_1;
    private TextView tvMenu2_2;
    private TextView tvMenu2_3;
    private TextView tvMenu3_1;
    private TextView tvMenu3_2;
    private TextView tvMenu3_3;
    private View v;

    private boolean showSignUpSuccess = false;
    private boolean showOTPVerify = false;

    protected LocationManager locationManager;
    protected LocationListener locationListener;

    private Waiter waiter;
    Location mLastLocation;
    private BadgeView badgeView;
    String[] PERMISSIONS={

            Manifest.permission.ACCESS_COARSE_LOCATION,
            Manifest.permission.ACCESS_FINE_LOCATION,
    };
    private Bundle savedInstanceState;
    public static final int MY_PERMISSIONS_REQUEST_LOCATION = 99;
    GoogleApiClient googleApiClient;
    int PERMISSION_ALL = 1;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

		/*if (!UserInfoManager.getInstance(getActivity()).isSignedIn(
				getActivity())) {

		}*/

        //callAdsService();

        getActivity().getActionBar().show();
        hideSystemUI();

        SharedPref.deleteString(getActivity(), SharedPref.LISTDATA_KEY);
        SharedPref.deleteString(getActivity(), SharedPref.LISTDATA_1_KEY);
        SharedPref.deleteString(getActivity(), SharedPref.LISTDATA_2_KEY);
        SharedPref.deleteString(getActivity(), SharedPref.SUBDATA_1_KEY);
        SharedPref.deleteString(getActivity(), SharedPref.SUBDATA_2_KEY);

        // Start analytics service
        // send broadcast to start Altitude service
        Intent i = new Intent();
        i.setAction(AppConst.ACTION_PROCESS_ANALYTICS);
        getActivity().sendBroadcast(i);

    }

    private void hideSystemUI() {
        getHome().getWindow().getDecorView()
                .setSystemUiVisibility(View.SYSTEM_UI_FLAG_VISIBLE);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        v = inflater.inflate(R.layout.home_fragment_layout, container, false);

        textViewHomeHeaderInfo = (TextView) v.findViewById(R.id.textViewHomeHeaderInfo);
        tvMenu1_1 = (TextView) v.findViewById(R.id.textViewMenuTitleRow1_1);
        tvMenu1_2 = (TextView) v.findViewById(R.id.textViewMenuTitleRow1_2);
        tvMenu1_3 = (TextView) v.findViewById(R.id.textViewMenuTitleRow1_3);
        tvMenu2_1 = (TextView) v.findViewById(R.id.textViewMenuTitleRow2_1);
        tvMenu2_2 = (TextView) v.findViewById(R.id.textViewMenuTitleRow2_2);
        tvMenu2_3 = (TextView) v.findViewById(R.id.textViewMenuTitleRow2_3);
        tvMenu3_1 = (TextView) v.findViewById(R.id.textViewMenuTitleRow3_1);
        tvMenu3_2 = (TextView) v.findViewById(R.id.textViewMenuTitleRow3_2);
        tvMenu3_3 = (TextView) v.findViewById(R.id.textViewMenuTitleRow3_3);

        LLMenuOne = (LinearLayout) v.findViewById(R.id.LLMenuOne);
        LLMenuTwo = (LinearLayout) v.findViewById(R.id.LLMenuTwo);
        LLMenuThree = (LinearLayout) v.findViewById(R.id.LLMenuThree);
        LLMenuFour = (LinearLayout) v.findViewById(R.id.LLMenuFour);
        LLMenuFive = (LinearLayout) v.findViewById(R.id.LLMenuFive);
        LLMenuSix = (LinearLayout) v.findViewById(R.id.LLMenuSix);
        LLMenuSeven = (LinearLayout) v.findViewById(R.id.LLMenuSeven);
        LLMenuEight = (LinearLayout) v.findViewById(R.id.LLMenuEight);
        LLMenuNine = (LinearLayout) v.findViewById(R.id.LLMenuNine);
        LLSharing = (LinearLayout) v.findViewById(R.id.LLSharing);
        LLHeader = (LinearLayout) v.findViewById(R.id.LLHeader);
        LLB = (LinearLayout) v.findViewById(R.id.LLB);


        imgMenuIconRow2_1 = (TextView) v.findViewById(R.id.imgMenuIconRow2_1);
        imgMenuIconRow3_2 = (TextView) v.findViewById(R.id.imgMenuIconRow3_2);
        setHasOptionsMenu(false);
        //checkLocationPermission();
       /* locationManager = (LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE);
        getLocation();
        if (!hasPermissions(getActivity(), PERMISSIONS)) {
            ActivityCompat.requestPermissions(getActivity(), PERMISSIONS, PERMISSION_ALL);
        }
        //  if(!locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER)) {
        //}
        LocationListener1();
		*/
       /*locationManager = (LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE);
		locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0, 0, this);
*/

        ((MainActivity) getActivity()).setHomeActionBar();

        try {
            if (getArguments() != null) {
                showSignUpSuccess = getArguments().getBoolean(
                        "show_signup_dialog", false);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        try {
            if (getArguments() != null) {
                showOTPVerify = getArguments().getBoolean(
                        "show_otp_verify_dialog", false);
                getArguments().remove("show_otp_verify_dialog");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return v;
    }

    public void LocationListener1() {
        if (loc_permission) {
            if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {


            }else
            {
                locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0, 0, this);

            }
        }
    }


    public void getLocation() {

        if (googleApiClient == null) {
            googleApiClient = new GoogleApiClient.Builder(getActivity())
                    .addApi(LocationServices.API)
                    .addConnectionCallbacks(this)
                    .addOnConnectionFailedListener(this).build();
            googleApiClient.connect();

            LocationRequest locationRequest = LocationRequest.create();
            locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
            locationRequest.setInterval(30 * 1000);
            locationRequest.setFastestInterval(1 * 1000);
            LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                    .addLocationRequest(locationRequest);

            //**************************
            builder.setAlwaysShow(true); //this is the key ingredient
            //**************************

            PendingResult<LocationSettingsResult> result =
                    LocationServices.SettingsApi.checkLocationSettings(googleApiClient, builder.build());
            result.setResultCallback(new ResultCallback<LocationSettingsResult>() {
                @Override
                public void onResult(LocationSettingsResult result) {
                    final Status status = result.getStatus();

                    final LocationSettingsStates state = result.getLocationSettingsStates();
                    switch (status.getStatusCode()) {
                        case LocationSettingsStatusCodes.SUCCESS:
                            // All location settings are satisfied. The client can initialize location
                            // requests here.
                            LocationListener1();
                            break;
                        case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                            // Location settings are not satisfied. But could be fixed by showing the user
                            // a dialog.
                            try {
                                // Show the dialog by calling startResolutionForResult(),
                                // and check the result in onActivityResult().
                                status.startResolutionForResult(
                                        getActivity(), 1000);
                            } catch (IntentSender.SendIntentException e) {
                                // Ignore the error.
                            }
                            break;
                        case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                            // Location settings are not satisfied. However, we have no way to fix the
                            // settings so we won't show the dialog.
                            break;
                    }
                }
            });
        }
    }

    public void getCurrentLocation() {
        if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        mLastLocation = LocationServices.FusedLocationApi
                .getLastLocation(googleApiClient);
        if (mLastLocation != null) {
            try {
                Log.e("1conlat",""+mLastLocation.getLatitude());
                Utils.latitude = mLastLocation.getLatitude();
                Utils.longitude = mLastLocation.getLongitude();

            } catch (Exception e) {
                // TODO: handle exception
            }
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        Log.e("onActivityResult()", Integer.toString(resultCode));

        //final LocationSettingsStates states = LocationSettingsStates.fromIntent(data);
        switch (requestCode) {
            case 1000:
                switch (resultCode) {
                    case Activity.RESULT_OK: {
                        LocationListener1();
                        break;
                    }
                    case Activity.RESULT_CANCELED: {
                        // The user was asked to change settings, but chose not to
                        break;
                    }
                    default: {
                        break;
                    }
                }
                break;
        }
    }

    @Override

    public void onRequestPermissionsResult(int permsRequestCode, String[] permissions, int[] grantResults) {

        switch (permsRequestCode) {

            case 1:

                boolean first = grantResults[0] == PackageManager.PERMISSION_GRANTED;

                if (first) {
                    loc_permission = true;
                }
                if (loc_permission) {

                    if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                        return;
                    }
                    locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0, 0, this);
                }
                break;

        }

    }

    public static boolean hasPermissions(Context context, String... permissions) {
        if (context != null && permissions != null) {
            for (String permission : permissions) {
                if (ActivityCompat.checkSelfPermission(context, permission) != PackageManager.PERMISSION_GRANTED) {
                    return false;
                }
            }
        }
        return true;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        this.savedInstanceState = savedInstanceState;

/*
		textViewHomeHeaderInfo.setTextSize(21f);
		tvMenu1_1.setTextSize(25f);
		tvMenu1_2.setTextSize(25f);
		tvMenu1_3.setTextSize(25f);
		tvMenu2_1.setTextSize(25f);
		tvMenu2_2.setTextSize(25f);
		tvMenu2_3.setTextSize(25f);
		tvMenu3_1.setTextSize(25f);
		tvMenu3_2.setTextSize(25f);
		tvMenu3_3.setTextSize(25f);
*/
        Utils.setTextViewFontSizeBasedOnScreenDensity(getActivity(), textViewHomeHeaderInfo, 21f);
        Utils.setTextViewFontSizeBasedOnScreenDensity(getActivity(), tvMenu1_1, 25f);
        Utils.setTextViewFontSizeBasedOnScreenDensity(getActivity(), tvMenu1_2, 25f);
        Utils.setTextViewFontSizeBasedOnScreenDensity(getActivity(), tvMenu1_3, 25f);
        Utils.setTextViewFontSizeBasedOnScreenDensity(getActivity(), tvMenu2_1, 25f);
        Utils.setTextViewFontSizeBasedOnScreenDensity(getActivity(), tvMenu2_2, 25f);
        Utils.setTextViewFontSizeBasedOnScreenDensity(getActivity(), tvMenu2_3, 25f);
        Utils.setTextViewFontSizeBasedOnScreenDensity(getActivity(), tvMenu3_1, 25f);
        Utils.setTextViewFontSizeBasedOnScreenDensity(getActivity(), tvMenu3_2, 25f);
        Utils.setTextViewFontSizeBasedOnScreenDensity(getActivity(), tvMenu3_3, 25f);
        LLMenuOne.setOnClickListener(clickListeners);
        LLMenuTwo.setOnClickListener(clickListeners);
        LLMenuThree.setOnClickListener(clickListeners);
        LLMenuFour.setOnClickListener(clickListeners);
        LLMenuFive.setOnClickListener(clickListeners);
        LLMenuSix.setOnClickListener(clickListeners);
        LLMenuSeven.setOnClickListener(clickListeners);
        LLMenuEight.setOnClickListener(clickListeners);
        LLMenuNine.setOnClickListener(clickListeners);
        LLSharing.setOnClickListener(clickListeners);
        LLHeader.setOnClickListener(clickListeners);

        LLSharing.setVisibility(View.GONE);
        textViewHomeHeaderInfo.setText(AppUtil.getTimeOfDay());

        if (showSignUpSuccess) {
            try {
                SignUpSuccessDialog df = new SignUpSuccessDialog();
                df.show(getChildFragmentManager(), Keys.DIALOG);
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else if (showOTPVerify) {
            try {
                BaseHelper.confirm(getHome(), getString(R.string.app_name),
                        getString(R.string.rc_verify_request),
                        confirmListener1, new CancelListener() {

                            @Override
                            public void onNo() {
                                // getHome().backToHomeScreen();

                            }
                        });
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        badgeView = new BadgeView(getHome(), LLB);
    }

    View.OnClickListener clickListeners = new View.OnClickListener() {
        public void onClick(View v) {

            switch (v.getId()) {
                case R.id.LLMenuOne:

                    ((MainActivity) getActivity())
                            .setFragment(new StoresFragment());

                    break;

                case R.id.LLMenuTwo:

                    ((MainActivity) getActivity())
                            .setFragment(new UpcWithPaginationFragment());

                    break;
                case R.id.LLMenuThree:

                    if (!UserInfoManager.getInstance(getActivity()).isSignedIn(
                            getActivity())) {

                        ((MainActivity) getActivity())
                                .setFragment(new PromotionsFragment());
                    } else {

                        BaseHelper.confirm(getActivity(), null,
                                "You need to login to use this service",
                                confirmListener, new CancelListener() {

                                    @Override
                                    public void onNo() {
                                        if (isAdded()) {
                                            getHome().backToHomeScreen();
                                        }

                                    }
                                });
                    }
                    break;
                case R.id.LLMenuFour:

                    // ((MainActivity) getActivity())
                    // .setFragment(new ShoppingCartFragment());

                    ((MainActivity) getActivity())
                            .setFragment(new RewardsFragment());

                    break;
                case R.id.LLMenuFive:
                    if (!UserInfoManager.getInstance(getActivity()).isSignedIn(
                            getActivity())) {
                        ((MainActivity) getActivity())
                                .setFragment(new CardsFragment());
                    } else {

                        BaseHelper.confirm(getActivity(), null,
                                "You need to login to use this service",
                                confirmListener, new CancelListener() {

                                    @Override
                                    public void onNo() {
                                        getHome().backToHomeScreen();

                                    }
                                });
                    }
                    break;
                case R.id.LLMenuSix:
                    if (!UserInfoManager.getInstance(getActivity()).isSignedIn(
                            getActivity())) {
                        ((MainActivity) getActivity())
                                .setFragment(new EventCategoryFragment());
                    } else {

                        BaseHelper.confirm(getActivity(), null,
                                "You need to login to use this service",
                                confirmListener, new CancelListener() {

                                    @Override
                                    public void onNo() {
                                        getHome().backToHomeScreen();

                                    }
                                });
                    }

                    break;
                case R.id.LLMenuSeven:
                    if (!UserInfoManager.getInstance(getActivity()).isSignedIn(
                            getActivity())) {
                        ((MainActivity) getActivity())
                                .setFragment(new UserProfileFragment());
                    } else {

                        BaseHelper.confirm(getActivity(), null,
                                "You need to login to use this service",
                                confirmListener, new CancelListener() {

                                    @Override
                                    public void onNo() {
                                        getHome().backToHomeScreen();

                                    }
                                });
                    }

                    break;
                case R.id.LLMenuEight:
                    if (!UserInfoManager.getInstance(getActivity()).isSignedIn(
                            getActivity())) {

                        ((MainActivity) getActivity())
                                .setFragment(new NewReferralFragment());
                    } else {

                        BaseHelper.confirm(getActivity(), null,
                                "You need to login to use this service",
                                confirmListener, new CancelListener() {

                                    @Override
                                    public void onNo() {
                                        getHome().backToHomeScreen();

                                    }
                                });
                    }

                    break;
                case R.id.LLMenuNine:
                    if (!UserInfoManager.getInstance(getActivity()).isSignedIn(
                            getActivity())) {
                        ((MainActivity) getActivity())
                                .setFragment(new NewsFragment());
                    } else {

                        BaseHelper.confirm(getActivity(), null,
                                "You need to login to use this service",
                                confirmListener, new CancelListener() {

                                    @Override
                                    public void onNo() {
                                        getHome().backToHomeScreen();

                                    }
                                });
                    }

                    break;
            }
        }
    };

    ConfirmListener confirmListener = new ConfirmListener() {

        @Override
        public void onYes() {
            ((MainActivity) getActivity()).setFragment(new LoginFragment());

        }
    };

    ConfirmListener confirmListener1 = new ConfirmListener() {

        @Override
        public void onYes() {

            ((MainActivity) getActivity())
                    .setFragment(new VerifyMobileOTPFragment());
            showOTPVerify = false;

        }
    };

    public void callAPI(int TAG) {

        switch (TAG) {

            case TAG_GET_LIST_VENUES:

                GetGymListAsync gla = new GetGymListAsync();
                gla.setUrl(getApi().getFacilityVenues());
                gla.execute();

                break;
            case TAG_GET_LIST_SWIM_VENUES:

                GetSwimListAsync gsla = new GetSwimListAsync();
                gsla.setUrl(getApi().getFacilitySwimVenues());
                gsla.execute();

                break;

            case TAG_GO_LOGIN:

                break;

        }

    }

    @Override
    public void onConnected(Bundle bundle) {

        LocationListener1();

    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    public class GetGymListAsync extends HTTPAsyncTask {
        @Override
        protected void onPreExecute() {
            showProgressBarCF();
        }

        @Override
        protected void onPostExecute(Response response) {
            closeProgressBarCF();
            try {

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public class GetSwimListAsync extends HTTPAsyncTask {
        @Override
        protected void onPreExecute() {
            showProgressBarCF();
        }

        @Override
        protected void onPostExecute(Response response) {
            closeProgressBarCF();
            try {

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void onLocationChanged(Location location) {

        Log.e("1homelat", "" + location.getLatitude());
        if (location != null)
        {
            try {
                Utils.latitude = location.getLatitude();
                Utils.longitude = location.getLongitude();
            } catch (Exception e) {

            }
        }
    }

    @Override
    public void onProviderDisabled(String arg0) {
        // TODO Auto-generated method stub

    }

    @Override
    public void onProviderEnabled(String arg0) {
        // TODO Auto-generated method stub

    }

    @Override
    public void onStatusChanged(String arg0, int arg1, Bundle arg2) {
        // TODO Auto-generated method stub

    }

    // @Override
    // public void onDestroy() {
    // super.onDestroy();
    // try {
    // locationManager.removeUpdates(this);
    // locationManager = null;
    // } catch (Exception e) {
    // e.printStackTrace();
    // }
    //
    // }

    @Override
    public void onPause() {
        super.onPause();
       /* try {
            locationManager.removeUpdates(this);
            locationManager = null;
        } catch (Exception e) {
            e.printStackTrace();
        }*/
    }

    @Override
    public void onResume() {
        // TODO Auto-generated method stub
        super.onResume();
        /*if (loc_permission ) {
            if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

            }
            locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0, 0, this);

        }*/
        try {
            if (!UserInfoManager.getInstance(getActivity()).isSignedIn(
                    getActivity())) {
                callAPIService();
                // callAdsService();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        // start async task to get app version codes
        if (AppUtil.checkNetworkConnection(getActivity())) {
            CompanyDataAsyncTask asyncTask = new CompanyDataAsyncTask(
                    getActivity(), 1, getFragmentManager());
            asyncTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR,
                    (Void) null);
        }

        // GetADS();
    }

    private boolean checkPlayServices() {
        GoogleApiAvailability googleApiAvailability = GoogleApiAvailability.getInstance();

        int resultCode = googleApiAvailability.isGooglePlayServicesAvailable(getActivity());

        if (resultCode != ConnectionResult.SUCCESS) {
            if (googleApiAvailability.isUserResolvableError(resultCode)) {
                googleApiAvailability.getErrorDialog(getActivity(),resultCode,
                        PLAY_SERVICES_REQUEST).show();
            } else {
                Toast.makeText(getActivity(),
                        "This device is not supported.", Toast.LENGTH_LONG)
                        .show();
                getActivity().finish();
            }
            return false;
        }
        return true;
    }
    private boolean callAPIService() {

        if (AppUtil.checkNetworkConnection(getActivity())) {

            OffersAsyncTask mPromotionsAsyncTask = new OffersAsyncTask(
                    getActivity(), 1, 100);
            mPromotionsAsyncTask.asyncResponse = HomeFragment.this;
            mPromotionsAsyncTask.executeOnExecutor(
                    AsyncTask.THREAD_POOL_EXECUTOR, (Void) null);

            return true;

        }
        return false;
    }

    private boolean callAdsService() {

        if (AppUtil.checkNetworkConnection(getActivity())) {

            AdsAsyncTask adsAsyncTask = new AdsAsyncTask(getActivity());

            adsAsyncTask.execute();

            return true;

        }
        return false;
    }

    @Override
    public void processFinish(ServiceResponse<ArrayList<BeanOffers>> details) {
        Log.d(TAG, "Process Finished");

        try {

            if (details != null && details.getStatus() == 200 && !details.error) {
                Log.e(TAG, "Promotions download Success "+details.getResponse().size());
                if (details.getResponse().size()>0)
                {
                    badgeView.setText(String.valueOf(details.getResponse().size()));
                    badgeView.show();
                }else
                {
                    badgeView.hide();
                }

                ArrayList<BeanOffers> list = details.getResponse();

                int[] coupons = new int[list.size()];
                StringBuilder newList = new StringBuilder();
                int i = 0;
                for (Iterator<BeanOffers> iterator = list.iterator(); iterator
                        .hasNext();) {
                    BeanOffers beanPromotions = (BeanOffers) iterator.next();

                    coupons[i] = beanPromotions.getId();
                    newList.append(String.valueOf(beanPromotions.getId())
                            .concat("|"));
                    i++;
                }

                // Compare coupon items
                String couponList = SharedPref.getString(getActivity(),
                        SharedPref.COUPON_LIST_KEY, null);

                int[] savedCoupon = new int[0];
                if (couponList != null) {
                    String[] s = couponList.split("\\|");
                    savedCoupon = new int[s.length];
                    for (int j = 0; j < s.length; j++) {
                        savedCoupon[j] = Integer.valueOf(s[j]);
                    }

                    int diffCount = 0;
                    boolean available = true;

                    for (int j = 0; j < coupons.length; j++) {
                        for (int j2 = 0; j2 < savedCoupon.length; j2++) {
                            if (coupons[j] != savedCoupon[j2]) {
                                available = available & true;
                            } else {
                                available = available & false;
                            }
                        }

                        if (available) {
                            diffCount++;
                        }
                    }
					/*if (diffCount > 0) {
						Log.d(TAG, "New coupon avaliable " + diffCount);
						// View badger test

						if (badgeView.isShown()) {
							badgeView.setText(String.valueOf(0));
							badgeView.increment(diffCount);
						} else {
							badgeView.setText(String.valueOf(diffCount));
						}
						badgeView.show();

					}*/

                } else {
                    Log.d(TAG, "No Saved coupons");
                    // saved coupons null
					/*if (coupons != null && coupons.length > 0) {
						badgeView.setText(String.valueOf(coupons.length));
						badgeView.show(true);
					}*/
                }

            } else {
                // BaseHelper.showAlert(getActivity(), "Harry's",
                // details.getError_message());
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    Comparator<NameValuePair> compare = new Comparator<NameValuePair>() {
        @Override
        public int compare(NameValuePair p1, NameValuePair p2) {
            return p1.getName().compareTo(p2.getName());
        }
    };

}
