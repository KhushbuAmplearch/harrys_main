package com.riverview.harrys.objects;

public class BeanLoyaltyCard {

	private int id;
	private String member_number, loyalty_card_number, joined_date,
			expiry_date, mobile_user_id, loyalty_program_id, created_at,
			updated_at, total_points_spent, total_points_earned,
			total_rebates_spent, total_rebates_earned, total_stamps_spent,
			total_stamps_earned;

	private BeanTier tier;

	public BeanLoyaltyCard() {
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getMember_number() {
		return member_number;
	}

	public void setMember_number(String member_number) {
		this.member_number = member_number;
	}

	public String getLoyalty_card_number() {
		return loyalty_card_number;
	}

	public void setLoyalty_card_number(String loyalty_card_number) {
		this.loyalty_card_number = loyalty_card_number;
	}

	public String getJoined_date() {
		return joined_date;
	}

	public void setJoined_date(String joined_date) {
		this.joined_date = joined_date;
	}

	public String getExpiry_date() {
		return expiry_date;
	}

	public void setExpiry_date(String expiry_date) {
		this.expiry_date = expiry_date;
	}

	public String getMobile_user_id() {
		return mobile_user_id;
	}

	public void setMobile_user_id(String mobile_user_id) {
		this.mobile_user_id = mobile_user_id;
	}

	public String getLoyalty_program_id() {
		return loyalty_program_id;
	}

	public void setLoyalty_program_id(String loyalty_program_id) {
		this.loyalty_program_id = loyalty_program_id;
	}

	public String getCreated_at() {
		return created_at;
	}

	public void setCreated_at(String created_at) {
		this.created_at = created_at;
	}

	public String getUpdated_at() {
		return updated_at;
	}

	public void setUpdated_at(String updated_at) {
		this.updated_at = updated_at;
	}

	public String getTotal_points_spent() {
		return total_points_spent;
	}

	public void setTotal_points_spent(String total_points_spent) {
		this.total_points_spent = total_points_spent;
	}

	public String getTotal_points_earned() {
		return total_points_earned;
	}

	public void setTotal_points_earned(String total_points_earned) {
		this.total_points_earned = total_points_earned;
	}

	public String getTotal_rebates_spent() {
		return total_rebates_spent;
	}

	public void setTotal_rebates_spent(String total_rebates_spent) {
		this.total_rebates_spent = total_rebates_spent;
	}

	public String getTotal_rebates_earned() {
		return total_rebates_earned;
	}

	public void setTotal_rebates_earned(String total_rebates_earned) {
		this.total_rebates_earned = total_rebates_earned;
	}

	public String getTotal_stamps_spent() {
		return total_stamps_spent;
	}

	public void setTotal_stamps_spent(String total_stamps_spent) {
		this.total_stamps_spent = total_stamps_spent;
	}

	public String getTotal_stamps_earned() {
		return total_stamps_earned;
	}

	public void setTotal_stamps_earned(String total_stamps_earned) {
		this.total_stamps_earned = total_stamps_earned;
	}

	public BeanTier getTier() {
		return tier;
	}

	public void setTier(BeanTier tier) {
		this.tier = tier;
	}
}
