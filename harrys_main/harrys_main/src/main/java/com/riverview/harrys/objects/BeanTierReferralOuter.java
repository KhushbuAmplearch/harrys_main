package com.riverview.harrys.objects;

public class BeanTierReferralOuter {

	private int id;
	private String tier_id, created_at;

	private BeanTierReferralInner tier;

	public BeanTierReferralOuter() {
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getTier_id() {
		return tier_id;
	}

	public void setTier_id(String tier_id) {
		this.tier_id = tier_id;
	}

	public String getCreated_at() {
		return created_at;
	}

	public void setCreated_at(String created_at) {
		this.created_at = created_at;
	}

	public BeanTierReferralInner getTier() {
		return tier;
	}

	public void setTier(BeanTierReferralInner tier) {
		this.tier = tier;
	}

}
