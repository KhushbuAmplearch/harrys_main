package com.riverview.harrys.helpers;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.IntentService;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.iapps.libs.helpers.BaseHelper;
import com.riverview.harrys.MainActivity;
import com.riverview.harrys.R;
import com.riverview.harrys.constant.AppConst;

/**
 * Created by wiraj on 3/23/15.
 */
public class GcmIntentService extends IntentService {

	private static final String TAG = GcmIntentService.class.getSimpleName();

	public static int NOTIFICATION_ID = 0;
	private NotificationManager mNotificationManager;
	NotificationCompat.Builder builder;

	public GcmIntentService() {
		super(TAG);
	}

	@Override
	protected void onHandleIntent(Intent intent) {
		Bundle extras = intent.getExtras();
		GoogleCloudMessaging gcm = GoogleCloudMessaging.getInstance(this);
		// The getMessageType() intent parameter must be the intent you received
		// in your BroadcastReceiver.
		String messageType = gcm.getMessageType(intent);

		if (!extras.isEmpty()) { // has effect of unparcelling Bundle
			/*
			 * Filter messages based on message type. Since it is likely that
			 * GCM will be extended in the future with new message types, just
			 * ignore any message types you're not interested in, or that you
			 * don't recognize.
			 */
			
			Log.e("test", "MESSAGE ----> " + messageType);
			
			if (GoogleCloudMessaging.MESSAGE_TYPE_SEND_ERROR
					.equals(messageType)) {
				// sendNotification("Send error: " + extras.toString());
			} else if (GoogleCloudMessaging.MESSAGE_TYPE_DELETED
					.equals(messageType)) {
				// sendNotification("Deleted messages on server: " +
				// extras.toString());
				// If it's a regular GCM message, do some work.
			} else if (GoogleCloudMessaging.MESSAGE_TYPE_MESSAGE
					.equals(messageType)) {

				processGCMMessage(extras);
			}
		}
		// Release the wake lock provided by the WakefulBroadcastReceiver.
		GcmBroadcastReceiver.completeWakefulIntent(intent);
	}

	// Put the message into a notification and post it.
	// This is just one simple example of what you might choose to do with
	// a GCM message.
	private void processGCMMessage(Bundle bundle) {

		try {

			if (bundle != null
					&& bundle.getString("message").toString() != null) {

				// get the JSOn object
				JSONObject gcmObject = new JSONObject(bundle.getString("message").toString());
				Log.e(TAG, "MESSAGE ----> " + gcmObject.toString());
				// check weather action type available
				if (!gcmObject.isNull("action_type")) {

					String actionType = gcmObject.getString("action_type");
					Log.d(TAG, "ACTION TYPE ----> " + actionType);

					if (actionType.equalsIgnoreCase(Constants.SUSPEND_TEXT)) {
						// suspend the user
						Utils.suspendUser(getApplicationContext());
					} else {
						// show message as a notification
						generateNotification(bundle);
					}

				} else {
					// show message as a notification
					generateNotification(bundle);
				}

			} else if (bundle != null) {
				// show message as a notification
				generateNotification(bundle);
			}

		} catch (Exception e) {

		}

	}

	private void generateNotification(Bundle bundle) {
		mNotificationManager = (NotificationManager) this
				.getSystemService(Context.NOTIFICATION_SERVICE);

		Intent dismissIntent = new Intent();

		NOTIFICATION_ID = Integer
				.valueOf((!BaseHelper.isEmpty(bundle.getString("id")) ? Integer
						.valueOf(bundle.getString("id")) : 0));
		
		String message = bundle.getString("default").toString();
		String description = null;
		
		try {
			JSONObject gcmObject = new JSONObject(bundle.getString("message").toString());
			if (!gcmObject.isNull("description")) {
				description = gcmObject.getString("description");
			}
			
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		dismissIntent.putExtra(AppConst.NOTIFICATON_ID, NOTIFICATION_ID);
		dismissIntent.setAction(AppConst.NOTIFICATON_BROADCAST);
		PendingIntent piDismiss = PendingIntent.getBroadcast(this, 0,
				dismissIntent, PendingIntent.FLAG_UPDATE_CURRENT);

		PendingIntent i = PendingIntent.getActivity(this, 0, new Intent(this,
				MainActivity.class)
				.addFlags(Intent.FLAG_ACTIVITY_BROUGHT_TO_FRONT
						| Intent.FLAG_ACTIVITY_SINGLE_TOP), 0);

		// note.setLatestEventInfo(this, "Notification Title",
		// "This is the notification message", i);

		NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(
				this)
				.setSmallIcon(R.drawable.ic_icon_harry)
				.setContentTitle("Harry's")
				.setContentText(message)
				.setDefaults(Notification.DEFAULT_ALL)
				.addAction(android.R.drawable.ic_menu_close_clear_cancel, "Dismiss", piDismiss);
		// getString(R.string.rc_harrys_notification)
		NotificationCompat.BigTextStyle inboxStyle = new NotificationCompat.BigTextStyle();

		// Sets a title for the Inbox in expanded layout
		inboxStyle.setBigContentTitle("Harry's");
		
		if(description != null){
			inboxStyle.bigText(description);
		} else{
			inboxStyle.bigText(message);
		}
		
		mBuilder.setContentIntent(i);
		mBuilder.setStyle(inboxStyle);
		mNotificationManager.notify(NOTIFICATION_ID, mBuilder.build());
	}
}
