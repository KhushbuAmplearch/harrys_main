package com.riverview.harrys;

import java.lang.reflect.Field;
import java.util.ArrayList;

import uk.co.chrisjenx.calligraphy.CalligraphyConfig;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

import android.app.ActionBar;
import android.content.Context;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;

import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewConfiguration;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import com.riverview.harrys.helpers.Analytics;
import com.riverview.harrys.helpers.Constants;
import com.riverview.harrys.helpers.FontUtil;
import com.riverview.harrys.helpers.Utils;

public class RCBaseActivity extends AppCompatActivity implements
		ActionBar.OnNavigationListener {

	private static final String TAG = RCBaseActivity.class.getSimpleName();

	/**
	 * The serialization (saved instance state) Bundle key representing the
	 * current dropdown position.
	 */

	private static final String STATE_SELECTED_NAVIGATION_ITEM = "selected_navigation_item";

	//ActionBarHelper mActionBar;
	//ActionBarDrawerToggle mDrawerToggle;
	ActionBar actionBar;
	/*class ActionBarHelper {
		private final ActionBar mActionBar;
		private CharSequence mDrawerTitle;
		private CharSequence mTitle;

		private ActionBarHelper() {
			mActionBar = getActionBar();
		}

		public void init() {
			mActionBar.setDisplayHomeAsUpEnabled(true);
			mActionBar.setHomeButtonEnabled(true);
			mTitle = mDrawerTitle = getTitle();
		}

		*//**
		 * When the drawer is closed we restore the action bar state reflecting
		 * the specific contents in view.
		 *//*
		public void onDrawerClosed() {
			// mActionBar.setTitle(mTitle);
		}

		*//**
		 * When the drawer is open we set the action bar to a generic title. The
		 * action bar should only contain data relevant at the top level of the
		 * nav hierarchy represented by the drawer, as the rest of your content
		 * will be dimmed down and non-interactive.
		 *//*
		public void onDrawerOpened() {
			// mActionBar.setTitle(mDrawerTitle);
		}

		public void setTitle(CharSequence title) {
			mTitle = title;
		}
	}*/

	/**
	 * Create a compatible helper that will manipulate the action bar if
	 * available.
	 */
	/*protected ActionBarHelper createActionBarHelper() {
		return new ActionBarHelper();
	}*/

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		CalligraphyConfig.initDefault(Constants.GLOBAL_FONT_APP);
		try {
			ViewConfiguration config = ViewConfiguration.get(this);
			Field menuKeyField = ViewConfiguration.class
					.getDeclaredField("sHasPermanentMenuKey");
			if (menuKeyField != null) {
				menuKeyField.setAccessible(true);
				menuKeyField.setBoolean(config, false);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	/*public void setHomeActionBar() {

		//mDrawerToggle.setDrawerIndicatorEnabled(true);
		actionBar = getActionBar();

		actionBar.setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
		actionBar.setCustomView(R.layout.custom_action_bar_main);

		getWindow().setFeatureInt(Window.FEATURE_CUSTOM_TITLE, R.layout.custom_action_bar_main);

		int titleId = getResources().getIdentifier("action_bar_title", "id",
				"android");

		// If you're using sherlock, in older versions of android you are not
		// supposed to get a reference to android.R.id.action_bar_title, So
		// here's little hack for that.
		if (titleId == 0) {
			titleId = com.actionbarsherlock.R.id.abs__action_bar_title;
		}

		actionBar.setDisplayShowTitleEnabled(false);
		actionBar.setDisplayHomeAsUpEnabled(true);
		actionBar.setDisplayShowHomeEnabled(true);
		actionBar.setDisplayUseLogoEnabled(false);

		//actionBar.setIcon(new ColorDrawable(getResources().getColor(android.R.color.transparent)));

		//mActionBar = createActionBarHelper();
		//mActionBar.init();
		//mDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout, R.string.drawer_open,R.string.drawer_close);
		//mDrawerToggle.syncState();

	}*/

	public void setBackActionBar(String Title) {/*
		Log.e(TAG, "setBackActionBar(String Title)");
		//mDrawerToggle.setDrawerIndicatorEnabled(false);

		ActionBar actionBar = getActionBar();

		actionBar.setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
		actionBar.setCustomView(R.layout.custom_title_action_bar);

		getWindow().setFeatureInt(Window.FEATURE_CUSTOM_TITLE,
				R.layout.custom_title_action_bar);

		TextView actionBarTitleView = (TextView) actionBar.getCustomView()
				.findViewById(R.id.myTitle);
		actionBarTitleView.setSelected(true);
		// TextView actionBarBackActionView = (TextView)
		// actionBar.getCustomView()
		// .findViewById(R.id.backTitle);

		Utils.setTextViewFontSizeBasedOnScreenDensity(this, actionBarTitleView,
				18f, Typeface.BOLD);

		actionBar.setDisplayShowTitleEnabled(false);
		actionBar.setDisplayHomeAsUpEnabled(true);
		actionBar.setDisplayShowHomeEnabled(true);
		actionBar.setDisplayUseLogoEnabled(false);

		actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_STANDARD);

		actionBar.setIcon(new ColorDrawable(getResources().getColor(
				android.R.color.transparent)));

		int titleId = getResources().getIdentifier("action_bar_title", "id",
				"android");

		// If you're using sherlock, in older versions of android you are not
		// supposed to get a reference to android.R.id.action_bar_title, So
		// here's little hack for that.
		if (titleId == 0) {
			titleId = com.actionbarsherlock.R.id.abs__action_bar_title;
		}

		// TextView actionBarTitleView1 = (TextView) findViewById(titleId);
		// actionBarTitleView1.setText("Harry's");
		new FontUtil(this).setFontRegularType(actionBarTitleView);
		actionBarTitleView.setText(Title);
		// actionBarBackActionView.setText(backActionTitle);
		//mDrawerToggle.syncState();*/
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater = this.getMenuInflater();
		inflater.inflate(R.menu.menu_product, menu);
		return super.onCreateOptionsMenu(menu);
	}

	@Override
	public void onRestoreInstanceState(Bundle savedInstanceState) {
		// Restore the previously serialized current dropdown position.
		if (savedInstanceState.containsKey(STATE_SELECTED_NAVIGATION_ITEM)) {
			try {
				getActionBar().setSelectedNavigationItem(
						savedInstanceState
								.getInt(STATE_SELECTED_NAVIGATION_ITEM));
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	@Override
	protected void onResume() {
		super.onResume();
		try {
			Analytics.getInstance(this).checkAndStartOrNot();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	protected void onDestroy() {
		try {
			Analytics.getInstance(this).stopIT();
		} catch (Exception e) {
			e.printStackTrace();
		}
		super.onDestroy();
	}

	@Override
	protected void onPause() {
		try {
			Analytics.getInstance(this).stopIT();
		} catch (Exception e) {
			e.printStackTrace();
		}
		super.onPause();
	}

	@Override
	public void onSaveInstanceState(Bundle outState) {
		// Serialize the current dropdown position.
		outState.putInt(STATE_SELECTED_NAVIGATION_ITEM, getActionBar()
				.getSelectedNavigationIndex());
	}

	@Override
	protected void attachBaseContext(Context newBase) {
		super.attachBaseContext(new CalligraphyContextWrapper(newBase));

	}

	@Override
	public boolean onNavigationItemSelected(int itemPosition, long itemId) {

		return true;
	}

	private class SpinnerAdapter extends ArrayAdapter<String> {
		Context context;
		ArrayList<String> items = new ArrayList<String>();

		public SpinnerAdapter(final Context context,
							  final int textViewResourceId, ArrayList<String> objects) {
			super(context, textViewResourceId, objects);
			this.items = objects;
			this.context = context;
		}

		@Override
		public View getDropDownView(int position, View convertView,
									ViewGroup parent) {

			if (convertView == null) {
				LayoutInflater inflater = LayoutInflater.from(context);
				convertView = inflater.inflate(
						android.R.layout.simple_spinner_item, parent, false);
			}

			TextView tv = (TextView) convertView
					.findViewById(android.R.id.text1);
			tv.setText(items.get(position));
			try {
				Utils.setTextViewFontSizeBasedOnScreenDensity(
						RCBaseActivity.this, tv);
			} catch (Exception e) {
			}
			return convertView;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			if (convertView == null) {
				LayoutInflater inflater = LayoutInflater.from(context);
				convertView = inflater.inflate(
						android.R.layout.simple_spinner_item, parent, false);
			}

			// android.R.id.text1 is default text view in resource of the
			// android.
			// android.R.layout.simple_spinner_item is default layout in
			// resources of android.

			TextView tv = (TextView) convertView
					.findViewById(android.R.id.text1);
			tv.setText(items.get(position));
			try {
				Utils.setTextViewFontSizeBasedOnScreenDensity(
						RCBaseActivity.this, tv);
			} catch (Exception e) {
			}
			return convertView;
		}
	}

}
