package com.riverview.harrys.objects;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by wiraj on 5/2/15.
 */
public class BeanVoucherInner implements Parcelable {

	private int id;
	private String title, description, image;

	public BeanVoucherInner() {
	}

	public BeanVoucherInner(Parcel parcel) {
		super();

		this.id = parcel.readInt();
		this.title = parcel.readString();
		this.image = parcel.readString();
		this.description = parcel.readString();
	}

	@Override
	public void writeToParcel(Parcel parcel, int i) {
		parcel.writeInt(id);
		parcel.writeString(title);
		parcel.writeString(image);
		parcel.writeString(description);
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}

	@Override
	public int describeContents() {
		return 0;
	}

	public static final Creator<BeanVoucherInner> CREATOR = new Creator<BeanVoucherInner>() {

		@Override
		public BeanVoucherInner createFromParcel(Parcel source) {
			return new BeanVoucherInner(source);
		}

		@Override
		public BeanVoucherInner[] newArray(int size) {
			return new BeanVoucherInner[size];
		}
	};
}
