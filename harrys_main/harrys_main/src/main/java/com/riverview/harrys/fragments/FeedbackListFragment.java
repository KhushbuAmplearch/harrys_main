package com.riverview.harrys.fragments;

import static com.riverview.harrys.constant.AppConst.PULL_TO_REFRESH_TIMEOUT;

import java.util.ArrayList;

import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewStub;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;

import com.iapps.libs.helpers.BaseHelper;
import com.iapps.libs.helpers.BaseHelper.CancelListener;
import com.iapps.libs.helpers.BaseHelper.ConfirmListener;
import com.iapps.libs.views.LoadingCompound;
import com.orangegangsters.github.swipyrefreshlayout.library.SwipyRefreshLayout;
import com.orangegangsters.github.swipyrefreshlayout.library.SwipyRefreshLayout.OnRefreshListener;
import com.orangegangsters.github.swipyrefreshlayout.library.SwipyRefreshLayoutDirection;
import com.riverview.harrys.MainActivity;
import com.riverview.harrys.R;
import com.riverview.harrys.RCGenericFragment;
import com.riverview.harrys.adapter.FeedbackAdapter;
import com.riverview.harrys.asynctask.AsyncResponse;
import com.riverview.harrys.asynctask.FeedbackListAsyncTask;
import com.riverview.harrys.helpers.ObjectSerializer;
import com.riverview.harrys.helpers.SharedPref;
import com.riverview.harrys.objects.BeanFeedback;
import com.riverview.harrys.objects.BeanFeedbackQueries;
import com.riverview.harrys.objects.ServiceResponse;
import com.riverview.harrys.util.AnaylaticsAppender;
import com.riverview.harrys.util.AppUtil;

public class FeedbackListFragment extends RCGenericFragment implements
		OnRefreshListener, AsyncResponse<ServiceResponse<BeanFeedbackQueries>> {

	private static final String TAG = FeedbackListFragment.class
			.getSimpleName();

	public static final int TAG_LIST_STORE = 0;

	private ListView lvStores;
	private LoadingCompound ld;
	private ViewStub emptyView;
	private SwipyRefreshLayout mSwipyRefreshLayout;

	private View v;
	private FeedbackAdapter mAdapter;
	private ArrayList<BeanFeedback> storeList;

	private FeedbackListAsyncTask mNewsTask = null;

	private AnaylaticsAppender mAppender = null;

	private int mobileUserId;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		SharedPref.deleteString(getActivity(), SharedPref.LISTDATA_KEY);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		v = inflater.inflate(R.layout.feedback_list_fragment_layout, container,
				false);

		lvStores= (ListView)v.findViewById(R.id.listViewStore);
		ld= (LoadingCompound)v.findViewById(R.id.ld);
		emptyView= (ViewStub)v.findViewById(R.id.emptyView);
		mSwipyRefreshLayout= (SwipyRefreshLayout)v.findViewById(R.id.swipyrefreshlayout);

		setHasOptionsMenu(false);
		((MainActivity) getActivity()).setBackActionBar(getActivity()
				.getResources().getString(R.string.rc_menu_home_ten),View.INVISIBLE,"");

		return v;
	}

	public void onPrepareOptionsMenu(Menu menu) {
		//getSherlockActivity().getSupportMenuInflater().inflate(R.menu.menu_feedback_list, menu);
	}

	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.rv_menu_feedback_add:
			((MainActivity) getActivity()).setFragment(new FeedbackFragment());

			break;

		default:
			return true;
		}
		return true;
	}

	@Override
	public void onResume() {
		super.onResume();

		try {
			callAPI();
			mAppender = AnaylaticsAppender.getInstance(getActivity());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);

		mSwipyRefreshLayout.setOnRefreshListener(this);
		lvStores.setOnItemClickListener(ListenerClickItem);

		lvStores.setEmptyView(emptyView);

		mobileUserId = SharedPref.getInteger(getActivity(),
				SharedPref.USERID_KEY, 0);
	}

	@SuppressWarnings("unchecked")
	public void callAPI() {

		// get saved json data
		String serilizedJsonString = SharedPref.getString(getActivity(),
				SharedPref.LISTDATA_KEY, null);
		if (serilizedJsonString != null) {
			try {
				storeList = (ArrayList<BeanFeedback>) ObjectSerializer
						.deserialize(serilizedJsonString);

				mAdapter = new FeedbackAdapter(getActivity(), storeList);
				lvStores.setAdapter(mAdapter);
				mAdapter.notifyDataSetChanged();

				ld.hide();

			} catch (Exception e) {
				callAPIService();
				Log.e(FeedbackListFragment.TAG,
						"Error deserilization " + e.getMessage());
			}
		} else {
			callAPIService();
		}
	}

	@Override
	public void onPause() {
		super.onPause();

		if (mNewsTask != null) {
			mNewsTask.cancel(true);
		}
	}

	private boolean callAPIService() {

		if (AppUtil.checkNetworkConnection(getActivity())) {

			mNewsTask = new FeedbackListAsyncTask(getActivity(),
					String.valueOf(mobileUserId));
			mNewsTask.asyncResponse = FeedbackListFragment.this;
			mNewsTask.execute((Void) null);

			ld.bringToFront();
			ld.showLoading();

			return true;

		} else {

			BaseHelper.confirm(getActivity(), "Network Unavaliable",
					"Please turn on network to connect with server",
					new ConfirmListener() {

						@Override
						public void onYes() {
							AppUtil.showSystemSettingsDialog(getActivity());
						}
					}, new CancelListener() {

						@Override
						public void onNo() {
							getHome().backToHomeScreen();

						}
					});
		}
		return false;

	}

	public OnItemClickListener ListenerClickItem = new OnItemClickListener() {

		@Override
		public void onItemClick(AdapterView<?> adapter, View view, int pos,
				long arg3) {

			// Set the data as json in Shared Prefs
			try {
				SharedPref.saveString(getActivity(), SharedPref.LISTDATA_KEY,
						ObjectSerializer.serialize(storeList));
			} catch (Exception e) {
				Log.e(TAG, "Error serilization " + e.getMessage());
			}

			FeedbackDetailsFragment frag = new FeedbackDetailsFragment();
			Bundle bundle = new Bundle();
			bundle.putParcelable("feedback", storeList.get(pos));
			frag.setArguments(bundle);
			getHome().setFragment(frag);

			mAppender.updateAnaylitsData(AppUtil
					.createAnalyticsJsonEventAction("feedback",
							String.valueOf(storeList.get(pos).getId()),
							"click", null));
		}
	};

	View.OnClickListener clickListeners = new View.OnClickListener() {
		public void onClick(View v) {

		}
	};

	@Override
	public void processFinish(ServiceResponse<BeanFeedbackQueries> details) {
		Log.d(TAG, "Process Finished");

		ld.hide();
		mSwipyRefreshLayout.setRefreshing(false);

		if (details != null && details.getStatus() == 200 && !details.error) {
			Log.i(TAG, "News feed download Success ");

			storeList = details.getResponse().getQueries();

			mAdapter = new FeedbackAdapter(getActivity(), storeList);
			lvStores.setAdapter(mAdapter);
			mAdapter.notifyDataSetChanged();

		} else {
//			BaseHelper.showAlert(getActivity(), "Harry's",
//					details.getError_message());
		}
	}

	@Override
	public void onRefresh(SwipyRefreshLayoutDirection direction) {
		if (callAPIService()) {
			new Handler().postDelayed(new Runnable() {
				@Override
				public void run() {
					// Hide the refresh after n sec
					try {
						getActivity().runOnUiThread(new Runnable() {
							@Override
							public void run() {
								mSwipyRefreshLayout.setRefreshing(false);
							}
						});
					} catch (Exception e) {
					}
				}
			}, PULL_TO_REFRESH_TIMEOUT);
		} else {
			mSwipyRefreshLayout.setRefreshing(false);
		}
	}
}
