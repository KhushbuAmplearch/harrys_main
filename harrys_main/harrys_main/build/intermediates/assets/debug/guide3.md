######Top up mWallet with Credit Card

Follow the simple steps for topping up your mWallet with a credit card
1.  Select Top up from your wallet2.  Select top up with credit card fromthe available options3.  Select the amount you want to topup4.  Select your credit card from thecards stored securely on file5.  Enter user name and password associated with your credit card6.  Confirm your top up7.  Done! Your App will display thenew balance