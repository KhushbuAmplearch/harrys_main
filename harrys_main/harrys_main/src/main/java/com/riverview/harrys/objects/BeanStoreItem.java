package com.riverview.harrys.objects;

import java.io.Serializable;

import android.os.Parcel;
import android.os.Parcelable;

public class BeanStoreItem implements Parcelable, Serializable,
		Comparable<BeanStoreItem> {

	private static final long serialVersionUID = -6337584641306819958L;

	public static final String OBJ_NAME = "BeanStoreItem";

	private int id;
	private String name, info, url, address, postelcode, telephome, email,
			openinghours;
	private Double longitude=0.0, latitude=0.0, distance;
	private String photos;

	public BeanStoreItem(int id, String name) {
		super();
		this.id = id;
		this.name = name;

	}

	public BeanStoreItem(Parcel parcel) {
		super();
		this.id = parcel.readInt();
		this.name = parcel.readString();
		this.info = parcel.readString();
		this.distance = parcel.readDouble();
		this.url = parcel.readString();
		this.address = parcel.readString();
		this.postelcode = parcel.readString();
		this.telephome = parcel.readString();
		this.email = parcel.readString();
		this.openinghours = parcel.readString();
		this.longitude = parcel.readDouble();
		this.latitude = parcel.readDouble();
		this.photos = parcel.readString();
	}

	public static final Parcelable.Creator<BeanStoreItem> CREATOR = new Parcelable.Creator<BeanStoreItem>() {

		@Override
		public BeanStoreItem createFromParcel(Parcel source) {
			return new BeanStoreItem(source);
		}

		@Override
		public BeanStoreItem[] newArray(int size) {
			return new BeanStoreItem[size];
		}
	};

	@Override
	public int describeContents() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public void writeToParcel(Parcel out, int flag) {
		out.writeInt(id);
		out.writeString(name);
		out.writeString(info);
		out.writeDouble(distance);
		out.writeString(url);
		out.writeString(address);
		out.writeString(postelcode);
		out.writeString(telephome);
		out.writeString(email);
		out.writeString(openinghours);
		out.writeDouble(longitude);
		out.writeDouble(latitude);
		out.writeString(photos);
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getInfo() {
		return info;
	}

	public void setInfo(String info) {
		this.info = info;
	}

	public Double getDistance() {
		return distance;
	}

	public void setDistance(Double distance) {
		this.distance = distance;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getPostelcode() {
		return postelcode;
	}

	public void setPostelcode(String postelcode) {
		this.postelcode = postelcode;
	}

	public String getTelephome() {
		return telephome;
	}

	public void setTelephome(String telephome) {
		this.telephome = telephome;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getOpeninghours() {
		return openinghours;
	}

	public void setOpeninghours(String openinghours) {
		this.openinghours = openinghours;
	}

	public Double getLongitude() {
		return longitude;
	}

	public void setLongitude(Double longitude) {
		this.longitude = longitude;
	}

	public Double getLatitude() {
		return latitude;
	}

	public void setLatitude(Double latitude) {
		this.latitude = latitude;
	}

	public String getPhotos() {
		return photos;
	}

	public void setPhotos(String photos) {
		this.photos = photos;
	}

	@Override
	public int compareTo(BeanStoreItem another) {

		return Double
				.compare(this.distance, ((BeanStoreItem) another).distance);
	}

}
