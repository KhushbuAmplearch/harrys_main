package com.riverview.harrys.helpers;

import com.iapps.libs.helpers.BaseKeys;

public class Keys extends BaseKeys {

	public static final String NAME = "name";
	public static final String SHARING_DIALOG = "sharing_dialog";
	public static final String DIALOG = "dialog";
	
	public static final String RESPONSE = "response";
	public static final String ACTION = "action";
	public static final String ACTION_ITEM = "action_item";
	public static final String ACTION_URL = "action_url";
	public static final String APPLICATION_ID = "application_id";
	public static final String BUDGET = "budget";
	public static final String CLICK_TO_DATE = "click_to_date";
	public static final String CLICK_TOTAL = "click_total";
	public static final String COMPANY_ID = "company_id";
	public static final String DELETED_AT = "deleted_at";
	public static final String DISPLAY_TIME = "display_time";
	public static final String END_DATE = "end_date";
	public static final String IMAGEURL = "imageurl";
	public static final String IMPRESSIONS_TO_DATE = "impressions_to_date";
	public static final String IMPRESSIONS_TOTAL = "impressions_total";
	public static final String LANG = "lang";
	public static final String MODE = "mode";
	public static final String PRIORITY = "priority";
	public static final String REMAINING_BUDGET = "remaining_budget";
	public static final String START_DATE = "start_date";
	public static final String UPDATED_AT = "updated_at";
	public static final String ZONE_ID = "zone_id";
	public static final String TYPE = "type";
	public static final String AUTH_VERSION = "auth_version";
	public static final String AUTH_KEY = "auth_key";
	public static final String AUTH_TIMESTAMP = "auth_timestamp";
	public static final String AUTH_SIGNATURE = "auth_signature";

}