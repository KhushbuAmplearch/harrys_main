package com.riverview.harrys.adapter;

import java.util.ArrayList;

import android.app.Activity;
import android.content.Context;
import android.graphics.Typeface;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.iapps.libs.helpers.BaseUIHelper;
import com.riverview.harrys.R;
import com.riverview.harrys.helpers.Utils;
import com.riverview.harrys.objects.BeanProductItem;
import com.riverview.harrys.objects.BeanReferralTireVoucherData;

public class ReferralResponseAdapter extends BaseAdapter {

	private static final String TAG = ReferralResponseAdapter.class.getSimpleName();

	private ArrayList<BeanReferralTireVoucherData> mItems = new ArrayList<BeanReferralTireVoucherData>();
	//private ArrayList<BeanProductItem> mFilteredItems = new ArrayList<BeanProductItem>();
	private Activity context;

	public ReferralResponseAdapter(Activity context, ArrayList<BeanReferralTireVoucherData> list) {
		this.context = context;

		this.mItems = list;
	}

	@Override
	public int getCount() {
		try {
			return mItems.size();
		} catch (Exception e) {
			return 0;
		}
	}

	@Override
	public BeanReferralTireVoucherData getItem(int pos) {
		return mItems.get(pos);
	}

	@Override
	public long getItemId(int position) {
		return mItems.get(position).getId();
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ViewHolder holder;

		if (convertView == null) {
			holder = new ViewHolder();
			LayoutInflater inflater = (LayoutInflater) context
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

			convertView = inflater.inflate(R.layout.cell_referral_tier_voucher_item, parent,
					false);
			holder.LLRoot = (LinearLayout) convertView
					.findViewById(R.id.LLRoot);
			holder.infoStore = (TextView) convertView
					.findViewById(R.id.textViewStoreNameAddress);
			holder.imgStore = (ImageView) convertView
					.findViewById(R.id.imgStoreItem);
			holder.title = (TextView) convertView
					.findViewById(R.id.textViewProductTitle);
			convertView.setTag(holder);

		} else {
			holder = (ViewHolder) convertView.getTag();
		}

		// set cell height by screen height ratio 30 15
		Utils.setWidthHeightByPercent(context, holder.imgStore, 0.15f, 0.08f);

		Utils.setTextViewFontSizeBasedOnScreenDensity(context,
				holder.infoStore, 30f);
		Utils.setTextViewFontSizeBasedOnScreenDensity(context, holder.title,
				23f, Typeface.BOLD);

		Utils.setMarginByPercent(context, holder.LLRoot, 0.01f, true);

		BeanReferralTireVoucherData obj = getItem(position);
		holder.infoStore.setText(obj.getDescription());Log.d(TAG, "Des " + obj.getDescription());
		BaseUIHelper.loadImageWithPlaceholder(context, obj.getImage(),
				holder.imgStore, R.drawable.placeholder);
		holder.title.setText(obj.getName());

		return convertView;
	}

	public class ViewHolder {

		TextView infoStore;
		ImageView imgStore;
		LinearLayout LLRoot;
		TextView title;
	}
}
