package com.riverview.harrys.objects;

public class BeanPrice {

	private String id, product_id, price_1, price1_start_date, price1_end_date,
			price_2, price2_start_date, price2_end_date, price_3,
			price3_start_date, price3_end_date, created_at, updated_at,
			deleted_at;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getProduct_id() {
		return product_id;
	}

	public void setProduct_id(String product_id) {
		this.product_id = product_id;
	}

	public String getPrice_1() {
		return price_1;
	}

	public void setPrice_1(String price_1) {
		this.price_1 = price_1;
	}

	public String getPrice1_start_date() {
		return price1_start_date;
	}

	public void setPrice1_start_date(String price1_start_date) {
		this.price1_start_date = price1_start_date;
	}

	public String getPrice1_end_date() {
		return price1_end_date;
	}

	public void setPrice1_end_date(String price1_end_date) {
		this.price1_end_date = price1_end_date;
	}

	public String getPrice_2() {
		return price_2;
	}

	public void setPrice_2(String price_2) {
		this.price_2 = price_2;
	}

	public String getPrice2_start_date() {
		return price2_start_date;
	}

	public void setPrice2_start_date(String price2_start_date) {
		this.price2_start_date = price2_start_date;
	}

	public String getPrice2_end_date() {
		return price2_end_date;
	}

	public void setPrice2_end_date(String price2_end_date) {
		this.price2_end_date = price2_end_date;
	}

	public String getPrice_3() {
		return price_3;
	}

	public void setPrice_3(String price_3) {
		this.price_3 = price_3;
	}

	public String getPrice3_start_date() {
		return price3_start_date;
	}

	public void setPrice3_start_date(String price3_start_date) {
		this.price3_start_date = price3_start_date;
	}

	public String getPrice3_end_date() {
		return price3_end_date;
	}

	public void setPrice3_end_date(String price3_end_date) {
		this.price3_end_date = price3_end_date;
	}

	public String getCreated_at() {
		return created_at;
	}

	public void setCreated_at(String created_at) {
		this.created_at = created_at;
	}

	public String getUpdated_at() {
		return updated_at;
	}

	public void setUpdated_at(String updated_at) {
		this.updated_at = updated_at;
	}

	public String getDeleted_at() {
		return deleted_at;
	}

	public void setDeleted_at(String deleted_at) {
		this.deleted_at = deleted_at;
	}

}
