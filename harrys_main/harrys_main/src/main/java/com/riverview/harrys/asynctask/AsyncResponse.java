package com.riverview.harrys.asynctask;

/**
 * Callback interface
 * 
 * @author Wiraj
 * 
 * @param <T>
 */
public interface AsyncResponse<T> {

	void processFinish(T details);

}
