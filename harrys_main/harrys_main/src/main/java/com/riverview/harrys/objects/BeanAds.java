package com.riverview.harrys.objects;

import java.io.Serializable;

import com.iapps.libs.objects.SimpleBean;

public class BeanAds implements Serializable{
	
	private static final long serialVersionUID = -3310353790781388047L;
	
	private String company_id, application_id, priority, image, start_date, end_date, zone_id, action, action_url,
    action_item, type, mode, click_to_date, click_total, impressions_to_date, impressions_total, display_time,
    budget, remaining_budget, created_at, updated_at, deleted_at, imageurl;
	
	BeanAdZone adszone;

	public String getCompany_id() {
		return company_id;
	}

	public void setCompany_id(String company_id) {
		this.company_id = company_id;
	}

	public String getApplication_id() {
		return application_id;
	}

	public void setApplication_id(String application_id) {
		this.application_id = application_id;
	}

	public String getPriority() {
		return priority;
	}

	public void setPriority(String priority) {
		this.priority = priority;
	}

	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}

	public String getStart_date() {
		return start_date;
	}

	public void setStart_date(String start_date) {
		this.start_date = start_date;
	}

	public String getEnd_date() {
		return end_date;
	}

	public void setEnd_date(String end_date) {
		this.end_date = end_date;
	}

	public String getZone_id() {
		return zone_id;
	}

	public void setZone_id(String zone_id) {
		this.zone_id = zone_id;
	}

	public String getAction() {
		return action;
	}

	public void setAction(String action) {
		this.action = action;
	}

	public String getAction_url() {
		return action_url;
	}

	public void setAction_url(String action_url) {
		this.action_url = action_url;
	}

	public String getAction_item() {
		return action_item;
	}

	public void setAction_item(String action_item) {
		this.action_item = action_item;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getMode() {
		return mode;
	}

	public void setMode(String mode) {
		this.mode = mode;
	}

	public String getClick_to_date() {
		return click_to_date;
	}

	public void setClick_to_date(String click_to_date) {
		this.click_to_date = click_to_date;
	}

	public String getClick_total() {
		return click_total;
	}

	public void setClick_total(String click_total) {
		this.click_total = click_total;
	}

	public String getImpressions_to_date() {
		return impressions_to_date;
	}

	public void setImpressions_to_date(String impressions_to_date) {
		this.impressions_to_date = impressions_to_date;
	}

	public String getImpressions_total() {
		return impressions_total;
	}

	public void setImpressions_total(String impressions_total) {
		this.impressions_total = impressions_total;
	}

	public String getDisplay_time() {
		return display_time;
	}

	public void setDisplay_time(String display_time) {
		this.display_time = display_time;
	}

	public String getBudget() {
		return budget;
	}

	public void setBudget(String budget) {
		this.budget = budget;
	}

	public String getRemaining_budget() {
		return remaining_budget;
	}

	public void setRemaining_budget(String remaining_budget) {
		this.remaining_budget = remaining_budget;
	}

	public String getCreated_at() {
		return created_at;
	}

	public void setCreated_at(String created_at) {
		this.created_at = created_at;
	}

	public String getUpdated_at() {
		return updated_at;
	}

	public void setUpdated_at(String updated_at) {
		this.updated_at = updated_at;
	}

	public String getDeleted_at() {
		return deleted_at;
	}

	public void setDeleted_at(String deleted_at) {
		this.deleted_at = deleted_at;
	}

	public String getImageurl() {
		return imageurl;
	}

	public void setImageurl(String imageurl) {
		this.imageurl = imageurl;
	}

	public BeanAdZone getAdszone() {
		return adszone;
	}

	public void setAdszone(BeanAdZone adszone) {
		this.adszone = adszone;
	}
	
	
	
}