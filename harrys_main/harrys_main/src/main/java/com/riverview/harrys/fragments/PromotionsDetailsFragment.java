package com.riverview.harrys.fragments;

import org.apache.commons.lang3.StringEscapeUtils;
import org.json.JSONException;
import org.json.JSONObject;

import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.LinearLayout;
import com.iapps.libs.helpers.BaseHelper;
import com.iapps.libs.helpers.BaseHelper.ConfirmListener;
import com.iapps.libs.views.LoadingCompound;
import com.riverview.harrys.MainActivity;
import com.riverview.harrys.R;
import com.riverview.harrys.RCGenericFragment;
import com.riverview.harrys.asynctask.AsyncResponse;
import com.riverview.harrys.asynctask.OfferContentAsyncTask;
import com.riverview.harrys.helpers.Utils;
import com.riverview.harrys.objects.BeanPromotionItem;
import com.riverview.harrys.objects.ServiceResponse;
import com.riverview.harrys.util.AnaylaticsAppender;
import com.riverview.harrys.util.AppUtil;

public class PromotionsDetailsFragment extends RCGenericFragment implements
		AsyncResponse<ServiceResponse<String>> {

	private LinearLayout LLBGContainer;
	private WebView offerWebView;
	private LoadingCompound ld;
	private Button buttonRedeem;

	private static final String TAG = PromotionsDetailsFragment.class
			.getSimpleName();


	private View v;

	private BeanPromotionItem promoItem;

	private AnaylaticsAppender mAppender = null;
	private OfferContentAsyncTask mOfferContentAsyncTask = null;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		promoItem = (BeanPromotionItem) getArguments().getParcelable(
				"promotions");
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
							 Bundle savedInstanceState) {
		v = inflater.inflate(R.layout.promotion_detail_fragment_layout,
				container, false);
		LLBGContainer = (LinearLayout)v.findViewById(R.id.LLBGContainer);
		offerWebView = (WebView)v.findViewById(R.id.offerWebView);
		ld=(LoadingCompound)v.findViewById(R.id.ld);
		buttonRedeem = (Button)v.findViewById(R.id.buttonRedeem);
		setHasOptionsMenu(false);
		((MainActivity) getActivity()).setBackActionBar(promoItem.getName(),View.INVISIBLE,"");

		return v;
	}

	@Override
	public void onResume() {
		// TODO Auto-generated method stub
		super.onResume();

		// call async task to get offer content
		callOfferContentAsyncTask(promoItem.getId());

		try {
			mAppender = AnaylaticsAppender.getInstance(getActivity());

			JSONObject attributes = new JSONObject();
			try {
				attributes.put("title", promoItem.getName());
			} catch (JSONException e) {
				e.printStackTrace();
			}

			mAppender.updateAnaylitsData(AppUtil
					.createAnalyticsJsonEventAction("promotion",
							String.valueOf(promoItem.getId()), "view",
							attributes.toString()));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void callOfferContentAsyncTask(int offerId) {
		if (AppUtil.checkNetworkConnection(getActivity())) {

			mOfferContentAsyncTask = new OfferContentAsyncTask(getActivity(),
					String.valueOf(offerId));
			mOfferContentAsyncTask.asyncResponse = PromotionsDetailsFragment.this;
			mOfferContentAsyncTask.execute((Void) null);

		} else {
			BaseHelper.confirm(getActivity(), "Network Unavaliable",
					"Please turn on network to connect with server",
					new ConfirmListener() {

						@Override
						public void onYes() {
							AppUtil.showSystemSettingsDialog(getActivity());
						}
					});
		}

	}

	public void onPrepareOptionsMenu(Menu menu) {
		// getSherlockActivity().getSupportMenuInflater().inflate(
		// R.menu.menu_promotions, menu);
	}

	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);

		Utils.setTextViewFontSizeBasedOnScreenDensity(getActivity(),
				buttonRedeem, Utils.FONT_SMALL_DENSITY_SIZE);

		buttonRedeem.setOnClickListener(clickListeners);
	}

	View.OnClickListener clickListeners = new View.OnClickListener() {
		public void onClick(View v) {

			if (v.getId() == R.id.buttonRedeem) {

				if (AppUtil.checkNetworkConnection(getActivity())) {

					BaseHelper.confirm(getActivity(), "Harry's",
							getString(R.string.rc_redeem_request),
							new ConfirmListener() {

								@Override
								public void onYes() {
									// Redeem Voucher Fragment show
									PromotionRedeemFragment frag = new PromotionRedeemFragment();
									Bundle bundle = new Bundle();
									bundle.putParcelable("promotions",
											promoItem);
									frag.setArguments(bundle);
									getHome().setFragment(frag);
								}
							});

				} else {
					BaseHelper.confirm(getActivity(), "Network Unavaliable",
							"Please turn on network to connect with server",
							new ConfirmListener() {

								@Override
								public void onYes() {
									AppUtil.showSystemSettingsDialog(getActivity());
								}
							});
				}

			}
		}
	};

	@Override
	public void processFinish(ServiceResponse<String> details) {

		ld.hide();

		if (details != null && details.getStatus() == 200 && !details.error) {
			Log.d(TAG, "Offer content downloaded");

			String webContent = StringEscapeUtils.unescapeHtml4(details
					.getResponse());

			offerWebView.getSettings().setUseWideViewPort(false);
			offerWebView.getSettings().setLoadWithOverviewMode(true);
			offerWebView.getSettings().setJavaScriptEnabled(true);

			offerWebView.loadDataWithBaseURL(null, webContent, "text/html",
					"UTF-8", null);

		} else {
			try {

				BaseHelper.showAlert(getActivity(), "Harry's",
						details.getError_message());
			} catch (Exception e) {
				// TODO: handle exception
			}
		}
	}
}
