package com.riverview.harrys.fragments;

import org.apache.commons.lang3.StringEscapeUtils;
import org.json.JSONException;
import org.json.JSONObject;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;

import com.iapps.libs.helpers.BaseHelper;
import com.iapps.libs.helpers.BaseHelper.ConfirmListener;
import com.iapps.libs.views.LoadingCompound;
import com.riverview.harrys.MainActivity;
import com.riverview.harrys.R;
import com.riverview.harrys.RCGenericFragment;
import com.riverview.harrys.asynctask.AsyncResponse;
import com.riverview.harrys.asynctask.NewsContentAsyncTask;
import com.riverview.harrys.asynctask.OfferContentAsyncTask;
import com.riverview.harrys.objects.BeanPromotionItem;
import com.riverview.harrys.objects.ServiceResponse;
import com.riverview.harrys.util.AnaylaticsAppender;
import com.riverview.harrys.util.AppUtil;

public class NewsDetailsFragment extends RCGenericFragment implements
		AsyncResponse<ServiceResponse<String>> {

	private static final String TAG = NewsDetailsFragment.class.getSimpleName();
	
	private WebView newsWebView;
	private LoadingCompound ld;

	private View v;

	private BeanPromotionItem newsItem;

	private AnaylaticsAppender mAppender = null;

	private NewsContentAsyncTask mNewsContentAsyncTask;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		newsItem = (BeanPromotionItem) getArguments().getParcelable("news");
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		v = inflater.inflate(R.layout.news_detail_fragment_layout, container,
				false);

		newsWebView = (WebView)v.findViewById(R.id.newsWebView);
		ld = (LoadingCompound)v.findViewById(R.id.ld);
		setHasOptionsMenu(false);
		((MainActivity) getActivity()).setBackActionBar(newsItem.getName(),View.INVISIBLE,"");

		return v;
	}
	
	private void callNewsContentAsyncTask(int newsId) {
		if (AppUtil.checkNetworkConnection(getActivity())) {

			mNewsContentAsyncTask = new NewsContentAsyncTask(getActivity(),
					String.valueOf(newsId));
			mNewsContentAsyncTask.asyncResponse = NewsDetailsFragment.this;
			mNewsContentAsyncTask.execute((Void) null);

		} else {
			BaseHelper.confirm(getActivity(), "Network Unavaliable",
					"Please turn on network to connect with server",
					new ConfirmListener() {

						@Override
						public void onYes() {
							AppUtil.showSystemSettingsDialog(getActivity());
						}
					});
		}

	}

	@Override
	public void onResume() {
		super.onResume();

		callNewsContentAsyncTask(newsItem.getId());
		
		try {
			mAppender = AnaylaticsAppender.getInstance(getActivity());

			JSONObject attributes = new JSONObject();
			try {
				attributes.put("title", newsItem.getName());
			} catch (JSONException e) {
				e.printStackTrace();
			}

			mAppender.updateAnaylitsData(AppUtil
					.createAnalyticsJsonEventAction("news", newsItem.getName(),
							"view", attributes.toString()));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);

//		newsWebView.getSettings().setJavaScriptEnabled(true);
//		newsWebView.loadData(newsItem.getDescription(), "text/html", "UTF-8");
	}
	
	@Override
	public void processFinish(ServiceResponse<String> details) {

		ld.hide();

		if (details != null && details.getStatus() == 200 && !details.error) {
			Log.d(TAG, "Offer content downloaded");

			String webContent = StringEscapeUtils.unescapeHtml4(details
					.getResponse());

			newsWebView.getSettings().setUseWideViewPort(false);
			newsWebView.getSettings().setLoadWithOverviewMode(true);
			newsWebView.getSettings().setJavaScriptEnabled(true);

			newsWebView.loadDataWithBaseURL(null, webContent, "text/html",
					"UTF-8", null);

		} else {
			try {

				BaseHelper.showAlert(getActivity(), "Harry's",
						details.getError_message());
			} catch (Exception e) {
				// TODO: handle exception
			}
		}
	}

}
