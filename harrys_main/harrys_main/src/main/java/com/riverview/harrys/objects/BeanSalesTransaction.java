package com.riverview.harrys.objects;

public class BeanSalesTransaction {

	private int id, reversed, company_id, application_id, loyalty_program_id,
			loyalty_user_id, mobile_user_id, outlet_id;
	private String type, discounted_total, invoice_total, total_stamps_spent,
			total_stamps_earned, total_points_spent, total_points_earned,
			transaction_created_by, transaction_created_at,
			promotional_rules_applied, currency, reference, deleted_at,
			created_at, updated_at, skip_loyalty, current_points,
			current_stamps, current_rebates, previous_points, previous_stamps,
			previous_rebates, current_mobile_balance, previous_mobile_balance;

	private String outlet_name;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getReversed() {
		return reversed;
	}

	public void setReversed(int reversed) {
		this.reversed = reversed;
	}

	public int getCompany_id() {
		return company_id;
	}

	public void setCompany_id(int company_id) {
		this.company_id = company_id;
	}

	public int getApplication_id() {
		return application_id;
	}

	public void setApplication_id(int application_id) {
		this.application_id = application_id;
	}

	public int getLoyalty_program_id() {
		return loyalty_program_id;
	}

	public void setLoyalty_program_id(int loyalty_program_id) {
		this.loyalty_program_id = loyalty_program_id;
	}

	public int getLoyalty_user_id() {
		return loyalty_user_id;
	}

	public void setLoyalty_user_id(int loyalty_user_id) {
		this.loyalty_user_id = loyalty_user_id;
	}

	public int getMobile_user_id() {
		return mobile_user_id;
	}

	public void setMobile_user_id(int mobile_user_id) {
		this.mobile_user_id = mobile_user_id;
	}

	public int getOutlet_id() {
		return outlet_id;
	}

	public void setOutlet_id(int outlet_id) {
		this.outlet_id = outlet_id;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getDiscounted_total() {
		return discounted_total;
	}

	public void setDiscounted_total(String discounted_total) {
		this.discounted_total = discounted_total;
	}

	public String getInvoice_total() {
		return invoice_total;
	}

	public void setInvoice_total(String invoice_total) {
		this.invoice_total = invoice_total;
	}

	public String getTotal_stamps_spent() {
		return total_stamps_spent;
	}

	public void setTotal_stamps_spent(String total_stamps_spent) {
		this.total_stamps_spent = total_stamps_spent;
	}

	public String getTotal_stamps_earned() {
		return total_stamps_earned;
	}

	public void setTotal_stamps_earned(String total_stamps_earned) {
		this.total_stamps_earned = total_stamps_earned;
	}

	public String getTotal_points_spent() {
		return total_points_spent;
	}

	public void setTotal_points_spent(String total_points_spent) {
		this.total_points_spent = total_points_spent;
	}

	public String getTotal_points_earned() {
		return total_points_earned;
	}

	public void setTotal_points_earned(String total_points_earned) {
		this.total_points_earned = total_points_earned;
	}

	public String getTransaction_created_by() {
		return transaction_created_by;
	}

	public void setTransaction_created_by(String transaction_created_by) {
		this.transaction_created_by = transaction_created_by;
	}

	public String getTransaction_created_at() {
		return transaction_created_at;
	}

	public void setTransaction_created_at(String transaction_created_at) {
		this.transaction_created_at = transaction_created_at;
	}

	public String getPromotional_rules_applied() {
		return promotional_rules_applied;
	}

	public void setPromotional_rules_applied(String promotional_rules_applied) {
		this.promotional_rules_applied = promotional_rules_applied;
	}

	public String getCurrency() {
		return currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}

	public String getReference() {
		return reference;
	}

	public void setReference(String reference) {
		this.reference = reference;
	}

	public String getDeleted_at() {
		return deleted_at;
	}

	public void setDeleted_at(String deleted_at) {
		this.deleted_at = deleted_at;
	}

	public String getCreated_at() {
		return created_at;
	}

	public void setCreated_at(String created_at) {
		this.created_at = created_at;
	}

	public String getUpdated_at() {
		return updated_at;
	}

	public void setUpdated_at(String updated_at) {
		this.updated_at = updated_at;
	}

	public String getSkip_loyalty() {
		return skip_loyalty;
	}

	public void setSkip_loyalty(String skip_loyalty) {
		this.skip_loyalty = skip_loyalty;
	}

	public String getCurrent_points() {
		return current_points;
	}

	public void setCurrent_points(String current_points) {
		this.current_points = current_points;
	}

	public String getCurrent_stamps() {
		return current_stamps;
	}

	public void setCurrent_stamps(String current_stamps) {
		this.current_stamps = current_stamps;
	}

	public String getCurrent_rebates() {
		return current_rebates;
	}

	public void setCurrent_rebates(String current_rebates) {
		this.current_rebates = current_rebates;
	}

	public String getPrevious_points() {
		return previous_points;
	}

	public void setPrevious_points(String previous_points) {
		this.previous_points = previous_points;
	}

	public String getPrevious_stamps() {
		return previous_stamps;
	}

	public void setPrevious_stamps(String previous_stamps) {
		this.previous_stamps = previous_stamps;
	}

	public String getPrevious_rebates() {
		return previous_rebates;
	}

	public void setPrevious_rebates(String previous_rebates) {
		this.previous_rebates = previous_rebates;
	}

	public String getCurrent_mobile_balance() {
		return current_mobile_balance;
	}

	public void setCurrent_mobile_balance(String current_mobile_balance) {
		this.current_mobile_balance = current_mobile_balance;
	}

	public String getPrevious_mobile_balance() {
		return previous_mobile_balance;
	}

	public void setPrevious_mobile_balance(String previous_mobile_balance) {
		this.previous_mobile_balance = previous_mobile_balance;
	}

	public String getOutlet_name() {
		return outlet_name;
	}

	public void setOutlet_name(String outlet_name) {
		this.outlet_name = outlet_name;
	}
}
