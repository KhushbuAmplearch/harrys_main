package com.riverview.harrys.adapter;

import java.util.ArrayList;

import org.apache.commons.collections4.map.HashedMap;
import org.jraf.android.backport.switchwidget.Switch;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.riverview.harrys.R;
import com.riverview.harrys.helpers.Utils;
import com.riverview.harrys.objects.BeanPreference;
import com.riverview.harrys.objects.SwitchPosition;

public class PreferencesAdapter extends BaseAdapter {

	private static final String TAG = PreferencesAdapter.class.getSimpleName();

	private ArrayList<BeanPreference> mItems = new ArrayList<BeanPreference>();
	private Activity context;

	private HashedMap<Integer, SwitchPosition> sp = null;

	private final String SWITCH_DES = "preferences[%s]";

	public PreferencesAdapter(Activity context, ArrayList<BeanPreference> list) {
		this.context = context;
		this.mItems = list;

		try {
			sp = new HashedMap<>(list.size());
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	@Override
	public int getCount() {
		try {
			return mItems.size();
		} catch (Exception e) {
			return 0;
		}
	}

	@Override
	public BeanPreference getItem(int pos) {
		return mItems.get(pos);
	}

	@Override
	public long getItemId(int position) {
		return mItems.get(position).getId();
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ViewHolder holder;

		if (convertView == null) {
			holder = new ViewHolder();
			LayoutInflater inflater = (LayoutInflater) context
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

			convertView = inflater.inflate(R.layout.cell_pref_item, parent,
					false);
			holder.LLRoot = (LinearLayout) convertView
					.findViewById(R.id.LLRoot);
			holder.infoPref = (TextView) convertView
					.findViewById(R.id.textPrefTitle);
			holder.prefSwitch = (Switch) convertView
					.findViewById(R.id.preferenceValue);

			convertView.setTag(holder);

		} else {
			holder = (ViewHolder) convertView.getTag();
		}

		Utils.setTextViewFontSizeBasedOnScreenDensity(context, holder.infoPref,
				23f);

		Utils.setMarginByPercent(context, holder.LLRoot, 0.02f, true);

		BeanPreference obj = getItem(position);

		holder.infoPref.setText(obj.getOption_title());
		holder.prefSwitch.setChecked(obj.getValue());

		obj.setPosition(position);
		// holder.prefSwitch.setTag(new Integer(position));
		holder.prefSwitch.setTag(obj);

		try {
			sp.put(position,
					new SwitchPosition(obj.getId(), String.format(SWITCH_DES,
							obj.getId()), (holder.prefSwitch.isChecked()) ? 1
							: 0));
		} catch (Exception e) {
			// TODO: handle exception
		}

		holder.prefSwitch
				.setOnCheckedChangeListener(new OnCheckedChangeListener() {

					@Override
					public void onCheckedChanged(CompoundButton buttonView,
							boolean isChecked) {
						// int position = (int) buttonView.getTag();
						BeanPreference beanPref = (BeanPreference) buttonView
								.getTag();
						sp.put(beanPref.getPosition(),
								new SwitchPosition(beanPref.getId(), String
										.format(SWITCH_DES, beanPref.getId()),
										(isChecked) ? 1 : 0));
					}
				});

		return convertView;
	}

	public HashedMap<Integer, SwitchPosition> getSp() {
		return sp;
	}

	public class ViewHolder {

		TextView infoPref;
		Switch prefSwitch;
		LinearLayout LLRoot;
	}
}
