package com.riverview.harrys.fragments;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.model.LatLng;
import com.riverview.harrys.MainActivity;
import com.riverview.harrys.R;
import com.riverview.harrys.RCGenericDialogFragment;

public class MenuCatagoryFragment extends RCGenericDialogFragment {


	static final LatLng RIVERVIEW = new LatLng(53.558, 9.927);
	static final LatLng YOU = new LatLng(53.551, 9.993);
	private GoogleMap map;
	MapView m;
	private View v;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		v = inflater.inflate(R.layout.product_catagory_dropdown_layout,
				container, false);
		setHasOptionsMenu(false);
		((MainActivity) getActivity()).setBackActionBar("title",View.INVISIBLE,"");

		return v;
	}

	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);

		// LLMapDirection.setOnClickListener(clickListeners);
		//
		// Utils.setTextViewFontSizeBasedOnScreenDensity(getActivity(),
		// textViewMapInfo, Utils.FONT_SMALL_DENSITY_SIZE);

	}

	View.OnClickListener clickListeners = new View.OnClickListener() {
		public void onClick(View v) {

			if (v.getId() == R.id.LLMapDirection) {
				Intent intent = new Intent(
						android.content.Intent.ACTION_VIEW,
						Uri.parse("http://maps.google.com/maps?saddr=20.344,34.34&daddr=20.5666,45.345"));
				startActivity(intent);
			}
		}
	};

}
