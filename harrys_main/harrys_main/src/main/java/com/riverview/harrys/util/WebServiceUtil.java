package com.riverview.harrys.util;

import static com.riverview.harrys.constant.AppConst.CONNECTION_TIMEOUT;
import static com.riverview.harrys.constant.AppConst.DATA_TIMEOUT;

import org.codehaus.jackson.annotate.JsonAutoDetect;
import org.codehaus.jackson.map.DeserializationConfig;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.introspect.VisibilityChecker;
import org.springframework.http.client.ClientHttpRequestFactory;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.http.converter.FormHttpMessageConverter;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.http.converter.json.MappingJacksonHttpMessageConverter;
import org.springframework.web.client.RestTemplate;

/**
 * @author wiraj
 * 
 */
public class WebServiceUtil {

	private static final String TAG = WebServiceUtil.class.getSimpleName();

	private static RestTemplate mRestTemplate = null;
	private static ObjectMapper mObjectMapper = null;

	@SuppressWarnings("deprecation")
	public static RestTemplate getRestTemplateInstance() {

		if (mRestTemplate == null) {
			mRestTemplate = new RestTemplate(clientHttpRequestFactory());
		}
		MappingJacksonHttpMessageConverter jsonConverter = new MappingJacksonHttpMessageConverter();
		jsonConverter
				.getObjectMapper()
				.configure(
						DeserializationConfig.Feature.FAIL_ON_UNKNOWN_PROPERTIES,
						false);
		jsonConverter
				.getObjectMapper()
				.configure(
						DeserializationConfig.Feature.ACCEPT_EMPTY_STRING_AS_NULL_OBJECT,
						true);

		mRestTemplate.getMessageConverters().add(jsonConverter);
		mRestTemplate.getMessageConverters()
				.add(new FormHttpMessageConverter());
		mRestTemplate.getMessageConverters().add(
				new StringHttpMessageConverter());
		return mRestTemplate;
	}

	public static ObjectMapper getErrorObjMapper() {
		if (mObjectMapper == null) {
			mObjectMapper = new ObjectMapper();

			mObjectMapper.configure(
					DeserializationConfig.Feature.FAIL_ON_UNKNOWN_PROPERTIES,
					false);
			mObjectMapper
					.configure(
							DeserializationConfig.Feature.ACCEPT_EMPTY_STRING_AS_NULL_OBJECT,
							true);
			mObjectMapper.setVisibilityChecker(VisibilityChecker.Std
					.defaultInstance().withFieldVisibility(
							JsonAutoDetect.Visibility.ANY));
		}
		return mObjectMapper;
	}

	private static ClientHttpRequestFactory clientHttpRequestFactory() {
		HttpComponentsClientHttpRequestFactory factory = new HttpComponentsClientHttpRequestFactory();
		factory.setReadTimeout(DATA_TIMEOUT);
		factory.setConnectTimeout(CONNECTION_TIMEOUT);
		return factory;
	}
}
