package com.riverview.harrys.fragments;

import java.util.ArrayList;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;


import com.riverview.harrys.MainActivity;
import com.riverview.harrys.R;
import com.riverview.harrys.RCGenericFragment;
import com.riverview.harrys.adapter.UserGuideAdapter;
import com.riverview.harrys.objects.BeanSlideMenu;

public class UserGuideFragment extends RCGenericFragment {

	private ListView lv;

	private View v;
	private ArrayList<BeanSlideMenu> mMenu;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
							 Bundle savedInstanceState) {
		v = inflater.inflate(R.layout.userguide_fragment_layout, container,
				false);

		lv = (ListView)v.findViewById(R.id.lv);
		setHasOptionsMenu(false);
		((MainActivity) getActivity()).setBackActionBar(getActivity()
				.getResources().getString(R.string.rc_menu_user_guide),View.INVISIBLE,"");

		return v;
	}

	public void onPrepareOptionsMenu(Menu menu) {
		// getSherlockActivity().getSupportMenuInflater().inflate(
		// R.menu.menu_userguide, menu);
	}

	public void populateSettingMenuItem() {
		mMenu = new ArrayList<BeanSlideMenu>();
		UserGuideAdapter mAdapter = new UserGuideAdapter(getActivity(), mMenu);

		mMenu.add(new BeanSlideMenu(0, getActivity().getResources().getString(
				R.string.rc_guide1), R.drawable.profile_icon, true));
		mMenu.add(new BeanSlideMenu(1, getActivity().getResources().getString(
				R.string.rc_guide2), R.drawable.preferences_icon, true));
		mMenu.add(new BeanSlideMenu(2, getActivity().getResources().getString(
				R.string.rc_guide3), R.drawable.privacy_icon, true));
		mMenu.add(new BeanSlideMenu(3, getActivity().getResources().getString(
				R.string.rc_guide4), R.drawable.privacy_icon, true));
		mMenu.add(new BeanSlideMenu(4, getActivity().getResources().getString(
				R.string.rc_guide5), R.drawable.tnc_icon, true));
		mMenu.add(new BeanSlideMenu(5, getActivity().getResources().getString(
				R.string.rc_guide6), R.drawable.profile_icon, true));
		// mMenu.add(new BeanSlideMenu(5,
		// getActivity().getResources().getString(
		// R.string.rc_guide7), R.drawable.profile_icon, true));

		lv.setAdapter(mAdapter);
		lv.setOnItemClickListener(ListenerClickItem);
		mAdapter.notifyDataSetChanged();
		lv.invalidate();
		lv.invalidateViews();
	}

	public OnItemClickListener ListenerClickItem = new OnItemClickListener() {

		@Override
		public void onItemClick(AdapterView<?> adapter, View view, int pos,
								long arg3) {

			if (pos == 0) {
				UserGuideDetailFragment wf = new UserGuideDetailFragment();
				Bundle bundle = new Bundle();
				bundle.putString(
						"title",
						getHome().getResources().getString(
								R.string.rc_menu_user_guide));
				bundle.putString("url", "file:///android_asset/guide1.html");
				wf.setArguments(bundle);
				getHome().setFragment(wf);
			} else if (pos == 1) {
				UserGuideDetailFragment wf = new UserGuideDetailFragment();
				Bundle bundle = new Bundle();
				bundle.putString(
						"title",
						getHome().getResources().getString(
								R.string.rc_menu_user_guide));
				bundle.putString("url", "file:///android_asset/guide2.html");
				wf.setArguments(bundle);
				getHome().setFragment(wf);
			} else if (pos == 2) {
				UserGuideDetailFragment wf = new UserGuideDetailFragment();
				Bundle bundle = new Bundle();
				bundle.putString(
						"title",
						getHome().getResources().getString(
								R.string.rc_menu_user_guide));
				bundle.putString("url", "file:///android_asset/guide3.html");
				wf.setArguments(bundle);
				getHome().setFragment(wf);
			} else if (pos == 3) {
				UserGuideDetailFragment wf = new UserGuideDetailFragment();
				Bundle bundle = new Bundle();
				bundle.putString(
						"title",
						getHome().getResources().getString(
								R.string.rc_menu_user_guide));
				bundle.putString("url", "file:///android_asset/guide4.html");
				wf.setArguments(bundle);
				getHome().setFragment(wf);
			} else if (pos == 4) {
				UserGuideDetailFragment wf = new UserGuideDetailFragment();
				Bundle bundle = new Bundle();
				bundle.putString(
						"title",
						getHome().getResources().getString(
								R.string.rc_menu_user_guide));
				bundle.putString("url", "file:///android_asset/guide5.html");
				wf.setArguments(bundle);
				getHome().setFragment(wf);
			} else if (pos == 5) {
				// getHome().setFragment(new ReportProblemFragment());

				UserGuideDetailFragment wf = new UserGuideDetailFragment();
				Bundle bundle = new Bundle();
				bundle.putString(
						"title",
						getHome().getResources().getString(
								R.string.rc_menu_user_guide));
				bundle.putString("url", "file:///android_asset/guide6.html");
				wf.setArguments(bundle);
				getHome().setFragment(wf);

			}
			// else if (pos == 6) {
			// getHome().setFragment(new ReportProblemFragment());
			// }
		}
	};

	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);
		populateSettingMenuItem();
		// tvLoginInfo.setGravity(Gravity.CENTER);
		// tvButtonLogin.setGravity(Gravity.CENTER);

		// new FontUtil(getActivity()).setFontBoldType(editTextUsername);
		// new FontUtil(getActivity()).setFontBoldType(textViewUsernameInfo);
		//
		// Utils.procTextsizeBasedScreen(getActivity(),editTextUsername, 5f);
		// LLBtnCancel.setOnClickListener(clickListeners);
		// textViewNext.setOnClickListener(clickListeners);
	}

	View.OnClickListener clickListeners = new View.OnClickListener() {
		public void onClick(View v) {

		}
	};

}
