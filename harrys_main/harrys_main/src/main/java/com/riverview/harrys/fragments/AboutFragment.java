package com.riverview.harrys.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.riverview.harrys.MainActivity;
import com.riverview.harrys.R;
import com.riverview.harrys.RCGenericFragment;
import com.riverview.harrys.helpers.Utils;

public class AboutFragment extends RCGenericFragment {

	private static final String TAG = AboutFragment.class.getSimpleName();

	private TextView textDeveloperDetails;

	private View v;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setHasOptionsMenu(false);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		v = inflater.inflate(R.layout.about_fragment_layout, container, false);

		textDeveloperDetails = (TextView)v.findViewById(R.id.developerDetails);
		((MainActivity) getActivity()).setBackActionBar(getActivity()
				.getResources().getString(R.string.rc_menu_about),View.INVISIBLE,"");

		return v;
	}

	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);

		// mPullToRefresh = getPullToRefresh(view, R.id.listViewStore, this);
		// callAPI(TAG_LIST_STORE);
		Utils.setTextViewFontSizeBasedOnScreenDensity(getActivity(),
				textDeveloperDetails, 23f);
	}

	View.OnClickListener clickListeners = new View.OnClickListener() {
		public void onClick(View v) {

		}
	};
}
