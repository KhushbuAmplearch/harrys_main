package com.riverview.harrys.dialogfragment;

import android.content.DialogInterface;
import android.graphics.Typeface;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.riverview.harrys.MainActivity;
import com.riverview.harrys.R;
import com.riverview.harrys.RCGenericDialogFragment;
import com.riverview.harrys.fragments.AddClubCardFragment;
import com.riverview.harrys.fragments.HomeFragment;
import com.riverview.harrys.helpers.Utils;

public class SignUpSuccessDialog extends RCGenericDialogFragment {

	private Button btnYes;
	private Button btnNo;
	private TextView tv1;
	private TextView tv2;

	private View v;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		v = inflater.inflate(R.layout.dialog_signup_success_fragment_layout,
				container, false);
		setHasOptionsMenu(false);
		btnYes = (Button)v.findViewById (R.id.btnYes);
		btnNo = (Button)v.findViewById (R.id.btnNo);
		tv1 = (TextView) v.findViewById (R.id.tv1);
		tv2 = (TextView) v.findViewById (R.id.tv2);

		return v;
	}

	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);

		Utils.setTextViewFontSizeBasedOnScreenDensity(getActivity(), tv1, 20f,
				Typeface.BOLD);
		Utils.setTextViewFontSizeBasedOnScreenDensity(getActivity(), tv2, 23f);

		Utils.setButtonFontSizeBasedOnScreenDensity(getActivity(), btnYes,
				Utils.FONT_SMALL_DENSITY_SIZE);
		Utils.setButtonFontSizeBasedOnScreenDensity(getActivity(), btnNo,
				Utils.FONT_SMALL_DENSITY_SIZE);
		btnYes.setOnClickListener(clickListeners);
		btnNo.setOnClickListener(clickListeners);
	}

	View.OnClickListener clickListeners = new View.OnClickListener() {
		public void onClick(View v) {

			switch (v.getId()) {

			case R.id.btnYes:
				getHome().setFragmentClearStack(new HomeFragment());
				getHome().setFragment(new AddClubCardFragment());
				break;

			case R.id.btnNo:
				getHome().setFragmentClearStack(new HomeFragment());
				break;

			default:
				break;
			}
		}

	};

	@Override
	public void onDismiss(DialogInterface dialog) {
		super.onDismiss(dialog);
		Log.d("TAG", "onDismiss");
		try {
			((MainActivity) getActivity()).backToHomeScreen();
		} catch (Exception e) {
		}
	}

}
