package com.riverview.harrys.objects;

import java.util.ArrayList;

public class BeanPromotion {

	private int id;
	private String company_id, application_id, merchant_id, title, description,
			start_date, end_date, tnc, type, has_coupon, coupon_title,
			coupon_description, valid_start_date, valid_end_date,
			valid_start_time, valid_end_time, valid_days, inventory_global,
			inventory_daily, redemption_type, inventory_per_user, prefix,
			amount, viewed, redeemed, created_at, updated_at, deleted_at, lang,
			audience_filter_id, audience_delivery_method, fulfilment_channel,
			priority, test_device_uuid, status, redemption_method;

	private String audience_redeem_expire, roi;

	private BeanImage image;
	private BeanMerchant merchant;
	private ArrayList<BeanOutlet> outlets;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getCompany_id() {
		return company_id;
	}

	public void setCompany_id(String company_id) {
		this.company_id = company_id;
	}

	public String getApplication_id() {
		return application_id;
	}

	public void setApplication_id(String application_id) {
		this.application_id = application_id;
	}

	public String getMerchant_id() {
		return merchant_id;
	}

	public void setMerchant_id(String merchant_id) {
		this.merchant_id = merchant_id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getStart_date() {
		return start_date;
	}

	public void setStart_date(String start_date) {
		this.start_date = start_date;
	}

	public String getEnd_date() {
		return end_date;
	}

	public void setEnd_date(String end_date) {
		this.end_date = end_date;
	}

	public String getTnc() {
		return tnc;
	}

	public void setTnc(String tnc) {
		this.tnc = tnc;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getHas_coupon() {
		return has_coupon;
	}

	public void setHas_coupon(String has_coupon) {
		this.has_coupon = has_coupon;
	}

	public String getCoupon_title() {
		return coupon_title;
	}

	public void setCoupon_title(String coupon_title) {
		this.coupon_title = coupon_title;
	}

	public String getCoupon_description() {
		return coupon_description;
	}

	public void setCoupon_description(String coupon_description) {
		this.coupon_description = coupon_description;
	}

	public String getValid_start_date() {
		return valid_start_date;
	}

	public void setValid_start_date(String valid_start_date) {
		this.valid_start_date = valid_start_date;
	}

	public String getValid_end_date() {
		return valid_end_date;
	}

	public void setValid_end_date(String valid_end_date) {
		this.valid_end_date = valid_end_date;
	}

	public String getValid_start_time() {
		return valid_start_time;
	}

	public void setValid_start_time(String valid_start_time) {
		this.valid_start_time = valid_start_time;
	}

	public String getValid_end_time() {
		return valid_end_time;
	}

	public void setValid_end_time(String valid_end_time) {
		this.valid_end_time = valid_end_time;
	}

	public String getValid_days() {
		return valid_days;
	}

	public void setValid_days(String valid_days) {
		this.valid_days = valid_days;
	}

	public String getInventory_global() {
		return inventory_global;
	}

	public void setInventory_global(String inventory_global) {
		this.inventory_global = inventory_global;
	}

	public String getInventory_daily() {
		return inventory_daily;
	}

	public void setInventory_daily(String inventory_daily) {
		this.inventory_daily = inventory_daily;
	}

	public String getRedemption_type() {
		return redemption_type;
	}

	public void setRedemption_type(String redemption_type) {
		this.redemption_type = redemption_type;
	}

	public String getInventory_per_user() {
		return inventory_per_user;
	}

	public void setInventory_per_user(String inventory_per_user) {
		this.inventory_per_user = inventory_per_user;
	}

	public String getPrefix() {
		return prefix;
	}

	public void setPrefix(String prefix) {
		this.prefix = prefix;
	}

	public String getAmount() {
		return amount;
	}

	public void setAmount(String amount) {
		this.amount = amount;
	}

	public String getViewed() {
		return viewed;
	}

	public void setViewed(String viewed) {
		this.viewed = viewed;
	}

	public String getRedeemed() {
		return redeemed;
	}

	public void setRedeemed(String redeemed) {
		this.redeemed = redeemed;
	}

	public String getCreated_at() {
		return created_at;
	}

	public void setCreated_at(String created_at) {
		this.created_at = created_at;
	}

	public String getUpdated_at() {
		return updated_at;
	}

	public void setUpdated_at(String updated_at) {
		this.updated_at = updated_at;
	}

	public String getDeleted_at() {
		return deleted_at;
	}

	public void setDeleted_at(String deleted_at) {
		this.deleted_at = deleted_at;
	}

	public String getLang() {
		return lang;
	}

	public void setLang(String lang) {
		this.lang = lang;
	}

	public String getAudience_filter_id() {
		return audience_filter_id;
	}

	public void setAudience_filter_id(String audience_filter_id) {
		this.audience_filter_id = audience_filter_id;
	}

	public String getAudience_delivery_method() {
		return audience_delivery_method;
	}

	public void setAudience_delivery_method(String audience_delivery_method) {
		this.audience_delivery_method = audience_delivery_method;
	}

	public String getFulfilment_channel() {
		return fulfilment_channel;
	}

	public void setFulfilment_channel(String fulfilment_channel) {
		this.fulfilment_channel = fulfilment_channel;
	}

	public String getPriority() {
		return priority;
	}

	public void setPriority(String priority) {
		this.priority = priority;
	}

	public String getTest_device_uuid() {
		return test_device_uuid;
	}

	public void setTest_device_uuid(String test_device_uuid) {
		this.test_device_uuid = test_device_uuid;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getRedemption_method() {
		return redemption_method;
	}

	public void setRedemption_method(String redemption_method) {
		this.redemption_method = redemption_method;
	}

	public BeanImage getImage() {
		return image;
	}

	public void setImage(BeanImage image) {
		this.image = image;
	}

	public BeanMerchant getMerchant() {
		return merchant;
	}

	public void setMerchant(BeanMerchant merchant) {
		this.merchant = merchant;
	}

	public ArrayList<BeanOutlet> getOutlets() {
		return outlets;
	}

	public void setOutlets(ArrayList<BeanOutlet> outlets) {
		this.outlets = outlets;
	}

	public String getAudience_redeem_expire() {
		return audience_redeem_expire;
	}

	public void setAudience_redeem_expire(String audience_redeem_expire) {
		this.audience_redeem_expire = audience_redeem_expire;
	}

	public String getRoi() {
		return roi;
	}

	public void setRoi(String roi) {
		this.roi = roi;
	}

}
