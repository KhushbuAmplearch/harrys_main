package com.riverview.harrys.helpers;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;

public class SharedPref {

	public static final String FILE_KEY = "com.riverview.cafe.file.key";

	public static final String USERNAME_KEY = "com.riverview.cafe.username.key";

	public static final String USERFNAME_KEY = "com.riverview.cafe.userfname.key";
	public static final String USERLNAME_KEY = "com.riverview.cafe.userlname.key";
	public static final String USERID_KEY = "com.riverview.cafe.userid.key";
	public static final String APPID_KEY = "com.riverview.cafe.appid.key";
	public static final String COMPANY_ID_KEY = "com.riverview.cafe.companyid.key";
	public static final String MOBILE_NO_KEY = "com.riverview.cafe.mobile.key";
	public static final String EMAIL_KEY = "com.riverview.cafe.email.key";
	public static final String VERIFY_KEY = "com.riverview.cafe.verified.key";
	public static final String REMEMBER_ME_KEY = "com.riverview.cafe.remember.key";

	public static final String VENUE_ID_KEY = "com.iapps.gopanel.venue_id.key";
	public static final String VENUE_NAME_KEY = "com.iapps.gopanel.venue_name.key";
	public static final String ACCES_TOKEN_KEY = "com.iapps.gopanel.access_token.key";

	public static final String FIRST_TIMEAPPSTART_KEY = "com.iapps.gopanel.startup.key";
	public static final String DEFAULT_VENUE_ID = "701";
	public static final String DEFAULT_VENUE_NAME = "UNKNOWN";

	public static final String LISTDATA_KEY = "com.riverview.cafe.listdata.key";
	public static final String PROD_LISTDATA_KEY = "com.riverview.cafe.prod.listdata.key";

	public static final String LISTDATA_1_KEY = "com.riverview.cafe.sublistdata1.key";
	public static final String LISTDATA_2_KEY = "com.riverview.cafe.sublistdata2.key";

	public static final String EVENT_1_KEY = "com.riverview.cafe.event1.key";
	public static final String EVENT_2_KEY = "com.riverview.cafe.event2.key";

	public static final String SUBDATA_1_KEY = "com.riverview.cafe.sublistdata3.key";
	public static final String SUBDATA_2_KEY = "com.riverview.cafe.sublistdata4.key";

	public static final String LOC_LAT_KEY = "com.riverview.harrys.latitude.key";
	public static final String LOC_LON_KEY = "com.riverview.harrys.longitude.key";

	public static final String GCM_ID_KEY = "com.riverview.harrys.gcm.id.key";
	public static final String ANALYTICS_JSON_ID_KEY = "com.riverview.harrys.anaylits.json.id.key";

	public static final String UPC_CURRENT_LIST_ID_KEY = "com.riverview.harrys.upc.list.id.key";

	public static final String SPLASH_AD_IMAGE_URL = "com.riverview.harrys.spalsh.ad.image";
	public static final String SPLASH_AD_IMAGE_SHOW_TIME = "com.riverview.harrys.spalsh.ad.showtime";

	public static final String COUPON_LIST_KEY = "com.riverview.harrys.coupon.viewpager.key";
	@SuppressWarnings("static-access")
	public static SharedPreferences getPref(Context ctx) {
		return ctx.getSharedPreferences(SharedPref.FILE_KEY, ctx.MODE_PRIVATE);
	}

	@SuppressWarnings("static-access")
	public static void saveString(Activity act, String key, String value) {
		SharedPreferences.Editor editor = act.getSharedPreferences(
				SharedPref.FILE_KEY, act.MODE_PRIVATE).edit();
		editor.putString(key, value);
		editor.commit();
	}

	@SuppressWarnings("static-access")
	public static void saveBoolean(Activity act, String key, boolean value) {
		SharedPreferences.Editor editor = act.getSharedPreferences(
				SharedPref.FILE_KEY, act.MODE_PRIVATE).edit();
		editor.putBoolean(key, value);
		editor.commit();
	}

	@SuppressWarnings("static-access")
	public static String getString(Activity act, String key, String defaultval) {
		SharedPreferences prefs = act.getSharedPreferences(SharedPref.FILE_KEY,
				act.MODE_PRIVATE);
		return prefs.getString(key, defaultval);
	}

	@SuppressWarnings("static-access")
	public static String getString(Context act, String key, String defaultval) {
		SharedPreferences prefs = act.getSharedPreferences(SharedPref.FILE_KEY,
				act.MODE_PRIVATE);
		return prefs.getString(key, defaultval);
	}

	@SuppressWarnings("static-access")
	public static boolean getBoolean(Activity act, String key,
									 boolean defaultval) {
		SharedPreferences prefs = act.getSharedPreferences(SharedPref.FILE_KEY,
				act.MODE_PRIVATE);
		return prefs.getBoolean(key, defaultval);
	}

	@SuppressWarnings("static-access")
	public static void saveInteger(Activity act, String key, Integer value) {
		SharedPreferences.Editor editor = act.getSharedPreferences(
				SharedPref.FILE_KEY, act.MODE_PRIVATE).edit();
		editor.putInt(key, value);
		editor.commit();
	}

	@SuppressWarnings("static-access")
	public static int getInteger(Activity act, String key, int defaultval) {
		SharedPreferences prefs = act.getSharedPreferences(SharedPref.FILE_KEY,
				act.MODE_PRIVATE);
		return prefs.getInt(key, defaultval);
	}

	@SuppressWarnings("static-access")
	public static void deleteString(Activity act, String key) {
		SharedPreferences.Editor editor = act.getSharedPreferences(
				SharedPref.FILE_KEY, act.MODE_PRIVATE).edit();
		editor.remove(key);
		editor.commit();
	}

	// @SuppressWarnings("static-access")
	// public static void saveString(Context act, String key, String value) {
	// SharedPreferences.Editor editor = act.getSharedPreferences(
	// SharedPref.FILE_KEY, act.MODE_PRIVATE).edit();
	// editor.putString(key, value);
	// editor.commit();
	// }
	//
	// @SuppressWarnings("static-access")
	// public static void saveInteger(Context act, String key, Integer value) {
	// SharedPreferences.Editor editor = act.getSharedPreferences(
	// SharedPref.FILE_KEY, act.MODE_PRIVATE).edit();
	// editor.putInt(key, value);
	// editor.commit();
	// }
}
