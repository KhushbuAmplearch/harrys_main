package com.riverview.harrys.fragments;

import static com.riverview.harrys.constant.AppConst.PULL_TO_REFRESH_TIMEOUT;
import static com.riverview.harrys.constant.AppConst.ATTR_TAG_FB_POST_ID;
import static com.riverview.harrys.constant.AppConst.ATTR_TAG_FB_REPLY;
import static com.riverview.harrys.constant.AppConst.ATTR_TAG_MOBILEUSER_ID;
import static com.riverview.harrys.constant.AppConst.HTTP_METHOD_POST;
import static com.riverview.harrys.constant.AppConst.ATTR_TAG_DEVICE_ID;
import static com.riverview.harrys.constant.AppConst.SERVICE_END_POINT;

import java.io.IOException;
import java.util.ArrayList;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.type.TypeReference;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.client.ClientHttpResponse;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.DefaultResponseErrorHandler;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewStub;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Toast;
import android.widget.ListView;

import com.iapps.libs.helpers.BaseHelper;
import com.iapps.libs.helpers.BaseHelper.CancelListener;
import com.iapps.libs.helpers.BaseHelper.ConfirmListener;
import com.iapps.libs.views.LoadingCompound;
import com.orangegangsters.github.swipyrefreshlayout.library.SwipyRefreshLayout;
import com.orangegangsters.github.swipyrefreshlayout.library.SwipyRefreshLayout.OnRefreshListener;
import com.orangegangsters.github.swipyrefreshlayout.library.SwipyRefreshLayoutDirection;
import com.riverview.harrys.MainActivity;
import com.riverview.harrys.R;
import com.riverview.harrys.RCGenericFragment;
import com.riverview.harrys.adapter.FeedbackReplyAdapter;

import com.riverview.harrys.asynctask.AsyncResponse;
import com.riverview.harrys.asynctask.FeedbackReplyListAsyncTask;
import com.riverview.harrys.constant.AppConst;
import com.riverview.harrys.exception.RestServiceException;
import com.riverview.harrys.helpers.ObjectSerializer;
import com.riverview.harrys.helpers.SharedPref;
import com.riverview.harrys.helpers.Utils;
import com.riverview.harrys.objects.BeanFeedback;
import com.riverview.harrys.objects.BeanFeedbackReply;
import com.riverview.harrys.objects.BeanReply;
import com.riverview.harrys.objects.ErrorServiceResponse;
import com.riverview.harrys.objects.ServiceResponse;
import com.riverview.harrys.util.AnaylaticsAppender;
import com.riverview.harrys.util.AppUtil;
import com.riverview.harrys.util.WebServiceUtil;

import fr.castorflex.android.circularprogressbar.CircularProgressDrawable;

public class FeedbackPostListFragment extends RCGenericFragment implements
		OnRefreshListener, AsyncResponse<ServiceResponse<BeanReply>> {

	private static final String TAG = FeedbackPostListFragment.class
			.getSimpleName();

	public static final int TAG_LIST_STORE = 0;

	private ListView lvStores;
	private LoadingCompound ld;
	private ViewStub emptyView;
	private ImageButton btnSend;
	private EditText etReply;
	private SwipyRefreshLayout mSwipyRefreshLayout;

	private View v;
	private FeedbackReplyAdapter mAdapter;
	private ArrayList<BeanFeedbackReply> storeList;
	private FeedbackReplyListAsyncTask mNewsTask = null;
	private FeedbackSendReplyAsyncTask mFeedbackSendReplyAsyncTask = null;
	private AnaylaticsAppender mAppender = null;
	private int mobileUserId;
	private BeanFeedback mBeanFeedbackItem;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		SharedPref.deleteString(getActivity(), SharedPref.LISTDATA_KEY);

		mBeanFeedbackItem = (BeanFeedback) getArguments().getParcelable(
				"feedback");
		mobileUserId = SharedPref.getInteger(getActivity(),
				SharedPref.USERID_KEY, 0);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		v = inflater.inflate(R.layout.feedback_post_list_fragment_layout,
				container, false);
		lvStores= (ListView)v.findViewById(R.id.listViewStore);
		ld = (LoadingCompound)v.findViewById(R.id.ld);
		emptyView= (ViewStub)v.findViewById(R.id.emptyView);
		btnSend = (ImageButton)v.findViewById(R.id.btnSend);
		etReply = (EditText)v.findViewById(R.id.etReply);
		mSwipyRefreshLayout = (SwipyRefreshLayout)v.findViewById(R.id.swipyrefreshlayout);

		setHasOptionsMenu(false);
		((MainActivity) getActivity()).setBackActionBar("Inquiry Messages",View.INVISIBLE,"");

		return v;
	}

	@Override
	public void onResume() {
		super.onResume();

		try {
			callAPI();
			mAppender = AnaylaticsAppender.getInstance(getActivity());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);

		mSwipyRefreshLayout.setOnRefreshListener(this);
		// lvStores.setOnItemClickListener(ListenerClickItem);

		lvStores.setEmptyView(emptyView);

		mobileUserId = SharedPref.getInteger(getActivity(),
				SharedPref.USERID_KEY, 0);

		btnSend.setOnClickListener(clickListeners);
	}

	@SuppressWarnings("unchecked")
	public void callAPI() {

		// get saved json data
		String serilizedJsonString = SharedPref.getString(getActivity(),
				SharedPref.LISTDATA_KEY, null);
		if (serilizedJsonString != null) {
			try {
				storeList = (ArrayList<BeanFeedbackReply>) ObjectSerializer
						.deserialize(serilizedJsonString);

				mAdapter = new FeedbackReplyAdapter(getActivity(), storeList);
				lvStores.setAdapter(mAdapter);
				mAdapter.notifyDataSetChanged();

				ld.hide();

			} catch (Exception e) {
				callAPIService();
				Log.e(FeedbackPostListFragment.TAG,
						"Error deserilization " + e.getMessage());
			}
		} else {
			callAPIService();
		}
	}

	@Override
	public void onPause() {
		super.onPause();

		if (mNewsTask != null) {
			mNewsTask.cancel(true);
		}
	}

	private boolean callReplyAPIService() {

		if (AppUtil.checkNetworkConnection(getActivity())) {

			mFeedbackSendReplyAsyncTask = new FeedbackSendReplyAsyncTask(
					getActivity(), mobileUserId, mBeanFeedbackItem.getId(),
					etReply.getText().toString());
			mFeedbackSendReplyAsyncTask.execute((Void) null);

			ld.bringToFront();
			ld.showLoading();

			return true;

		} else {

			BaseHelper.confirm(getActivity(), "Network Unavaliable",
					"Please turn on network to connect with server",
					new ConfirmListener() {

						@Override
						public void onYes() {
							AppUtil.showSystemSettingsDialog(getActivity());
						}
					}, new CancelListener() {

						@Override
						public void onNo() {
							getHome().backToHomeScreen();

						}
					});
		}
		return false;

	}

	private boolean callAPIService() {

		if (AppUtil.checkNetworkConnection(getActivity())) {

			mNewsTask = new FeedbackReplyListAsyncTask(getActivity(),
					String.valueOf(mBeanFeedbackItem.getId()));
			mNewsTask.asyncResponse = FeedbackPostListFragment.this;
			mNewsTask.execute((Void) null);

			ld.bringToFront();
			ld.showLoading();

			return true;

		} else {

			BaseHelper.confirm(getActivity(), "Network Unavaliable",
					"Please turn on network to connect with server",
					new ConfirmListener() {

						@Override
						public void onYes() {
							AppUtil.showSystemSettingsDialog(getActivity());
						}
					}, new CancelListener() {

						@Override
						public void onNo() {
							getHome().backToHomeScreen();

						}
					});
		}
		return false;

	}

	View.OnClickListener clickListeners = new View.OnClickListener() {
		public void onClick(View v) {
			switch (v.getId()) {
			case R.id.btnSend:
				callReplyAPIService();
				break;

			default:
				break;
			}
		}
	};

	@Override
	public void processFinish(ServiceResponse<BeanReply> details) {
		Log.d(TAG, "Process Finished");

		ld.hide();
		mSwipyRefreshLayout.setRefreshing(false);

		if (details != null && details.getStatus() == 200 && !details.error) {
			Log.i(TAG, "News feed download Success ");

			storeList = details.getResponse().getReplies();

			mAdapter = new FeedbackReplyAdapter(getActivity(), storeList);
			lvStores.setAdapter(mAdapter);
			mAdapter.notifyDataSetChanged();

		} else {
			// BaseHelper.showAlert(getActivity(), "Harry's",
			// details.getError_message());
		}
	}

	@Override
	public void onRefresh(SwipyRefreshLayoutDirection direction) {
		if (callAPIService()) {
			new Handler().postDelayed(new Runnable() {
				@Override
				public void run() {
					// Hide the refresh after n sec
					try {
						getActivity().runOnUiThread(new Runnable() {
							@Override
							public void run() {
								mSwipyRefreshLayout.setRefreshing(false);
							}
						});
					} catch (Exception e) {
					}
				}
			}, PULL_TO_REFRESH_TIMEOUT);
		} else {
			mSwipyRefreshLayout.setRefreshing(false);
		}
	}

	// Feedback reply Async task
	// Async task to send feedback replies
	public class FeedbackSendReplyAsyncTask extends
			AsyncTask<Void, Void, ServiceResponse<BeanFeedbackReply>> {

		private final String TAG = FeedbackSendReplyAsyncTask.class
				.getSimpleName();

		public AsyncResponse<ServiceResponse<BeanFeedbackReply>> asyncResponse;

		// private ProgressDialog mProgressDialog;
		private Context mContext;
		private int mobileuserId;
		private int feedbackId;
		private String feedbackReply;

		String[] commandString = null;
		ArrayList<NameValuePair> httpParams = null;
		private String urlApi = null;

		private ProgressDialog mProgressDialog;

		public FeedbackSendReplyAsyncTask(Context context, int userId, int id,
				String reply) {
			this.mContext = context;
			this.mobileuserId = userId;
			this.feedbackId = id;
			this.feedbackReply = reply;
		}

		@Override
		protected void onPreExecute() {
			Log.d(TAG, "onPreExecute");
			super.onPreExecute();

			mProgressDialog = new ProgressDialog(mContext);
			mProgressDialog
					.setIndeterminateDrawable(new CircularProgressDrawable.Builder(
							mContext)
							.colors(mContext.getResources().getIntArray(
									R.array.harrys_colors)).sweepSpeed(1f)
							.strokeWidth(12f)
							.style(CircularProgressDrawable.Style.ROUNDED)
							.build());

			mProgressDialog.setCancelable(false);
			mProgressDialog.setMessage("Uploading feedback.....");
			mProgressDialog.show();

			try {
				ArrayList<NameValuePair> arrayList = new ArrayList<>();

				arrayList.add(new BasicNameValuePair(ATTR_TAG_FB_POST_ID,
						String.valueOf(feedbackId)));
				arrayList.add(new BasicNameValuePair(ATTR_TAG_FB_REPLY,
						feedbackReply));
				arrayList.add(new BasicNameValuePair(ATTR_TAG_MOBILEUSER_ID,
						String.valueOf(mobileuserId)));

				urlApi = AppConst.URL_FEEDBACK_REPLY_CREATE;
				httpParams = AppUtil.generateAuthCommandString(arrayList,
						urlApi, HTTP_METHOD_POST);

			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		@Override
		protected void onPostExecute(ServiceResponse<BeanFeedbackReply> response) {

			if (mProgressDialog != null && mProgressDialog.isShowing()) {
				mProgressDialog.dismiss();
			}

			if (response != null
					&& response.getStatus() == HttpStatus.OK.value()) {
				if (storeList == null) {

					storeList = new ArrayList<>();
				}

				storeList.add(response.getResponse());

				// set up the adapter and refresh it
				if (mAdapter != null) {
					mAdapter.notifyDataSetChanged();
				} else {
					mAdapter = new FeedbackReplyAdapter(getActivity(),
							storeList);
					lvStores.setAdapter(mAdapter);
					mAdapter.notifyDataSetChanged();
				}
			}
		}

		@Override
		protected ServiceResponse<BeanFeedbackReply> doInBackground(
				Void... voids) {
			Log.d(TAG, "doInBackground");

			ServiceResponse<BeanFeedbackReply> serviceResponse = null;

			RestTemplate restTemplate = WebServiceUtil
					.getRestTemplateInstance();

			restTemplate.setErrorHandler(new DefaultResponseErrorHandler() {
				@Override
				public void handleError(ClientHttpResponse response)
						throws IOException {
					ObjectMapper mapper = WebServiceUtil.getErrorObjMapper();
					ErrorServiceResponse errorRes;
					errorRes = mapper.readValue(response.getBody(),
							new TypeReference<ErrorServiceResponse>() {
							});

					throw new RestServiceException(errorRes, errorRes
							.getMessage());
				}

				protected boolean hasError(HttpStatus statusCode) {
					Log.d(TAG, "Status ----> " + statusCode);
					if (statusCode.value() == 200) {
						return false;
					}
					return true;
				}
			});

			String url = String.format(SERVICE_END_POINT + "%s", urlApi);

			Log.d(TAG, "Final URL ------> " + url);

			HttpHeaders requestHeaders = new HttpHeaders();
			requestHeaders
					.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
			requestHeaders.set("Accept", "application/json");
			requestHeaders.add(ATTR_TAG_DEVICE_ID,
					AppUtil.getDeviceId(mContext));

			MultiValueMap<String, String> map = new LinkedMultiValueMap<String, String>();
			for (NameValuePair a : httpParams) {
				map.add(a.getName(), a.getValue());
			}

			HttpEntity<MultiValueMap<String, String>> requestEntity = new HttpEntity<>(
					map, requestHeaders);

			Log.d(TAG, "Url " + url);
			Log.d(TAG, "Request Entity " + requestEntity.toString());

			try {
				serviceResponse = restTemplate
						.exchange(
								url,
								HttpMethod.POST,
								requestEntity,
								new ParameterizedTypeReference<ServiceResponse<BeanFeedbackReply>>() {
								}).getBody();

			} catch (RestClientException e) {
				serviceResponse = new ServiceResponse<>();
				serviceResponse.setError(true);
				serviceResponse.setError_message(e.getCause().getMessage());

			} catch (Exception e) {
				e.printStackTrace();
			}
			return serviceResponse;
		}

		@Override
		protected void onCancelled() {
			asyncResponse.processFinish(null);
		}
	}
}
