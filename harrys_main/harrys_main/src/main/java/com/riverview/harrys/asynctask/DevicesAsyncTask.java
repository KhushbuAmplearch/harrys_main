package com.riverview.harrys.asynctask;

import static com.riverview.harrys.constant.AppConst.ATTR_TAG_APPLICATION_ID;
import static com.riverview.harrys.constant.AppConst.ATTR_TAG_APP_VERSION;
import static com.riverview.harrys.constant.AppConst.ATTR_TAG_COMPANY_ID;
import static com.riverview.harrys.constant.AppConst.ATTR_TAG_DEVICE;
import static com.riverview.harrys.constant.AppConst.ATTR_TAG_DEVICE_ID;
import static com.riverview.harrys.constant.AppConst.ATTR_TAG_MOBILEUSER_ID;
import static com.riverview.harrys.constant.AppConst.ATTR_TAG_OS_VERSION;
import static com.riverview.harrys.constant.AppConst.ATTR_TAG_STATUS;
import static com.riverview.harrys.constant.AppConst.ATTR_TAG_TOKEN_ID;
import static com.riverview.harrys.constant.AppConst.HTTP_METHOD_POST;
import static com.riverview.harrys.constant.AppConst.SERVICE_END_POINT;
import static com.riverview.harrys.constant.AppConst.URL_DEVICES;

import java.io.IOException;
import java.util.ArrayList;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.type.TypeReference;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.client.ClientHttpResponse;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.DefaultResponseErrorHandler;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.util.Log;

import com.riverview.harrys.constant.AppConst;
import com.riverview.harrys.exception.RestServiceException;
import com.riverview.harrys.helpers.Constants;
import com.riverview.harrys.helpers.PreferenceUpdateService;
import com.riverview.harrys.helpers.SharedPref;
import com.riverview.harrys.objects.BeanDevice;
import com.riverview.harrys.objects.ErrorServiceResponse;
import com.riverview.harrys.objects.ServiceResponse;
import com.riverview.harrys.util.AppUtil;
import com.riverview.harrys.util.GcmConnectionHandler;
import com.riverview.harrys.util.WebServiceUtil;

public class DevicesAsyncTask extends
		AsyncTask<Void, Void, ServiceResponse<Object>> {

	private static final String TAG = DevicesAsyncTask.class.getSimpleName();

	private Context mContext;
	private BeanDevice beanDevice;
	private boolean isUpdatePref;
	private String refCode;

	private GcmConnectionHandler mGcmConnectionHandler;

	String[] commandString = null;
	ArrayList<NameValuePair> httpParams = null;
	ArrayList<NameValuePair> arrayList = null;

	public DevicesAsyncTask(Context context, BeanDevice beanDevice,
			boolean updatePref) {
		this.mContext = context;
		this.beanDevice = beanDevice;
		this.isUpdatePref = updatePref;
		this.mGcmConnectionHandler = new GcmConnectionHandler(mContext);
	}

	public DevicesAsyncTask(Context context, BeanDevice beanDevice,
			boolean updatePref, String referralCode) {
		this.mContext = context;
		this.beanDevice = beanDevice;
		this.isUpdatePref = updatePref;
		this.refCode = referralCode;
		this.mGcmConnectionHandler = new GcmConnectionHandler(mContext);
	}

	@Override
	protected void onPreExecute() {
		super.onPreExecute();

		try {
			arrayList = new ArrayList<NameValuePair>();

			arrayList.add(new BasicNameValuePair(ATTR_TAG_COMPANY_ID, String
					.valueOf(beanDevice.getCompany_id())));
			arrayList.add(new BasicNameValuePair(ATTR_TAG_APPLICATION_ID,
					String.valueOf(beanDevice.getApplication_id())));
			arrayList.add(new BasicNameValuePair(ATTR_TAG_STATUS, beanDevice
					.getStatus()));
			arrayList.add(new BasicNameValuePair(ATTR_TAG_DEVICE, beanDevice
					.getDevice()));
			arrayList.add(new BasicNameValuePair(ATTR_TAG_DEVICE_ID
					.toLowerCase(), beanDevice.getUDID()));
			arrayList.add(new BasicNameValuePair(ATTR_TAG_MOBILEUSER_ID,
					beanDevice.getMobileuser_id()));
			arrayList.add(new BasicNameValuePair(ATTR_TAG_APP_VERSION,
					beanDevice.getApp_version()));
			arrayList.add(new BasicNameValuePair(ATTR_TAG_OS_VERSION,
					beanDevice.getOs_version()));

		} catch (Exception e) {
			e.printStackTrace();
			onCancelled();
		}
	}

	@Override
	protected void onPostExecute(ServiceResponse<Object> response) {
		Log.e(TAG, "onPostExecute"+response);

		if (response != null && response.getStatus() == 200 && !response.error) {
			Log.d(TAG, "Device API call success");
			if (isUpdatePref) {
				int mobileUserId = SharedPref.getPref(mContext).getInt(
						SharedPref.USERID_KEY, 0);
				
				Log.d(TAG, "Call preference update service");
				Intent i = new Intent(mContext, PreferenceUpdateService.class);
				i.putExtra(AppConst.TAG_REF_ID, refCode);
				i.putExtra(ATTR_TAG_MOBILEUSER_ID, mobileUserId);
				mContext.startService(i);
			}
		} else {
			Log.w(TAG, "Device API call fail");
		}
	}

	@Override
	protected ServiceResponse<Object> doInBackground(Void... voids) {
		Log.d(TAG, "doInBackground");

		ServiceResponse<Object> serviceResponse = null;

		// generate GCM Id
		String gcmRegistrationId = mGcmConnectionHandler.getGcmRegistrationId();

		SharedPreferences pref = SharedPref.getPref(mContext);
		if (gcmRegistrationId == null) {

			gcmRegistrationId = pref.getString(SharedPref.GCM_ID_KEY, "");
		} else {
			// Save GCM ID in Shared pref
			pref.edit().putString(SharedPref.GCM_ID_KEY, gcmRegistrationId)
					.commit();
		}
		Log.e(TAG, "GCM Id " + gcmRegistrationId);
		arrayList.add(new BasicNameValuePair(ATTR_TAG_TOKEN_ID,
				gcmRegistrationId));
		Log.e("arrayList",""+arrayList);

		httpParams = AppUtil.generateAuthCommandString(arrayList, URL_DEVICES,
				HTTP_METHOD_POST);

		RestTemplate restTemplate = WebServiceUtil.getRestTemplateInstance();
		restTemplate.setErrorHandler(new DefaultResponseErrorHandler() {
			@Override
			public void handleError(ClientHttpResponse response)
					throws IOException {
				ObjectMapper mapper = WebServiceUtil.getErrorObjMapper();
				ErrorServiceResponse errorRes;
				errorRes = mapper.readValue(response.getBody(),
						new TypeReference<ErrorServiceResponse>() {
						});

				throw new RestServiceException(errorRes, errorRes
						.getError_message());

			}

			protected boolean hasError(HttpStatus statusCode) {
				Log.d(TAG, "Status ----> " + statusCode);
				if (statusCode.value() == HttpStatus.OK.value()) {
					return false;
				}
				return true;
			}
		});

		String url = String.format(SERVICE_END_POINT + "%s", URL_DEVICES);

		HttpHeaders requestHeaders = new HttpHeaders();
		requestHeaders.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
		requestHeaders.set("Accept", "application/json");
		// requestHeaders.add(ATTR_TAG_UDID, AppUtil.getDeviceId(mContext));

		MultiValueMap<String, String> map = new LinkedMultiValueMap<String, String>();
		for (NameValuePair a : httpParams) {
			if (a.getName().equals(ATTR_TAG_DEVICE_ID.toLowerCase())) {
				map.add(ATTR_TAG_DEVICE_ID, a.getValue());
			} else {
				map.add(a.getName(), a.getValue());
			}
		}

		HttpEntity<MultiValueMap<String, String>> requestEntity = new HttpEntity<MultiValueMap<String, String>>(
				map, requestHeaders);

		Log.d(TAG, "Url " + url);
		Log.d(TAG, "Request Entity " + requestEntity.toString());

		try {
			serviceResponse = restTemplate.exchange(url, HttpMethod.POST,
					requestEntity,
					new ParameterizedTypeReference<ServiceResponse<Object>>() {
					}).getBody();

		} catch (RestClientException e) {
			serviceResponse = new ServiceResponse<Object>();
			serviceResponse.setError(true);
			serviceResponse.setError_message(e.getCause().getMessage());

			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return serviceResponse;
	}

	@Override
	protected void onCancelled() {
		Log.d(TAG, "onCalcelled");
	}
}
