package com.riverview.harrys.rcinterface;

public interface TransactionListener {

	public void clickONList(int pos);

	public void openMoreDialog(int pos);

	public void deleteItem(int pos);

}
