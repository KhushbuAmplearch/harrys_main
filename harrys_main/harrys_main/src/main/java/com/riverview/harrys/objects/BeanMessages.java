package com.riverview.harrys.objects;

import com.iapps.libs.objects.SimpleBean;

public class BeanMessages extends SimpleBean {

	public static final String OBJ_NAME = "BeanMessages";

	private String title, date, content;

	public BeanMessages(int id, String name) {
		super(id, name);
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

}
