package com.riverview.harrys.helpers;

import com.iapps.libs.helpers.BaseKeys;

public class Constants extends BaseKeys {
	public static final String LOG = "Riverview Cafe";
	public static final String APP_PACKAGE = "com.riverview.cafe";
	// public static final String GLOBAL_FONT_APP = "arial.ttf";
	public static final String GLOBAL_FONT_APP = "Georgia.ttf";
	// public static final String GLOBAL_FONT_APP = "arialmt_light.ttf";
	public static String gcm_register_id ="";
	public static final int STATUSCODE_403 = 403; // invalid oauth

	public static final int STATUSCODE_9100 = 9100; // If a non-member
	public static final int STATUSCODE_9101 = 9101; // If it's a clubFitt member
	public static final int STATUSCODE_9120 = 9120; // Successfully checked out
	// from the gym
	public static final int STATUSCODE_9104 = 9104; // No usable pass
	public static final int STATUSCODE_9106 = 9106; // No birthdate

	public static final int STATUSCODE_9103 = 9103; // Unknown NRIC/FIN number
	public static final int STATUSCODE_9134 = 9134; // Unknown gym
	public static final int STATUSCODE_9125 = 9125; // Failed to checkout

	public static final int STATUSCODE_9150 = 9150; // dob successfully updated
	public static final int STATUSCODE_9155 = 9155; // dob failed to update

	public static final int STATUSCODE_9205 = 9205; // unknown message ewallet

	public static final int STATUSCODE_9214 = 9214; // unknown message

	public static final int STATUSCODE_1010 = 1010;// login success
	public static final int STATUSCODE_9200 = 9200;// login success

	public static final int STATUSCODE_1105 = 1105;

	public static final int YEAR_RANGE_START = 1902;
	public static final int YEAR_RANGE_END = 2037;

	public static final String NULL = "null";

	public static final String DATE_EMDY = "EEE, MMM dd, yyyy";
	public static final String DATE_MDY = "MMM dd, yyyy";
	public static final String DATE_JSON = "yyyy-MM-dd";
	public static final String DATE_TIME_JSON = "yyyy-MM-dd HH:mm:ss";
	public static final String TIME_HMA = "h:mm a";
	public static final String TIME_hM = "h:mm";
	public static final String TIME_JSON_HM = "HH:mm";
	public static final String TIME_JSON_HMS = "HH:mm:ss";

	public static final String FONT_BEBAS_NUEUE_REGULAR = "BebasNeueRegular.ttf";
	public static final String FONT_BEBAS_NUEUE_BOLD = "BebasNeueBold.ttf";
	public static final String FONT_BEBAS_BOOK = "BebasNeueBook.ttf";
	public static final String FONT_DEFAULT = "sans-serif";

	public static final String REFERRAL_TYPE_NETWORK = "network";
	public static final String REFERRAL_TYPE_ASSOCIATE = "associate";

	public static final String SUSPEND_TEXT = "SUSPENDED";

	public static final String UTF8 = "utf-8";
}
