package com.riverview.harrys.asynctask;

import static com.riverview.harrys.constant.AppConst.ATTR_TAG_ADDRESS_LINE_1;
import static com.riverview.harrys.constant.AppConst.ATTR_TAG_ADDRESS_LINE_2;
import static com.riverview.harrys.constant.AppConst.ATTR_TAG_DOB;
import static com.riverview.harrys.constant.AppConst.ATTR_TAG_EMAIL;
import static com.riverview.harrys.constant.AppConst.ATTR_TAG_FIRST_NAME;
import static com.riverview.harrys.constant.AppConst.ATTR_TAG_MOBILE;
import static com.riverview.harrys.constant.AppConst.ATTR_TAG_PASSWORD;
import static com.riverview.harrys.constant.AppConst.ATTR_TAG_PASSWORD_CONFIR;
import static com.riverview.harrys.constant.AppConst.ATTR_TAG_POSTEL_CODE;
import static com.riverview.harrys.constant.AppConst.ATTR_TAG_SUR_NAME;
import static com.riverview.harrys.constant.AppConst.ATTR_TAG_UDID;
import static com.riverview.harrys.constant.AppConst.ATTR_TAG_USER_NAME;
import static com.riverview.harrys.constant.AppConst.HTTP_METHOD_POST;
import static com.riverview.harrys.constant.AppConst.SERVICE_END_POINT;
import static com.riverview.harrys.constant.AppConst.URL_SIGN_UP;
import static com.riverview.harrys.constant.AppConst.ATTR_TAG_REFERRAL_CODE;

import java.io.IOException;
import java.util.ArrayList;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.type.TypeReference;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.client.ClientHttpResponse;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.DefaultResponseErrorHandler;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.iapps.libs.helpers.BaseHelper;
import com.riverview.harrys.R;
import com.riverview.harrys.exception.RestServiceException;
import com.riverview.harrys.objects.BeanLogin;
import com.riverview.harrys.objects.BeanSignUp;
import com.riverview.harrys.objects.ErrorServiceResponse;
import com.riverview.harrys.objects.ServiceResponse;
import com.riverview.harrys.util.AppUtil;
import com.riverview.harrys.util.WebServiceUtil;

import fr.castorflex.android.circularprogressbar.CircularProgressDrawable;

public class SignUpAsyncTask extends
		AsyncTask<Void, Void, ServiceResponse<BeanLogin>> {

	private static final String TAG = SignUpAsyncTask.class.getSimpleName();

	public AsyncResponse<ServiceResponse<BeanLogin>> asyncResponse;

	private ProgressDialog mProgressDialog;

	private Context mContext;
	private BeanSignUp mBeanSignUp;

	String[] commandString = null;
	ArrayList<NameValuePair> httpParams = null;

	public SignUpAsyncTask(Context context, BeanSignUp beanSignUp) {
		this.mContext = context;
		this.mBeanSignUp = beanSignUp;
	}

	@Override
	protected void onPreExecute() {
		Log.d(TAG, "onPreExecute");
		super.onPreExecute();

		mProgressDialog = new ProgressDialog(mContext);
		mProgressDialog
				.setIndeterminateDrawable(new CircularProgressDrawable.Builder(
						mContext)
						.colors(mContext.getResources().getIntArray(
								R.array.harrys_colors)).sweepSpeed(1f)
						.strokeWidth(12f)
						.style(CircularProgressDrawable.Style.ROUNDED).build());

		mProgressDialog.setCancelable(false);
		mProgressDialog.setMessage("Signing up user ....");
		mProgressDialog.show();

		try {
			ArrayList<NameValuePair> arrayList = new ArrayList<NameValuePair>();

			arrayList.add(new BasicNameValuePair(ATTR_TAG_USER_NAME,
					mBeanSignUp.getUsername()));

			arrayList.add(new BasicNameValuePair(ATTR_TAG_PASSWORD, mBeanSignUp
					.getPassword()));

			arrayList.add(new BasicNameValuePair(ATTR_TAG_PASSWORD_CONFIR,
					mBeanSignUp.getPassword_confirmation()));

			arrayList.add(new BasicNameValuePair(ATTR_TAG_FIRST_NAME,
					mBeanSignUp.getFirstname()));

			arrayList.add(new BasicNameValuePair(ATTR_TAG_SUR_NAME, mBeanSignUp
					.getSurname()));

			arrayList.add(new BasicNameValuePair(ATTR_TAG_MOBILE, mBeanSignUp
					.getMobilenumber()));

			arrayList.add(new BasicNameValuePair(ATTR_TAG_EMAIL, mBeanSignUp
					.getEmail()));

			arrayList.add(new BasicNameValuePair(ATTR_TAG_POSTEL_CODE,
					mBeanSignUp.getPostalcode()));

			arrayList.add(new BasicNameValuePair(ATTR_TAG_DOB, mBeanSignUp
					.getDob()));

			if (!BaseHelper.isEmpty(mBeanSignUp.getAddressLine1())) {
				arrayList.add(new BasicNameValuePair(ATTR_TAG_ADDRESS_LINE_1,
						mBeanSignUp.getAddressLine1()));
			}
			if (!BaseHelper.isEmpty(mBeanSignUp.getAddressLine2())) {
				arrayList.add(new BasicNameValuePair(ATTR_TAG_ADDRESS_LINE_2,
						mBeanSignUp.getAddressLine2()));
			}
			if (!BaseHelper.isEmpty(mBeanSignUp.getCode())) {
				arrayList.add(new BasicNameValuePair(ATTR_TAG_REFERRAL_CODE,
						mBeanSignUp.getCode()));
			}

			httpParams = AppUtil.generateAuthCommandString(arrayList,
					URL_SIGN_UP, HTTP_METHOD_POST);

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	@Override
	protected void onPostExecute(ServiceResponse<BeanLogin> response) {
		Log.d(TAG, "onPostExecute");

		if (mProgressDialog != null && mProgressDialog.isShowing()) {
			mProgressDialog.dismiss();
		}
		asyncResponse.processFinish(response);
	}

	@Override
	protected ServiceResponse<BeanLogin> doInBackground(Void... voids) {
		Log.d(TAG, "doInBackground");

		ServiceResponse<BeanLogin> serviceResponse = null;

		RestTemplate restTemplate = WebServiceUtil.getRestTemplateInstance();

		restTemplate.setErrorHandler(new DefaultResponseErrorHandler() {
			@Override
			public void handleError(ClientHttpResponse response)
					throws IOException {
				if (response.getRawStatusCode() == 500) {
					throw new RestServiceException(null,
							"Request failed: internal server error");
				}

				ObjectMapper mapper = WebServiceUtil.getErrorObjMapper();
				ErrorServiceResponse errorRes;
				errorRes = mapper.readValue(response.getBody(),
						new TypeReference<ErrorServiceResponse>() {
						});

				throw new RestServiceException(errorRes, errorRes
						.getError_message());
			}

			protected boolean hasError(HttpStatus statusCode) {
				Log.d(TAG, "Status ----> " + statusCode);
				if (statusCode.value() == 200) {
					return false;
				}
				return true;
			}
		});

		String url = String.format(SERVICE_END_POINT + "%s", URL_SIGN_UP);

		HttpHeaders requestHeaders = new HttpHeaders();
		requestHeaders.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
		requestHeaders.set("Accept", "application/json");
		requestHeaders.add(ATTR_TAG_UDID, AppUtil.getDeviceId(mContext));

		MultiValueMap<String, String> map = new LinkedMultiValueMap<String, String>();

		for (NameValuePair a : httpParams) {
			map.add(a.getName(), a.getValue());
		}

		HttpEntity<MultiValueMap<String, String>> requestEntity = new HttpEntity<MultiValueMap<String, String>>(
				map, requestHeaders);

		Log.d(TAG, "Url " + url);
		Log.d(TAG, "Request Entity " + requestEntity.toString());

		try {
			serviceResponse = restTemplate
					.exchange(
							url,
							HttpMethod.POST,
							requestEntity,
							new ParameterizedTypeReference<ServiceResponse<BeanLogin>>() {
							}).getBody();

			Log.i(TAG, "MEssage -> " + serviceResponse.getError_message());

		} catch (RestClientException e) {

			Log.e(TAG, "Message 11 -> " + e.getLocalizedMessage());

			serviceResponse = new ServiceResponse<BeanLogin>();
			serviceResponse.setError(true);
			serviceResponse.setError_message(e.getCause().getMessage());

		} catch (Exception e) {
			Log.e(TAG, "Exception " + e.getLocalizedMessage());
			e.printStackTrace();
		}

		return serviceResponse;
	}

	@Override
	protected void onCancelled() {

		if (mProgressDialog != null && mProgressDialog.isShowing()) {
			mProgressDialog.dismiss();
			asyncResponse.processFinish(null);
		}
	}
}
