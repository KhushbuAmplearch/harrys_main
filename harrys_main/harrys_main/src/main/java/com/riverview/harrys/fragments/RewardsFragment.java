package com.riverview.harrys.fragments;

import uk.co.chrisjenx.calligraphy.CalligraphyConfig;
import android.os.Bundle;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.TextView;

import com.iapps.libs.views.LoadingCompound;
import com.riverview.harrys.MainActivity;
import com.riverview.harrys.R;
import com.riverview.harrys.RCGenericFragment;
import com.riverview.harrys.helpers.Constants;
import com.riverview.harrys.helpers.Utils;

public class RewardsFragment extends RCGenericFragment {

	private static final String TAG = RewardsFragment.class.getSimpleName();

	WebView webViewRewards;
	private View v;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setHasOptionsMenu(false);

		CalligraphyConfig.initDefault(Constants.GLOBAL_FONT_APP);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
							 Bundle savedInstanceState) {
		v = inflater
				.inflate(R.layout.rewards_fragment_layout, container, false);

		webViewRewards = (WebView)v.findViewById(R.id.webViewRewards);
		String pdf = "http://harryscard.com.sg/about-card/privileges-google";
		webViewRewards.loadUrl(pdf);
		webViewRewards.setWebViewClient(new MyBrowser());
		WebSettings webSettings = webViewRewards.getSettings();
		webSettings.setJavaScriptEnabled(true);

		((MainActivity) getActivity()).setBackActionBar(getActivity()
				.getResources().getString(R.string.rc_menu_rewards),View.INVISIBLE,"");

		return v;
	}

	@Override
	public LayoutInflater getLayoutInflater(Bundle savedInstanceState) {
		return getActivity().getLayoutInflater();
	}

	// @Override
	// public void onPrepareOptionsMenu(Menu menu) {
	// super.onPrepareOptionsMenu(menu);
	// getSherlockActivity().getSupportMenuInflater().inflate(
	// R.menu.menu_rewards, menu);
	// }

	/*
	 * @Override public void onCreateOptionsMenu(Menu menu, MenuInflater
	 * inflater) { inflater.inflate( R.menu.menu_location, menu); }
	 *
	 * @Override public boolean onOptionsItemSelected(MenuItem item) { switch
	 * (item.getItemId()) { case R.id.rv_menu_locations: return true; //
	 * default: // return super.onOptionsItemSelected(item); } return false; }
	 */

	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);

	}

	View.OnClickListener clickListeners = new View.OnClickListener() {
		public void onClick(View v) {

		}
	};
	private class MyBrowser extends WebViewClient {
		@Override
		public boolean shouldOverrideUrlLoading(WebView view, String url) {
			view.loadUrl(url);
			return true;
		}
	}
}
