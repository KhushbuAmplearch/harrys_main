package com.riverview.harrys.asynctask;

import static com.riverview.harrys.constant.AppConst.ATTR_TAG_DEVICE;
import static com.riverview.harrys.constant.AppConst.ATTR_TAG_MOBILEUSER_ID;
import static com.riverview.harrys.constant.AppConst.ATTR_TAG_DEVICE_ID;
import static com.riverview.harrys.constant.AppConst.ATTR_TAG_UDID;
import static com.riverview.harrys.constant.AppConst.HTTP_METHOD_POST;
import static com.riverview.harrys.constant.AppConst.SERVICE_END_POINT;
import static com.riverview.harrys.constant.AppConst.URL_COUPON_REDEEM;

import java.io.IOException;
import java.util.ArrayList;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.type.TypeReference;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.client.ClientHttpResponse;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.DefaultResponseErrorHandler;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.riverview.harrys.R;
import com.riverview.harrys.exception.RestServiceException;
import com.riverview.harrys.objects.BeanPromotionCode;
import com.riverview.harrys.objects.ErrorServiceResponse;
import com.riverview.harrys.objects.ServiceResponse;
import com.riverview.harrys.util.AppUtil;
import com.riverview.harrys.util.WebServiceUtil;

import fr.castorflex.android.circularprogressbar.CircularProgressDrawable;

public class PromotionRedeemAsyncTask extends
		AsyncTask<Void, Void, ServiceResponse<BeanPromotionCode>> {

	private static final String TAG = PromotionRedeemAsyncTask.class
			.getSimpleName();

	public AsyncResponse<ServiceResponse<BeanPromotionCode>> asyncResponse;

	private Context mContext;

	private ProgressDialog mProgressDialog;

	String[] commandString = null;
	ArrayList<NameValuePair> httpParams = null;

	private int promotionId;
	private String deviceType;
	private String mobileuserId;

	private String couponRedeemApi = null;

	public PromotionRedeemAsyncTask(Context context, int promotionId,
			String deviceType, String userId) {
		this.mContext = context;

		this.promotionId = promotionId;
		this.deviceType = deviceType;
		this.mobileuserId = userId;
	}

	@Override
	protected void onPreExecute() {
		Log.d(TAG, "onPreExecute");
		super.onPreExecute();

		mProgressDialog = new ProgressDialog(mContext);
		mProgressDialog
				.setIndeterminateDrawable(new CircularProgressDrawable.Builder(
						mContext)
						.colors(mContext.getResources().getIntArray(
								R.array.harrys_colors)).sweepSpeed(1f)
						.strokeWidth(12f)
						.style(CircularProgressDrawable.Style.ROUNDED).build());

		mProgressDialog.setCancelable(false);
		mProgressDialog.setMessage("Generating coupon number.....");
		mProgressDialog.show();

		try {
			couponRedeemApi = String.format(URL_COUPON_REDEEM, promotionId);

			ArrayList<NameValuePair> arrayList = new ArrayList<NameValuePair>();

			arrayList.add(new BasicNameValuePair(ATTR_TAG_DEVICE, deviceType));
			arrayList.add(new BasicNameValuePair(ATTR_TAG_DEVICE_ID
					.toLowerCase(), AppUtil.getDeviceId(mContext)));
			arrayList.add(new BasicNameValuePair(ATTR_TAG_MOBILEUSER_ID
					.toLowerCase(), mobileuserId));

			httpParams = AppUtil.generateAuthCommandString(arrayList,
					couponRedeemApi, HTTP_METHOD_POST);

		} catch (Exception e) {

			e.printStackTrace();
		}

	}

	@Override
	protected void onPostExecute(ServiceResponse<BeanPromotionCode> response) {
		Log.d(TAG, "onPostExecute");

		if (mProgressDialog != null && mProgressDialog.isShowing()) {
			mProgressDialog.dismiss();
		}
		asyncResponse.processFinish(response);
	}

	@Override
	protected ServiceResponse<BeanPromotionCode> doInBackground(Void... voids) {
		Log.d(TAG, "doInBackground");

		ServiceResponse<BeanPromotionCode> serviceResponse = null;

		RestTemplate restTemplate = WebServiceUtil.getRestTemplateInstance();

		restTemplate.setErrorHandler(new DefaultResponseErrorHandler() {
			@Override
			public void handleError(ClientHttpResponse response)
					throws IOException {
				ObjectMapper mapper = WebServiceUtil.getErrorObjMapper();
				ErrorServiceResponse errorRes;
				errorRes = mapper.readValue(response.getBody(),
						new TypeReference<ErrorServiceResponse>() {
						});

				throw new RestServiceException(errorRes, errorRes
						.getError_message());
			}

			protected boolean hasError(HttpStatus statusCode) {
				Log.d(TAG, "Status ----> " + statusCode);
				if (statusCode.value() == 200) {
					return false;
				}
				return true;
			}
		});

		String url = String.format(SERVICE_END_POINT + "%s", couponRedeemApi);

		// String urlParams = URLEncodedUtils.format(httpParams, "UTF-8");
		// String finalUrl = String.format(url + "?" + "%s", urlParams);

		Log.d(TAG, "Final URL ------> " + url);

		HttpHeaders requestHeaders = new HttpHeaders();
		requestHeaders.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
		requestHeaders.set("Accept", "application/json");
		requestHeaders.add(ATTR_TAG_UDID, AppUtil.getDeviceId(mContext));

		MultiValueMap<String, String> map = new LinkedMultiValueMap<String, String>();
		for (NameValuePair a : httpParams) {
			if (a.getName().equals(ATTR_TAG_DEVICE_ID.toLowerCase())) {
				map.add(ATTR_TAG_DEVICE_ID, a.getValue());
			} else {
				map.add(a.getName(), a.getValue());
			}
		}

		HttpEntity<MultiValueMap<String, String>> requestEntity = new HttpEntity<MultiValueMap<String, String>>(
				map, requestHeaders);

		try {
			serviceResponse = restTemplate
					.exchange(
							url,
							HttpMethod.POST,
							requestEntity,
							new ParameterizedTypeReference<ServiceResponse<BeanPromotionCode>>() {
							}).getBody();

		} catch (RestClientException e) {
			serviceResponse = new ServiceResponse<BeanPromotionCode>();
			serviceResponse.setError(true);
			serviceResponse.setError_message(e.getCause().getMessage());

		} catch (Exception e) {
			Log.e(TAG, "Exception " + e.getMessage());
			e.printStackTrace();
		}
		return serviceResponse;
	}

	@Override
	protected void onCancelled() {

		if (mProgressDialog != null && mProgressDialog.isShowing()) {
			mProgressDialog.dismiss();
			asyncResponse.processFinish(null);
		}
	}
}
