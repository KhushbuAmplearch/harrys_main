package com.riverview.harrys.helpers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.telephony.SmsMessage;
import android.util.Log;

import com.riverview.harrys.constant.AppConst;

public class SMSReceiver extends BroadcastReceiver {
	private static final String TAG = SMSReceiver.class.getSimpleName();

	@Override
	public void onReceive(Context arg0, Intent intent) {
		Log.d(TAG, "OnReceive");
		if (intent.getAction().equalsIgnoreCase(
				"android.provider.Telephony.SMS_RECEIVED")) {
			Log.d(TAG, "Got SMS");

			final Bundle bundle = intent.getExtras();

			try {

				if (bundle != null) {

					final Object[] pdusObj = (Object[]) bundle.get("pdus");

					for (int i = 0; i < pdusObj.length; i++) {

						SmsMessage currentMessage = SmsMessage
								.createFromPdu((byte[]) pdusObj[i]);
						String phoneNumber = currentMessage
								.getDisplayOriginatingAddress();
						String phoneSS = currentMessage
								.getServiceCenterAddress();
						String phoneOO = currentMessage.getOriginatingAddress();

						String message = currentMessage.getDisplayMessageBody();

						if (phoneNumber
								.equalsIgnoreCase(AppConst.HARRYS_OTP_HEADER)
								| phoneSS
										.equalsIgnoreCase(AppConst.HARRYS_OTP_HEADER)
								| phoneOO
										.equalsIgnoreCase(AppConst.HARRYS_OTP_HEADER)) {
							// send broadcast
							Intent intent2 = new Intent();
							intent2.setAction(AppConst.HARRYS_OTP_ACTION);
							intent2.putExtra("content", message);
							LocalBroadcastManager.getInstance(arg0)
									.sendBroadcast(intent2);
						}

					} // end for loop
				} // bundle is null

			} catch (Exception e) {
				Log.e("SmsReceiver", "Exception smsReceiver" + e);

			}

		}
	}

}
