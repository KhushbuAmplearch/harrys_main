package com.riverview.harrys.objects;

/**
 * 
 * @author Wiraj
 */
public class ErrorServiceResponse {

	public ErrorServiceResponse() {

	}

	public int status;
	public boolean error;
	public String response;
	public Object request_data;
	public Object error_messages;
	private String error_message;
	private String message;

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public boolean isError() {
		return error;
	}

	public void setError(boolean error) {
		this.error = error;
	}

	public String getResponse() {
		return response;
	}

	public void setResponse(String response) {
		this.response = response;
	}

	public Object getRequest_data() {
		return request_data;
	}

	public void setRequest_data(Object request_data) {
		this.request_data = request_data;
	}

	public Object getError_messages() {
		return error_messages;
	}

	public void setError_messages(Object error_messages) {
		this.error_messages = error_messages;
	}

	public String getError_message() {
		return error_message;
	}

	public void setError_message(String error_message) {
		this.error_message = error_message;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

}
