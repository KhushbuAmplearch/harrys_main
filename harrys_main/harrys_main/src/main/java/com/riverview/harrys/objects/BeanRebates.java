package com.riverview.harrys.objects;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by wiraj on 5/2/15.
 */
public class BeanRebates implements Parcelable {

    private String id, rebates_amount, created_at, updated_at, description;

    public BeanRebates() {
    }

    public BeanRebates(Parcel parcel) {
        super();

        this.id = parcel.readString();
        this.rebates_amount = parcel.readString();
        this.created_at = parcel.readString();
        this.updated_at = parcel.readString();
        this.description = parcel.readString();
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(id);
        parcel.writeString(rebates_amount);
        parcel.writeString(created_at);
        parcel.writeString(updated_at);
        parcel.writeString(description);
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getRebates_amount() {
        return rebates_amount;
    }

    public void setRebates_amount(String rebates_amount) {
        this.rebates_amount = rebates_amount;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(String updated_at) {
        this.updated_at = updated_at;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<BeanRebates> CREATOR = new Creator<BeanRebates>() {

        @Override
        public BeanRebates createFromParcel(Parcel source) {
            return new BeanRebates(source);
        }

        @Override
        public BeanRebates[] newArray(int size) {
            return new BeanRebates[size];
        }
    };
}
