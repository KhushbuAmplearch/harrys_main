package com.riverview.harrys.objects;

import java.io.Serializable;
import java.util.ArrayList;

public class BeanFeedbackQueries implements Serializable {

    public static final String OBJ_NAME = "BeanFeedbackQueries";
    private static final long serialVersionUID = 5588254270496437773L;

    private ArrayList<BeanFeedback> queries;

    public BeanFeedbackQueries(){super();}

    public ArrayList<BeanFeedback> getQueries() {
        return queries;
    }

    public void setQueries(ArrayList<BeanFeedback> queries) {
        this.queries = queries;
    }
}
