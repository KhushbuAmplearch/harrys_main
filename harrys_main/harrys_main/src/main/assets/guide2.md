######Top up mWallet with Cash

Follow the simple steps for topping up your mWallet in any of our stores1.  Select Top up from your wallet2.  Select top up with cash from theavailable options3.  Let our cashier know you want totop up4.  Scan your QR code using thescanner at the cash register5.  Pay the cashier for your top up6.  Done! Your App will display thenew balance