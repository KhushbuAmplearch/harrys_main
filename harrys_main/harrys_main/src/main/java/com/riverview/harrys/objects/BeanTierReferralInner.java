package com.riverview.harrys.objects;

public class BeanTierReferralInner {

	private String id, card_name, card_face_image, description;

	public BeanTierReferralInner() {
	}

	public String getCard_name() {
		return card_name;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getCard_face_image() {
		return card_face_image;
	}

	public void setCard_face_image(String card_face_image) {
		this.card_face_image = card_face_image;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	
}
