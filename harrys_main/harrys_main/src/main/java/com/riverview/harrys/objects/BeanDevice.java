package com.riverview.harrys.objects;

public class BeanDevice {

	private String company_id, application_id;
	private String status, device, token_id, UDID, os_version, app_version,
			mobileuser_id;

	public BeanDevice(String companyId, String appId) {
		this.company_id = companyId;
		this.application_id = appId;
	}

	public String getCompany_id() {
		return company_id;
	}

	public void setCompany_id(String company_id) {
		this.company_id = company_id;
	}

	public String getApplication_id() {
		return application_id;
	}

	public void setApplication_id(String application_id) {
		this.application_id = application_id;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getDevice() {
		return device;
	}

	public void setDevice(String device) {
		this.device = device;
	}

	public String getToken_id() {
		return token_id;
	}

	public void setToken_id(String token_id) {
		this.token_id = token_id;
	}

	public String getUDID() {
		return UDID;
	}

	public void setUDID(String uDID) {
		UDID = uDID;
	}

	public String getOs_version() {
		return os_version;
	}

	public void setOs_version(String os_version) {
		this.os_version = os_version;
	}

	public String getApp_version() {
		return app_version;
	}

	public void setApp_version(String app_version) {
		this.app_version = app_version;
	}

	public String getMobileuser_id() {
		return mobileuser_id;
	}

	public void setMobileuser_id(String mobileuser_id) {
		this.mobileuser_id = mobileuser_id;
	}

}
