package com.riverview.harrys.objects;

import java.util.ArrayList;

public class BeanMerchant {

	private String id, company_id, application_id, category_id, subcategory_id,
			title, description, detailed_description;
	private String url, use_category_image, created_at, updated_at, deleted_at,
			lang, imageurl;

	// private Boolean use_parent_image;

	private BeanImage image;
	private ArrayList<BeanPhoto> photos;
	private ArrayList<BeanOutlet> outlets;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getCompany_id() {
		return company_id;
	}

	public void setCompany_id(String company_id) {
		this.company_id = company_id;
	}

	public String getApplication_id() {
		return application_id;
	}

	public void setApplication_id(String application_id) {
		this.application_id = application_id;
	}

	public String getCategory_id() {
		return category_id;
	}

	public void setCategory_id(String category_id) {
		this.category_id = category_id;
	}

	public String getSubcategory_id() {
		return subcategory_id;
	}

	public void setSubcategory_id(String subcategory_id) {
		this.subcategory_id = subcategory_id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getDetailed_description() {
		return detailed_description;
	}

	public void setDetailed_description(String detailed_description) {
		this.detailed_description = detailed_description;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getUse_category_image() {
		return use_category_image;
	}

	public void setUse_category_image(String use_category_image) {
		this.use_category_image = use_category_image;
	}

	public String getCreated_at() {
		return created_at;
	}

	public void setCreated_at(String created_at) {
		this.created_at = created_at;
	}

	public String getUpdated_at() {
		return updated_at;
	}

	public void setUpdated_at(String updated_at) {
		this.updated_at = updated_at;
	}

	public String getDeleted_at() {
		return deleted_at;
	}

	public void setDeleted_at(String deleted_at) {
		this.deleted_at = deleted_at;
	}

	public String getLang() {
		return lang;
	}

	public void setLang(String lang) {
		this.lang = lang;
	}

	public String getImageurl() {
		return imageurl;
	}

	public void setImageurl(String imageurl) {
		this.imageurl = imageurl;
	}

	// public String getUse_parent_image() {
	// return use_parent_image;
	// }
	// public void setUse_parent_image(String use_parent_image) {
	// this.use_parent_image = use_parent_image;
	// }

	public BeanImage getImage() {
		return image;
	}

	// public Boolean getUse_parent_image() {
	// return use_parent_image;
	// }
	// public void setUse_parent_image(Boolean use_parent_image) {
	// this.use_parent_image = use_parent_image;
	// }
	public void setImage(BeanImage image) {
		this.image = image;
	}

	public ArrayList<BeanPhoto> getPhotos() {
		return photos;
	}

	public void setPhotos(ArrayList<BeanPhoto> photos) {
		this.photos = photos;
	}

	public ArrayList<BeanOutlet> getOutlets() {
		return outlets;
	}

	public void setOutlets(ArrayList<BeanOutlet> outlets) {
		this.outlets = outlets;
	}
}
