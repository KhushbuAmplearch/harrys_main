package com.riverview.harrys.fragments;

import static com.riverview.harrys.constant.AppConst.PULL_TO_REFRESH_TIMEOUT;
import static com.riverview.harrys.constant.AppConst.ATTR_TAG_MOBILEUSER_ID;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.Map;

import org.apache.commons.collections4.map.HashedMap;
import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewStub;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import com.iapps.libs.helpers.BaseHelper;
import com.iapps.libs.helpers.BaseHelper.CancelListener;
import com.iapps.libs.helpers.BaseHelper.ConfirmListener;
import com.iapps.libs.views.LoadingCompound;
import com.orangegangsters.github.swipyrefreshlayout.library.SwipyRefreshLayout;
import com.orangegangsters.github.swipyrefreshlayout.library.SwipyRefreshLayout.OnRefreshListener;
import com.orangegangsters.github.swipyrefreshlayout.library.SwipyRefreshLayoutDirection;
import com.riverview.harrys.MainActivity;
import com.riverview.harrys.R;
import com.riverview.harrys.RCGenericFragment;
import com.riverview.harrys.adapter.PreferencesAdapter;
import com.riverview.harrys.asynctask.AsyncResponse;
import com.riverview.harrys.asynctask.PreferencesAsyncTask;
import com.riverview.harrys.asynctask.PreferencesUpdateAsyncTask;
import com.riverview.harrys.helpers.SharedPref;
import com.riverview.harrys.helpers.Utils;
import com.riverview.harrys.objects.BeanPreference;
import com.riverview.harrys.objects.ServiceResponse;
import com.riverview.harrys.objects.SwitchPosition;
import com.riverview.harrys.util.AnaylaticsAppender;
import com.riverview.harrys.util.AppUtil;

public class PreferencesFragment extends RCGenericFragment implements
		OnRefreshListener,
		AsyncResponse<ServiceResponse<ArrayList<BeanPreference>>> {

	private static final String TAG = PreferencesFragment.class.getSimpleName();

	private LinearLayout LLSubmitView;
	private ListView lvPref;
	private ViewStub emptyView;
	private Button buttonSubmit;
	private LoadingCompound ld;
	private SwipyRefreshLayout mSwipyRefreshLayout;

	private View v;
	private PreferencesAdapter mAdapter;
	private ArrayList<BeanPreference> prefList;
	private PreferencesAsyncTask mPreferencesTask = null;
	private AnaylaticsAppender mAppender = null;
	int mobileUserId = 0;
	private boolean IS_UPDATE = false;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		mobileUserId = SharedPref.getInteger(getActivity(),
				SharedPref.USERID_KEY, 0);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
							 Bundle savedInstanceState) {
		v = inflater.inflate(R.layout.preferences_fragment_layout, container,
				false);
		LLSubmitView = (LinearLayout)v.findViewById(R.id.LLSubmitView);
		lvPref = (ListView)v.findViewById(R.id.listViewPref);
		emptyView = (ViewStub)v.findViewById(R.id.emptyView);
		buttonSubmit = (Button)v.findViewById(R.id.buttonSignUp);
		ld=(LoadingCompound)v.findViewById(R.id.ld);
		mSwipyRefreshLayout = (SwipyRefreshLayout)v.findViewById(R.id.swipyrefreshlayout);

		setHasOptionsMenu(false);
		((MainActivity) getActivity()).setBackActionBar(getActivity()
				.getResources().getString(R.string.rc_menu_preferences),View.INVISIBLE,"");

		return v;
	}

	public void onPrepareOptionsMenu(Menu menu) {
		// getSherlockActivity().getSupportMenuInflater().inflate(
		// R.menu.menu_settings, menu);
	}

	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);

		Utils.setHeightByPercent(getActivity(), LLSubmitView, 0.08f);
		Utils.setButtonFontSizeBasedOnScreenDensity(getActivity(),
				buttonSubmit, Utils.FONT_SMALL_DENSITY_SIZE, Typeface.BOLD);

		buttonSubmit.setText("Submit");
		buttonSubmit.setOnClickListener(clickListeners);

		lvPref.setEmptyView(emptyView);

		mSwipyRefreshLayout.setOnRefreshListener(this);

		callAPIService();
	}

	@Override
	public void onPause() {
		super.onPause();

		if (mPreferencesTask != null) {
			mPreferencesTask.cancel(true);
		}
	}

	@Override
	public void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		try {
			mAppender = AnaylaticsAppender.getInstance(getActivity());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void callAPIService() {

		if (AppUtil.checkNetworkConnection(getActivity())) {

			mPreferencesTask = new PreferencesAsyncTask(getActivity(),
					String.valueOf(mobileUserId));
			mPreferencesTask.asyncResponse = PreferencesFragment.this;
			mPreferencesTask.execute((Void) null);

		} else {

			BaseHelper.confirm(getActivity(), "Network Unavaliable",
					"Please turn on network to connect with server",
					new ConfirmListener() {

						@Override
						public void onYes() {
							AppUtil.showSystemSettingsDialog(getActivity());
						}
					}, new CancelListener() {

						@Override
						public void onNo() {
							getHome().backToHomeScreen();

						}
					});
		}

	}

	private boolean callAPIService(ArrayList<NameValuePair> arrayList) {

		if (AppUtil.checkNetworkConnection(getActivity())) {

			PreferencesUpdateAsyncTask mPreferencesTask = new PreferencesUpdateAsyncTask(
					getActivity(), arrayList, true);
			mPreferencesTask.asyncResponse = PreferencesFragment.this;
			mPreferencesTask.execute((Void) null);

			mAppender.updateAnaylitsData(AppUtil
					.createAnalyticsJsonEventAction("preferences", null,
							"update", null));

			IS_UPDATE = true;
			return true;

		} else {

			BaseHelper.confirm(getActivity(), "Network Unavaliable",
					"Please turn on network to connect with server",
					new ConfirmListener() {

						@Override
						public void onYes() {
							AppUtil.showSystemSettingsDialog(getActivity());
						}
					}, new CancelListener() {

						@Override
						public void onNo() {
							getHome().backToHomeScreen();

						}
					});
		}

		return false;

	}

	private ArrayList<NameValuePair> getPreferenceListValues() {
		if (mAdapter != null && mAdapter.getCount() > 0) {
			ArrayList<NameValuePair> arrayList = new ArrayList<NameValuePair>();
			HashedMap<Integer, SwitchPosition> sp = mAdapter.getSp();

			Iterator<Map.Entry<Integer, SwitchPosition>> entries = sp
					.entrySet().iterator();
			while (entries.hasNext()) {
				Map.Entry<Integer, SwitchPosition> entry = entries.next();
				SwitchPosition s = entry.getValue();
				arrayList.add(new BasicNameValuePair(s.getDes(), String
						.valueOf(s.getStatus())));
			}

			// add mobile user id
			arrayList.add(new BasicNameValuePair(ATTR_TAG_MOBILEUSER_ID, String
					.valueOf(mobileUserId)));

			return arrayList;
		} else {
			return null;
		}
	}

	View.OnClickListener clickListeners = new View.OnClickListener() {
		public void onClick(View v) {

			if (v.getId() == R.id.buttonSignUp) {

				ArrayList<NameValuePair> values = getPreferenceListValues();
				if (values != null) {
					callAPIService(values);
				}
			}
		}
	};

	@Override
	public void processFinish(ServiceResponse<ArrayList<BeanPreference>> details) {
		Log.d(TAG, "Process Finished ");

		ld.hide();
		mSwipyRefreshLayout.setRefreshing(false);

		if (details == null) {
			// BaseHelper.showAlert(getActivity(), "News feed Error");
			return;
		}

		if (details != null && details.getStatus() == 200 && !details.error) {
			Log.i(TAG, "Preferences download Success ");

			prefList = details.getResponse();

			mAdapter = new PreferencesAdapter(getActivity(), prefList);
			lvPref.setAdapter(mAdapter);
			mAdapter.notifyDataSetChanged();

			if (IS_UPDATE) {
				IS_UPDATE = false;

				// show relevant message
				try {
					BaseHelper.showAlert(getActivity(), "Harry's",
							"Your preferences have been saved successfully");
				} catch (Exception e) {
					// TODO: handle exception
				}
			}
		} else {

			try {
				BaseHelper
						.showAlert(
								getActivity(),
								"Harry's",
								(BaseHelper.isEmpty(details.getError_message())) ? "Unable to get preferences"
										: details.getError_message());
			} catch (Exception e) {
				// TODO: handle exception
			}
		}
	}

	@Override
	public void onRefresh(SwipyRefreshLayoutDirection direction) {
		ArrayList<NameValuePair> values = getPreferenceListValues();

		if (values != null && callAPIService(values)) {
			new Handler().postDelayed(new Runnable() {
				@Override
				public void run() {
					// Hide the refresh after n sec
					try {
						getActivity().runOnUiThread(new Runnable() {
							@Override
							public void run() {
								mSwipyRefreshLayout.setRefreshing(false);
							}
						});
					} catch (Exception e) {
					}
				}
			}, PULL_TO_REFRESH_TIMEOUT);
		} else {
			mSwipyRefreshLayout.setRefreshing(false);
		}
	}
}
