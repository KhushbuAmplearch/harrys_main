package com.riverview.harrys.objects;

import java.util.ArrayList;

public class BeanReply {

    public static final String OBJ_NAME = "BeanReply";
    private static final long serialVersionUID = 5588254270496437773L;

    private ArrayList<BeanFeedbackReply> replies;

    public BeanReply() {
    }

    public ArrayList<BeanFeedbackReply> getReplies() {
        return replies;
    }

    public void setReplies(ArrayList<BeanFeedbackReply> replies) {
        this.replies = replies;
    }
}
