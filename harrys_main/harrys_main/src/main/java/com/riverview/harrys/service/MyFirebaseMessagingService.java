package com.riverview.harrys.service;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.riverview.harrys.MainActivity;
import com.riverview.harrys.R;
import com.riverview.harrys.helpers.Constants;
import com.riverview.harrys.helpers.Utils;

import org.json.JSONObject;

import java.util.Map;


/**
 * Created by khushbu on 29/12/17.
 */



public class MyFirebaseMessagingService extends FirebaseMessagingService {
    private static final String TAG = "FCM Service";

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        Log.e(TAG, "From: " + remoteMessage.getData());
//        Log.e(TAG, "Notification Message Body: " + remoteMessage.getNotification().getBody());
        Map<String, String> data = remoteMessage.getData();
        try {
            String message = data.get("default");

            if (data.get("message") != null) {

                Log.e(TAG, "message: " + data.get("message"));

                JSONObject jObj = new JSONObject(data.get("message"));

                if (!jObj.isNull("action_type")) {

                    String ActionType = jObj.getString("action_type");
                    String title = jObj.getString("title");
                    String description = jObj.getString("description");
                    if (ActionType.equals(Constants.SUSPEND_TEXT)) {

                        //Log.i(TAG, "Received message: " + intent.getExtras().toString());

                        Utils.suspendUser(getApplicationContext());

                    } else {

                        Log.i(TAG, "Received message: " + title+description);
                        Notification(title,description);
                    }

                } else {

                    // Log.i(TAG, "Received message: " + intent.getExtras().toString());
                    Notification(getResources().getString(R.string.app_name), message);
                }
            } else {

                // Log.i(TAG, "Received message: " + intent.getExtras().toString());
                Notification(getResources().getString(R.string.app_name), message);
            }

            //common_value = intent.getExtras().toString();

        } catch (Exception e) {

            Notification(getResources().getString(R.string.app_name), "");
        }
        //  Notification(remoteMessage);
    }


    public void Notification(String title,String description) {
        // Set Notification Title
        String strtitle = title;
        // Set Notification Text
        String strtext = description;
        Log.e("strtext",""+strtext);

        // Open NotificationView Class on Notification Click
        Intent intent = new Intent(this, MainActivity.class);
        // Send data to NotificationView Class
        intent.putExtra("title", strtitle);
        intent.putExtra("text", strtext);
        // Open NotificationView.java Activity
        PendingIntent pIntent = PendingIntent.getActivity(this, 0, intent,
                PendingIntent.FLAG_UPDATE_CURRENT);

        //Create Notification using NotificationCompat.Builder
        //  NotificationCompat.Builder builder = (NotificationCompat.Builder) new NotificationCompat.Builder(this)
        NotificationCompat.Builder builder = (NotificationCompat.Builder) new NotificationCompat.Builder(this)
                // Set Icon
                .setSmallIcon(R.drawable.ic_icon_harry)
                // Set Ticker Message
                .setTicker(getString(R.string.app_name))
                // Set Title
                .setContentTitle(strtitle)
                // Set Text
                .setContentText(strtext)
                // Add an Action Button below Notification
                //.addAction(R.mipmap.ic_launcher, "Action Button", pIntent)
                // Set PendingIntent into Notification
                .setContentIntent(pIntent)
                // Dismiss Notification
                .setAutoCancel(true);

        // Create Notification Manager
        NotificationManager notificationmanager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        // Build Notification with Notification Manager
        notificationmanager.notify(0, builder.build());

    }



}