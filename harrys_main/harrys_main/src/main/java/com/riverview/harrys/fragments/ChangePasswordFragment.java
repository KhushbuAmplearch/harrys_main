package com.riverview.harrys.fragments;

import android.content.Context;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;

import com.iapps.libs.helpers.BaseHelper;
import com.iapps.libs.helpers.BaseHelper.ConfirmListener;
import com.riverview.harrys.MainActivity;
import com.riverview.harrys.R;
import com.riverview.harrys.RCGenericFragment;
import com.riverview.harrys.asynctask.ChangePasswordAsyncTask;
import com.riverview.harrys.helpers.SharedPref;
import com.riverview.harrys.helpers.Utils;
import com.riverview.harrys.util.AppUtil;

public class ChangePasswordFragment extends RCGenericFragment {

	private static final String TAG = ChangePasswordFragment.class
			.getSimpleName();

	private LinearLayout LLSignIn;
	private EditText editTextOldPassword;
	private EditText editTextNewPassword;
	private EditText editTextConfNewPassword;
	private Button buttonSignIn;
	private View v;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		v = inflater.inflate(R.layout.change_password_fragment_layout,
				container, false);

		LLSignIn = (LinearLayout)v.findViewById(R.id.LLSignIn);
		editTextOldPassword = (EditText)v.findViewById(R.id.editTextOldPassword);
		editTextNewPassword = (EditText)v.findViewById(R.id.editTextNewPassword);
		editTextConfNewPassword = (EditText)v.findViewById(R.id.editTextConfNewPassword);
		buttonSignIn = (Button)v.findViewById(R.id.buttonSignIn);

		setHasOptionsMenu(false);
		((MainActivity) getActivity()).setBackActionBar(getHome()
				.getResources().getString(R.string.rc_change_password),View.INVISIBLE,"");

		hideDefaultKeyboard();

		return v;
	}

	private void hideDefaultKeyboard() {
		try {
			getActivity().getWindow().setSoftInputMode(
					WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

			getActivity();
			InputMethodManager imm = (InputMethodManager) getActivity()
					.getSystemService(Context.INPUT_METHOD_SERVICE);

			imm.hideSoftInputFromWindow(editTextOldPassword.getWindowToken(), 0);
			imm.hideSoftInputFromWindow(editTextNewPassword.getWindowToken(), 0);
			imm.hideSoftInputFromWindow(
					editTextConfNewPassword.getWindowToken(), 0);

		} catch (Exception e) {
		}

	}

	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);

		LLSignIn.setOnClickListener(clickListeners);

		buttonSignIn.setText("Submit");
		buttonSignIn.setOnClickListener(clickListeners);

		Utils.setEditTextFontSizeBasedOnScreenDensity(getActivity(),
				editTextNewPassword, Utils.FONT_SMALL_DENSITY_SIZE);
		Utils.setEditTextFontSizeBasedOnScreenDensity(getActivity(),
				editTextConfNewPassword, Utils.FONT_SMALL_DENSITY_SIZE);
		Utils.setEditTextFontSizeBasedOnScreenDensity(getActivity(),
				editTextOldPassword, Utils.FONT_SMALL_DENSITY_SIZE);
		Utils.setButtonFontSizeBasedOnScreenDensity(getActivity(),
				buttonSignIn, Utils.FONT_SMALL_DENSITY_SIZE, Typeface.BOLD);
	}

	View.OnClickListener clickListeners = new View.OnClickListener() {
		public void onClick(View v) {

			switch (v.getId()) {
			case R.id.textViewSignUp:
				getHome().setFragment(new SignUpFragment());
				break;

			case R.id.buttonSignIn:
				if (BaseHelper.isEmpty(editTextNewPassword)
						|| BaseHelper.isEmpty(editTextConfNewPassword)
						|| BaseHelper.isEmpty(editTextOldPassword)) {
					BaseHelper.showAlert(getActivity(),
							"Please fill all the blank fields.");
				} else {
					if (AppUtil.checkNetworkConnection(getActivity())) {
						changePassword();
					} else {
						BaseHelper
								.confirm(
										getActivity(),
										"Network Unavaliable",
										"Please turn on network to connect with server",
										new ConfirmListener() {

											@Override
											public void onYes() {
												AppUtil.showSystemSettingsDialog(getActivity());
											}
										});
					}
					hideDefaultKeyboard();
				}
				break;

			default:
				break;
			}
		}
	};

	private void changePassword() {

		ChangePasswordAsyncTask mAuthTask = new ChangePasswordAsyncTask(
				getHome(), SharedPref.getInteger(getHome(),
						SharedPref.USERID_KEY, -1), editTextOldPassword
						.getText().toString(), editTextNewPassword.getText()
						.toString(), editTextConfNewPassword.getText()
						.toString());
		// mAuthTask.asyncResponse = ChangePasswordFragment.this;
		mAuthTask.execute((Void) null);
	}

	@Override
	public void onDestroy() {

		super.onDestroy();
		hideDefaultKeyboard();
	}

	@Override
	public void onDetach() {
		super.onDetach();
		hideDefaultKeyboard();
	}
}
