package com.riverview.harrys.objects;

import java.util.ArrayList;

public class BeanReceiver {

    private ArrayList<BeanRebates> rebates;
    private ArrayList<BeanStamps> stamps;
    private ArrayList<BeanPoints> points;
    private ArrayList<BeanTopup> topup;
	private ArrayList<BeanTierReferralOuter> tier;
    private ArrayList<BeanVoucher> voucher;

    public ArrayList<BeanRebates> getRebates() {
        return rebates;
    }

    public void setRebates(ArrayList<BeanRebates> rebates) {
        this.rebates = rebates;
    }

    public ArrayList<BeanStamps> getStamps() {
        return stamps;
    }

    public void setStamps(ArrayList<BeanStamps> stamps) {
        this.stamps = stamps;
    }

    public ArrayList<BeanPoints> getPoints() {
        return points;
    }

    public void setPoints(ArrayList<BeanPoints> points) {
        this.points = points;
    }

    public ArrayList<BeanTopup> getTopup() {
        return topup;
    }

    public void setTopup(ArrayList<BeanTopup> topup) {
        this.topup = topup;
    }

	public ArrayList<BeanTierReferralOuter> getTier() {
		return tier;
	}

	public void setTier(ArrayList<BeanTierReferralOuter> tier) {
		this.tier = tier;
	}

	public ArrayList<BeanVoucher> getVoucher() {
		return voucher;
	}

	public void setVoucher(ArrayList<BeanVoucher> voucher) {
		this.voucher = voucher;
	}    
    
}
