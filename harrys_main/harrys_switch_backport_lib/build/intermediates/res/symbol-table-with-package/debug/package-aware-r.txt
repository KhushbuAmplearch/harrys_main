org.jraf.android.backport.switchwidget
int attr disableDependentsState 0x7f040001
int attr summaryOff 0x7f040002
int attr summaryOn 0x7f040003
int attr switchMinWidth 0x7f040004
int attr switchPadding 0x7f040005
int attr switchPreferenceStyle 0x7f040006
int attr switchStyle 0x7f040007
int attr switchTextAppearance 0x7f040008
int attr switchTextOff 0x7f040009
int attr switchTextOn 0x7f04000a
int attr textOff 0x7f04000b
int attr textOn 0x7f04000c
int attr thumb 0x7f04000d
int attr thumbTextPadding 0x7f04000e
int attr track 0x7f04000f
int color background_holo_light 0x7f060001
int color bright_foreground_disabled_holo_dark 0x7f060002
int color bright_foreground_holo_dark 0x7f060003
int color dim_foreground_disabled_holo_dark 0x7f060004
int color dim_foreground_holo_dark 0x7f060005
int color primary_text_holo_dark 0x7f060006
int color secondary_text_holo_dark 0x7f060007
int dimen preference_icon_minWidth 0x7f080001
int dimen preference_item_padding_inner 0x7f080002
int dimen preference_item_padding_side 0x7f080003
int dimen preference_widget_width 0x7f080004
int drawable switch_bg_disabled_holo_dark 0x7f090001
int drawable switch_bg_disabled_holo_light 0x7f090002
int drawable switch_bg_focused_holo_dark 0x7f090003
int drawable switch_bg_focused_holo_light 0x7f090004
int drawable switch_bg_holo_dark 0x7f090005
int drawable switch_bg_holo_light 0x7f090006
int drawable switch_inner_holo_dark 0x7f090007
int drawable switch_inner_holo_light 0x7f090008
int drawable switch_thumb_activated_holo_dark 0x7f090009
int drawable switch_thumb_activated_holo_light 0x7f09000a
int drawable switch_thumb_disabled_holo_dark 0x7f09000b
int drawable switch_thumb_disabled_holo_light 0x7f09000c
int drawable switch_thumb_holo_dark 0x7f09000d
int drawable switch_thumb_holo_light 0x7f09000e
int drawable switch_thumb_holo_light_v2 0x7f09000f
int drawable switch_thumb_pressed_holo_dark 0x7f090010
int drawable switch_thumb_pressed_holo_light 0x7f090011
int drawable switch_track_holo_dark 0x7f090012
int drawable switch_track_holo_light 0x7f090013
int id switchWidget 0x7f0c0001
int layout preference 0x7f0f0001
int layout preference_widget_switch 0x7f0f0002
int string switch_off 0x7f150001
int string switch_on 0x7f150002
int style Preference_SwitchPreference 0x7f160001
int style TextAppearance_Holo_Light_Widget_Switch 0x7f160002
int style TextAppearance_Holo_Widget_Switch 0x7f160003
int style Widget_Holo_CompoundButton_Switch 0x7f160004
int style Widget_Holo_Light_CompoundButton_Switch 0x7f160005
int[] styleable Android { 0x01010098, 0x01010099, 0x0101009a, 0x0101009b, 0x01010095, 0x01010097, 0x01010096 }
int styleable Android_android_textColor 0
int styleable Android_android_textColorHighlight 1
int styleable Android_android_textColorHint 2
int styleable Android_android_textColorLink 3
int styleable Android_android_textSize 4
int styleable Android_android_textStyle 5
int styleable Android_android_typeface 6
int[] styleable Switch { 0x7f040004, 0x7f040005, 0x7f040008, 0x7f04000b, 0x7f04000c, 0x7f04000d, 0x7f04000e, 0x7f04000f }
int styleable Switch_switchMinWidth 0
int styleable Switch_switchPadding 1
int styleable Switch_switchTextAppearance 2
int styleable Switch_textOff 3
int styleable Switch_textOn 4
int styleable Switch_thumb 5
int styleable Switch_thumbTextPadding 6
int styleable Switch_track 7
int[] styleable SwitchBackportTheme { 0x7f040006, 0x7f040007 }
int styleable SwitchBackportTheme_switchPreferenceStyle 0
int styleable SwitchBackportTheme_switchStyle 1
int[] styleable SwitchPreference { 0x7f040001, 0x7f040002, 0x7f040003, 0x7f040009, 0x7f04000a }
int styleable SwitchPreference_disableDependentsState 0
int styleable SwitchPreference_summaryOff 1
int styleable SwitchPreference_summaryOn 2
int styleable SwitchPreference_switchTextOff 3
int styleable SwitchPreference_switchTextOn 4
