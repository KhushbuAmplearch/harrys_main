package com.riverview.harrys.service;

import org.json.JSONObject;

import android.app.IntentService;
import android.content.Intent;
import android.os.AsyncTask;
import android.util.Log;
import com.riverview.harrys.asynctask.AnalyticesAsyncTask;
import com.riverview.harrys.constant.AppConst;
import com.riverview.harrys.util.AnaylaticsAppender;
import com.riverview.harrys.util.AppUtil;

/**
 * Created by wiraj on 4/29/15.
 */
public class UploadAnaylaticsService extends IntentService {

	private static final String TAG = UploadAnaylaticsService.class
			.getSimpleName();

	private AnaylaticsAppender mAppender = null;
//	private AmazonSNSClient sns;
//	static AWSCredentials credentials;

	public UploadAnaylaticsService() {
		super(TAG);
	}

	public UploadAnaylaticsService(String name) {
		super(name);
	}

	@Override
	protected void onHandleIntent(Intent intent) {
		Log.d(TAG, "onHandleIntent");

		//8890349401 M.R Wimala Balasooriya
		
		// start the altitude async task
		if (AppUtil.checkNetworkConnection(getApplicationContext())) {
			mAppender = AnaylaticsAppender.getInstance(getApplicationContext());

			JSONObject object = mAppender.getFinalData();
			Log.e(TAG, "Final Data " + object);
			if (object != null) {
				Log.e(TAG, "Final Data " + object.toString());
				
			
				
				// Initialize the Amazon Cognito credentials provider

//				CognitoCachingCredentialsProvider credentialsProvider = new CognitoCachingCredentialsProvider(
//				  getApplicationContext(),
//				   "ap-northeast-1:a4c17c57-5d57-4623-b0bd-2be5c06d7c85", // Identity Pool ID
//				   Regions.AP_NORTHEAST_1 // Region
//				);
//				
//				 credentials = new BasicAWSCredentials(AppConst.accessKey, AppConst.secretKey);
//				 sns = new AmazonSNSClient(credentials);
//			     sns.setRegion(Region.getRegion(Regions.AP_SOUTHEAST_1));
//			     
//			     PublishRequest publishRequest = new PublishRequest(AppConst.topicArn, object.toString());
//				 PublishResult publishResult = sns.publish(publishRequest);
//				 
//
//				System.out.println("MessageId - " + publishResult.getMessageId());

				
				AnalyticesAsyncTask asyncTask = new AnalyticesAsyncTask(
						getApplicationContext(), object);
				asyncTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, (Void) null);

			}
		}
	}
}
