##Sign up & be Rewarded

###Spend More, Earn More !

Our Rewards Program is the best in the business, cause we actually care about our customers more than anyone.

Join our rewards program by downloading the app and signing up and every 1$ you spend will earn you 5 points.

Redeeming points can be done in any amount to exchange points for either eVouchers, or for our promotional eCoupons valid on many items.

### Triple points on your Birthday!

Because we think you’re special, you will automatically receive 3 x the rewards points for the entire week of your birthday! AND we’ll give you a free birthday cake on your birthday!

### Silver Members

Spend more than $500 in any rolling 3 month period and we’ll give you 20% bonus. You’ll earn 6 points for every $1 you spend

### Gold Members

Once you hit $1,000 in any rolling 6 month period we’ll automatically upgrade you to Gold and give you 7 points for every $1 you spend

And for every ten drinks you buy you’ll get an extra one free of charge!

### Platinum Members

Spend more than $5,000 in any rolling 12 month period and we’ll automatically upgrade you to Platinum and give you 10 points for every $1 you spend !

And you’ll receive an additional 10% off when you dine in with two or more guests.

`* Terms & conditions apply`
