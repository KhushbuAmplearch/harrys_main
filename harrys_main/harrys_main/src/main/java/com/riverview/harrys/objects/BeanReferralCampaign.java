package com.riverview.harrys.objects;

/**
 * Created by wiraj on 5/2/15.
 */
public class BeanReferralCampaign {

    private String id, type, company_id, application_id, user_id, referral_campaign_id, list_id, name, unique, start_date, end_date, max_referrals, status;

    private BeanCode code;
    private BeanReceiver receiver;
    private BeanSender sender;


    public BeanReferralCampaign() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getCompany_id() {
        return company_id;
    }

    public void setCompany_id(String company_id) {
        this.company_id = company_id;
    }

    public String getApplication_id() {
        return application_id;
    }

    public void setApplication_id(String application_id) {
        this.application_id = application_id;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getReferral_campaign_id() {
        return referral_campaign_id;
    }

    public void setReferral_campaign_id(String referral_campaign_id) {
        this.referral_campaign_id = referral_campaign_id;
    }

    public String getList_id() {
        return list_id;
    }

    public void setList_id(String list_id) {
        this.list_id = list_id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUnique() {
        return unique;
    }

    public void setUnique(String unique) {
        this.unique = unique;
    }

    public String getStart_date() {
        return start_date;
    }

    public void setStart_date(String start_date) {
        this.start_date = start_date;
    }

    public String getEnd_date() {
        return end_date;
    }

    public void setEnd_date(String end_date) {
        this.end_date = end_date;
    }

    public String getMax_referrals() {
        return max_referrals;
    }

    public void setMax_referrals(String max_referrals) {
        this.max_referrals = max_referrals;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public BeanCode getCode() {
        return code;
    }

    public void setCode(BeanCode code) {
        this.code = code;
    }

    public BeanReceiver getReceiver() {
        return receiver;
    }

    public void setReceiver(BeanReceiver receiver) {
        this.receiver = receiver;
    }

    public BeanSender getSender() {
        return sender;
    }

    public void setSender(BeanSender sender) {
        this.sender = sender;
    }
}
