package com.riverview.harrys.objects;

public class BeanFeedbackAttachment  {

    public static final String OBJ_NAME = "BeanFeedbackAttachment";

    private int id, feedback_post_id;
    private String title, description, attachment, attachment_type;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getFeedback_post_id() {
		return feedback_post_id;
	}
	public void setFeedback_post_id(int feedback_post_id) {
		this.feedback_post_id = feedback_post_id;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getAttachment() {
		return attachment;
	}
	public void setAttachment(String attachment) {
		this.attachment = attachment;
	}
	public String getAttachment_type() {
		return attachment_type;
	}
	public void setAttachment_type(String attachment_type) {
		this.attachment_type = attachment_type;
	}
}
