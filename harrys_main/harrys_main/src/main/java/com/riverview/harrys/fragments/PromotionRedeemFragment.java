package com.riverview.harrys.fragments;

import org.json.JSONException;
import org.json.JSONObject;

import android.graphics.Typeface;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.iapps.libs.helpers.BaseHelper;
import com.riverview.harrys.MainActivity;
import com.riverview.harrys.R;
import com.riverview.harrys.RCGenericFragment;
import com.riverview.harrys.asynctask.AsyncResponse;
import com.riverview.harrys.asynctask.PromotionRedeemAsyncTask;
import com.riverview.harrys.helpers.SharedPref;
import com.riverview.harrys.helpers.Utils;
import com.riverview.harrys.objects.BeanPromotionCode;
import com.riverview.harrys.objects.BeanPromotionItem;
import com.riverview.harrys.objects.ServiceResponse;
import com.riverview.harrys.util.AnaylaticsAppender;
import com.riverview.harrys.util.AppUtil;

public class PromotionRedeemFragment extends RCGenericFragment implements
		AsyncResponse<ServiceResponse<BeanPromotionCode>> {

	private static final String TAG = PromotionRedeemFragment.class
			.getSimpleName();

	private TextView textViewTitle;
	private TextView textInstructions;
	private LinearLayout LLSpaceContainer;
	private LinearLayout LLSpaceContainer2;
	private TextView textPromotionType;
	private TextView textGeneratedCode;

	private View v;

	private BeanPromotionItem promoItem;

	private AnaylaticsAppender mAppender = null;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		promoItem = (BeanPromotionItem) getArguments().getParcelable(
				"promotions");
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		v = inflater.inflate(R.layout.promotion_redeem_fragment_layout,
				container, false);

		textViewTitle = (TextView)v.findViewById(R.id.textViewTitle);
		textInstructions = (TextView)v.findViewById(R.id.textViewContent);
		LLSpaceContainer = (LinearLayout)v.findViewById(R.id.LLSpaceContainer);
		LLSpaceContainer2 =(LinearLayout)v.findViewById(R.id.LLSpaceContainer2);
		textPromotionType = (TextView)v.findViewById(R.id.textPromotionType);
		textGeneratedCode = (TextView)v.findViewById(R.id.textGeneratedCode);
		setHasOptionsMenu(false);
		((MainActivity) getActivity()).setBackActionBar("",View.INVISIBLE,"");

		return v;
	}

	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);

		Utils.setMarginByPercent(getActivity(), LLSpaceContainer, .02f);
		Utils.setMarginByPercent(getActivity(), LLSpaceContainer2, .02f);

		Utils.setTextViewFontSizeBasedOnScreenDensity(getActivity(),
				textPromotionType, Utils.FONT_SMALL_DENSITY_SIZE,
				Typeface.BOLD_ITALIC);
		Utils.setTextViewFontSizeBasedOnScreenDensity(getActivity(),
				textGeneratedCode, Utils.FONT_LARGE_DENSITY_SIZE,
				Typeface.BOLD_ITALIC);
		Utils.setTextViewFontSizeBasedOnScreenDensity(getActivity(),
				textInstructions, Utils.FONT_SMALLER_DENSITY_SIZE);
		Utils.setTextViewFontSizeBasedOnScreenDensity(getActivity(),
				textViewTitle, Utils.FONT_SMALL_DENSITY_SIZE, Typeface.BOLD);

		try {

			StringBuilder sb = new StringBuilder();
			if (promoItem.getDescription() != null
					&& promoItem.getDescription() != "") {
				sb.append(promoItem.getDescription().concat("\n\n"));
			}
			if (!BaseHelper.isEmpty(promoItem.getTerms())) {
				sb.append("Terms and Conditions \n".concat(promoItem.getTerms()));
			}

			textViewTitle.setText(promoItem.getName());
			textInstructions.setText(sb.toString());

			switch (promoItem.getCoupon_title()) {
			case "GENERAL":
				textPromotionType.setText("General");
				break;

			case "BDAY_PACK":
				textPromotionType.setText("Birthday");
				break;

			case "WELCOME_PACK":
				textPromotionType.setText("Welcome");
				break;

			case "PATNER":
				textPromotionType.setText("Partner Campaign");
				break;
			default:
				break;
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	View.OnClickListener clickListeners = new View.OnClickListener() {
		public void onClick(View v) {
		}
	};

	@Override
	public void onResume() {
		// TODO Auto-generated method stub
		super.onResume();

		try {
			mAppender = AnaylaticsAppender.getInstance(getActivity());
			generateVoucherCode();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void generateVoucherCode() {

		int mobileUserId = SharedPref.getInteger(getActivity(),
				SharedPref.USERID_KEY, 0);
		PromotionRedeemAsyncTask mAuthTask = new PromotionRedeemAsyncTask(
				getActivity(), promoItem.getId(), "ANDROID",
				String.valueOf(mobileUserId));
		mAuthTask.asyncResponse = PromotionRedeemFragment.this;
		mAuthTask.execute((Void) null);
	}

	@Override
	public void onDestroy() {

		super.onDestroy();
	}

	@Override
	public void onDetach() {
		super.onDetach();
	}

	@Override
	public void processFinish(ServiceResponse<BeanPromotionCode> details) {

		if (details == null) {
			BaseHelper
					.showAlert(getActivity(), "Generate promotion code error");
			return;
		}

		if (details.getStatus() == 200 && !details.error) {
			Log.i(TAG, "Generate code Success ");

			BeanPromotionCode promotionCode = details.getResponse();
			if (!BaseHelper.isEmpty(promotionCode.getCode())) {
				// set value to text field
				textGeneratedCode.setText(promotionCode.getCode());

				JSONObject attributes = new JSONObject();
				try {
					attributes.put("title", promoItem.getName());
				} catch (JSONException e) {
					e.printStackTrace();
				}

				mAppender.updateAnaylitsData(AppUtil
						.createAnalyticsJsonEventAction("promotion",
								String.valueOf(promoItem.getId()),
								"generate_code", attributes.toString()));
			}

		} else {
			BaseHelper.showAlert(getActivity(),
					"Promotion Code generate error ",
					details.getError_message());
		}
	}
}
