package com.riverview.harrys.adapter;

import java.util.ArrayList;

import android.app.Activity;
import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.iapps.libs.helpers.BaseUIHelper;
import com.riverview.harrys.R;
import com.riverview.harrys.helpers.Utils;
import com.riverview.harrys.objects.BeanPromotionItem;

public class NewsAdapter extends BaseAdapter {
	private ArrayList<BeanPromotionItem> mItems = new ArrayList<BeanPromotionItem>();
	private Activity context;

	public NewsAdapter(Activity context, ArrayList<BeanPromotionItem> list) {
		this.context = context;
		this.mItems = list;
	}

	@Override
	public int getCount() {
		try {
			return mItems.size();
		} catch (Exception e) {
			return 0;
		}
	}

	@Override
	public BeanPromotionItem getItem(int pos) {
		return mItems.get(pos);
	}

	@Override
	public long getItemId(int position) {
		return mItems.get(position).getId();
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ViewHolder holder;

		if (convertView == null) {
			holder = new ViewHolder();
			LayoutInflater inflater = (LayoutInflater) context
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

			convertView = inflater.inflate(R.layout.cell_news_item, parent,
					false);
			holder.LLRoot = (LinearLayout) convertView
					.findViewById(R.id.LLRoot);
			holder.infoStore = (TextView) convertView
					.findViewById(R.id.textViewPromotionContent);
			holder.imgStore = (ImageView) convertView
					.findViewById(R.id.imgStoreItem);
			holder.newsTitle = (TextView) convertView
					.findViewById(R.id.textViewPromotionTitle);
			convertView.setTag(holder);

		} else {
			holder = (ViewHolder) convertView.getTag();
		}

		// set cell height by screen height ratio
		//Utils.setWidthHeightByPercent(context, holder.imgStore, 0.4f, 0.2f);

		Utils.setTextViewFontSizeBasedOnScreenDensity(context,holder.infoStore, 30f);
		Utils.setTextViewFontSizeBasedOnScreenDensity(context,holder.newsTitle, 23f, Typeface.BOLD);

		//Utils.setMarginByPercent(context, holder.LLRoot, 0.01f, true);

		BeanPromotionItem obj = getItem(position);
	    holder.infoStore.setText(obj.getDescription());
		BaseUIHelper.loadImageWithPlaceholder(context, obj.getImageurl(),
				holder.imgStore, R.drawable.placeholder);
		holder.newsTitle.setText(obj.getName());

		return convertView;
	}

	public class ViewHolder {

		TextView infoStore;
		ImageView imgStore;
		TextView newsTitle;
		LinearLayout LLRoot;
	}
}
