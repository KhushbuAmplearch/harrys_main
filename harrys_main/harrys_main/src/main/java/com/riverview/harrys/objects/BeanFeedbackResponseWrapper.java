package com.riverview.harrys.objects;

public class BeanFeedbackResponseWrapper {

    public static final String OBJ_NAME = "BeanFeedbackResponseWrapper";

    private BeanFeedbackPost Post;

	public BeanFeedbackPost getPost() {
		return Post;
	}

	public void setPost(BeanFeedbackPost post) {
		Post = post;
	}

	@Override
	public String toString() {
		return "BeanFeedbackResponseWrapper [Post=" + Post + "]";
	}

  
}
