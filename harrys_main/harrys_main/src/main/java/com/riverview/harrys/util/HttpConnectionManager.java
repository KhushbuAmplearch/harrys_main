package com.riverview.harrys.util;

import java.io.IOException;

import org.apache.http.HttpException;
import org.apache.http.HttpHost;
import org.apache.http.HttpRequest;
import org.apache.http.HttpRequestInterceptor;
import org.apache.http.auth.AuthScheme;
import org.apache.http.auth.AuthScope;
import org.apache.http.auth.AuthState;
import org.apache.http.auth.Credentials;
import org.apache.http.client.CredentialsProvider;
import org.apache.http.client.HttpClient;
import org.apache.http.client.protocol.ClientContext;
import org.apache.http.conn.ClientConnectionManager;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.conn.tsccm.ThreadSafeClientConnManager;
import org.apache.http.params.HttpParams;
import org.apache.http.protocol.ExecutionContext;
import org.apache.http.protocol.HttpContext;

/**
 * Application connection management with server.
 * 
 * @author Wiraj Gunasignhe
 */
public class HttpConnectionManager {

	private static DefaultHttpClient HTTP_CLIENT = null;

	private HttpConnectionManager() {
	}

	static Object LOCK = new Object();

	public static HttpClient getSessionManager() {
		if (HTTP_CLIENT == null) {
			synchronized (LOCK) {
				HTTP_CLIENT = new DefaultHttpClient();
				ClientConnectionManager manager = HTTP_CLIENT
						.getConnectionManager();
				HttpParams params = HTTP_CLIENT.getParams();

				HTTP_CLIENT = new DefaultHttpClient(
						new ThreadSafeClientConnManager(params,
								manager.getSchemeRegistry()), params);
				// HTTP_CLIENT.addRequestInterceptor(new PreemptiveAuth(), 0);
			}
		}
		return HTTP_CLIENT;
	}

	static class PreemptiveAuth implements HttpRequestInterceptor {

		public void process(final HttpRequest request, final HttpContext context)
				throws HttpException, IOException {

			AuthState authState = (AuthState) context
					.getAttribute(ClientContext.TARGET_AUTH_STATE);

			// If no auth scheme avaialble yet, try to initialize it
			// preemptively
			if (authState.getAuthScheme() == null) {
				AuthScheme authScheme = (AuthScheme) context
						.getAttribute("preemptive-auth");
				CredentialsProvider credsProvider = (CredentialsProvider) context
						.getAttribute(ClientContext.CREDS_PROVIDER);
				HttpHost targetHost = (HttpHost) context
						.getAttribute(ExecutionContext.HTTP_TARGET_HOST);
				if (authScheme != null) {
					Credentials creds = credsProvider
							.getCredentials(new AuthScope(targetHost
									.getHostName(), targetHost.getPort()));
					if (creds == null) {
						throw new HttpException(
								"No credentials for preemptive authentication");
					}
					authState.setAuthScheme(authScheme);
					authState.setCredentials(creds);
				}
			}

		}

	}

}
