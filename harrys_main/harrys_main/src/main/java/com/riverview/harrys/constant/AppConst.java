package com.riverview.harrys.constant;

//import com.riverview.test.dto.ImageDto;
/**
 * Created by Wiraj on 9/12/2014.
 */
public class AppConst {

	public static final int SPLASH_TIMEOUT = 2000;
	public static final int PULL_TO_REFRESH_TIMEOUT = 10000;

	public static final int CONNECTION_TIMEOUT = 60000; // 60 seconds
	public static final int DATA_TIMEOUT = 60000; // 120 seconds

	public static final String SHA_ALGORITHM = "HmacSHA1";
	public static final String SHA_ALGORITHM_1 = "HmacSHA256";
	public static final String ENCODING_UTF_8 = "UTF-8";

	//public static final String GOOGLE_PROJECT_NUMBER = "310679783291";//AIzaSyDex7f5LYNK2W1C_EBlbr_Av_JK5LnVexY
	
	//created from harrys mail
	public static final String GOOGLE_PROJECT_NUMBER = "984616884521"; //AIzaSyDdLnfcNGEzN6LzT513yimDBpjyV3eZFo0
	
	//for GCM notification test (only for test)
	//public static final String GOOGLE_PROJECT_NUMBER = "509952815575";

	public static final String NOTIFICATON_ID = "notificationId";
	public static final String NOTIFICATON_BROADCAST = "com.riverview.harrys.notification.broadcast";

	 //public static final String SERVICE_API_KEY =
	// "fa2bc4d6e579b8ba4a443953264a4489ceea7842";
	// public static final String SERVICE_PRIVATE_KEY =
	// "7c1196b38169cc1bf2a18eb2db3b337aad9f914f8c0a107f613102ebba808a7feada3124d4dba606b2f6eb49829cf158f47c1c473d0fd0afec379e4479360a40";

	//public static final String SERVICE_API_KEY = "62231adad05a7b55ad31d54f372534fcfaaaa2ca";
	//public static final String SERVICE_PRIVATE_KEY = "e71f604cf9426d6f11bedae13fdda9022d7918ff50823b1a5d9d54f7e0ff0303";
	//public static final String SERVICE_END_POINT = "https://primary-harrys.riverviewms.com/";
	
	//UAT
    /*public static final String SERVICE_END_POINT = "http://uat.riverviewms.com/";
    public static final String SERVICE_API_KEY = "44d33b66598bbe77e85e4f2c9e16164efd1864a8";
    public static final String SERVICE_PRIVATE_KEY = "8b274ef58c35ae31525b47fff3648d74bd99ebe2d37f43e670d0753596124721";*/

    
    //DEV
   /* public static final String SERVICE_END_POINT = "https://dev.riverviewms.com/";
    public static final String SERVICE_API_KEY = "cc4891395009901f8b5517c82cbd931749ec433c";
    public static final String SERVICE_PRIVATE_KEY = "572f88d1f53f144a784f2e74fd20cd066e9ad67839923caf108b991aa89b299b";*/
   
	
	//public static final String GOOGLE_PROJECT_NUMBER = "596455325325";
	//public static final String GOOGLE_PROJECT_NUMBER = "57357209538";
    
	//LIVE
	public static final String SERVICE_END_POINT = "https://primary-harrys.riverviewms.com/";
    public static final String SERVICE_API_KEY = "62231adad05a7b55ad31d54f372534fcfaaaa2ca";
    public static final String SERVICE_PRIVATE_KEY = "e71f604cf9426d6f11bedae13fdda9022d7918ff50823b1a5d9d54f7e0ff0303";

	//Demo V2
	//public static final String SERVICE_END_POINT = "http://demov2.riverviewms.com/";
	//public static final String SERVICE_API_KEY = "2d6cef8448b1a8d3169dfb91749c06a6fb7613e7";
	//public static final String SERVICE_PRIVATE_KEY = "470ada2e2d92e96a71d0573b4c708ebfeeda539669bbb1e97856265a47a130f3";
	
	//Demo QA2
		/*public static final String SERVICE_END_POINT = "https://qa2.riverviewms.com/";
		public static final String SERVICE_API_KEY = "a2adf754ff62a6d693888f96926d5ba90603cc7e";
	    public static final String SERVICE_PRIVATE_KEY = "c196df5cee6758dc8a147e413e5ccce5b00dd7231cc39827c7d857c535e664d2";*/
	
	//Staging V2
//khushbu add comment for live
	/*public static final String SERVICE_END_POINT = "https://staging.riverviewms.com/";
	public static final String SERVICE_API_KEY = "0725fa1afaa9f024040118e1fc3763ed55e28323";
	public static final String SERVICE_PRIVATE_KEY = "5981616a7c8b8fca4686602e8480ef9f3ba82e0fcbe8d8022f4ee3716f0cfdaa";
*/
	 //public static final String SERVICE_END_POINT =
	// "http://dev.riverviewms.com/";

	// public static final String SERVICE_END_POINT =
	// "http://staging.riverviewms.com/";
    
    //AWS Analytics Configuration Keys
    /*public static String topicArn = "arn:aws:sns:ap-southeast-1:272397067126:Analytic-Worker";
    public static String secretKey = "ctWzcP2JhsL30yq1mJ1tt4jTXguG0WXAwqzE9nj1";
    public static String accessKey = "AKIAIJD7RBV3VSTKEI6Q";*/

	// Primary
	// public static final String SERVICE_API_KEY =
	// "f50218e40698a8fa6f0ffd1512ca67ef247c043c";
	// public static final String SERVICE_PRIVATE_KEY =
	// "7219918730127af114d48b44406a2240f47cb842baf7ec63e804e40362ff20ea";

	// public static final String SERVICE_END_POINT =
	// "https://primary.riverviewms.com/";
    
	//UAT-HTTP
    /*public static final String SERVICE_END_POINT = "http://uat.riverviewms.com/";
    public static final String SERVICE_API_KEY = "44d33b66598bbe77e85e4f2c9e16164efd1864a8";
    public static final String SERVICE_PRIVATE_KEY = "8b274ef58c35ae31525b47fff3648d74bd99ebe2d37f43e670d0753596124721";
*/
	public static final String HARRYS_OTP_HEADER = "HarrysBar";
	public static final String HARRYS_OTP_ACTION = "com.riverview.harrys.otp.sms.action";

	public static final String URL_SIGN_IN = "api/signin";
	public static final String URL_SIGN_UP = "api/signup";
	public static final String URL_PROMOTIONS = "api/promotions";
	public static final String URL_COUPON_PROMOTIONS = "api/promotions/filter/coupon";
	public static final String URL_OUTLETS = "api/outlets";
	public static final String URL_PRODUCTS = "api/products";
	public static final String URL_PRODUCTS_CATEGORIES = "api/product/categories";
	public static final String URL_PRODUCTS_CATEGORY_DATA = "api/product/categories/%s";
	public static final String URL_DEVICES = "api/devices";
	public static final String URL_MERCHANTS = "api/merchants";
	public static final String URL_EVENTS = "api/events";
	public static final String URL_EVENTS_CATEGORIES = "api/events/categories";
	public static final String URL_EVENTS_BUILDER_CATEGORIES = "api/eventbuilder/categories";
	public static final String URL_PREFERENCES = "api/preferences";

	public static final String URL_RESET_PASSWORD = "api/mobileusers/%s/reset_password";
	public static final String URL_RESEND_OTP = "api/users/%s/resend_otp";
	public static final String URL_VERIFY_OTP = "api/verify/%s";
	
	public static final String URL_GET_REFERRALS = "api/referrals/user/%s/type/%s/campaigns";
	public static final String URL_GET_REFERRALS_WITH_CODE = "api/referrals/user/%s/type/%s/campaigns/%s";

	public static final String URL_NEWS = "api/news/%s";
	public static final String URL_NEWS_BUILDER = "api/newsbuilder";
	public static final String URL_NEWS_CONTENT = "api/newsbuilder/content/%s";

	public static final String URL_COUPON_VOUCHER_PROMOTIONS = "api/promotions/filter/coupon,voucher";

	public static final String URL_GET_OFFERS = "api/offers";
	public static final String URL_GET_OFFER_CONTENT = "api/offers/content/%s";
	public static final String URL_GET_OFFER_GENCODE = "api/offers/generatecode/%s";
	
	public static final String URL_GET_EVENT_CONTENT = "api/eventbuilder/content/%s";
	
	public static final String URL_COUPON_REDEEM = "api/offers/generatecode/%s";
	public static final String URL_LOYALTY_MOBILEUSER = "api/loyalty/mobileuser/%s";
	public static final String URL_MOBILE_USERDATA = "api/loyalty/mobileuser/%s";
	public static final String URL_CREATE_LOYALTY_USER = "api/loyalty/user/create";
	public static final String URL_PROMOTOIONS_FILTER = "api/promotions";
	public static final String URL_PROMOTOIONS_NONE = "api/promotions/filter/none";
	public static final String URL_PROMOTOIONS_VOUCHER = "api/promotions/filter/voucher";
	public static final String URL_PROMOTOIONS_COUPON = "api/promotions/filter/coupon";

	public static final String URL_PASSWORD_CHANGE = "api/mobileusers/%s/password";

	public static final String URL_MOBILE_USER_UPDATE = "api/mobileusers/%s";

	public static final String URL_MOBILE_USER_TRANSACTIONS = "api/loyalty/mobileuser/transactions/%s/%s";
	public static final String URL_MOBILE_USER_SALES_TRANSACTIONS = "api/loyalty/mobileuser/sales/transactions/%s/%s";
	public static final String URL_UPC = "api/universalproducts";
	public static final String URL_UPC_PRODUCTS = "api/universalproducts/%s/true";
	public static final String URL_UPC_BY_CATAGORY = "api/universalproductsByCategories/%s";
	public static final String URL_UPC_BY_CATAGORY_SUBCATAGORY = "api/universalproductsByCategories/%s/%s";

	public static final String URL_MOBILE_NUMBER_VERIFY = "api/verify_mobile_number_update/%s";
	

	public static final String URL_GET_COMPANY_DATA = "api/company/%s/application/ANDROID";

	public static final String URL_FEEDBACK_BY_USER_ID = "api/feedback/posts/%s";
	public static final String URL_FEEDBACK_REPLY_BY_ID = "api/feedback/replies/%s";
	public static final String URL_FEEDBACK_REPLY_CREATE = "api/feedback/reply-create";
	public static final String URL_FEEDBACK_QUERY_CREATE = "api/feedback/query-create";
	public static final String URL_FEEDBACK_ATTACHEMENTS = "api/feedback/posts-attachments/%s";
	
	
	public static final String URL_GET_FEEDBACK_TYPES = "api/feedback/types";
	public static final String URL_GET_FEEDBACK_CATEGORIES = "api/feedback/categories";
	public static final String URL_GET_FEEDBACK_SUB_CATEGORIES = "api/feedback/sub-categories/%s";

	public static final String URL_ANALYTICES = "http://worker.riverviewms.com/";
	
	public static final String API_GET_ADS = "api/v2/ads";

	public static final String AUTH_VERSION = "1.0";

	public static final String HTTP_METHOD_GET = "GET";
	public static final String HTTP_METHOD_POST = "POST";
	
	public static final String NEWLINE = "\n";

	public static final String ATTR_TAG_USER_NAME = "username";
	public static final String ATTR_TAG_PASSWORD = "password";
	public static final String ATTR_TAG_OLD_PASSWORD = "old_password";
	public static final String ATTR_TAG_CONT_PASSWORD = "password_confirmation";

	public static final String ATTR_TAG_PASSWORD_CONFIR = "password_confirmation";
	public static final String ATTR_TAG_FIRST_NAME = "firstname";
	public static final String ATTR_TAG_SUR_NAME = "surname";
	public static final String ATTR_TAG_MOBILE = "mobilenumber";
	public static final String ATTR_TAG_EMAIL = "email";
	public static final String ATTR_TAG_POSTEL_CODE = "post_code";

	public static final String ATTR_TAG_DOB = "dob";
	public static final String ATTR_TAG_GENDER = "gender";
	public static final String ATTR_TAG_NATIONALITY = "nationality";
	public static final String ATTR_TAG_IDENTIFICATION = "icno";

	public static final String ATTR_TAG_ADDRESS_LINE_1 = "address_line_1";
	public static final String ATTR_TAG_ADDRESS_LINE_2 = "address_line_2";
	
	public static final String ATTR_TAG_REFERRAL_CODE = "code";

	public static final String ATTR_TAG_COMPANY_ID = "company_id";
	public static final String ATTR_TAG_APPLICATION_ID = "application_id";
	public static final String ATTR_TAG_STATUS = "status";
	public static final String ATTR_TAG_DEVICE = "device";
	public static final String ATTR_TAG_TOKEN_ID = "token_id";
	public static final String ATTR_TAG_DEVICE_ID = "UDID";

	public static final String ATTR_TAG_MOBILEUSER_ID = "mobileuser_id";

	public static final String ATTR_TAG_MOBILE_USER_ID = "mobile_user_id";

	public static final String ATTR_TAG_APP_VERSION = "app_version";
	public static final String ATTR_TAG_OS_VERSION = "os_version";

	public static final String ATTR_TAG_AUTH_VERSION = "auth_version";
	public static final String ATTR_TAG_AUTH_KEY = "auth_key";
	public static final String ATTR_TAG_UNIX_TIMESTAMP = "auth_timestamp";
	public static final String ATTR_TAG_AUTH_SIGNATURE = "auth_signature";

	public static final String ATTR_TAG_UDID = "Udid";
	public static final String ATTR_TAG_ID = "id";

	public static final String ATTR_TAG_CONFIRMATION_CODE = "confirmation_code";
	public static final String ATTR_TAG_MOBILE_CONFIRMATION_CODE = "code";

	public static final String ATTR_TAG_CARD_NUMBER = "card_number";

	public static final String ATTR_TAG_PAGE = "page";
	public static final String ATTR_TAG_LIMIT = "limit";
	public static final String ATTR_TAG_WITH = "with";
	
    public static final String ATTR_TAG_FB_POST_ID = "feedback_post_id";
    public static final String ATTR_TAG_FB_REPLY = "reply";
	
	// Shared preference values
	public static final String P_FILE_USER_CREDENTIALS = "USER_DETAILS";

	public static final String PREFS_APP_USERNAME = "APP_USERNAME";
	public static final String PREFS_APP_PASSWORD = "APP_PASSWORD";
	public static final String PREFS_APP_MERCHANT_DATA = "APP_MERCHANT_DATA";

	public static final String TAG_MERCHANTS = "merchants";
	public static final String TAG_ID = "id";
	public static final String TAG_COMPANY_ID = "company_id";
	public static final String TAG_APPLICATION_ID = "application_id";
	public static final String TAG_CATAGORY_ID = "category_id";
	public static final String TAG_SUB_CATAGORY_ID = "subcategory_id";
	public static final String TAG_IMAGES = "image";
	public static final String TAG_TITLE = "title";
	public static final String TAG_DESCRIPTION = "description";
	public static final String TAG_DETAILED_DESCRIPTION = "detailed_description";
	public static final String TAG_URL = "url";

	public static final String TAG_IMAGE_ID = "id";
	public static final String TAG_USER_ID = "user_id";
	public static final String TAG_IMAGABLE_ID = "imageable_id";
	public static final String TAG_IMAGABLE_TYPE = "imageable_type";
	public static final String TAG_FILE_NAME = "filename";
	public static final String TAG_CREATED_AT = "created_at";
	public static final String TAG_UPDATED_AT = "updated_at";
	public static final String TAG_ORIGINAL = "original";
	public static final String TAG_MEDIUM = "medium";
	public static final String TAG_SMALL = "small";
	public static final String TAG_THUMB = "thumb";
	public static final String TAG_FILE_TYPE = "filetype";
	public static final String TAG_FILE_SIZE = "filesize";
	
	public static final String TAG_REF_ID = "referral_code";
	
	public static final String TAG_FEEDBACK_TYPE = "feedback_type";
	public static final String TAG_FEEDBACK_CATEGORY_TYPE = "feedback_category_id";
	public static final String TAG_FEEDBACK_SUB_CATEGORY_TYPE = "feedback_sub_category_id";
	public static final String TAG_FEEDBACK_TITLE = "feedback_title";
	public static final String TAG_FEEDBACK_POST = "feedback_post";
	public static final String TAG_MOBILEUSERID = "mobileuser_id";
	public static final String TAG_FILE = "file%s";
	
	public static final int OTP_VERIFIED = 1;
	public static final int OTP_NOT_VERIFIED = 0;

	public static final int ANALYTICS_SERVICE_SCHEDULAR_INTERVAL_SECONDS = 120;
	public static final String ACTION_PROCESS_ANALYTICS = "harrys.analytics.task.start";
	
	

}
