package com.riverview.harrys.helpers;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;

import com.iapps.libs.helpers.BaseHelper;
import com.iapps.libs.helpers.CertHelper;
import com.iapps.libs.helpers.SecureProxy;

public class UserInfoManager {

	private final String KEY_ACCESS_TOKEN = "HJ789";
	private final String KEY_ACCOUNT_ID = "GDHJ7";

	private static UserInfoManager _userInfo = null;
	private static String FILE_NAME = "gopanel_user_sec";
	private String accessToken;
	private String accountId;
	private SharedPreferences prefs;
	private SecureProxy security;
	private static Context ctx;

	private UserInfoManager() {
		super();
	}

	public static UserInfoManager getInstance(Context c) {
		if (_userInfo == null) {
			ctx = c;
			_userInfo = new UserInfoManager();
			_userInfo.openPrefs(c.getApplicationContext());
			_userInfo.openSecurity(c);
		}

		return _userInfo;
	}

	private void openPrefs(Context c) {
		this.prefs = c.getSharedPreferences(FILE_NAME, Context.MODE_PRIVATE);
	}

	private void openSecurity(Context c) {
		CertHelper ch = new CertHelper();
		@SuppressWarnings("static-access")
		String p = ch.getSHA1Fingerprint(c.getPackageManager(),
				c.getPackageName());
		this.security = new SecureProxy(p);
	}

	public void saveUserId(String accountId) {
		this.accountId = security.encrypt(accountId);
		SharedPreferences.Editor editor = this.prefs.edit();
		editor.putString(KEY_ACCOUNT_ID, this.accountId);
		editor.commit();
	}

	public void saveUserInfo(String accessToken, String accountId) {
		this.accessToken = security.encrypt(accessToken);
		this.accountId = security.encrypt(accountId);
		SharedPreferences.Editor editor = this.prefs.edit();
		editor.putString(KEY_ACCESS_TOKEN, this.accessToken);
		editor.putString(KEY_ACCOUNT_ID, this.accountId);
		editor.commit();
	}

	public void saveUserInfo(String accessToken) {
		this.accessToken = security.encrypt(accessToken);
		SharedPreferences.Editor editor = this.prefs.edit();
		editor.putString(KEY_ACCESS_TOKEN, this.accessToken);
		editor.commit();
	}

	public String getAuthToken() {
		if (accessToken == null) {
			this.accessToken = this.prefs.getString(KEY_ACCESS_TOKEN, null);
		}
		return security.decrypt(accessToken);
	}

	public String getAccountId() {
		if (accountId == null) {
			this.accountId = this.prefs.getString(KEY_ACCOUNT_ID, null);
		}
		return security.decrypt(accountId);
	}

	public void logout(Activity act) {
		try {
			SharedPref.getPref(act).edit().clear().commit();
			prefs.edit().clear().commit();
			_userInfo = null;
		} catch (Exception e) {
		}
	}
	
	public void logout(Context act) {
		try {
			SharedPref.getPref(act).edit().clear().commit();
			prefs.edit().clear().commit();
			_userInfo = null;
		} catch (Exception e) {
		}
	}

	public boolean isSignedIn(Activity act) {
		// if (this.getAccountId() != null && this.getAuthToken() != null) {
		// return true;
		// }
		// return false;
		return BaseHelper.isEmpty(SharedPref.getString(act,
				SharedPref.USERNAME_KEY, ""))
				| !SharedPref.getBoolean(act, SharedPref.VERIFY_KEY, false);

	}
	
	public boolean isSignedIn(Context act) {
		// if (this.getAccountId() != null && this.getAuthToken() != null) {
		// return true;
		// }
		// return false;
		SharedPreferences sf = SharedPref.getPref(act);
		return BaseHelper.isEmpty(sf.getString(
				SharedPref.USERNAME_KEY, ""))
				| !sf.getBoolean(SharedPref.VERIFY_KEY, false);

	}

}