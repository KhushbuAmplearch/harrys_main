package com.riverview.harrys.fragments;

import java.util.ArrayList;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.daimajia.slider.library.SliderLayout;
import com.daimajia.slider.library.SliderTypes.BaseSliderView;
import com.daimajia.slider.library.SliderTypes.TextSliderView;
import com.riverview.harrys.MainActivity;
import com.riverview.harrys.R;
import com.riverview.harrys.RCGenericFragment;

public class StoreImageGallaryFragment extends RCGenericFragment implements
		BaseSliderView.OnSliderClickListener {

	private static final String TAG = StoreImageGallaryFragment.class
			.getSimpleName();

	private SliderLayout sliderLayout;
	private Button buttonBack;

	private View v;

	private ArrayList<String> storeItem;
	private String storeName;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		getActivity().getActionBar().hide();
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		v = inflater.inflate(R.layout.store_images_fragment_layout, container,
				false);

		sliderLayout = (SliderLayout)v.findViewById(R.id.slider);
		buttonBack = (Button)v.findViewById(R.id.buttonBack);
		setHasOptionsMenu(false);
		storeItem = getArguments().getStringArrayList("photo_details");
		storeName = getArguments().getString("name");

		return v;
	}

	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
		inflater.inflate(R.menu.menu_location, menu);
	}

	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);

		buttonBack.setOnClickListener(clickListeners);

		for (String url : storeItem) {

			TextSliderView textSliderView = new TextSliderView(getHome());
			// initialize a SliderLayout
			textSliderView.description(storeName).image(url)
					.setScaleType(BaseSliderView.ScaleType.Fit)
					.setOnSliderClickListener(this);

			// add your extra information
			textSliderView.getBundle().putString("extra", "Extra information");

			sliderLayout.addSlider(textSliderView);
		}
		// sliderLayout.setPresetTransformer(SliderLayout.Transformer.Stack);
		// sliderLayout
		// .setPresetIndicator(SliderLayout.PresetIndicators.Center_Bottom);
		// sliderLayout.setCustomAnimation(new DescriptionAnimation());
		// sliderLayout.setDuration(4000);

	}

	View.OnClickListener clickListeners = new View.OnClickListener() {
		public void onClick(View v) {

			switch (v.getId()) {
			case R.id.buttonBack:
				((MainActivity) getActivity()).onBackPressed();
				break;

			default:
				break;
			}
		}
	};

	@Override
	public void onSliderClick(BaseSliderView slider) {
		// TODO Auto-generated method stub

	}

}
