package com.riverview.harrys.adapter;

import java.text.DecimalFormat;
import java.util.ArrayList;

import android.app.Activity;
import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.iapps.libs.helpers.BaseHelper;
import com.riverview.harrys.R;
import com.riverview.harrys.helpers.Utils;
import com.riverview.harrys.objects.BeanSalesTransaction;


public class TransactionsAdapter extends BaseAdapter {

	private static final String TAG = TransactionsAdapter.class.getSimpleName();

	private ArrayList<BeanSalesTransaction> mItems = new ArrayList<BeanSalesTransaction>();

	private Activity context;


	private LinearLayout LLRoot;

	public TransactionsAdapter(Activity context,
			ArrayList<BeanSalesTransaction> list) {
		this.context = context;

		this.mItems = list;
	}

	@Override
	public int getCount() {
		try {
			return mItems.size();
		} catch (Exception e) {
			return 0;
		}
	}

	@Override
	public BeanSalesTransaction getItem(int pos) {
		return mItems.get(pos);
	}

	@Override
	public long getItemId(int position) {
		return mItems.get(position).getId();
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ViewHolder holder;

		if (convertView == null) {
			holder = new ViewHolder();
			LayoutInflater inflater = (LayoutInflater) context
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

			convertView = inflater.inflate(R.layout.cell_transactions_item,
					parent, false);
			holder.transId = (TextView) convertView
					.findViewById(R.id.textViewTransId);
			holder.transDate = (TextView) convertView
					.findViewById(R.id.textViewTransDate);
			holder.transOutlet = (TextView) convertView
					.findViewById(R.id.textViewOutlet);
			// holder.transType = (TextView) convertView
			// .findViewById(R.id.textViewType);
			holder.transAmount = (TextView) convertView
					.findViewById(R.id.textViewAmount);
			holder.LLRoot = (LinearLayout) convertView
					.findViewById(R.id.LLRoot);

			convertView.setTag(holder);

		} else {
			holder = (ViewHolder) convertView.getTag();
		}

		Utils.setMarginByPercent(context, holder.LLRoot, 0.025f, true);

		Utils.setTextViewFontSizeBasedOnScreenDensity(context, holder.transId,
				32f);
		Utils.setTextViewFontSizeBasedOnScreenDensity(context,
				holder.transDate, 30f);
		Utils.setTextViewFontSizeBasedOnScreenDensity(context,
				holder.transOutlet, 30f);
		Utils.setTextViewFontSizeBasedOnScreenDensity(context,
				holder.transAmount, 30f, Typeface.BOLD);

		BeanSalesTransaction obj = getItem(position);
		holder.transId.setText(String.valueOf(obj.getReference()));
		holder.transDate.setText(obj.getCreated_at());
		if (obj.getOutlet_name() != null) {
			holder.transOutlet.setText(obj.getOutlet_name());
		}
		if (!BaseHelper.isEmpty(obj.getDiscounted_total())) {			
			Double val = Double.valueOf(obj.getDiscounted_total());

			// Define decimal format
			DecimalFormat decimalFormat = new DecimalFormat("0.00");
			
			String formattedWalletBalance =  decimalFormat.format(val);
			holder.transAmount.setText(String.valueOf(formattedWalletBalance));
		}

		return convertView;
	}

	public class ViewHolder {

		TextView transId;
		TextView transDate;
		TextView transOutlet;
		// TextView transType;
		TextView transAmount;
		LinearLayout LLRoot;
	}
}
