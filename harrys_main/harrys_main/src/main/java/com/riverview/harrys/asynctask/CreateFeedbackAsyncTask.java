package com.riverview.harrys.asynctask;

import static com.riverview.harrys.constant.AppConst.ATTR_TAG_DEVICE_ID;
import static com.riverview.harrys.constant.AppConst.TAG_FEEDBACK_TYPE;
import static com.riverview.harrys.constant.AppConst.TAG_FEEDBACK_CATEGORY_TYPE;
import static com.riverview.harrys.constant.AppConst.TAG_FEEDBACK_SUB_CATEGORY_TYPE;
import static com.riverview.harrys.constant.AppConst.TAG_FEEDBACK_TITLE;
import static com.riverview.harrys.constant.AppConst.TAG_FEEDBACK_POST;
import static com.riverview.harrys.constant.AppConst.TAG_MOBILEUSERID;
import static com.riverview.harrys.constant.AppConst.TAG_FILE;
import static com.riverview.harrys.constant.AppConst.HTTP_METHOD_POST;
import static com.riverview.harrys.constant.AppConst.SERVICE_END_POINT;
import static com.riverview.harrys.constant.AppConst.URL_FEEDBACK_QUERY_CREATE;

import java.io.IOException;
import java.util.ArrayList;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.type.TypeReference;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.core.io.FileSystemResource;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.client.ClientHttpResponse;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.DefaultResponseErrorHandler;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.riverview.harrys.R;
import com.riverview.harrys.exception.RestServiceException;
import com.riverview.harrys.objects.BeanFeedbackResponseWrapper;

import com.riverview.harrys.objects.ErrorServiceResponse;
import com.riverview.harrys.objects.ServiceResponse;
import com.riverview.harrys.util.AppUtil;
import com.riverview.harrys.util.WebServiceUtil;

import fr.castorflex.android.circularprogressbar.CircularProgressDrawable;

public class CreateFeedbackAsyncTask extends
		AsyncTask<Void, Void, ServiceResponse<BeanFeedbackResponseWrapper>> {

	private static final String TAG = CreateFeedbackAsyncTask.class
			.getSimpleName();

	public AsyncResponse<ServiceResponse<BeanFeedbackResponseWrapper>> asyncResponse;

	private ProgressDialog mProgressDialog;

	private Context mContext;
	private String mFeedbackType = null;
	private String mFeedbackTitle = null;
	private String mFeedbackPost = null;
	private int mCategoryId = 0;
	private int mSubCategoryId = 0;
	private int mMobileUserId = 0;
	private ArrayList<String> mResourceArray;

	String[] commandString = null;
	ArrayList<NameValuePair> httpParams = null;

	public CreateFeedbackAsyncTask(Context context, String type, String title,
			String post, int catId, int subCatId, int userId,
			ArrayList<String> res) {
		this.mContext = context;
		this.mFeedbackType = type;
		this.mFeedbackTitle = title;
		this.mFeedbackPost = post;
		this.mCategoryId = catId;
		this.mSubCategoryId = subCatId;
		this.mMobileUserId = userId;
		this.mResourceArray = res;
	}

	@Override
	protected void onPreExecute() {
		Log.d(TAG, "onPreExecute");
		super.onPreExecute();

		mProgressDialog = new ProgressDialog(mContext);
		mProgressDialog
				.setIndeterminateDrawable(new CircularProgressDrawable.Builder(
						mContext)
						.colors(mContext.getResources().getIntArray(
								R.array.harrys_colors)).sweepSpeed(1f)
						.strokeWidth(12f)
						.style(CircularProgressDrawable.Style.ROUNDED).build());

		mProgressDialog.setCancelable(false);
		mProgressDialog.setMessage("Creating feedback...");
		mProgressDialog.show();

		try {
			ArrayList<NameValuePair> arrayList = new ArrayList<NameValuePair>();

			arrayList.add(new BasicNameValuePair(TAG_FEEDBACK_TYPE,
					mFeedbackType));
			if (mCategoryId != 0) {
				arrayList
						.add(new BasicNameValuePair(TAG_FEEDBACK_CATEGORY_TYPE,
								String.valueOf(mCategoryId)));
			}
			// if (mSubCategoryId != 0) {
			arrayList.add(new BasicNameValuePair(
					TAG_FEEDBACK_SUB_CATEGORY_TYPE, String
							.valueOf(mSubCategoryId)));
			// }
			arrayList.add(new BasicNameValuePair(TAG_FEEDBACK_TITLE,
					mFeedbackTitle));
			arrayList.add(new BasicNameValuePair(TAG_FEEDBACK_POST,
					mFeedbackPost));
			arrayList.add(new BasicNameValuePair(TAG_MOBILEUSERID, String
					.valueOf(mMobileUserId)));

			httpParams = AppUtil.generateAuthCommandString(arrayList,
					URL_FEEDBACK_QUERY_CREATE, HTTP_METHOD_POST);

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	@Override
	protected void onPostExecute(
			ServiceResponse<BeanFeedbackResponseWrapper> response) {
		Log.d(TAG, "onPostExecute");

		if (mProgressDialog != null && mProgressDialog.isShowing()) {
			mProgressDialog.dismiss();
		}
		asyncResponse.processFinish(response);
	}

	@Override
	protected ServiceResponse<BeanFeedbackResponseWrapper> doInBackground(
			Void... voids) {
		Log.d(TAG, "doInBackground");

		ServiceResponse<BeanFeedbackResponseWrapper> serviceResponse = null;

		RestTemplate restTemplate = WebServiceUtil.getRestTemplateInstance();

		restTemplate.setErrorHandler(new DefaultResponseErrorHandler() {

			@Override
			public void handleError(ClientHttpResponse response)
					throws IOException {
				ObjectMapper mapper = WebServiceUtil.getErrorObjMapper();
				ErrorServiceResponse errorRes;
				errorRes = mapper.readValue(response.getBody(),
						new TypeReference<ErrorServiceResponse>() {
						});

				throw new RestServiceException(errorRes, errorRes
						.getError_message());

			}

			protected boolean hasError(HttpStatus statusCode) {
				Log.d(TAG, "Error ----> " + statusCode);
				if (statusCode.value() == 200) {
					return false;
				}
				return true;
			}
		});

		String url = String.format(SERVICE_END_POINT + "%s",
				URL_FEEDBACK_QUERY_CREATE);

		HttpHeaders requestHeaders = new HttpHeaders();
		requestHeaders.setContentType(MediaType.MULTIPART_FORM_DATA);
		requestHeaders.set("Accept", "application/json");
		requestHeaders.add(ATTR_TAG_DEVICE_ID, AppUtil.getDeviceId(mContext));

		MultiValueMap<String, Object> map = new LinkedMultiValueMap<String, Object>();
		for (NameValuePair a : httpParams) {
			map.add(a.getName(), a.getValue());
		}

		if (mResourceArray != null && mResourceArray.size() > 0) {

			for (int i = 0; i < mResourceArray.size(); i++) {
				
				map.add(String.format(TAG_FILE, (i + 1)),
						new FileSystemResource(mResourceArray.get(i)));
			}
		}

		HttpEntity<MultiValueMap<String, Object>> requestEntity = new HttpEntity<MultiValueMap<String, Object>>(
				map, requestHeaders);

		Log.d(TAG, "Url " + url);
		Log.d(TAG, "Request Entity " + requestEntity.toString());

		try {
			serviceResponse = restTemplate
					.exchange(
							url,
							HttpMethod.POST,
							requestEntity,
							new ParameterizedTypeReference<ServiceResponse<BeanFeedbackResponseWrapper>>() {
							}).getBody();
			Log.d(TAG, "Sssss ---->> " + serviceResponse.getStatus());
		} catch (RestClientException e) {
			serviceResponse = new ServiceResponse<BeanFeedbackResponseWrapper>();
			serviceResponse.setError(true);
			serviceResponse.setError_message(e.getCause().getMessage());

		} catch (Exception e) {
			e.printStackTrace();
		}
		return serviceResponse;
	}

	@Override
	protected void onCancelled() {

		if (mProgressDialog != null && mProgressDialog.isShowing()) {
			mProgressDialog.dismiss();
			asyncResponse.processFinish(null);
		}
	}
}
