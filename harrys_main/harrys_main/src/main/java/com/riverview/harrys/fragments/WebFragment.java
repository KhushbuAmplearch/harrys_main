package com.riverview.harrys.fragments;

import android.graphics.Color;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.riverview.harrys.MainActivity;
import com.riverview.harrys.R;
import com.riverview.harrys.RCGenericFragment;

public class WebFragment extends RCGenericFragment {

	private final int TAG_PRIVACY_POLICY = 0, TAG_TNC = 1, TAG_CUSTOM_URL = 2;

	private WebView wv;

	private View v;
	private int whichPage;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		v = getActivity().getLayoutInflater().inflate(
				R.layout.web_fragment_layout, container, false);
		wv = (WebView)v.findViewById(R.id.wv);

		setHasOptionsMenu(false);
		String title = getArguments().getString("title");
		whichPage = getArguments().getInt("whichpage");
		if (whichPage == 0) {
			((MainActivity) getActivity()).setBackActionBar(title,View.INVISIBLE,"");
		} else if (whichPage == 1) {
			((MainActivity) getActivity()).setBackActionBar(title,View.INVISIBLE,"");
		} else {
			((MainActivity) getActivity()).setBackActionBar(title,View.INVISIBLE,"");
		}
		return v;
	}

	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);

		wv.setBackgroundColor(Color.TRANSPARENT);
		wv.setLayerType(WebView.LAYER_TYPE_SOFTWARE, null);

		if (whichPage == TAG_PRIVACY_POLICY) {
			wv.loadUrl("http://harryscard.com.sg/faq-page#n403");
		} else if (whichPage == TAG_TNC) {
			String pdf = "http://harryscard.com.sg/sites/hc/files/page/attachment/TCs_19Mar18.pdf";
			wv.loadUrl("http://docs.google.com/viewer?embedded=true&url="+pdf);
			wv.setWebViewClient(new MyBrowser());
			WebSettings webSettings = wv.getSettings();
			webSettings.setJavaScriptEnabled(true);
		} else if (whichPage == TAG_CUSTOM_URL) {

		}

	}
	private class MyBrowser extends WebViewClient {
		@Override
		public boolean shouldOverrideUrlLoading(WebView view, String url) {
			view.loadUrl(url);
			return true;
		}
	}

	View.OnClickListener clickListeners = new View.OnClickListener() {
		public void onClick(View v) {

		}
	};

}
