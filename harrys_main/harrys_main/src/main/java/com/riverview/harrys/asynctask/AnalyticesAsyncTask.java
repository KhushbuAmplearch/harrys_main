package com.riverview.harrys.asynctask;

import static com.riverview.harrys.constant.AppConst.URL_ANALYTICES;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.UnsupportedEncodingException;
import java.net.URI;
import java.util.ArrayList;

import org.apache.http.NameValuePair;
import org.apache.http.client.utils.URLEncodedUtils;
import org.apache.http.message.BasicNameValuePair;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.type.TypeReference;
import org.json.JSONObject;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.client.ClientHttpResponse;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.DefaultResponseErrorHandler;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.util.Log;

import com.riverview.harrys.constant.AppConst;
import com.riverview.harrys.exception.RestServiceException;
import com.riverview.harrys.helpers.SharedPref;
import com.riverview.harrys.objects.AnalyticesResponse;
import com.riverview.harrys.objects.ErrorServiceResponse;
import com.riverview.harrys.util.WebServiceUtil;

public class AnalyticesAsyncTask extends
		AsyncTask<Void, Void, AnalyticesResponse> {

	private static final String TAG = AnalyticesAsyncTask.class.getSimpleName();

	String[] commandString = null;
	ArrayList<NameValuePair> httpParams = null;

	private String finalUrl = null;
	private JSONObject object;
	private Context mContext = null;

	public AnalyticesAsyncTask(Context context, JSONObject jsonObject) {
		this.mContext = context;
		this.object = jsonObject;
	}

	@Override
	protected void onPreExecute() {
		Log.d(TAG, "onPreExecute");
		super.onPreExecute();

		try {
			httpParams = new ArrayList<NameValuePair>();
			httpParams.add(new BasicNameValuePair("api_key",
					AppConst.SERVICE_API_KEY));
			httpParams.add(new BasicNameValuePair("data", object.toString()));

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	@Override
	protected void onPostExecute(AnalyticesResponse response) {
		Log.d(TAG, "onPostExecute");

		if (response == null) {
			Log.d(TAG, "Fail");
			return;
		}

		try {
			if (response != null && response.ststus != null
					&& Integer.valueOf(response.ststus) == 200) {
				Log.d(TAG, "Success. Delete Old data " + response.message);

				try {
					SharedPreferences preferences = SharedPref
							.getPref(mContext);
					preferences.edit().remove(SharedPref.ANALYTICS_JSON_ID_KEY)
							.apply();
				} catch (Exception e) {
					Log.d(TAG, "Shared Pref Exception " + e.getMessage());
				}

			} else {
				Log.e(TAG, "Fail " + response.getStstus());
				Log.e(TAG, "Fail " + response.getMessage());
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	protected AnalyticesResponse doInBackground(Void... voids) {
		Log.d(TAG, "doInBackground");

		AnalyticesResponse serviceResponse = null;

		RestTemplate restTemplate = WebServiceUtil.getRestTemplateInstance();

		restTemplate.setErrorHandler(new DefaultResponseErrorHandler() {
			@Override
			public void handleError(ClientHttpResponse response)
					throws IOException {
				ObjectMapper mapper = WebServiceUtil.getErrorObjMapper();
				ErrorServiceResponse errorRes;
				errorRes = mapper.readValue(response.getBody(),
						new TypeReference<ErrorServiceResponse>() {
						});

				throw new RestServiceException(errorRes, errorRes
						.getError_message());
			}

			protected boolean hasError(HttpStatus statusCode) {
				Log.d(TAG, "Status ----> " + statusCode);
				if (statusCode.value() == 200) {
					return false;
				}
				return true;
			}
		});

		HttpHeaders requestHeaders = new HttpHeaders();
		requestHeaders.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
		requestHeaders.set("Accept", "application/json");

		HttpEntity<MultiValueMap<String, String>> requestEntity = new HttpEntity<MultiValueMap<String, String>>(
				null, requestHeaders);

		String urlParams = URLEncodedUtils.format(httpParams, "UTF-8");

		finalUrl = String.format(URL_ANALYTICES + "?" + "%s", urlParams);

		try {
			serviceResponse = restTemplate.exchange(new URI(finalUrl),
					HttpMethod.GET, requestEntity,
					new ParameterizedTypeReference<AnalyticesResponse>() {
					}).getBody();

		} catch (RestClientException e) {
			e.printStackTrace();
			serviceResponse = new AnalyticesResponse();
			serviceResponse.setStstus("1000");
			serviceResponse.setMessage(e.getMessage());

		} catch (Exception e) {
			Log.e(TAG, "Exception " + e.getMessage());
			e.printStackTrace();
		}
		return serviceResponse;
	}

	public String readIt(InputStream stream, int len) throws IOException,
			UnsupportedEncodingException {
		Reader reader = null;
		reader = new InputStreamReader(stream, "UTF-8");
		char[] buffer = new char[len];
		reader.read(buffer);
		return new String(buffer);
	}

	@Override
	protected void onCancelled() {

		// if (mProgressDialog != null && mProgressDialog.isShowing()) {
		// mProgressDialog.dismiss();
		// asyncResponse.processFinish(null);
		// }
	}
}
