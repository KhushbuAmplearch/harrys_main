package com.riverview.harrys.fragments;

import static com.riverview.harrys.constant.AppConst.OTP_NOT_VERIFIED;
import static com.riverview.harrys.constant.AppConst.OTP_VERIFIED;

import org.json.JSONException;
import org.json.JSONObject;
import android.content.Context;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.iapps.libs.helpers.BaseHelper;
import com.iapps.libs.helpers.BaseHelper.CancelListener;
import com.iapps.libs.helpers.BaseHelper.ConfirmListener;
import com.riverview.harrys.MainActivity;
import com.riverview.harrys.R;
import com.riverview.harrys.RCGenericFragment;
import com.riverview.harrys.asynctask.AdsAsyncTask;
import com.riverview.harrys.asynctask.AsyncResponse;
import com.riverview.harrys.asynctask.DevicesAsyncTask;
import com.riverview.harrys.asynctask.LoginAsyncTask;
import com.riverview.harrys.asynctask.ResendOTPAsyncTask;
import com.riverview.harrys.helpers.SharedPref;
import com.riverview.harrys.helpers.Utils;
import com.riverview.harrys.objects.BeanDevice;
import com.riverview.harrys.objects.BeanLogin;
import com.riverview.harrys.objects.ServiceResponse;
import com.riverview.harrys.util.AnaylaticsAppender;
import com.riverview.harrys.util.AppUtil;

public class LoginFragment extends RCGenericFragment implements
		AsyncResponse<ServiceResponse<BeanLogin>> {

	static final String TAG = LoginFragment.class.getSimpleName();

	LinearLayout LLSignUp;
	LinearLayout LLSignIn;
	TextView textViewUsername;
	EditText editTextUsername;
	TextView textViewPassword;
	EditText editTextPassword;
	TextView textViewSignUp;
	TextView textViewForgotPassword;
	Button buttonSignIn;
	CheckBox chkRememberMe;
	View v;

	AnaylaticsAppender mAppender = null;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		v = inflater.inflate(R.layout.signin_fragment_layout, container, false);
		LLSignUp = (LinearLayout)v.findViewById(R.id.LLSignUp);
		LLSignIn = (LinearLayout)v.findViewById(R.id.LLSignIn);
		textViewUsername = (TextView) v.findViewById(R.id.textViewUsername);
		editTextUsername = (EditText)v.findViewById(R.id.editTextUsername);
		textViewPassword = (TextView) v.findViewById(R.id.textViewPassword);
		editTextPassword = (EditText)v.findViewById(R.id.editTextPassword);

		textViewSignUp = (TextView) v.findViewById(R.id.textViewSignUp);
		textViewForgotPassword = (TextView) v.findViewById(R.id.textViewForgotPassword);
		buttonSignIn = (Button) v.findViewById(R.id.buttonSignIn);
		chkRememberMe = (CheckBox) v.findViewById(R.id.checkBoxRememberMe);

		setHasOptionsMenu(false);
		((MainActivity) getActivity()).setBackActionBar(getHome()
				.getResources().getString(R.string.rc_login_signin),View.INVISIBLE,"");

		hideDefaultKeyboard();

		return v;
	}

	private void hideDefaultKeyboard() {
		try {
			getActivity().getWindow().setSoftInputMode(
					WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

			getActivity();
			InputMethodManager imm = (InputMethodManager) getActivity()
					.getSystemService(Context.INPUT_METHOD_SERVICE);

			imm.hideSoftInputFromWindow(editTextUsername.getWindowToken(), 0);
			imm.hideSoftInputFromWindow(editTextPassword.getWindowToken(), 0);
		} catch (Exception e) {
		}

	}

	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);

		LLSignUp.setOnClickListener(clickListeners);
		LLSignIn.setOnClickListener(clickListeners);

		textViewSignUp.setOnClickListener(clickListeners);
		textViewForgotPassword.setOnClickListener(clickListeners);

		buttonSignIn.setOnClickListener(clickListeners);

		Utils.setTextViewFontSizeBasedOnScreenDensity(getActivity(),
				textViewUsername, Utils.FONT_SMALL_DENSITY_SIZE);
		Utils.setEditTextFontSizeBasedOnScreenDensity(getActivity(),
				editTextUsername, Utils.FONT_SMALL_DENSITY_SIZE);
		Utils.setTextViewFontSizeBasedOnScreenDensity(getActivity(),
				textViewPassword, Utils.FONT_SMALL_DENSITY_SIZE);
		Utils.setEditTextFontSizeBasedOnScreenDensity(getActivity(),
				editTextPassword, Utils.FONT_SMALL_DENSITY_SIZE);
		Utils.setButtonFontSizeBasedOnScreenDensity(getActivity(),
				buttonSignIn, Utils.FONT_SMALL_DENSITY_SIZE, Typeface.BOLD);
		Utils.setTextViewFontSizeBasedOnScreenDensity(getActivity(),
				textViewSignUp, Utils.FONT_SMALL_DENSITY_SIZE, Typeface.BOLD);
		Utils.setTextViewFontSizeBasedOnScreenDensity(getActivity(),
				chkRememberMe, Utils.FONT_SMALL_DENSITY_SIZE, Typeface.BOLD);
		Utils.setTextViewFontSizeBasedOnScreenDensity(getActivity(),
				textViewForgotPassword, Utils.FONT_SMALL_DENSITY_SIZE,
				Typeface.BOLD);

		try {
			mAppender = AnaylaticsAppender.getInstance(getActivity());
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	View.OnClickListener clickListeners = new View.OnClickListener() {
		public void onClick(View v) {

			switch (v.getId()) {
			case R.id.textViewSignUp:
				JSONObject attributes = new JSONObject();
				try {
					attributes.put("title", "signup");
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				mAppender.updateAnaylitsData(AppUtil
						.createAnalyticsJsonEventAction("login", "click",
								"sign_up", attributes.toString()));
				getHome().setFragment(new SignUpFragment());
				break;

			case R.id.buttonSignIn:
				if (AppUtil.checkNetworkConnection(getActivity())) {
					mAppender.updateAnaylitsData(AppUtil
							.createAnalyticsJsonEventAction("login", "click",
									"login", null));
					login();
				} else {
					BaseHelper.confirm(getActivity(), "Network Unavaliable",
							"Please turn on network to connect with server",
							new ConfirmListener() {

								@Override
								public void onYes() {
									AppUtil.showSystemSettingsDialog(getActivity());
								}
							});
				}
				hideDefaultKeyboard();

				break;

			case R.id.textViewForgotPassword:
				JSONObject attributes1 = new JSONObject();
				try {
					attributes1.put("title", "forgot password");
					mAppender.updateAnaylitsData(AppUtil
							.createAnalyticsJsonEventAction("login", "click",
									"Forget_Password", attributes1.toString()));
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				getHome().setFragment(new ResetPasswordFragment());
				break;
			default:
				break;
			}
		}
	};

	private void login() {

		LoginAsyncTask mAuthTask = new LoginAsyncTask(getActivity(),
				editTextUsername.getText().toString(), editTextPassword
						.getText().toString());
		mAuthTask.asyncResponse = LoginFragment.this;
		mAuthTask.execute((Void) null);
	}

	@Override
	public void onDestroy() {

		super.onDestroy();
		// hideDefaultKeyboard();
		Utils.hideKeyboard(getActivity());
	}

	@Override
	public void onDetach() {
		super.onDetach();
		// hideDefaultKeyboard();
		Utils.hideKeyboard(getActivity());
	}

	private void callDeviceRegApi() {

		BeanDevice beanDevice = AppUtil.calculateDeviceRegData(getActivity(),
				SharedPref.getString(getActivity(), SharedPref.COMPANY_ID_KEY,
						"1"), SharedPref.getString(getActivity(),
						SharedPref.APPID_KEY, "1"), SharedPref.getInteger(
						getActivity(), SharedPref.USERID_KEY, 0));

		DevicesAsyncTask mDeviceTask = new DevicesAsyncTask(getActivity(),
				beanDevice, false);
		mDeviceTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR,
				(Void) null);
	}

	@Override
	public void processFinish(ServiceResponse<BeanLogin> details) {

		if (details == null) {
			//Log.e(TAG, "Null details"+details.getError_message());
			try {
				BaseHelper.showAlert(getActivity(), "Harry's");
			} catch (Exception e) {
				// TODO: handle exception
			}
			return;
		}

		if (details.getStatus() == 200 && !details.error) {
			Log.i(TAG, "Login Success ");

			SharedPref.saveInteger(getActivity(), SharedPref.USERID_KEY,
					details.getResponse().getId());

			if (Integer.valueOf(details.getResponse().getConfirmed()) == OTP_NOT_VERIFIED) {

				SharedPref.saveBoolean(getActivity(), SharedPref.VERIFY_KEY,
						false);
				// OTP not verified
				BaseHelper.confirm(getActivity(), getString(R.string.app_name),
						getString(R.string.rc_verify_request), confirmListener,
						new CancelListener() {

							@Override
							public void onNo() {
								getHome().backToHomeScreen();

							}
						});

			} else if (Integer.valueOf(details.getResponse().getConfirmed()) == OTP_VERIFIED) {
				SharedPref.saveBoolean(getActivity(), SharedPref.VERIFY_KEY,
						true);
				SharedPref.saveString(getActivity(), SharedPref.USERNAME_KEY,
						details.getResponse().getFirstname());
				SharedPref.saveString(getActivity(), SharedPref.USERFNAME_KEY,
						details.getResponse().getFirstname());
				SharedPref.saveString(getActivity(), SharedPref.USERLNAME_KEY,
						details.getResponse().getSurname());
				SharedPref.saveString(getActivity(), SharedPref.MOBILE_NO_KEY,
						details.getResponse().getMobilenumber());
				SharedPref.saveString(getActivity(), SharedPref.EMAIL_KEY,
						details.getResponse().getEmail());
				SharedPref.saveInteger(getActivity(), SharedPref.USERID_KEY,
						details.getResponse().getId());
				SharedPref.saveString(getActivity(), SharedPref.APPID_KEY,
						details.getResponse().getApplication_id());
				SharedPref.saveString(getActivity(), SharedPref.COMPANY_ID_KEY,
						details.getResponse().getCompany_id());

				callDeviceRegApi();

				// save remember me preferences
				SharedPref.saveBoolean(getActivity(),
						SharedPref.REMEMBER_ME_KEY, chkRememberMe.isChecked());
				Log.d(TAG, "REm " + chkRememberMe.isChecked());

				String verifyMobile = details.getResponse()
						.getPendingMobileUpdate();
				if (!BaseHelper.isEmpty(verifyMobile)
						&& Boolean.valueOf(details.getResponse()
								.getPendingMobileUpdate())) {
					// OTP not verified
					HomeFragment frag = new HomeFragment();
					Bundle bundle = new Bundle();

					bundle.putBoolean("show_otp_verify_dialog", true);

					frag.setArguments(bundle);
					getHome().setFragment(frag);

					mAppender.updateAnaylitsData(AppUtil
							.createAnalyticsJsonEventAction("login", String
									.valueOf(details.getResponse().getId()),
									"verify screen", null));

				} else {
					getHome().setFragmentClearStack(new HomeFragment());
					mAppender.updateAnaylitsData(AppUtil
							.createAnalyticsJsonEventAction("login", String
									.valueOf(details.getResponse().getId()),
									"logged in", null));
				}
			}

		} else {
			BaseHelper.showAlert(getActivity(), "Login Error ", (details
					.getError_message() != null) ? details.getError_message()
					: "Unable to login. Please try again.");
		}

	}

	ConfirmListener confirmListener = new ConfirmListener() {

		@Override
		public void onYes() {

			if (resendOTPApiCall()) {
				Log.d(TAG, "OTP resend");
				((MainActivity) getActivity())
						.setFragment(new VerifyOTPFragment());
			}
		}
	};

	private boolean resendOTPApiCall() {
		if (AppUtil.checkNetworkConnection(getHome())) {
			ResendOTPAsyncTask resendOTPAsyncTask = new ResendOTPAsyncTask(
					getActivity(), SharedPref.getInteger(getActivity(),
							SharedPref.USERID_KEY, -1), false);
			resendOTPAsyncTask.execute((Void) null);

			return true;
		} else {
			BaseHelper.confirm(getActivity(), "Network Unavaliable",
					"Please turn on network to connect with server",
					new ConfirmListener() {

						@Override
						public void onYes() {
							AppUtil.showSystemSettingsDialog(getActivity());
						}
					});
		}
		return false;
	}

	@Override
	public void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		if (SharedPref.getString(getActivity(), "SPLASH_AD_IMAGE_URL", "")
				.equals("")) {
			if (SharedPref.getString(getActivity(),
					"SPLASH_AD_IMAGE_SHOW_TIME", "").equals("")) {
				SharedPref.saveString(getActivity(), "SPLASH_AD_IMAGE_SHOW_TIME", "1");
				AdsAsyncTask adsAsyncTask = new AdsAsyncTask(getActivity());
				adsAsyncTask.execute();
			} else {

			}
		}
	}

}
