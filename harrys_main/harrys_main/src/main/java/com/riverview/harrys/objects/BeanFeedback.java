package com.riverview.harrys.objects;

import android.os.Parcel;
import android.os.Parcelable;

import java.io.Serializable;

public class BeanFeedback implements Parcelable, Serializable {

	public static final String OBJ_NAME = "BeanFeedback";
	private static final long serialVersionUID = 5588254270496437773L;

	private int id, feedback_category_id, feedback_sub_category_id,
			mobileuser_id, company_id, application_id;
	private String feedback_type, title, post, feedback_status, cms_user_id,
			created_at, updated_at, deleted_at;

	public BeanFeedback(Parcel parcel) {
		super();
		this.id = parcel.readInt();
		this.feedback_category_id = parcel.readInt();
		this.feedback_sub_category_id = parcel.readInt();
		this.mobileuser_id = parcel.readInt();
		this.company_id = parcel.readInt();
		this.application_id = parcel.readInt();
		this.feedback_type = parcel.readString();
		this.title = parcel.readString();
		this.post = parcel.readString();
		this.feedback_status = parcel.readString();
		this.cms_user_id = parcel.readString();
		this.created_at = parcel.readString();
		this.updated_at = parcel.readString();
		this.deleted_at = parcel.readString();
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeInt(id);
		dest.writeInt(feedback_category_id);
		dest.writeInt(feedback_sub_category_id);
		dest.writeInt(mobileuser_id);
		dest.writeInt(company_id);
		dest.writeInt(application_id);
		dest.writeString(feedback_type);
		dest.writeString(title);
		dest.writeString(post);
		dest.writeString(feedback_status);
		dest.writeString(cms_user_id);
		dest.writeString(created_at);
		dest.writeString(updated_at);
		dest.writeString(deleted_at);
	}

	public static final Creator<BeanFeedback> CREATOR = new Creator<BeanFeedback>() {

		@Override
		public BeanFeedback createFromParcel(Parcel source) {
			return new BeanFeedback(source);
		}

		@Override
		public BeanFeedback[] newArray(int size) {
			return new BeanFeedback[size];
		}
	};

	@Override
	public int describeContents() {
		// TODO Auto-generated method stub
		return 0;
	}

	public BeanFeedback() {
		super();
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getFeedback_category_id() {
		return feedback_category_id;
	}

	public void setFeedback_category_id(int feedback_category_id) {
		this.feedback_category_id = feedback_category_id;
	}

	public int getFeedback_sub_category_id() {
		return feedback_sub_category_id;
	}

	public void setFeedback_sub_category_id(int feedback_sub_category_id) {
		this.feedback_sub_category_id = feedback_sub_category_id;
	}

	public int getMobileuser_id() {
		return mobileuser_id;
	}

	public void setMobileuser_id(int mobileuser_id) {
		this.mobileuser_id = mobileuser_id;
	}

	public int getCompany_id() {
		return company_id;
	}

	public void setCompany_id(int company_id) {
		this.company_id = company_id;
	}

	public int getApplication_id() {
		return application_id;
	}

	public void setApplication_id(int application_id) {
		this.application_id = application_id;
	}

	public String getFeedback_type() {
		return feedback_type;
	}

	public void setFeedback_type(String feedback_type) {
		this.feedback_type = feedback_type;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getPost() {
		return post;
	}

	public void setPost(String post) {
		this.post = post;
	}

	public String getFeedback_status() {
		return feedback_status;
	}

	public void setFeedback_status(String feedback_status) {
		this.feedback_status = feedback_status;
	}

	public String getCms_user_id() {
		return cms_user_id;
	}

	public void setCms_user_id(String cms_user_id) {
		this.cms_user_id = cms_user_id;
	}

	public String getCreated_at() {
		return created_at;
	}

	public void setCreated_at(String created_at) {
		this.created_at = created_at;
	}

	public String getUpdated_at() {
		return updated_at;
	}

	public void setUpdated_at(String updated_at) {
		this.updated_at = updated_at;
	}

	public String getDeleted_at() {
		return deleted_at;
	}

	public void setDeleted_at(String deleted_at) {
		this.deleted_at = deleted_at;
	}
}
