package com.riverview.harrys.objects;

public class BeanCard {

	private String id, card_name, card_face_image, description, tier_type,
			tier_range_min, tier_range_max, loyalty_program_id, created_at,
			updated_at, deleted_at;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getCard_name() {
		return card_name;
	}

	public void setCard_name(String card_name) {
		this.card_name = card_name;
	}

	public String getCard_face_image() {
		return card_face_image;
	}

	public void setCard_face_image(String card_face_image) {
		this.card_face_image = card_face_image;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getTier_type() {
		return tier_type;
	}

	public void setTier_type(String tier_type) {
		this.tier_type = tier_type;
	}

	public String getTier_range_min() {
		return tier_range_min;
	}

	public void setTier_range_min(String tier_range_min) {
		this.tier_range_min = tier_range_min;
	}

	public String getTier_range_max() {
		return tier_range_max;
	}

	public void setTier_range_max(String tier_range_max) {
		this.tier_range_max = tier_range_max;
	}

	public String getLoyalty_program_id() {
		return loyalty_program_id;
	}

	public void setLoyalty_program_id(String loyalty_program_id) {
		this.loyalty_program_id = loyalty_program_id;
	}

	public String getCreated_at() {
		return created_at;
	}

	public void setCreated_at(String created_at) {
		this.created_at = created_at;
	}

	public String getUpdated_at() {
		return updated_at;
	}

	public void setUpdated_at(String updated_at) {
		this.updated_at = updated_at;
	}

	public String getDeleted_at() {
		return deleted_at;
	}

	public void setDeleted_at(String deleted_at) {
		this.deleted_at = deleted_at;
	}
}
