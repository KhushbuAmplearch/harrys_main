package com.riverview.harrys.adapter;

import java.util.ArrayList;

import android.app.Activity;
import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.riverview.harrys.R;
import com.riverview.harrys.helpers.Utils;
import com.riverview.harrys.objects.BeanFeedback;
import com.riverview.harrys.objects.BeanFeedbackReply;

public class FeedbackReplyAdapter extends BaseAdapter {
	private ArrayList<BeanFeedbackReply> mItems = new ArrayList<BeanFeedbackReply>();
	private Activity context;

	public FeedbackReplyAdapter(Activity context,
			ArrayList<BeanFeedbackReply> list) {
		this.context = context;
		this.mItems = list;
	}

	@Override
	public int getCount() {
		try {
			return mItems.size();
		} catch (Exception e) {
			return 0;
		}
	}

	@Override
	public BeanFeedbackReply getItem(int pos) {
		return mItems.get(pos);
	}

	@Override
	public long getItemId(int position) {
		return mItems.get(position).getId();
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ViewHolder holder;

		if (convertView == null) {
			holder = new ViewHolder();
			LayoutInflater inflater = (LayoutInflater) context
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

			convertView = inflater.inflate(R.layout.cell_feedback_reply_item,
					parent, false);
			holder.tvReply = (TextView) convertView.findViewById(R.id.tvReply);
			holder.tvReplyDate = (TextView) convertView
					.findViewById(R.id.tvReplyDate);
			holder.imgSenderItem = (ImageView) convertView
					.findViewById(R.id.imgSenderItem);
			convertView.setTag(holder);

		} else {
			holder = (ViewHolder) convertView.getTag();
		}

		Utils.setTextViewFontSizeBasedOnScreenDensity(context, holder.tvReply,
				23f);
		Utils.setTextViewFontSizeBasedOnScreenDensity(context,
				holder.tvReplyDate, 28f);

		BeanFeedbackReply obj = getItem(position);

		holder.tvReply.setText(obj.getReply());
		holder.tvReplyDate.setText(obj.getCreated_at());

		if (obj.getMobileuser_id() > 0) {
			holder.imgSenderItem.setImageDrawable(context.getResources()
					.getDrawable(R.drawable.ic_action_social_person));
		} else if (obj.getCms_user_id() > 0) {
			holder.imgSenderItem.setImageDrawable(context.getResources()
					.getDrawable(R.drawable.ic_icon_harry));
		}

		return convertView;
	}

	public class ViewHolder {

		TextView tvReply;
		TextView tvReplyDate;
		ImageView imgSenderItem;
	}
}
