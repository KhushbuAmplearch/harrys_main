package com.riverview.harrys.fragments;

import static com.riverview.harrys.MainActivity.loc_permission;
import static com.riverview.harrys.constant.AppConst.PULL_TO_REFRESH_TIMEOUT;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.ActivityCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewStub;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.LinearLayout;
import android.widget.ListView;

import com.google.android.gms.maps.model.LatLng;
import com.iapps.libs.helpers.BaseHelper;
import com.iapps.libs.helpers.BaseHelper.CancelListener;
import com.iapps.libs.helpers.BaseHelper.ConfirmListener;
import com.iapps.libs.views.LoadingCompound;
import com.orangegangsters.github.swipyrefreshlayout.library.SwipyRefreshLayout;
import com.orangegangsters.github.swipyrefreshlayout.library.SwipyRefreshLayout.OnRefreshListener;
import com.orangegangsters.github.swipyrefreshlayout.library.SwipyRefreshLayoutDirection;
import com.riverview.harrys.MainActivity;
import com.riverview.harrys.R;
import com.riverview.harrys.RCGenericFragment;
import com.riverview.harrys.adapter.StoresAdapter;
import com.riverview.harrys.asynctask.AsyncResponse;
import com.riverview.harrys.asynctask.MerchantAsyncTask;
import com.riverview.harrys.helpers.ObjectSerializer;
import com.riverview.harrys.helpers.SharedPref;
import com.riverview.harrys.helpers.Utils;
import com.riverview.harrys.objects.BeanMerchant;
import com.riverview.harrys.objects.BeanOutlet;
import com.riverview.harrys.objects.BeanPhoto;
import com.riverview.harrys.objects.BeanStoreItem;
import com.riverview.harrys.objects.ServiceResponse;
import com.riverview.harrys.util.AnaylaticsAppender;
import com.riverview.harrys.util.AppUtil;

public class StoresFragment extends RCGenericFragment implements
		OnRefreshListener, LocationListener,
		AsyncResponse<ServiceResponse<ArrayList<BeanMerchant>>> {

	private static final String TAG = StoresFragment.class.getSimpleName();

	public static final int TAG_LIST_STORE = 0;

	private ListView lvStores;
	private LoadingCompound ld;
	private ViewStub emptyView;

	private View v;
	private StoresAdapter mAdapter;
	private ArrayList<BeanStoreItem> storeList;

	private SwipyRefreshLayout mSwipyRefreshLayout;
	private MerchantAsyncTask mStoreAsyncTask = null;
	LatLng myLocation;
	LocationManager locationManager;
	int PERMISSION_ALL = 1;
	private AnaylaticsAppender mAppender = null;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		SharedPref.deleteString(getActivity(), SharedPref.LISTDATA_KEY);

	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
							 Bundle savedInstanceState) {
		v = inflater.inflate(R.layout.stores_fragment_layout, container, false);

		lvStores = (ListView) v.findViewById(R.id.listViewStore);
		ld = (LoadingCompound) v.findViewById(R.id.ld);
		emptyView = (ViewStub) v.findViewById(R.id.emptyView);
		mSwipyRefreshLayout = (SwipyRefreshLayout) v.findViewById(R.id.swipyrefreshlayout);

		setHasOptionsMenu(false);
		((MainActivity) getActivity()).setBackActionBar(getActivity()
				.getResources().getString(R.string.rc_stores), View.INVISIBLE, "");
		locationManager = (LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE);
		if(loc_permission) {
			if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

			}
			locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0, 0, this);
		}else
		{
			callAPI();
		}
		return v;
	}

	public void onPrepareOptionsMenu(Menu menu) {

	}

	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);

		mSwipyRefreshLayout.setOnRefreshListener(this);
		lvStores.setOnItemClickListener(ListenerClickItem);

		lvStores.setEmptyView(emptyView);

		//callAPI();
	}

	public OnItemClickListener ListenerClickItem = new OnItemClickListener() {

		@Override
		public void onItemClick(AdapterView<?> adapter, View view, int pos,
								long arg3) {

			// Set the data as json in Shared Prefs
			try {
				SharedPref.saveString(getActivity(), SharedPref.LISTDATA_KEY,
						ObjectSerializer.serialize(storeList));
			} catch (Exception e) {
				Log.e(TAG, "Error serilization " + e.getMessage());
			}

			StoreDetailsFragment frag = new StoreDetailsFragment();
			Bundle bundle = new Bundle();

			bundle.putParcelable("details", storeList.get(pos));

			frag.setArguments(bundle);
			getHome().setFragment(frag);

			mAppender.updateAnaylitsData(AppUtil
					.createAnalyticsJsonEventAction("Outlets",
							String.valueOf(storeList.get(pos).getId()),
							"click", null));
		}
	};

	@SuppressWarnings("unchecked")
	public void callAPI() {

		// get saved json data
		String serilizedJsonString = SharedPref.getString(getActivity(),
				SharedPref.LISTDATA_KEY, null);
		if (serilizedJsonString != null) {
			try {
				storeList = (ArrayList<BeanStoreItem>) ObjectSerializer
						.deserialize(serilizedJsonString);

				mAdapter = new StoresAdapter(getActivity(), storeList);
				lvStores.setAdapter(mAdapter);
				mAdapter.notifyDataSetChanged();

				ld.hide();
				// mPullToRefresh.setRefreshComplete();

			} catch (Exception e) {
				callAPIService();
				Log.e(StoresFragment.TAG,
						"Error deserilization " + e.getMessage());
			}
		} else {
			callAPIService();

		}
	}

	View.OnClickListener clickListeners = new View.OnClickListener() {
		public void onClick(View v) {

		}
	};

	@Override
	public void onPause() {
		super.onPause();
		try {
			locationManager.removeUpdates(this);
			locationManager = null;
		} catch (Exception e) {
			e.printStackTrace();
		}
		if (mStoreAsyncTask != null) {
			mStoreAsyncTask.cancel(true);
		}
	}

	private boolean callAPIService() {
		if (AppUtil.checkNetworkConnection(getActivity())) {

			mStoreAsyncTask = new MerchantAsyncTask(getActivity());
			mStoreAsyncTask.asyncResponse = StoresFragment.this;
			mStoreAsyncTask.execute((Void) null);

			ld.showLoading();
			return true;

		} else {
			BaseHelper.confirm(getActivity(), "Network Unavaliable",
					"Please turn on network to connect with server",
					new ConfirmListener() {

						@Override
						public void onYes() {
							AppUtil.showSystemSettingsDialog(getActivity());
						}
					}, new CancelListener() {

						@Override
						public void onNo() {
							getHome().backToHomeScreen();

						}
					});
		}
		return false;
	}

	@Override
	public void onResume() {
		super.onResume();

		try {
			mAppender = AnaylaticsAppender.getInstance(getActivity());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public void processFinish(ServiceResponse<ArrayList<BeanMerchant>> details) {
		Log.d(TAG, "Process Finished");

		ld.hide();
		mSwipyRefreshLayout.setRefreshing(false);

		ArrayList<BeanOutlet> list = null;

		if (details != null && details.getStatus() == 200 && !details.error) {
			storeList = new ArrayList<BeanStoreItem>();

			try {
				ArrayList<BeanMerchant> merchantList = details.getResponse();
				list = merchantList.get(0).getOutlets();
				if (list == null || list.size() == 0) {

					BaseHelper.showAlert(getActivity(),
							"No outlet details avaliable");
					return;
				}
			} catch (Exception e) {
				BaseHelper.showAlert(getActivity(),
						"No outlet details avaliable");
				return;
			}


			for (Iterator<BeanOutlet> iterator = list.iterator(); iterator
					.hasNext();) {
				BeanOutlet beanOutlet = (BeanOutlet) iterator.next();

				BeanStoreItem item = new BeanStoreItem(
						Integer.valueOf(beanOutlet.getId()), "");
				item.setInfo(beanOutlet.getTitle());

				try {
					item.setUrl((BaseHelper.isEmpty(beanOutlet.getImage()
							.getOriginal()) ? null : beanOutlet.getImage()
							.getOriginal()));
				} catch (Exception e) {
				}

				try {
					ArrayList<BeanPhoto> photos = beanOutlet.getPhotos();
					StringBuilder sb = new StringBuilder();

					for (BeanPhoto photo : photos) {
						sb.append(photo.getOriginal());
						sb.append(",");
					}
					item.setPhotos(sb.toString());
				} catch (Exception e) {
				}
				try {
					item.setLongitude(Double.parseDouble(beanOutlet
							.getLongitude()));
					item.setLatitude(Double.parseDouble(beanOutlet
							.getLatitude()));
				} catch (Exception e) {
					e.printStackTrace();
				}

				item.setAddress(beanOutlet.getAddress());
				item.setPostelcode(beanOutlet.getPostcode());
				item.setTelephome(beanOutlet.getTelephone());
				item.setEmail(beanOutlet.getEmail());
				item.setOpeninghours(beanOutlet.getOpening_hours());
				try {
					myLocation = new LatLng(
							Utils.latitude,
							Utils.longitude);
				} catch (Exception e) {
				}
				try {
					if (myLocation != null) {
						calculateDistance(beanOutlet,item);

					} else {
						Log.e(TAG, "Unable to calculate location");
						item.setDistance(0.0);
					}
				} catch (Exception e) {
					e.printStackTrace();
					item.setDistance(0.0);
				}

				storeList.add(item);
			}

			// Sort List
			try {
				Collections.sort(storeList);
			} catch (Exception e) {
				e.printStackTrace();
			}

			mAdapter = new StoresAdapter(getActivity(), storeList);
			lvStores.setAdapter(mAdapter);
			mAdapter.notifyDataSetChanged();

		} else {

			Log.e(TAG, "Fails download outlets.... Try it again ");
		}
	}

	public void calculateDistance(BeanOutlet beanOutlet,BeanStoreItem item) {
		Double distance = Utils.distance(myLocation.latitude,
				myLocation.longitude,
				Double.parseDouble(beanOutlet.getLatitude()),
				Double.parseDouble(beanOutlet.getLongitude()));
		item.setDistance(distance);
	}

	@Override
	public void onRefresh(SwipyRefreshLayoutDirection direction) {
		if (callAPIService()) {
			new Handler().postDelayed(new Runnable() {
				@Override
				public void run() {
					// Hide the refresh after n sec
					try {
						getActivity().runOnUiThread(new Runnable() {
							@Override
							public void run() {
								mSwipyRefreshLayout.setRefreshing(false);
							}
						});
					} catch (Exception e) {
					}
				}
			}, PULL_TO_REFRESH_TIMEOUT);
		} else {
			mSwipyRefreshLayout.setRefreshing(false);
		}
	}

	@Override
	public void onLocationChanged(Location location) {
		Log.e("storlat",""+location.getLatitude());
		if(location!=null)
		try {
			Utils.latitude = location.getLatitude();
			Utils.longitude = location.getLongitude();
			callAPI();
		} catch (Exception e) {
			// TODO: handle exception
		}
	}

	@Override
	public void onStatusChanged(String provider, int status, Bundle extras) {

	}

	@Override
	public void onProviderEnabled(String provider) {

	}

	@Override
	public void onProviderDisabled(String provider) {

	}
}
