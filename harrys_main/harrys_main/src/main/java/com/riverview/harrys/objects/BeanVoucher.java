package com.riverview.harrys.objects;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by wiraj on 5/2/15.
 */
public class BeanVoucher implements Parcelable {

	private int id, voucher_id;
	private String created_at, updated_at, description;
	private BeanVoucherInner voucher;

	public BeanVoucher() {
	}

	public BeanVoucher(Parcel parcel) {
		super();

		this.id = parcel.readInt();
		this.voucher_id = parcel.readInt();
		this.created_at = parcel.readString();
		this.updated_at = parcel.readString();
		this.description = parcel.readString();
		this.voucher = parcel.readParcelable(BeanVoucherInner.class
				.getClassLoader());
	}

	@Override
	public void writeToParcel(Parcel parcel, int i) {
		parcel.writeInt(id);
		parcel.writeInt(voucher_id);
		parcel.writeString(created_at);
		parcel.writeString(updated_at);
		parcel.writeString(description);
		parcel.writeParcelable(voucher, i);
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getVoucher_id() {
		return voucher_id;
	}

	public void setVoucher_id(int voucher_id) {
		this.voucher_id = voucher_id;
	}

	public String getCreated_at() {
		return created_at;
	}

	public void setCreated_at(String created_at) {
		this.created_at = created_at;
	}

	public String getUpdated_at() {
		return updated_at;
	}

	public void setUpdated_at(String updated_at) {
		this.updated_at = updated_at;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public BeanVoucherInner getVoucher() {
		return voucher;
	}

	public void setVoucher(BeanVoucherInner voucher) {
		this.voucher = voucher;
	}

	@Override
	public int describeContents() {
		return 0;
	}

	public static final Creator<BeanVoucher> CREATOR = new Creator<BeanVoucher>() {

		@Override
		public BeanVoucher createFromParcel(Parcel source) {
			return new BeanVoucher(source);
		}

		@Override
		public BeanVoucher[] newArray(int size) {
			return new BeanVoucher[size];
		}
	};
}
