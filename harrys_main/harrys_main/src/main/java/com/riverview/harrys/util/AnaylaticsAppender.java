package com.riverview.harrys.util;

import org.json.JSONArray;
import org.json.JSONObject;

import android.content.Context;
import android.content.SharedPreferences;

import com.riverview.harrys.helpers.SharedPref;

public class AnaylaticsAppender {

	private static final String TAG = AnaylaticsAppender.class.getSimpleName();

	private static AnaylaticsAppender APPENDER_OBJ = null;

	private Context context;
	private static SharedPreferences preferences = null;

	private AnaylaticsAppender(Context context) {
		this.context = context;
		preferences = SharedPref.getPref(context);
	}

	public static synchronized AnaylaticsAppender getInstance(Context context) {
		if (APPENDER_OBJ == null) {
			synchronized (AnaylaticsAppender.class) {
				if (APPENDER_OBJ == null) {
					APPENDER_OBJ = new AnaylaticsAppender(context);
				}
			}
		}
		return APPENDER_OBJ;
	}

	public void updateAnaylitsData(JSONObject data) {

		JSONArray jsonObject = null;
		try {

			String json = preferences.getString(
					SharedPref.ANALYTICS_JSON_ID_KEY, null);
			if (json == null) {
				jsonObject = new JSONArray();
			} else {
				jsonObject = new JSONArray(json);
			}

			// save JSON data
			jsonObject.put(data);

			// Convert to string and write back to SF
			preferences
					.edit()
					.putString(SharedPref.ANALYTICS_JSON_ID_KEY,
							jsonObject.toString()).apply();

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public JSONObject getFinalData() {

		JSONArray jsonObject = null;
		try {

			String json = preferences.getString(
					SharedPref.ANALYTICS_JSON_ID_KEY, null);
			if (json == null) {
				return null;
			} else {
				jsonObject = new JSONArray(json);
			}
			return AppUtil.getAnalyticsWrapper(context, jsonObject);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
}
