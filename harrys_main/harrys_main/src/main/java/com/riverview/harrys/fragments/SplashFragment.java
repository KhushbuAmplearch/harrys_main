package com.riverview.harrys.fragments;

import static com.riverview.harrys.constant.AppConst.SPLASH_TIMEOUT;

import org.json.JSONObject;

import android.content.Intent;
import android.content.pm.PackageInfo;
import android.graphics.PixelFormat;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Toast;

import com.riverview.harrys.R;
import com.riverview.harrys.RCGenericFragment;
import com.riverview.harrys.SplashAdActivity;
import com.riverview.harrys.asynctask.AdsAsyncTask;
import com.riverview.harrys.asynctask.AnalyticesAsyncTask;
import com.riverview.harrys.asynctask.CompanyDataAsyncTask;
import com.riverview.harrys.helpers.SharedPref;
import com.riverview.harrys.helpers.UserInfoManager;
import com.riverview.harrys.helpers.Utils;
import com.riverview.harrys.util.AnaylaticsAppender;
import com.riverview.harrys.util.AppUtil;

public class SplashFragment extends RCGenericFragment {

	private static final String TAG = SplashFragment.class.getSimpleName();

	private View v;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		getActivity().getActionBar().hide();
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		v = inflater.inflate(R.layout.splash_layout, container, false);
		setHasOptionsMenu(false);

		hideSystemUI();

		return v;
	}

	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);

		Window window = getHome().getWindow();
		window.setFormat(PixelFormat.RGBA_8888);

		// hideSystemUI();

		new Handler().postDelayed(new Runnable() {

			@Override
			public void run() {

				try {
//					getHome().setFragmentClearStack(new HomeFragment());
//					
//					// start user suspend service
//					Utils.suspendUser(getActivity());
					doThings();
				} catch (Exception e) {
					e.printStackTrace();
				}

			}
		}, SPLASH_TIMEOUT);		
		

	}
	
	private void doThings(){
		/*if (!UserInfoManager.getInstance(getActivity()).isSignedIn(getActivity())) {
			
		}*/
		callAdsService();
		getHome().setFragmentClearStack(new HomeFragment());

		// start user suspend service
		Utils.suspendUser(getActivity());
	}

	private boolean callAdsService() {

		if (AppUtil.checkNetworkConnection(getActivity())) {
			if(SharedPref.getString(getActivity(), "SPLASH_AD_IMAGE_URL", "").equals("")){
				//Toast.makeText(getActivity(), "no Shared", Toast.LENGTH_SHORT).show();
				AdsAsyncTask adsAsyncTask = new AdsAsyncTask(getActivity());
				// adsAsyncTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR,(Void) null);
				adsAsyncTask.execute();
				//Toast.makeText(getActivity(), "yes Shared: "+txt, Toast.LENGTH_SHORT).show();
			}else{
				String txt = SharedPref.getString(getActivity(), "SPLASH_AD_IMAGE_URL", "");
				//Toast.makeText(getActivity(), "yes Shared: "+txt, Toast.LENGTH_SHORT).show();
				
				Intent intent = new Intent(getActivity(), SplashAdActivity.class);
				intent.putExtra("time", "3000");
				intent.putExtra("imgPath", txt);
				
				getActivity().startActivity(intent);
				AdsAsyncTask adsAsyncTask = new AdsAsyncTask(getActivity());
				// adsAsyncTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR,(Void) null);
				adsAsyncTask.execute();
			}
//			AdsAsyncTask adsAsyncTask = new AdsAsyncTask(getActivity());
//			// adsAsyncTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR,(Void)
//			// null);
//			adsAsyncTask.execute();

			return true;

		}
		return false;
	}

	private void hideSystemUI() {
		getHome()
				.getWindow()
				.getDecorView()
				.setSystemUiVisibility(
						View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
								| View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
								| View.SYSTEM_UI_FLAG_FULLSCREEN);

		if (Build.VERSION.SDK_INT < 16) {
			getHome().getWindow().setFlags(
					WindowManager.LayoutParams.FLAG_FULLSCREEN,
					WindowManager.LayoutParams.FLAG_FULLSCREEN);
		}
	}
	// private void hideSystemUI() {
	// getHome()
	// .getWindow()
	// .getDecorView()
	// .setSystemUiVisibility(
	// View.SYSTEM_UI_FLAG_LAYOUT_STABLE
	// | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
	// | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
	// | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
	// | View.SYSTEM_UI_FLAG_FULLSCREEN // hide status
	// // bar
	// | View.SYSTEM_UI_FLAG_IMMERSIVE);
	// }

}
