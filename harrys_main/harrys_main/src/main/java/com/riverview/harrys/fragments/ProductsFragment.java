package com.riverview.harrys.fragments;

import static com.riverview.harrys.constant.AppConst.PULL_TO_REFRESH_TIMEOUT;

import java.util.ArrayList;
import java.util.Iterator;

import org.apache.commons.collections4.CollectionUtils;

import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnFocusChangeListener;
import android.view.ViewGroup;
import android.view.ViewStub;
import android.view.animation.AccelerateInterpolator;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ListView;
import com.iapps.libs.helpers.BaseHelper;
import com.iapps.libs.helpers.BaseHelper.CancelListener;
import com.iapps.libs.helpers.BaseHelper.ConfirmListener;
import com.iapps.libs.helpers.BaseUIHelper;
import com.iapps.libs.views.LoadingCompound;
import com.nineoldandroids.animation.ValueAnimator;
import com.orangegangsters.github.swipyrefreshlayout.library.SwipyRefreshLayout;
import com.orangegangsters.github.swipyrefreshlayout.library.SwipyRefreshLayout.OnRefreshListener;
import com.orangegangsters.github.swipyrefreshlayout.library.SwipyRefreshLayoutDirection;
import com.riverview.harrys.MainActivity;
import com.riverview.harrys.R;
import com.riverview.harrys.RCGenericFragment;
import com.riverview.harrys.adapter.ProductsAdapter;
import com.riverview.harrys.adapter.SubCategoryAdapter;
import com.riverview.harrys.asynctask.AsyncResponse;
import com.riverview.harrys.asynctask.ProductsCatagoryAsyncTask;
import com.riverview.harrys.helpers.HeightEvaluator;
import com.riverview.harrys.helpers.MatchCategoryPredicate;
import com.riverview.harrys.helpers.ObjectSerializer;
import com.riverview.harrys.helpers.SharedPref;
import com.riverview.harrys.objects.BeanCategoryWiseProduct;
import com.riverview.harrys.objects.BeanProductItem;
import com.riverview.harrys.objects.BeanProducts;
import com.riverview.harrys.objects.BeanSubCategory;
import com.riverview.harrys.objects.BeanSubcategoryItem;
import com.riverview.harrys.objects.ServiceResponse;
import com.riverview.harrys.util.AppUtil;

public class ProductsFragment extends RCGenericFragment implements
		OnRefreshListener,
		AsyncResponse<ServiceResponse<ArrayList<BeanCategoryWiseProduct>>> {

	private static final String TAG = ProductsFragment.class.getSimpleName();

	public static final int TAG_LIST_STORE = 0;

	private static final int FOOD_CATAGORY = 2;
	private static final int DRINK_CATAGORY = 1;

	private boolean currentListSub = false;

	private static int TAB_CURRENT_CATEGORY = DRINK_CATAGORY;

	private ListView lvStores;
	private ViewStub emptyView;
	private ListView lvSubCategory;
	private Button buttonDrinks;
	private Button buttonFoods;
	private LoadingCompound ld;
	private FrameLayout slideDownView;
	private FrameLayout frame;
	private SwipyRefreshLayout mSwipyRefreshLayout;

	private View v;
	private ProductsAdapter mAdapter;
	private SubCategoryAdapter mSubAdapter;
	private ArrayList<BeanProductItem> productList;
	private ArrayList<BeanProductItem> drinkList;
	private ArrayList<BeanProductItem> foodList;

	private ArrayList<BeanProductItem> sortedDrinkList;
	private ArrayList<BeanProductItem> sortedFoodList;

	private ArrayList<BeanSubcategoryItem> drinkSubList;
	private ArrayList<BeanSubcategoryItem> foodSubList;


	private static float height = 0f;
	private static final int VIEW_ANIMATION_INTERVAL = 300;

	private static boolean toogleStatus = false;

	private ProductsCatagoryAsyncTask mProductCatagoryTask = null;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		SharedPref.deleteString(getActivity(), SharedPref.LISTDATA_KEY);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		v = inflater
				.inflate(R.layout.product_fragment_layout, container, false);

		lvStores = (ListView)v.findViewById(R.id.listViewStore);
		emptyView = (ViewStub)v.findViewById(R.id.emptyView);
		lvSubCategory = (ListView)v.findViewById(R.id.listViewSubCatagory);
		buttonDrinks = (Button)v.findViewById(R.id.buttonDrink);
		buttonFoods =(Button)v.findViewById(R.id.buttonFood);
		ld = (LoadingCompound)v.findViewById(R.id.ld);
		slideDownView = (FrameLayout)v.findViewById(R.id.product_catagory_dropdown_layout);
		frame = (FrameLayout)v.findViewById(R.id.frame);
		mSwipyRefreshLayout = (SwipyRefreshLayout)v.findViewById(R.id.swipyrefreshlayout);

		setHasOptionsMenu(false);

		((MainActivity) getActivity()).setBackActionBar(getActivity()
				.getResources().getString(R.string.rc_menu),View.INVISIBLE,"");

		toogleStatus = false;

		return v;
	}

	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
		inflater.inflate(R.menu.menu_product, menu);
	}

	private void toggle(final FrameLayout v) {

		if (!toogleStatus) {
			lvStores.setEnabled(false);

			height = BaseUIHelper.getScreenHeight(getHome());

			height = (float) (height * 0.7);

			v.setVisibility(View.VISIBLE);
			ValueAnimator va = ValueAnimator.ofFloat(0f, height).setDuration(
					VIEW_ANIMATION_INTERVAL);
			va.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
				public void onAnimationUpdate(ValueAnimator animation) {
					Integer value = (Integer) Math.round((float) animation
							.getAnimatedValue());
					v.getLayoutParams().height = value.intValue();
					v.invalidate();
					v.requestLayout();

				}
			});
			va.setInterpolator(new AccelerateInterpolator(2));
			va.start();

			toogleStatus = true;
		} else {
			toogleStatus = false;
			closeToggle();
		}
	}

	private void closeToggle() {

		lvStores.setEnabled(true);

		int startHeight = slideDownView.getHeight();
		ValueAnimator animation = ValueAnimator.ofObject(
				new HeightEvaluator(slideDownView), startHeight, (int) 0)
				.setDuration(VIEW_ANIMATION_INTERVAL);
		animation.setInterpolator(new AccelerateInterpolator(2));
		animation.start();
	}

	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.rv_menu_catagory:

			toggle(slideDownView);
			break;

		default:
			return true;
		}
		return true;
	}

	@Override
	public void onResume() {
		super.onResume();
	}

	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);

		mSwipyRefreshLayout.setOnRefreshListener(this);

		closeToggle();

		lvStores.setOnItemClickListener(ListenerClickItem);
		lvSubCategory.setOnItemClickListener(listenerSubListClickItem);

		lvStores.setEmptyView(emptyView);

		buttonDrinks.setOnFocusChangeListener(focusChangeListener);
		buttonFoods.setOnFocusChangeListener(focusChangeListener);

		buttonDrinks.setOnClickListener(clickListeners);
		buttonFoods.setOnClickListener(clickListeners);

		buttonDrinks.requestFocus();

		callAPI();
	}

	public OnFocusChangeListener focusChangeListener = new OnFocusChangeListener() {

		@Override
		public void onFocusChange(View v, boolean hasFocus) {
			switch (v.getId()) {
			case R.id.buttonDrink:
				if (hasFocus && drinkList != null) {

					TAB_CURRENT_CATEGORY = DRINK_CATAGORY;
					mAdapter.notifyDataSetInvalidated();

					mAdapter = new ProductsAdapter(getActivity(), drinkList);
					lvStores.setAdapter(mAdapter);
					mAdapter.notifyDataSetChanged();

					mSubAdapter = new SubCategoryAdapter(getActivity(),
							drinkSubList);
					lvSubCategory.setAdapter(mSubAdapter);
					mSubAdapter.notifyDataSetChanged();

					// set sub category flag
					currentListSub = false;
				}
				break;

			case R.id.buttonFood:
				if (hasFocus && foodList != null) {

					TAB_CURRENT_CATEGORY = FOOD_CATAGORY;
					mAdapter.notifyDataSetInvalidated();

					mAdapter = new ProductsAdapter(getActivity(), foodList);
					lvStores.setAdapter(mAdapter);
					mAdapter.notifyDataSetChanged();

					mSubAdapter = new SubCategoryAdapter(getActivity(),
							foodSubList);
					lvSubCategory.setAdapter(mSubAdapter);
					mSubAdapter.notifyDataSetChanged();

					// set sub category flag
					currentListSub = false;

				}
				break;

			default:
				break;
			}

		}
	};

	public OnItemClickListener ListenerClickItem = new OnItemClickListener() {

		@Override
		public void onItemClick(AdapterView<?> adapter, View view, int pos,
				long arg3) {

			// Set the data as json in Shared Prefs
			try {
				SharedPref.saveString(getActivity(), SharedPref.LISTDATA_KEY,
						ObjectSerializer.serialize(productList));
				SharedPref.saveString(getActivity(), SharedPref.LISTDATA_1_KEY,
						ObjectSerializer.serialize(drinkList));
				SharedPref.saveString(getActivity(), SharedPref.LISTDATA_2_KEY,
						ObjectSerializer.serialize(foodList));

				// save sub category lists too
				SharedPref.saveString(getActivity(), SharedPref.SUBDATA_1_KEY,
						ObjectSerializer.serialize(drinkSubList));
				SharedPref.saveString(getActivity(), SharedPref.SUBDATA_2_KEY,
						ObjectSerializer.serialize(foodSubList));

			} catch (Exception e) {
				Log.e(TAG, "Error serilization " + e.getMessage());
			}

			ProductsDetailsFragment frag = new ProductsDetailsFragment();
			Bundle bundle = new Bundle();

			if (!currentListSub) {
				if (TAB_CURRENT_CATEGORY == DRINK_CATAGORY) {
					bundle.putParcelable("details", drinkList.get(pos));
				} else {
					bundle.putParcelable("details", foodList.get(pos));
				}

			} else if (currentListSub) {
				if (TAB_CURRENT_CATEGORY == DRINK_CATAGORY) {
					bundle.putParcelable("details", sortedDrinkList.get(pos));
				} else {
					bundle.putParcelable("details", sortedFoodList.get(pos));
				}
			}

			bundle.putString("position", String.valueOf(pos));
			frag.setArguments(bundle);
			getHome().setFragment(frag);
		}
	};

	public OnItemClickListener listenerSubListClickItem = new OnItemClickListener() {

		@Override
		public void onItemClick(AdapterView<?> adapter, View view, int pos,
				long arg3) {

			if (TAB_CURRENT_CATEGORY == DRINK_CATAGORY) {
				try {
					ArrayList<BeanProductItem> sortedDrink = new ArrayList<BeanProductItem>();
					if (drinkSubList.get(pos).getId() != -1) {
						MatchCategoryPredicate predicateDrink = new MatchCategoryPredicate(
								drinkSubList.get(pos).getId());
						sortedDrink.addAll(drinkList);

						CollectionUtils.filter(sortedDrink, predicateDrink);
					} else {
						sortedDrink.addAll(drinkList);
					}
					// set sorted list to adapter
					mAdapter = new ProductsAdapter(getActivity(), sortedDrink);
					lvStores.setAdapter(mAdapter);
					mAdapter.notifyDataSetChanged();

					// set sub category flag
					sortedDrinkList = sortedDrink;
					currentListSub = true;

					// Close Sub menu
					toogleStatus = false;
					closeToggle();

				} catch (Exception e) {
					e.printStackTrace();
				}

			} else {

				try {
					ArrayList<BeanProductItem> sortedFood = new ArrayList<BeanProductItem>();
					if (drinkSubList.get(pos).getId() != -1) {
						MatchCategoryPredicate predicateFood = new MatchCategoryPredicate(
								foodSubList.get(pos).getId());

						sortedFood.addAll(foodList);

						CollectionUtils.filter(sortedFood, predicateFood);
					} else {
						sortedFood.addAll(foodList);
					}

					// set sorted list to adapter
					mAdapter = new ProductsAdapter(getActivity(), sortedFood);
					lvStores.setAdapter(mAdapter);
					mAdapter.notifyDataSetChanged();

					// set sub category flag
					sortedFoodList = sortedFood;
					currentListSub = true;

					// Close Sub menu
					toogleStatus = false;
					closeToggle();

				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}
	};

	@SuppressWarnings("unchecked")
	public void callAPI() {

		// get saved json data
		String serilizedJsonString = SharedPref.getString(getActivity(),
				SharedPref.LISTDATA_KEY, null);
		String serilizedJsonString1 = SharedPref.getString(getActivity(),
				SharedPref.LISTDATA_1_KEY, null);
		String serilizedJsonString2 = SharedPref.getString(getActivity(),
				SharedPref.LISTDATA_2_KEY, null);

		String serilizedJsonString3 = SharedPref.getString(getActivity(),
				SharedPref.SUBDATA_1_KEY, null);
		String serilizedJsonString4 = SharedPref.getString(getActivity(),
				SharedPref.SUBDATA_2_KEY, null);

		if (serilizedJsonString != null) {
			try {
				productList = (ArrayList<BeanProductItem>) ObjectSerializer
						.deserialize(serilizedJsonString);
				drinkList = (ArrayList<BeanProductItem>) ObjectSerializer
						.deserialize(serilizedJsonString1);
				foodList = (ArrayList<BeanProductItem>) ObjectSerializer
						.deserialize(serilizedJsonString2);

				drinkSubList = (ArrayList<BeanSubcategoryItem>) ObjectSerializer
						.deserialize(serilizedJsonString3);
				foodSubList = (ArrayList<BeanSubcategoryItem>) ObjectSerializer
						.deserialize(serilizedJsonString4);

				mAdapter = new ProductsAdapter(getActivity(), drinkList);
				lvStores.setAdapter(mAdapter);
				mAdapter.notifyDataSetChanged();

				ld.hide();
				// mPullToRefresh.setRefreshComplete();

			} catch (Exception e) {
				callAPIService();
				Log.e(ProductsFragment.TAG,
						"Error deserilization " + e.getMessage());
			}
		} else {
			callAPIService();
		}

	}

	@Override
	public void onPause() {
		super.onPause();

		if (mProductCatagoryTask != null) {
			mProductCatagoryTask.cancel(true);
		}
	}

	private boolean callAPIService() {

		if (AppUtil.checkNetworkConnection(getActivity())) {

			mProductCatagoryTask = new ProductsCatagoryAsyncTask(getActivity());
			mProductCatagoryTask.asyncResponse = ProductsFragment.this;
			mProductCatagoryTask.execute((Void) null);

			ld.showLoading();
			return true;

		} else {

			BaseHelper.confirm(getActivity(), "Network Unavaliable",
					"Please turn on network to connect with server",
					new ConfirmListener() {

						@Override
						public void onYes() {
							AppUtil.showSystemSettingsDialog(getActivity());
						}

					}, new CancelListener() {

						@Override
						public void onNo() {
							getHome().backToHomeScreen();

						}
					});
		}
		return false;
	}

	View.OnClickListener clickListeners = new View.OnClickListener() {
		public void onClick(View v) {

			switch (v.getId()) {

			case R.id.buttonDrink:
				if (drinkList != null) {

					TAB_CURRENT_CATEGORY = DRINK_CATAGORY;
					mAdapter.notifyDataSetInvalidated();

					mAdapter = new ProductsAdapter(getActivity(), drinkList);
					lvStores.setAdapter(mAdapter);
					mAdapter.notifyDataSetChanged();

					mSubAdapter = new SubCategoryAdapter(getActivity(),
							drinkSubList);
					lvSubCategory.setAdapter(mSubAdapter);
					mSubAdapter.notifyDataSetChanged();
				}
				break;

			case R.id.buttonFood:
				if (foodList != null) {

					TAB_CURRENT_CATEGORY = FOOD_CATAGORY;
					mAdapter.notifyDataSetInvalidated();

					mAdapter = new ProductsAdapter(getActivity(), foodList);
					lvStores.setAdapter(mAdapter);
					mAdapter.notifyDataSetChanged();

					mSubAdapter = new SubCategoryAdapter(getActivity(),
							foodSubList);
					lvSubCategory.setAdapter(mSubAdapter);
					mSubAdapter.notifyDataSetChanged();

				}

			default:
				break;
			}

		}
	};

	@Override
	public void processFinish(
			ServiceResponse<ArrayList<BeanCategoryWiseProduct>> details) {

		ld.hide();
		mSwipyRefreshLayout.setRefreshing(false);

		if (details != null && details.getStatus() == 200 && !details.error) {
			Log.i(TAG, "Products download Success ");

			productList = new ArrayList<BeanProductItem>();
			drinkList = new ArrayList<BeanProductItem>();
			foodList = new ArrayList<BeanProductItem>();

			drinkSubList = new ArrayList<BeanSubcategoryItem>();
			foodSubList = new ArrayList<BeanSubcategoryItem>();

			// Add default item to get all menu list
			BeanSubcategoryItem allItem = new BeanSubcategoryItem(-1, "All");
			drinkSubList.add(allItem);
			foodSubList.add(allItem);

			ArrayList<BeanCategoryWiseProduct> list = details.getResponse();
			for (Iterator<BeanCategoryWiseProduct> iterator = list.iterator(); iterator
					.hasNext();) {
				BeanCategoryWiseProduct beanCaragoryProduct = (BeanCategoryWiseProduct) iterator
						.next();

				if (Integer.valueOf(beanCaragoryProduct.getId()) == 1) {
					// Drink products

					ArrayList<BeanProducts> beanProducts = beanCaragoryProduct
							.getProducts();

					for (Iterator<BeanProducts> iteratorProducts = beanProducts
							.iterator(); iteratorProducts.hasNext();) {
						BeanProducts beanProduct = (BeanProducts) iteratorProducts
								.next();
						BeanProductItem item = null;

						// get values
						item = new BeanProductItem(Integer.valueOf(beanProduct
								.getId()), beanProduct.getTitle());
						try {
							item.setSubcategory_id(beanProduct
									.getSubcategory_id());
						} catch (Exception e) {
						}

						try {
							item.setOrgUrl((BaseHelper.isEmpty(beanProduct
									.getImage().getOriginal()) ? null
									: beanProduct.getImage().getOriginal()));
						} catch (Exception e) {
						}

						item.setBase_price(beanProduct.getBase_price());
						item.setDescription(beanProduct.getDescription());
						// item.setDetail_description(beanProduct.get);

						drinkList.add(item);
						productList.add(item);

					}

					// get drink Sub category list
					ArrayList<BeanSubCategory> beanSubDrink = beanCaragoryProduct
							.getProduct_subcategories();

					for (Iterator<BeanSubCategory> iteratorSubDrink = beanSubDrink
							.iterator(); iteratorSubDrink.hasNext();) {
						BeanSubCategory beanSub = (BeanSubCategory) iteratorSubDrink
								.next();
						BeanSubcategoryItem item = null;

						// get values
						item = new BeanSubcategoryItem(Integer.valueOf(beanSub
								.getId()), beanSub.getName());

						drinkSubList.add(item);
					}

				} else if (Integer.valueOf(beanCaragoryProduct.getId()) == 2) {
					// Food products

					ArrayList<BeanProducts> beanProducts = beanCaragoryProduct
							.getProducts();

					for (Iterator<BeanProducts> iteratorFood = beanProducts
							.iterator(); iteratorFood.hasNext();) {
						BeanProducts beanProduct = (BeanProducts) iteratorFood
								.next();
						BeanProductItem item = null;

						// get values
						item = new BeanProductItem(Integer.valueOf(beanProduct
								.getId()), beanProduct.getTitle());
						try {
							item.setSubcategory_id(beanProduct
									.getSubcategory_id());
						} catch (Exception e) {
						}

						try {
							item.setOrgUrl(beanProduct.getImage().getOriginal());
						} catch (Exception e) {
						}

						item.setBase_price(beanProduct.getBase_price());
						item.setDescription(beanProduct.getDescription());
						// item.setDetail_description(beanProduct.get);

						foodList.add(item);
						productList.add(item);

					}

					// get food Sub category list
					ArrayList<BeanSubCategory> beanSubFood = beanCaragoryProduct
							.getProduct_subcategories();

					for (Iterator<BeanSubCategory> iteratorSubFood = beanSubFood
							.iterator(); iteratorSubFood.hasNext();) {
						BeanSubCategory beanSub = (BeanSubCategory) iteratorSubFood
								.next();
						BeanSubcategoryItem item = null;

						// get values
						item = new BeanSubcategoryItem(Integer.valueOf(beanSub
								.getId()), beanSub.getName());

						foodSubList.add(item);
					}
				}
			}

			try {

				// Set adapters
				if (buttonFoods.isFocused()) {
					mAdapter = new ProductsAdapter(getActivity(), foodList);
					mSubAdapter = new SubCategoryAdapter(getActivity(),
							foodSubList);
				} else if (buttonDrinks.isFocused()) {
					mAdapter = new ProductsAdapter(getActivity(), drinkList);
					mSubAdapter = new SubCategoryAdapter(getActivity(),
							drinkSubList);
				} else {
					mAdapter = new ProductsAdapter(getActivity(), productList);
				}

				lvStores.setAdapter(mAdapter);
				mAdapter.notifyDataSetChanged();

				lvSubCategory.setAdapter(mSubAdapter);
				mSubAdapter.notifyDataSetChanged();

			} catch (Exception e) {
				// TODO: handle exception
			}
		} else {
			Log.e(TAG, "Error");
			// Log.e(TAG, "Fails download promotions.... Try it again " +
			// details.getError_message());
		}
	}

	@Override
	public void onRefresh(SwipyRefreshLayoutDirection direction) {
		if (callAPIService()) {
			new Handler().postDelayed(new Runnable() {
				@Override
				public void run() {
					// Hide the refresh after n sec
					try {
						getActivity().runOnUiThread(new Runnable() {
							@Override
							public void run() {
								mSwipyRefreshLayout.setRefreshing(false);
							}
						});
					} catch (Exception e) {
					}
				}
			}, PULL_TO_REFRESH_TIMEOUT);
		} else {
			mSwipyRefreshLayout.setRefreshing(false);
		}
	}
}
