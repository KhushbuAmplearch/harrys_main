package com.riverview.harrys.asynctask;

import static com.riverview.harrys.constant.AppConst.ATTR_TAG_DOB;
import static com.riverview.harrys.constant.AppConst.ATTR_TAG_UDID;
import static com.riverview.harrys.constant.AppConst.HTTP_METHOD_GET;
import static com.riverview.harrys.constant.AppConst.SERVICE_END_POINT;
import static com.riverview.harrys.constant.AppConst.URL_RESET_PASSWORD;

import java.io.IOException;
import java.util.ArrayList;

import org.apache.http.NameValuePair;
import org.apache.http.client.utils.URLEncodedUtils;
import org.apache.http.message.BasicNameValuePair;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.type.TypeReference;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.client.ClientHttpResponse;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.DefaultResponseErrorHandler;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.riverview.harrys.R;
import com.riverview.harrys.exception.RestServiceException;
import com.riverview.harrys.objects.BeanLoyaltyCard;
import com.riverview.harrys.objects.BeanMobileUser;
import com.riverview.harrys.objects.BeanUserData;
import com.riverview.harrys.objects.ErrorServiceResponse;
import com.riverview.harrys.objects.ServiceResponse;
import com.riverview.harrys.util.AppUtil;
import com.riverview.harrys.util.WebServiceUtil;

import fr.castorflex.android.circularprogressbar.CircularProgressDrawable;

public class ResetPasswordAsyncTask extends
		AsyncTask<Void, Void, ServiceResponse<Object>> {

	private static final String TAG = ResetPasswordAsyncTask.class
			.getSimpleName();

	public AsyncResponse<ServiceResponse<Object>> asyncResponse;

	private Context mContext;

	String[] commandString = null;
	ArrayList<NameValuePair> httpParams = null;
	private ProgressDialog mProgressDialog;

	private String userName = null;
	private String dob = null;
	private String apiUrl = null;
	private int errorCode;

	public ResetPasswordAsyncTask(Context context, String userName, String dobValue) {
		this.mContext = context;
		this.userName = userName;
		this.dob = dobValue;
	}

	@Override
	protected void onPreExecute() {
		Log.d(TAG, "onPreExecute");
		super.onPreExecute();

		mProgressDialog = new ProgressDialog(mContext);
		mProgressDialog
				.setIndeterminateDrawable(new CircularProgressDrawable.Builder(
						mContext)
						.colors(mContext.getResources().getIntArray(
								R.array.harrys_colors)).sweepSpeed(1f)
						.strokeWidth(12f)
						.style(CircularProgressDrawable.Style.ROUNDED).build());

		mProgressDialog.setCancelable(false);
		mProgressDialog.setMessage("Resetting user password..");
		mProgressDialog.show();

		try {
			ArrayList<NameValuePair> arrayList = new ArrayList<NameValuePair>();
			arrayList.add(new BasicNameValuePair(ATTR_TAG_DOB, dob));
			
			apiUrl = String.format(URL_RESET_PASSWORD, userName);
			httpParams = AppUtil.generateAuthCommandString(arrayList, apiUrl,
					HTTP_METHOD_GET);

		} catch (Exception e) {

			e.printStackTrace();
		}

	}

	@Override
	protected void onPostExecute(ServiceResponse<Object> response) {
		Log.d(TAG, "onPostExecute");

		if (mProgressDialog != null && mProgressDialog.isShowing()) {
			mProgressDialog.dismiss();
		}
		asyncResponse.processFinish(response);
	}

	@Override
	protected ServiceResponse<Object> doInBackground(Void... voids) {
		Log.d(TAG, "doInBackground");

		ServiceResponse<Object> serviceResponse = null;
		//BeanUserData response = null;

		RestTemplate restTemplate = WebServiceUtil.getRestTemplateInstance();

		restTemplate.setErrorHandler(new DefaultResponseErrorHandler() {
			@Override
			public void handleError(ClientHttpResponse response)
					throws IOException {

				ObjectMapper mapper = WebServiceUtil.getErrorObjMapper();
				ErrorServiceResponse errorRes;
				errorRes = mapper.readValue(response.getBody(),
						new TypeReference<ErrorServiceResponse>() {
						});

				throw new RestServiceException(errorRes, errorRes
						.getError_message());
			}

			protected boolean hasError(HttpStatus statusCode) {
				Log.d(TAG, "Status ----> " + statusCode);
				errorCode = statusCode.value();
				if (statusCode.value() == 200) {
					return false;
				}
				return true;
			}
		});

		String url = String.format(SERVICE_END_POINT + "%s", apiUrl);

		String urlParams = URLEncodedUtils.format(httpParams, "UTF-8");

		String finalUrl = String.format(url + "?" + "%s", urlParams);

		Log.d(TAG, "Final URL ------> " + finalUrl);

		HttpHeaders requestHeaders = new HttpHeaders();
		requestHeaders.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
		requestHeaders.set("Accept", "application/json");
		requestHeaders.add(ATTR_TAG_UDID, AppUtil.getDeviceId(mContext));

		MultiValueMap<String, String> map = new LinkedMultiValueMap<String, String>();
		for (NameValuePair a : httpParams) {
			map.add(a.getName(), a.getValue());
		}

		HttpEntity<MultiValueMap<String, String>> requestEntity = new HttpEntity<MultiValueMap<String, String>>(
				map, requestHeaders);

		try {
			serviceResponse = restTemplate.exchange(finalUrl, HttpMethod.GET,
					requestEntity, new ParameterizedTypeReference<ServiceResponse<Object>>(){}).getBody();
//
//			serviceResponse = new ServiceResponse<BeanMobileUser>();
//			serviceResponse.setResponse(response.getMobileuser());
//			serviceResponse.setError(false);
//			serviceResponse.setStatus(200);

		} catch (RestClientException e) {
			serviceResponse = new ServiceResponse<Object>();
			serviceResponse.setError(true);

			if (errorCode == 500) {
				serviceResponse.setError_message("Internal server error");
			} else {
				serviceResponse.setError_message(e.getCause().getMessage());
			}

		} catch (Exception e) {
			Log.e(TAG, "Exception" + e.getMessage());
			e.printStackTrace();
		}
		return serviceResponse;
	}

	@Override
	protected void onCancelled() {

		if (mProgressDialog != null && mProgressDialog.isShowing()) {
			mProgressDialog.dismiss();
			asyncResponse.processFinish(null);
		}
	}
}
