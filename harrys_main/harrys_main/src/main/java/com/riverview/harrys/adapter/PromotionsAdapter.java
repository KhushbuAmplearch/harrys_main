package com.riverview.harrys.adapter;

import java.util.ArrayList;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.BitmapImageViewTarget;
import com.iapps.libs.helpers.BaseUIHelper;
import com.riverview.harrys.R;
import com.riverview.harrys.helpers.Utils;
import com.riverview.harrys.objects.BeanPromotionItem;

public class PromotionsAdapter extends BaseAdapter {
    private ArrayList<BeanPromotionItem> mItems = new ArrayList<BeanPromotionItem>();
    private Activity context;

    public PromotionsAdapter(Activity context, ArrayList<BeanPromotionItem> list) {
        this.context = context;
        this.mItems = list;
    }

    @Override
    public int getCount() {
        try {
            return mItems.size();
        } catch (Exception e) {
            return 0;
        }
    }

    @Override
    public BeanPromotionItem getItem(int pos) {
        return mItems.get(pos);
    }

    @Override
    public long getItemId(int position) {
        return mItems.get(position).getId();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final ViewHolder holder;

        if (convertView == null) {
            holder = new ViewHolder();
            LayoutInflater inflater = (LayoutInflater) context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

            convertView = inflater.inflate(R.layout.cell_promotions_item,
                    parent, false);
            holder.LLRoot = (LinearLayout) convertView
                    .findViewById(R.id.LLRoot);
            holder.infoStore = (TextView) convertView
                    .findViewById(R.id.textViewPromotionContent);
            holder.imgStore = (ImageView) convertView
                    .findViewById(R.id.imgStoreItem);
            holder.promoTitle = (TextView) convertView
                    .findViewById(R.id.textViewPromotionTitle);

            convertView.setTag(holder);

        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        // set cell height by screen height ratio
        //Utils.setWidthHeightByPercent(context, holder.imgStore, 0.30f, 0.23f);

        // set textview size by screen width ratio
        // Utils.procTextsizeBasedScreen(context, holder.infoStore, 0.65f);
        Utils.setTextViewFontSizeBasedOnScreenDensity(context,
                holder.infoStore, 28f);
        Utils.setTextViewFontSizeBasedOnScreenDensity(context,
                holder.promoTitle, 23f, Typeface.BOLD);

        //Utils.setMarginByPercent(context, holder.LLRoot, 0.01f, true);

        BeanPromotionItem obj = getItem(position);
        holder.infoStore.setText(obj.getDescription());
        holder.promoTitle.setText(obj.getName());
        if(obj.getStartdate().contains("2016"))
        {
            holder.imgStore.setScaleType(ImageView.ScaleType.CENTER_CROP);
        }else {
            holder.imgStore.setScaleType(ImageView.ScaleType.FIT_XY);
        }
        //BaseUIHelper.loadImageWithPlaceholder(context, obj.getImageurl(), holder.imgStore, R.drawable.placeholder);
        Glide.with(context).load( obj.getImageurl())
                .asBitmap()
                .into(new BitmapImageViewTarget(holder.imgStore) {
                    @Override
                    public void onResourceReady(Bitmap drawable, GlideAnimation anim) {
                        super.onResourceReady(drawable, anim);
                        holder.imgStore.setImageBitmap(drawable);
                    }
                });
        return convertView;
    }

    public class ViewHolder {

        TextView promoTitle;
        TextView infoStore;
        ImageView imgStore;
        LinearLayout LLRoot;
    }
}
