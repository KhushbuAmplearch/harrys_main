package com.riverview.harrys.fragments;

import static com.riverview.harrys.constant.AppConst.ATTR_TAG_UDID;
import static com.riverview.harrys.constant.AppConst.ATTR_TAG_WITH;
import static com.riverview.harrys.constant.AppConst.HTTP_METHOD_GET;
import static com.riverview.harrys.constant.AppConst.PULL_TO_REFRESH_TIMEOUT;
import static com.riverview.harrys.constant.AppConst.SERVICE_END_POINT;
import static com.riverview.harrys.constant.AppConst.URL_PRODUCTS_CATEGORY_DATA;
import static com.riverview.harrys.constant.AppConst.URL_UPC_BY_CATAGORY_SUBCATAGORY;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.http.NameValuePair;
import org.apache.http.client.utils.URLEncodedUtils;
import org.apache.http.message.BasicNameValuePair;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.type.TypeReference;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.client.ClientHttpResponse;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.DefaultResponseErrorHandler;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnFocusChangeListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.view.ViewStub;
import android.view.animation.AccelerateInterpolator;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ListView;
import com.iapps.libs.helpers.BaseHelper;
import com.iapps.libs.helpers.BaseHelper.CancelListener;
import com.iapps.libs.helpers.BaseHelper.ConfirmListener;
import com.iapps.libs.helpers.BaseUIHelper;
import com.iapps.libs.views.LoadingCompound;
import com.nineoldandroids.animation.ValueAnimator;
import com.orangegangsters.github.swipyrefreshlayout.library.SwipyRefreshLayout;
import com.orangegangsters.github.swipyrefreshlayout.library.SwipyRefreshLayout.OnRefreshListener;
import com.orangegangsters.github.swipyrefreshlayout.library.SwipyRefreshLayoutDirection;
import com.riverview.harrys.MainActivity;
import com.riverview.harrys.R;
import com.riverview.harrys.RCGenericFragment;
import com.riverview.harrys.adapter.ProductsAdapter;
import com.riverview.harrys.adapter.SubCategoryAdapter;
import com.riverview.harrys.asynctask.AsyncResponse;
import com.riverview.harrys.asynctask.UpcByCatagoryAsyncTask;
import com.riverview.harrys.exception.RestServiceException;
import com.riverview.harrys.helpers.HeightEvaluator;
import com.riverview.harrys.helpers.MatchCategoryPredicate;
import com.riverview.harrys.helpers.ObjectSerializer;
import com.riverview.harrys.helpers.SharedPref;
import com.riverview.harrys.objects.BeanCategoryWiseProduct;
import com.riverview.harrys.objects.BeanProductItem;
import com.riverview.harrys.objects.BeanSubCategory;
import com.riverview.harrys.objects.BeanSubcategoryItem;
import com.riverview.harrys.objects.BeanUpc;
import com.riverview.harrys.objects.ErrorServiceResponse;
import com.riverview.harrys.objects.Paginator;
import com.riverview.harrys.objects.ServiceResponse;
import com.riverview.harrys.util.AnaylaticsAppender;
import com.riverview.harrys.util.AppUtil;
import com.riverview.harrys.util.WebServiceUtil;

import fr.castorflex.android.circularprogressbar.CircularProgressDrawable;

public class UpcWithPaginationFragment extends RCGenericFragment{

	private static final String TAG = UpcWithPaginationFragment.class
			.getSimpleName();

	private ProgressDialog mProgressDialog;

	public static final int TAG_LIST_STORE = 0;

	private static final int DRINK_CATAGORY = 1;
	private static final int FOOD_CATAGORY = 2;

	private AnaylaticsAppender mAppender = null;

	private static int TAB_CURRENT_CATEGORY = DRINK_CATAGORY;
	private ViewStub emptyView;
	private ListView lvSubCategory;
	private Button buttonDrinks;
	private Button buttonFoods;
	private LoadingCompound ld;
	private FrameLayout slideDownView;
	private FrameLayout frame;
	//private SwipyRefreshLayout mSwipyRefreshLayout;
	View v;
	private ProductsAdapter mAdapter;
	private SubCategoryAdapter mSubAdapter;
	private ArrayList<BeanProductItem> productList;
	private ArrayList<BeanProductItem> drinkList;
	private ArrayList<BeanProductItem> foodList;

	private ArrayList<BeanProductItem> drinkFList = new ArrayList<BeanProductItem>();
	private ArrayList<BeanProductItem> foodFList = new ArrayList<BeanProductItem>();

	private ArrayList<BeanProductItem> sortedDrinkList = new ArrayList<BeanProductItem>();
	private ArrayList<BeanProductItem> sortedFoodList = new ArrayList<BeanProductItem>();

	private ArrayList<BeanSubcategoryItem> drinkSubList;
	private ArrayList<BeanSubcategoryItem> foodSubList;

	private static float height = 0f;
	private static final int VIEW_ANIMATION_INTERVAL = 300;

	private static boolean toogleStatus = false;
	WebView webView1,webView2;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		SharedPref.deleteString(getActivity(), SharedPref.LISTDATA_KEY);
		SharedPref.deleteString(getActivity(),
				SharedPref.UPC_CURRENT_LIST_ID_KEY);

	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
							 Bundle savedInstanceState) {
		v = inflater.inflate(R.layout.product_fragment_layout, container, false);
		setHasOptionsMenu(false);
		//lvStores = (ListView)v.findViewById(R.id.listViewStore);
		emptyView = (ViewStub)v.findViewById(R.id.emptyView);
		lvSubCategory = (ListView)v.findViewById(R.id.listViewSubCatagory);
		buttonDrinks = (Button) v.findViewById(R.id.buttonDrink);
		buttonFoods = (Button) v.findViewById(R.id.buttonFood);
		ld = (LoadingCompound)v.findViewById(R.id.ld);
		slideDownView = (FrameLayout) v.findViewById(R.id.product_catagory_dropdown_layout);
		frame = (FrameLayout) v.findViewById(R.id.frame);
		//mSwipyRefreshLayout = (SwipyRefreshLayout) v.findViewById(R.id.swipyrefreshlayout);
		webView1 = (WebView) v.findViewById(R.id.webView1);
		webView2 = (WebView)v.findViewById(R.id.webView2);
		((MainActivity) getActivity()).setBackActionBar(getActivity()
				.getResources().getString(R.string.rc_menu),View.INVISIBLE,"");
		TAB_CURRENT_CATEGORY = SharedPref.getInteger(getActivity(),
				SharedPref.UPC_CURRENT_LIST_ID_KEY, DRINK_CATAGORY);

		webView1.setVisibility(View.VISIBLE);
		webView2.setVisibility(View.GONE);
		webView1.getSettings().setJavaScriptEnabled(true);
		String pdf = "http://harrys.com.sg/sites/harrys/files/menu/Harrys_Beverage_Menu.pdf";
		webView1.loadUrl("http://docs.google.com/viewer?url="+pdf);
		webView1.setWebViewClient(new MyBrowser());

		toogleStatus = false;

		return v;
	}

	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
		inflater.inflate(R.menu.menu_product, menu);
	}

	private void toggle(final FrameLayout v) {
		try {

			if (!toogleStatus) {
				//	lvStores.setEnabled(false);

				height = BaseUIHelper.getScreenHeight(getHome());

				height = (float) (height * 0.7);

				v.setVisibility(View.VISIBLE);
				ValueAnimator va = ValueAnimator.ofFloat(0f, height)
						.setDuration(VIEW_ANIMATION_INTERVAL);
				va.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
					public void onAnimationUpdate(ValueAnimator animation) {
						Integer value = (Integer) Math.round((float) animation
								.getAnimatedValue());
						v.getLayoutParams().height = value.intValue();
						v.invalidate();
						v.requestLayout();

					}
				});
				va.setInterpolator(new AccelerateInterpolator(2));
				va.start();

				toogleStatus = true;
			} else {
				toogleStatus = false;
				closeToggle();
			}
		} catch (Exception e) {
			Log.e(TAG, "Error loading toggle " + e.getMessage());
		}
	}

	private void closeToggle() {

		//lvStores.setEnabled(true);

		int startHeight = slideDownView.getHeight();
		ValueAnimator animation = ValueAnimator.ofObject(
				new HeightEvaluator(slideDownView), startHeight, (int) 0)
				.setDuration(VIEW_ANIMATION_INTERVAL);
		animation.setInterpolator(new AccelerateInterpolator(2));
		animation.start();
	}

	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {

			case R.id.rv_menu_catagory:
				if (!toogleStatus) {

					if (TAB_CURRENT_CATEGORY == DRINK_CATAGORY
							&& drinkSubList != null && drinkSubList.size() > 0) {
						mSubAdapter = new SubCategoryAdapter(getActivity(),
								drinkSubList);
						lvSubCategory.setAdapter(mSubAdapter);
						mSubAdapter.notifyDataSetChanged();

						toggle(slideDownView);
					} else if (TAB_CURRENT_CATEGORY == FOOD_CATAGORY
							&& foodSubList != null && foodSubList.size() > 0) {
						mSubAdapter = new SubCategoryAdapter(getActivity(),
								foodSubList);
						lvSubCategory.setAdapter(mSubAdapter);
						mSubAdapter.notifyDataSetChanged();

						toggle(slideDownView);
					}
				} else {
					// Close Sub menu
					toogleStatus = false;
					closeToggle();
				}
				break;

			default:
				return true;
		}
		return true;
	}

	@Override
	public void onResume() {
		super.onResume();

		try {
			mAppender = AnaylaticsAppender.getInstance(getActivity());
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);

		closeToggle();
		buttonDrinks.setOnTouchListener(new OnTouchListener() {
			@Override
			public boolean onTouch(View v, MotionEvent event) {
				webView1.setVisibility(View.VISIBLE);
				webView2.setVisibility(View.GONE);
				String pdf = "http://harrys.com.sg/sites/harrys/files/menu/Harrys_Beverage_Menu.pdf";
				webView1.loadUrl("http://docs.google.com/viewer?url="+pdf);
				webView1.setWebViewClient(new MyBrowser());
				WebSettings webSettings = webView1.getSettings();
				webSettings.setJavaScriptEnabled(true);
				return false;
			}
		});
		buttonFoods.setOnTouchListener(new OnTouchListener() {
			@Override
			public boolean onTouch(View v, MotionEvent event) {

				webView1.setVisibility(View.GONE);
				webView2.setVisibility(View.VISIBLE);
				String pdf = "http://harrys.com.sg/sites/harrys/files/menu/Harrys_Food_Menu.pdf";
				webView2.loadUrl("http://docs.google.com/viewer?url="+pdf);
				webView2.setWebViewClient(new MyBrowser());
				WebSettings webSettings = webView2.getSettings();
				webSettings.setJavaScriptEnabled(true);
				return false;
			}
		});
		if (TAB_CURRENT_CATEGORY == FOOD_CATAGORY) {
			buttonFoods.requestFocus();

		} else {
			buttonDrinks.requestFocus();

		}

	}

	private class MyBrowser extends WebViewClient {
		@Override
		public boolean shouldOverrideUrlLoading(WebView view, String url) {
			view.loadUrl(url);
			view.setFocusableInTouchMode(false);
			return true;
		}
	}

	@Override
	public void onPause() {
		super.onPause();
	}

}
