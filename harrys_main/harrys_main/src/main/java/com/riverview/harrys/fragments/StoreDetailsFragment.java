package com.riverview.harrys.fragments;

import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.json.JSONException;
import org.json.JSONObject;

import android.graphics.Typeface;
import android.os.Bundle;
import android.text.Spannable;
import android.text.method.LinkMovementMethod;
import android.text.style.URLSpan;
import android.text.util.Linkify;
import android.text.util.Linkify.TransformFilter;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.model.LatLng;
import com.iapps.libs.helpers.BaseHelper;
import com.iapps.libs.helpers.BaseUIHelper;
import com.riverview.harrys.MainActivity;
import com.riverview.harrys.R;
import com.riverview.harrys.RCGenericFragment;
import com.riverview.harrys.helpers.Utils;
import com.riverview.harrys.objects.BeanStoreItem;
import com.riverview.harrys.util.AnaylaticsAppender;
import com.riverview.harrys.util.AppUtil;

import static com.riverview.harrys.MainActivity.result_main_image;

public class StoreDetailsFragment extends RCGenericFragment {

	private ImageView imgStoreDetail;
	private TextView textViewContent;
	private TextView textViewTitle;
	private TextView textViewTelephone;
	private LinearLayout LLSpaceContainer;
	private LinearLayout LLSpaceContainer1;
	private LinearLayout LLImageContainer;

	private View v;

	private BeanStoreItem storeItem;

	private AnaylaticsAppender mAppender = null;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setHasOptionsMenu(false);
	}

	@Override
	public void onResume() {
		// TODO Auto-generated method stub
		super.onResume();

		try {
			mAppender = AnaylaticsAppender.getInstance(getActivity());

			JSONObject attributes = new JSONObject();
			try {
				attributes.put("title", storeItem.getInfo());
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			mAppender
					.updateAnaylitsData(AppUtil.createAnalyticsJsonEventAction(
							"outlet", String.valueOf(storeItem.getId()), "view",
							attributes.toString()));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		getActivity().getActionBar().show();
		v = inflater.inflate(R.layout.stores_detail_fragment_layout, container,
				false);
		imgStoreDetail = (ImageView)v.findViewById(R.id.imgStoreDetail);
		textViewContent = (TextView)v.findViewById(R.id.textViewContent);
		textViewTitle = (TextView)v.findViewById(R.id.textViewTitle);
		textViewTelephone = (TextView)v.findViewById(R.id.textViewTelephone);
		LLSpaceContainer = (LinearLayout) v.findViewById(R.id.LLSpaceContainer);
		LLSpaceContainer1 = (LinearLayout)v.findViewById(R.id.LLSpaceContainer1);
		LLImageContainer = (LinearLayout)v.findViewById(R.id.LLImageContainer);
		storeItem = (BeanStoreItem) getArguments().getParcelable("details");
		((MainActivity) getActivity()).setBackActionBar(storeItem.getInfo(),View.VISIBLE,"map");
		result_main_image.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				Bundle bundle = new Bundle();
				bundle.putString("title", storeItem.getInfo());

				try {
					LatLng storeLocation = new LatLng(storeItem.getLatitude(),
							storeItem.getLongitude());
					bundle.putParcelable("storeLocation", storeLocation);
				} catch (Exception e) {
				}

				MapFragment fragment = new MapFragment();
				fragment.setArguments(bundle);
				getHome().setFragment(fragment);
			}
		});
		return v;
	}

	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
		inflater.inflate(R.menu.menu_location, menu);
	}

	public boolean onOptionsItemSelected(MenuItem item) {

		switch (item.getItemId()) {
		case R.id.rv_menu_locations:

			Bundle bundle = new Bundle();
			bundle.putString("title", storeItem.getInfo());

			try {
				LatLng storeLocation = new LatLng(storeItem.getLatitude(),
						storeItem.getLongitude());
				bundle.putParcelable("storeLocation", storeLocation);
			} catch (Exception e) {
			}

			MapFragment fragment = new MapFragment();
			fragment.setArguments(bundle);
			getHome().setFragment(fragment);
			return true;
		}
		return false;

	}

	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);

		BaseUIHelper.loadImageWithPlaceholder(getActivity(),
				storeItem.getUrl(), imgStoreDetail, R.drawable.placeholder);

		LLImageContainer.setOnClickListener(clickListeners);

		Utils.setMarginByPercent(getActivity(), LLSpaceContainer, .02f);
		Utils.setMarginByPercent(getActivity(), LLSpaceContainer1, .02f);

		Utils.setTextViewFontSizeBasedOnScreenDensity(getActivity(),
				textViewContent, Utils.FONT_SMALLER_DENSITY_SIZE);
		Utils.setTextViewFontSizeBasedOnScreenDensity(getActivity(),
				textViewTitle, Utils.FONT_SMALL_DENSITY_SIZE, Typeface.BOLD);

		textViewTitle.setText(storeItem.getInfo());

		StringBuilder sb = new StringBuilder("\nAddress : \n"
				.concat(storeItem.getAddress()).concat("\n")
				.concat("Singapore").concat("\t")
				.concat(storeItem.getPostelcode()));
		sb.append("\n\n");

		if (!BaseHelper.isEmpty(storeItem.getOpeninghours())) {
			sb.append("Operating Hours : \n".concat((storeItem
					.getOpeninghours() == "") ? "Unavaliable" : storeItem
					.getOpeninghours()));
			sb.append("\n");
		}

		if (!BaseHelper.isEmpty(storeItem.getEmail())) {
			sb.append("Email : ".concat(storeItem.getEmail()).concat("\n"));
		}

		this.textViewContent.setText(sb.toString());

		sb.setLength(0);
		if (!BaseHelper.isEmpty(storeItem.getTelephome())) {
			sb.append("Telephone : ".concat(storeItem.getTelephome()).concat(
					"\n"));
		}

		this.textViewTelephone.setText(sb.toString());
		Linkify.addLinks(textViewTelephone, Linkify.PHONE_NUMBERS);
		
		textViewTelephone.setLinksClickable(true);
		textViewTelephone.setMovementMethod(LinkMovementMethod.getInstance());
		

	}

	View.OnClickListener clickListeners = new View.OnClickListener() {
		public void onClick(View v) {

			switch (v.getId()) {
			case R.id.LLImageContainer:
				try {
					String[] list = storeItem.getPhotos().split(",");
					ArrayList<String> al = new ArrayList<>(list.length);

					for (int i = 0; i < list.length; i++) {
						if (!BaseHelper.isEmpty(list[i])) {
							al.add(list[i]);
						}
					}
					if (al != null && al.size() > 0) {
						StoreImageGallaryFragment frag = new StoreImageGallaryFragment();
						Bundle bundle = new Bundle();

						bundle.putStringArrayList("photo_details", al);
						bundle.putString("name", storeItem.getInfo());

						frag.setArguments(bundle);
						getHome().setFragment(frag);
					} else {
						Toast.makeText(getHome(), "No images found",
								Toast.LENGTH_SHORT).show();
					}

				} catch (Exception e) {
					e.printStackTrace();
				}

				break;

			default:
				break;
			}
		}
	};

}
