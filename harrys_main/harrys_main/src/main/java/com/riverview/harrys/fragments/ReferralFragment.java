package com.riverview.harrys.fragments;

import static com.riverview.harrys.constant.AppConst.ATTR_TAG_UDID;
import static com.riverview.harrys.constant.AppConst.HTTP_METHOD_GET;
import static com.riverview.harrys.constant.AppConst.SERVICE_END_POINT;
import static com.riverview.harrys.constant.AppConst.URL_GET_REFERRALS_WITH_CODE;

import java.io.IOException;
import java.text.DecimalFormat;
import java.util.ArrayList;

import org.apache.http.NameValuePair;
import org.apache.http.client.utils.URLEncodedUtils;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.type.TypeReference;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.client.ClientHttpResponse;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.DefaultResponseErrorHandler;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;


import android.content.Context;

import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager.NameNotFoundException;

import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;

import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.iapps.libs.helpers.BaseHelper;
import com.iapps.libs.helpers.BaseHelper.CancelListener;
import com.iapps.libs.helpers.BaseHelper.ConfirmListener;
import com.iapps.libs.views.LoadingCompound;
import com.riverview.harrys.MainActivity;
import com.riverview.harrys.R;
import com.riverview.harrys.RCGenericFragment;
import com.riverview.harrys.adapter.ReferralResponseAdapter;
import com.riverview.harrys.asynctask.AsyncResponse;
import com.riverview.harrys.asynctask.ReferralAsyncTask;
import com.riverview.harrys.exception.RestServiceException;
import com.riverview.harrys.helpers.Constants;
import com.riverview.harrys.helpers.SharedPref;
import com.riverview.harrys.objects.BeanCode;
import com.riverview.harrys.objects.BeanPoints;
import com.riverview.harrys.objects.BeanRebates;
import com.riverview.harrys.objects.BeanReceiver;
import com.riverview.harrys.objects.BeanReferralCampaign;
import com.riverview.harrys.objects.BeanReferralCampaigns;
import com.riverview.harrys.objects.BeanReferralTireVoucherData;
import com.riverview.harrys.objects.BeanSender;
import com.riverview.harrys.objects.BeanStamps;
import com.riverview.harrys.objects.BeanTierReferralOuter;
import com.riverview.harrys.objects.BeanTopup;
import com.riverview.harrys.objects.BeanVoucher;
import com.riverview.harrys.objects.ErrorServiceResponse;
import com.riverview.harrys.objects.ServiceResponse;
import com.riverview.harrys.util.AppUtil;
import com.riverview.harrys.util.WebServiceUtil;

public class ReferralFragment extends RCGenericFragment implements
		AsyncResponse<ServiceResponse<ArrayList<BeanReferralCampaigns>>> {

	private static final String TAG = ReferralFragment.class.getSimpleName();

	private TextView textDeviceData;
	private TextView textOsData;
	private TextView textAppNameData;
	private TextView textVersionData;
	private TextView textBuildData;
	private LinearLayout LLMainContainer;
	private ListView mainListView;
	private ListView secondryListView;
	private ListView mainListView1;
	private ListView secondryListView1;
	private Button buttonOptions;
	private LoadingCompound ld;

	private View v;

	private int mobileUserId;

	private BeanCode mBeanCode = null;

	private ReferralAsyncTask mReferralCampaignAsyncTask = null;
	private ReferralCampaignWithCodeAsyncTask mReferralCampaignWithCodeAsyncTask = null;
	private BeanReferralCampaigns mBeanReferralCampaigns;

	DecimalFormat decimalFormat = new DecimalFormat("0.00");

	ArrayList<String> friendBenifits = new ArrayList<>();
	ArrayList<String> friendVoucherBenifits = new ArrayList<>();

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		v = inflater.inflate(R.layout.referral_fragment_layout, container,
				false);
		textDeviceData = (TextView)v.findViewById (R.id.textDeviceData);
		textOsData = (TextView)v.findViewById (R.id.textOsData);
		textAppNameData = (TextView)v.findViewById (R.id.textAppNameData);
		textVersionData = (TextView)v.findViewById (R.id.textVersionData);
		textBuildData = (TextView)v.findViewById (R.id.textBuildData);
		LLMainContainer = (LinearLayout)v.findViewById(R.id.LLMainContainer);
		mainListView = (ListView)v.findViewById(R.id.mainListView);
		secondryListView = (ListView)v.findViewById(R.id.secondryListView);
		mainListView1 = (ListView)v.findViewById(R.id.mainListView1);
		secondryListView1 = (ListView)v.findViewById(R.id.secondryListView1);
		buttonOptions = (Button)v.findViewById(R.id.buttonOptions);
		ld = (LoadingCompound)v.findViewById(R.id.ld);

		setHasOptionsMenu(false);
		((MainActivity) getActivity()).setBackActionBar(getHome()
				.getResources().getString(R.string.rc_menu_invite_friend),View.INVISIBLE,"");
		hideDefaultKeyboard();

		return v;
	}

	private void hideDefaultKeyboard() {
		try {
			getActivity().getWindow().setSoftInputMode(
					WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

			getActivity();
			InputMethodManager imm = (InputMethodManager) getActivity()
					.getSystemService(Context.INPUT_METHOD_SERVICE);

		} catch (Exception e) {
		}

	}

	private PackageInfo getPackageData() {
		PackageInfo pInfo;
		try {
			pInfo = getHome().getPackageManager().getPackageInfo(
					getHome().getPackageName(), 0);
			return pInfo;
		} catch (NameNotFoundException e) {
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);

		// textSelectFeedback.setOnClickListener(clickListeners);
		textDeviceData.setText(AppUtil.getDeviceName());
		textOsData.setText(String.valueOf(Build.VERSION.SDK_INT));

		buttonOptions.setOnClickListener(clickListeners);
		try {

			PackageInfo pInfo = getPackageData();
			if (pInfo != null) {
				textVersionData.setText(pInfo.versionName);
				textBuildData.setText(String.valueOf(pInfo.versionCode));
				textAppNameData
						.setText(getString(pInfo.applicationInfo.labelRes));

			}
		} catch (Exception e) {

		}
	}

	@Override
	public void onResume() {
		// TODO Auto-generated method stub
		super.onResume();

		mobileUserId = SharedPref.getInteger(getActivity(),
				SharedPref.USERID_KEY, 0);
		// call service to get available referral programs
		callGetReferralsApi();
	}

	private boolean callGetReferralsApi() {
		if (AppUtil.checkNetworkConnection(getActivity())) {

			mReferralCampaignAsyncTask = new ReferralAsyncTask(getActivity(),
					mobileUserId, Constants.REFERRAL_TYPE_NETWORK);
			mReferralCampaignAsyncTask.asyncResponse = ReferralFragment.this;
			mReferralCampaignAsyncTask.execute((Void) null);

			ld.bringToFront();
			ld.showLoading();

			return true;

		} else {

			BaseHelper.confirm(getActivity(), "Network Unavaliable",
					"Please turn on network to connect with server",
					new ConfirmListener() {

						@Override
						public void onYes() {
							AppUtil.showSystemSettingsDialog(getActivity());
						}
					}, new CancelListener() {

						@Override
						public void onNo() {
							getHome().backToHomeScreen();

						}
					});
		}
		return false;

	}

	View.OnClickListener clickListeners = new View.OnClickListener() {
		public void onClick(View v) {

			switch (v.getId()) {
			case R.id.buttonOptions:
				shareCodeAndInvite();

				break;

			default:
				break;
			}

		}
	};

	private void shareCodeAndInvite() {

		if (mBeanCode != null) {
			String userEntry = "Hey, checkout Harry's application with my promo code %s and get %s. Download the app from https://play.google.com/store/apps/details?id=com.riverview.harrys&hl=en";

			String benifitsString = "";

			if (friendBenifits != null && friendBenifits.size() > 0) {

				for (String string : friendBenifits) {
					benifitsString += string;
					benifitsString.concat("\t");
				}
			}

			if (friendVoucherBenifits != null
					&& friendVoucherBenifits.size() > 0) {
				for (String string : friendVoucherBenifits) {
					benifitsString += string;
					benifitsString.concat("\t");
				}
			}

			String benifits = !BaseHelper.isEmpty(benifitsString) ? benifitsString
					.trim() : " many benefits";

			String formattedEntry = String.format(userEntry,
					mBeanCode.getCode(), benifits);
			Intent textShareIntent = new Intent(Intent.ACTION_SEND);
			textShareIntent.putExtra(Intent.EXTRA_TEXT, formattedEntry);
			textShareIntent.setType("text/plain");
			startActivity(textShareIntent);
		}

	}

	@Override
	public void processFinish(
			ServiceResponse<ArrayList<BeanReferralCampaigns>> details) {
		Log.d(TAG, "Process Finished");

		if (details != null && details.getStatus() == 200 && !details.error) {
			// Take the first campaign data and call api service to generate
			// referral code
			ArrayList<BeanReferralCampaigns> arrayList = details.getResponse();
			if (arrayList.size() > 0) {
				mBeanReferralCampaigns = arrayList.get(0);

				// call service to get generated referral code
				callGetReferralsWithCodeApi();
			} else {
				ld.hide();
				// no campaigns found
			}

		} else {
			try {

				ld.hide();
				BaseHelper.showAlert(
						getActivity(),
						"Harry's",
						details.getError_message() != null ? details
								.getError_message()
								: getString(R.string.msg_operation_fail));
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

	}

	private void callGetReferralsWithCodeApi() {
		// call method to get referral code for the campaign
		if (AppUtil.checkNetworkConnection(getActivity())) {

			mReferralCampaignWithCodeAsyncTask = new ReferralCampaignWithCodeAsyncTask(
					getActivity(), mobileUserId,
					Integer.valueOf(mBeanReferralCampaigns.getId()),
					Constants.REFERRAL_TYPE_NETWORK);
			mReferralCampaignWithCodeAsyncTask.execute((Void) null);

			ld.bringToFront();

		} else {

			BaseHelper.confirm(getActivity(), "Network Unavaliable",
					"Please turn on network to connect with server",
					new ConfirmListener() {

						@Override
						public void onYes() {
							AppUtil.showSystemSettingsDialog(getActivity());
						}
					}, new CancelListener() {

						@Override
						public void onNo() {
							getHome().backToHomeScreen();

						}
					});
		}

	}

	// Async task for generate referral code for related campaign
	public class ReferralCampaignWithCodeAsyncTask extends
			AsyncTask<Void, Void, ServiceResponse<BeanReferralCampaign>> {

		private final String TAG = ReferralCampaignWithCodeAsyncTask.class
				.getSimpleName();

		private Context mContext;
		private int mobileUserId;
		private int champaignId;
		private String type;

		String[] commandString = null;
		ArrayList<NameValuePair> httpParams = null;

		private String referralCodeUrl;

		public ReferralCampaignWithCodeAsyncTask(Context context, int id,
				int campId, String type) {
			this.mContext = context;
			this.mobileUserId = id;
			this.champaignId = campId;
			this.type = type;
		}

		@Override
		protected void onPreExecute() {
			Log.d(TAG, "onPreExecute");
			super.onPreExecute();

			try {
				referralCodeUrl = String.format(URL_GET_REFERRALS_WITH_CODE,
						String.valueOf(mobileUserId), type,
						String.valueOf(champaignId));
				Log.d(TAG, "onPreExecute " + referralCodeUrl);
				httpParams = AppUtil.generateAuthCommandString(null,
						referralCodeUrl, HTTP_METHOD_GET);

			} catch (Exception e) {

				e.printStackTrace();
			}

		}

		@Override
		protected void onPostExecute(
				ServiceResponse<BeanReferralCampaign> response) {
			Log.d(TAG, "onPostExecute");

			try {
				ld.hide();
			} catch (Exception e) {
				e.printStackTrace();
			}

			if (response.getStatus() == 200 && !response.error) {

				BeanReferralCampaign referralCampaign = response.getResponse();

				ArrayList<String> userBenifits = new ArrayList<>();

				ArrayList<BeanReferralTireVoucherData> userData = new ArrayList<>();
				ArrayList<BeanReferralTireVoucherData> receiverData = new ArrayList<>();

				// set the code data
				mBeanCode = referralCampaign.getCode();

				BeanReceiver receiver = referralCampaign.getReceiver();
				BeanSender sender = referralCampaign.getSender();

				// set rebates data to the list
				if (sender.getRebates() != null
						&& sender.getRebates().size() > 0) {
					BeanRebates beanRebates = sender.getRebates().get(0);

					Double rebates = Double.parseDouble(beanRebates
							.getRebates_amount());

					userBenifits.add("$"
							+ String.valueOf(decimalFormat.format(rebates))
							+ " Rebates");

				}

				if (sender.getStamps() != null && sender.getStamps().size() > 0) {
					BeanStamps beanStamps = sender.getStamps().get(0);

					userBenifits.add(beanStamps.getStamps_amount() + " Stamps");

				}

				if (sender.getTopup() != null && sender.getTopup().size() > 0) {
					BeanTopup beanTopup = sender.getTopup().get(0);
					Double topup = Double.parseDouble(beanTopup
							.getTopup_amount());

					userBenifits.add("$" + decimalFormat.format(topup)
							+ " Topup");

				}

				if (sender.getPoints() != null && sender.getPoints().size() > 0) {
					BeanPoints beanPoints = sender.getPoints().get(0);

					userBenifits.add(beanPoints.getPoints_amount() + " Points");

				}

				if (sender.getVoucher() != null
						&& sender.getVoucher().size() > 0) {
					BeanReferralTireVoucherData mData = null;

					for (BeanVoucher beanVoucher : sender.getVoucher()) {
						mData = new BeanReferralTireVoucherData();

						// set data
						mData.setId(beanVoucher.getId());
						mData.setImage((beanVoucher.getVoucher() != null) ? beanVoucher
								.getVoucher().getImage() : null);
						mData.setDescription((beanVoucher.getVoucher() != null) ? beanVoucher
								.getVoucher().getDescription() : beanVoucher
								.getDescription());
						mData.setName((beanVoucher.getVoucher() != null) ? beanVoucher
								.getVoucher().getTitle() : null);

						userData.add(mData);
					}
				}

				if (sender.getTier() != null && sender.getTier().size() > 0) {
					BeanReferralTireVoucherData mData = null;

					for (BeanTierReferralOuter beanTier : sender.getTier()) {
						mData = new BeanReferralTireVoucherData();

						// set data
						mData.setId(beanTier.getId());
						mData.setImage((beanTier.getTier() != null) ? beanTier
								.getTier().getCard_face_image() : null);
						mData.setDescription((beanTier.getTier() != null) ? beanTier
								.getTier().getDescription() : null);
						mData.setName((beanTier.getTier() != null) ? beanTier
								.getTier().getCard_name() : null);
					}
				}

				ReferralResponseAdapter userAdaper = new ReferralResponseAdapter(
						getActivity(), userData);
				mainListView1.setAdapter(userAdaper);

				// set array adapter
				ArrayAdapter<String> adapter = new ArrayAdapter<String>(
						mContext, android.R.layout.simple_list_item_1,
						android.R.id.text1, userBenifits);

				mainListView.setAdapter(adapter);

				// set rebates data to the list - Friend
				if (receiver.getRebates() != null
						&& receiver.getRebates().size() > 0) {
					BeanRebates beanRebates = receiver.getRebates().get(0);

					Double rebates = Double.parseDouble(beanRebates
							.getRebates_amount());
					friendBenifits.add("$"
							+ String.valueOf(decimalFormat.format(rebates))
							+ " Rebates ");
				}

				if (receiver.getStamps() != null
						&& receiver.getStamps().size() > 0) {
					BeanStamps beanStamps = receiver.getStamps().get(0);

					friendBenifits.add(beanStamps.getStamps_amount()
							+ " Stamps ");
				}

				if (receiver.getTopup() != null
						&& receiver.getTopup().size() > 0) {
					BeanTopup beanTopup = receiver.getTopup().get(0);

					Double topup = Double.parseDouble(beanTopup
							.getTopup_amount());
					friendBenifits.add("$"
							+ String.valueOf(decimalFormat.format(topup))
							+ " Topup ");
				}

				if (receiver.getPoints() != null
						&& receiver.getPoints().size() > 0) {
					BeanPoints beanPoints = receiver.getPoints().get(0);

					friendBenifits.add(beanPoints.getPoints_amount()
							+ " Points ");
				}

				if (receiver.getVoucher() != null
						&& receiver.getVoucher().size() > 0) {
					BeanReferralTireVoucherData mData = null;

					for (BeanVoucher beanVoucher : receiver.getVoucher()) {
						mData = new BeanReferralTireVoucherData();

						// set data
						mData.setId(beanVoucher.getId());
						mData.setImage((beanVoucher.getVoucher() != null) ? beanVoucher
								.getVoucher().getImage() : null);
						mData.setDescription((beanVoucher.getVoucher() != null) ? beanVoucher
								.getVoucher().getDescription() : beanVoucher
								.getDescription());
						mData.setName((beanVoucher.getVoucher() != null) ? beanVoucher
								.getVoucher().getTitle() : null);

						receiverData.add(mData);

						friendVoucherBenifits
								.add(((beanVoucher.getVoucher() != null) ? beanVoucher
										.getVoucher().getTitle() : null)
										+ ((beanVoucher.getVoucher() != null) ? "("
												+ beanVoucher.getVoucher()
														.getDescription() + ")"
												: "("
														+ beanVoucher
																.getDescription()
														+ ") "));
					}
				}

				if (receiver.getTier() != null && receiver.getTier().size() > 0) {
					BeanReferralTireVoucherData mData = null;

					for (BeanTierReferralOuter beanTier : receiver.getTier()) {
						mData = new BeanReferralTireVoucherData();

						// set data
						mData.setId(beanTier.getId());
						mData.setImage((beanTier.getTier() != null) ? beanTier
								.getTier().getCard_face_image() : null);
						mData.setDescription((beanTier.getTier() != null) ? beanTier
								.getTier().getDescription() : null);
						mData.setName((beanTier.getTier() != null) ? beanTier
								.getTier().getCard_name() : null);

						receiverData.add(mData);

						friendVoucherBenifits
								.add(((beanTier.getTier() != null) ? beanTier
										.getTier().getCard_name() : null)
										+ ((beanTier.getTier() != null) ? "("
												+ beanTier.getTier()
														.getDescription()
												+ ") " : null));
					}
				}

				ReferralResponseAdapter receiverAdaper = new ReferralResponseAdapter(
						getActivity(), receiverData);
				secondryListView1.setAdapter(receiverAdaper);

				// set array adapter
				ArrayAdapter<String> secondaryAdapter = new ArrayAdapter<String>(
						mContext, android.R.layout.simple_list_item_1,
						android.R.id.text1, friendBenifits);

				secondryListView.setAdapter(secondaryAdapter);

			}

		}

		@Override
		protected ServiceResponse<BeanReferralCampaign> doInBackground(
				Void... voids) {
			Log.d(TAG, "doInBackground");

			ServiceResponse<BeanReferralCampaign> serviceResponse = null;

			RestTemplate restTemplate = WebServiceUtil
					.getRestTemplateInstance();

			restTemplate.setErrorHandler(new DefaultResponseErrorHandler() {
				@Override
				public void handleError(ClientHttpResponse response)
						throws IOException {
					ObjectMapper mapper = WebServiceUtil.getErrorObjMapper();
					ErrorServiceResponse errorRes;
					errorRes = mapper.readValue(response.getBody(),
							new TypeReference<ErrorServiceResponse>() {
							});

					throw new RestServiceException(errorRes, errorRes
							.getError_message());
				}

				protected boolean hasError(HttpStatus statusCode) {
					Log.d(TAG, "Status ----> " + statusCode);
					if (statusCode.value() == 200) {
						return false;
					}
					return true;
				}
			});

			String url = String.format(SERVICE_END_POINT + "%s",
					referralCodeUrl);

			String urlParams = URLEncodedUtils.format(httpParams, "UTF-8");
			String finalUrl = String.format(url + "?" + "%s", urlParams);

			Log.d(TAG, "Final URL ------> " + finalUrl);

			HttpHeaders requestHeaders = new HttpHeaders();
			requestHeaders
					.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
			requestHeaders.set("Accept", "application/json");
			requestHeaders.add(ATTR_TAG_UDID, AppUtil.getDeviceId(mContext));

			HttpEntity<MultiValueMap<String, String>> requestEntity = new HttpEntity<>(
					null, requestHeaders);

			Log.d(TAG, "Url " + url);
			Log.d(TAG, "Request Entity " + requestEntity.toString());

			try {
				serviceResponse = restTemplate
						.exchange(
								finalUrl,
								HttpMethod.GET,
								requestEntity,
								new ParameterizedTypeReference<ServiceResponse<BeanReferralCampaign>>() {
								}).getBody();

			} catch (RestClientException e) {
				serviceResponse = new ServiceResponse<>();
				serviceResponse.setError(true);
				serviceResponse.setError_message(e.getCause().getMessage());

			} catch (Exception e) {
				e.printStackTrace();
			}
			return serviceResponse;
		}

		@Override
		protected void onCancelled() {

		}
	}

}
