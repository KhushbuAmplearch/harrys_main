package com.riverview.harrys.fragments;

import static com.riverview.harrys.constant.AppConst.PULL_TO_REFRESH_TIMEOUT;

import java.util.ArrayList;
import java.util.Iterator;

import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnFocusChangeListener;
import android.view.ViewGroup;
import android.view.ViewStub;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.ListView;

import com.iapps.libs.helpers.BaseHelper;
import com.iapps.libs.helpers.BaseHelper.CancelListener;
import com.iapps.libs.helpers.BaseHelper.ConfirmListener;
import com.iapps.libs.views.LoadingCompound;
import com.orangegangsters.github.swipyrefreshlayout.library.SwipyRefreshLayout;
import com.orangegangsters.github.swipyrefreshlayout.library.SwipyRefreshLayout.OnRefreshListener;
import com.orangegangsters.github.swipyrefreshlayout.library.SwipyRefreshLayoutDirection;
import com.riverview.harrys.MainActivity;
import com.riverview.harrys.R;
import com.riverview.harrys.RCGenericFragment;
import com.riverview.harrys.adapter.EventsAdapter;
import com.riverview.harrys.asynctask.AsyncResponse;
import com.riverview.harrys.asynctask.EventBuilderCategoryAsyncTask;
import com.riverview.harrys.helpers.ObjectSerializer;
import com.riverview.harrys.helpers.SharedPref;
import com.riverview.harrys.objects.BeanEventItem;
import com.riverview.harrys.objects.BeanEvents;
import com.riverview.harrys.objects.BeanEventsCategory;
import com.riverview.harrys.objects.ServiceResponse;
import com.riverview.harrys.util.AnaylaticsAppender;
import com.riverview.harrys.util.AppUtil;

public class EventCategoryFragment extends RCGenericFragment implements
		OnRefreshListener,
		AsyncResponse<ServiceResponse<ArrayList<BeanEventsCategory>>> {

	private static final String TAG = EventCategoryFragment.class
			.getSimpleName();

	public static final int TAG_LIST_STORE = 0;

	private static final int FOOD_CATAGORY = 2;
	private static final int DRINK_CATAGORY = 1;

	// private boolean currentListSub = false;

	private static int TAB_CURRENT_CATEGORY = DRINK_CATAGORY;

	private ListView lvStores;
	private ViewStub emptyView;
	private Button buttonDrinks;
	private Button buttonFoods;
	private LinearLayout LLMainContainer;
	private LoadingCompound ld;	private FrameLayout frame;
	private SwipyRefreshLayout mSwipyRefreshLayout;

	private View v;
	private EventsAdapter mAdapter;
	private ArrayList<BeanEventItem> drinkList;
	private ArrayList<BeanEventItem> foodList;
	private EventBuilderCategoryAsyncTask mEventCategoryAsyncTask = null;
	private AnaylaticsAppender mAppender = null;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		SharedPref.deleteString(getActivity(), SharedPref.LISTDATA_KEY);
		SharedPref.deleteString(getActivity(), SharedPref.LISTDATA_1_KEY);
		SharedPref.deleteString(getActivity(), SharedPref.LISTDATA_2_KEY);

		SharedPref.deleteString(getActivity(), SharedPref.EVENT_1_KEY);
		SharedPref.deleteString(getActivity(), SharedPref.EVENT_2_KEY);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		v = inflater.inflate(R.layout.event_fragment_layout, container, false);
		setHasOptionsMenu(false);
		lvStores = (ListView)v.findViewById(R.id.listViewStore);
		emptyView = (ViewStub)v.findViewById(R.id.emptyView);
		buttonDrinks = (Button)v.findViewById(R.id.buttonDrink);
		buttonFoods = (Button)v.findViewById(R.id.buttonFood);
		LLMainContainer = (LinearLayout)v.findViewById(R.id.LLMainContainer);
		ld = (LoadingCompound)v.findViewById(R.id.ld);
		frame = (FrameLayout)v.findViewById(R.id.frame);
		mSwipyRefreshLayout = (SwipyRefreshLayout)v.findViewById(R.id.swipyrefreshlayout);

		((MainActivity) getActivity()).setBackActionBar(getActivity()
				.getResources().getString(R.string.rc_menu_events),View.INVISIBLE,"");

		return v;
	}

	@Override
	public void onResume() {
		super.onResume();

		try {
			mAppender = AnaylaticsAppender.getInstance(getActivity());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);

		mSwipyRefreshLayout.setOnRefreshListener(this);

		lvStores.setOnItemClickListener(ListenerClickItem);
		// lvSubCategory.setOnItemClickListener(listenerSubListClickItem);

		lvStores.setEmptyView(emptyView);

		buttonDrinks.setOnFocusChangeListener(focusChangeListener);
		buttonFoods.setOnFocusChangeListener(focusChangeListener);

		buttonDrinks.setOnClickListener(clickListeners);
		buttonFoods.setOnClickListener(clickListeners);

		buttonDrinks.requestFocus();

		callAPI();
	}

	public OnFocusChangeListener focusChangeListener = new OnFocusChangeListener() {

		@Override
		public void onFocusChange(View v, boolean hasFocus) {
			switch (v.getId()) {
			case R.id.buttonDrink:
				if (hasFocus && drinkList != null) {

					TAB_CURRENT_CATEGORY = DRINK_CATAGORY;
					mAdapter.notifyDataSetInvalidated();

					mAdapter = new EventsAdapter(getActivity(), drinkList);
					lvStores.setAdapter(mAdapter);
					mAdapter.notifyDataSetChanged();

					if (drinkList != null && drinkList.size() < 1) {
						BaseHelper.showAlert(getActivity(), "Harry's",
								"No events to display");
					}
				}
				break;

			case R.id.buttonFood:
				if (hasFocus && foodList != null) {

					TAB_CURRENT_CATEGORY = FOOD_CATAGORY;
					mAdapter.notifyDataSetInvalidated();

					mAdapter = new EventsAdapter(getActivity(), foodList);
					lvStores.setAdapter(mAdapter);
					mAdapter.notifyDataSetChanged();

					if (foodList != null && foodList.size() < 1) {
						BaseHelper.showAlert(getActivity(), "Harry's",
								"No events to display");
					}

				}
				break;

			default:
				break;
			}

		}
	};

	public OnItemClickListener ListenerClickItem = new OnItemClickListener() {

		@Override
		public void onItemClick(AdapterView<?> adapter, View view, int pos,
				long arg3) {

			// Set the data as json in Shared Prefs
			// try {
			// SharedPref.saveString(getActivity(), SharedPref.LISTDATA_1_KEY,
			// ObjectSerializer.serialize(drinkList));
			// SharedPref.saveString(getActivity(), SharedPref.LISTDATA_2_KEY,
			// ObjectSerializer.serialize(foodList));
			//
			// } catch (Exception e) {
			// Log.e(TAG, "Error serilization " + e.getMessage());
			// }

			EventDetailsFragment frag = new EventDetailsFragment();
			Bundle bundle = new Bundle();

			// if (!currentListSub) {
			if (TAB_CURRENT_CATEGORY == DRINK_CATAGORY) {
				bundle.putParcelable("news", drinkList.get(pos));

				mAppender.updateAnaylitsData(AppUtil
						.createAnalyticsJsonEventAction("event",
								String.valueOf(drinkList.get(pos).getId()),
								"click", null));

			} else {
				bundle.putParcelable("news", foodList.get(pos));

				mAppender.updateAnaylitsData(AppUtil
						.createAnalyticsJsonEventAction("event",
								String.valueOf(foodList.get(pos).getId()),
								"click", null));
			}
			//
			// } else if (currentListSub) {
			// if (TAB_CURRENT_CATEGORY == DRINK_CATAGORY) {
			// bundle.putParcelable("news", sortedDrinkList.get(pos));
			// } else {
			// bundle.putParcelable("news", sortedFoodList.get(pos));
			// }
			// }

			bundle.putString("position", String.valueOf(pos));
			frag.setArguments(bundle);
			getHome().setFragment(frag);
		}
	};

	@SuppressWarnings("unchecked")
	public void callAPI() {

		// get saved json data
		// String serilizedJsonString1 = SharedPref.getString(getActivity(),
		// SharedPref.LISTDATA_1_KEY, null);
		// String serilizedJsonString2 = SharedPref.getString(getActivity(),
		// SharedPref.LISTDATA_2_KEY, null);

		String serilizedJsonString1 = null;
		String serilizedJsonString2 = null;

		if (serilizedJsonString1 != null | serilizedJsonString2 != null) {
			try {
				// set headers
				buttonDrinks.setText(SharedPref.getString(getActivity(),
						SharedPref.EVENT_1_KEY, " "));
				buttonFoods.setText(SharedPref.getString(getActivity(),
						SharedPref.EVENT_2_KEY, " "));

				drinkList = (ArrayList<BeanEventItem>) ObjectSerializer
						.deserialize(serilizedJsonString1);
				foodList = (ArrayList<BeanEventItem>) ObjectSerializer
						.deserialize(serilizedJsonString2);

				mAdapter = new EventsAdapter(getActivity(), drinkList);
				lvStores.setAdapter(mAdapter);
				mAdapter.notifyDataSetChanged();

				ld.hide();
				// mPullToRefresh.setRefreshComplete();

			} catch (Exception e) {
				callAPIService();
				Log.e(EventCategoryFragment.TAG,
						"Error deserilization " + e.getMessage());
			}
		} else {
			callAPIService();
		}

	}

	@Override
	public void onPause() {
		super.onPause();

		if (mEventCategoryAsyncTask != null) {
			mEventCategoryAsyncTask.cancel(true);
		}
	}

	private boolean callAPIService() {

		if (AppUtil.checkNetworkConnection(getActivity())) {

			mEventCategoryAsyncTask = new EventBuilderCategoryAsyncTask(
					getActivity());
			mEventCategoryAsyncTask.asyncResponse = EventCategoryFragment.this;
			mEventCategoryAsyncTask.execute((Void) null);

			ld.showLoading();
			return true;

		} else {

			BaseHelper.confirm(getActivity(), "Network Unavaliable",
					"Please turn on network to connect with server",
					new ConfirmListener() {

						@Override
						public void onYes() {
							AppUtil.showSystemSettingsDialog(getActivity());
						}

					}, new CancelListener() {

						@Override
						public void onNo() {
							getHome().backToHomeScreen();

						}
					});
		}
		return false;
	}

	View.OnClickListener clickListeners = new View.OnClickListener() {
		public void onClick(View v) {

			switch (v.getId()) {

			case R.id.buttonDrink:
				if (drinkList != null) {

					TAB_CURRENT_CATEGORY = DRINK_CATAGORY;
					mAdapter.notifyDataSetInvalidated();

					mAdapter = new EventsAdapter(getActivity(), drinkList);
					lvStores.setAdapter(mAdapter);
					mAdapter.notifyDataSetChanged();

				}
				break;

			case R.id.buttonFood:
				if (foodList != null) {

					TAB_CURRENT_CATEGORY = FOOD_CATAGORY;
					mAdapter.notifyDataSetInvalidated();

					mAdapter = new EventsAdapter(getActivity(), foodList);
					lvStores.setAdapter(mAdapter);
					mAdapter.notifyDataSetChanged();

				}

			default:
				break;
			}

		}
	};

	@Override
	public void processFinish(
			ServiceResponse<ArrayList<BeanEventsCategory>> details) {

		ld.hide();
		mSwipyRefreshLayout.setRefreshing(false);

		if (details != null && details.getStatus() == 200 && !details.error) {
			Log.i(TAG, "Event download Success ");

			drinkList = new ArrayList<BeanEventItem>();
			foodList = new ArrayList<BeanEventItem>();

			ArrayList<BeanEventsCategory> list = details.getResponse();
			ArrayList<BeanEventsCategory> arrayList = new ArrayList<>(
					list.size());
			for (Iterator<BeanEventsCategory> iterator = list.iterator(); iterator
					.hasNext();) {
				BeanEventsCategory beanEventCategory = (BeanEventsCategory) iterator
						.next();
				arrayList.add(beanEventCategory);

			}

			if (arrayList != null && arrayList.size() > 0) {
				BeanEventsCategory eventsCategory1 = null;
				ArrayList<BeanEvents> list1 = null;
				try {
					eventsCategory1 = arrayList.get(0);
				} catch (Exception e) {
					e.printStackTrace();
				}
				if (eventsCategory1 != null) {
					list1 = eventsCategory1.getEvents();
					buttonDrinks.setText(eventsCategory1.getName());
					// SharedPref.saveString(getActivity(),
					// SharedPref.EVENT_1_KEY, eventsCategory1.getName());
				}
				if (list1 != null && list1.size() > 0) {
					for (int i = 0; i < list1.size(); i++) {
						BeanEvents category = list1.get(i);
						BeanEventItem beanEventItem = new BeanEventItem(
								category.getId(), category.getTitle());
						beanEventItem.setDescription(category.getDescription());
						beanEventItem.setAdditionalDes(category
								.getMore_details());

						try {
							beanEventItem
									.setStartdate(AppUtil.convertDate(
											getHome(),
											category.getStart_date(), false));
							beanEventItem.setEnddate(AppUtil.convertDate(
									getHome(), category.getEnd_date(), false));
						} catch (Exception e) {
						}
						try {
							beanEventItem.setImageurl((BaseHelper
									.isEmpty(category.getImage()) ? null
									: category.getImage()));
						} catch (Exception e) {
							Log.e(TAG, "Promotion images null");
						}

						drinkList.add(beanEventItem);
					}
				}

				BeanEventsCategory eventsCategory2 = null;
				ArrayList<BeanEvents> list2 = null;
				try {
					eventsCategory2 = arrayList.get(1);
				} catch (Exception e) {
					e.printStackTrace();
				}
				if (eventsCategory2 != null) {
					list2 = eventsCategory2.getEvents();
					buttonFoods.setText(eventsCategory2.getName());
					// SharedPref.saveString(getActivity(),
					// SharedPref.EVENT_2_KEY, eventsCategory2.getName());
				}

				if (list2 != null && list2.size() > 0) {
					for (int i = 0; i < list2.size(); i++) {
						BeanEvents category = list2.get(i);
						BeanEventItem beanEventItem = new BeanEventItem(
								category.getId(), category.getTitle());
						beanEventItem.setDescription(category.getDescription());
						beanEventItem.setAdditionalDes(category
								.getMore_details());

						try {
							beanEventItem
									.setStartdate(AppUtil.convertDate(
											getHome(),
											category.getStart_date(), false));
							beanEventItem.setEnddate(AppUtil.convertDate(
									getHome(), category.getEnd_date(), false));
						} catch (Exception e) {
						}
						try {
							// temp.setImageurl(beanEvents.getImage().getOriginal());
							beanEventItem.setImageurl((BaseHelper
									.isEmpty(category.getImage()) ? null
									: category.getImage()));
						} catch (Exception e) {
							Log.e(TAG, "Promotion images null");
						}

						foodList.add(beanEventItem);
					}
				}
			}

			try {

				// Set adapters
				if (buttonFoods.isFocused()) {
					mAdapter = new EventsAdapter(getActivity(), foodList);

					if (foodList != null && foodList.size() < 1) {
						BaseHelper.showAlert(getActivity(), "Harry's",
								"No events to display");
					}
				} else if (buttonDrinks.isFocused()) {
					mAdapter = new EventsAdapter(getActivity(), drinkList);
					
					if (drinkList != null && drinkList.size() < 1) {
						BaseHelper.showAlert(getActivity(), "Harry's",
								"No events to display");
					}
				}

				lvStores.setAdapter(mAdapter);
				mAdapter.notifyDataSetChanged();

			} catch (Exception e) {
				e.printStackTrace();
			}
		} else {
			Log.e(TAG, "Error");
			try {
				LLMainContainer.setVisibility(View.INVISIBLE);

				BaseHelper
						.showAlert(
								getActivity(),
								"Harry's",
								(BaseHelper.isEmpty(details.getError_message())) ? "Unable to get load events"
										: details.getError_message());
			} catch (Exception e) {
				// TODO: handle exception
			}
		}
	}

	@Override
	public void onRefresh(SwipyRefreshLayoutDirection direction) {
		if (callAPIService()) {
			new Handler().postDelayed(new Runnable() {
				@Override
				public void run() {
					// Hide the refresh after n sec
					try {
						getActivity().runOnUiThread(new Runnable() {
							@Override
							public void run() {
								mSwipyRefreshLayout.setRefreshing(false);
							}
						});
					} catch (Exception e) {
					}
				}
			}, PULL_TO_REFRESH_TIMEOUT);
		} else {
			mSwipyRefreshLayout.setRefreshing(false);
		}
	}
}
