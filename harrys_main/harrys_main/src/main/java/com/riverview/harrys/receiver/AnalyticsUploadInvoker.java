package com.riverview.harrys.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.riverview.harrys.service.UploadAnaylaticsService;

/**
 * Created by wiraj on 4/29/15.
 */
public class AnalyticsUploadInvoker extends BroadcastReceiver {

	private static final String TAG = AnalyticsUploadInvoker.class
			.getSimpleName();

	@Override
	public void onReceive(Context context, Intent intent) {
		Log.i(TAG, "Alt receiver starting");

		Intent i = new Intent(context, UploadAnaylaticsService.class);
		context.startService(i);
	}
}
